# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1ItemDataCategoriesApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**itemdataCategoriesGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataCategoriesApi.md#itemdataCategoriesGet) | **GET** /itemdata/categories | unavailable
[**itemdataCategoriesIdDelete()**](SimplifySoftPecuniariusServerAPIv1ItemDataCategoriesApi.md#itemdataCategoriesIdDelete) | **DELETE** /itemdata/categories/{id} | unavailable
[**itemdataCategoriesIdGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataCategoriesApi.md#itemdataCategoriesIdGet) | **GET** /itemdata/categories/{id} | unavailable
[**itemdataCategoriesIdPut()**](SimplifySoftPecuniariusServerAPIv1ItemDataCategoriesApi.md#itemdataCategoriesIdPut) | **PUT** /itemdata/categories/{id} | unavailable
[**itemdataCategoriesPost()**](SimplifySoftPecuniariusServerAPIv1ItemDataCategoriesApi.md#itemdataCategoriesPost) | **POST** /itemdata/categories | unavailable


## `itemdataCategoriesGet()`

```php
itemdataCategoriesGet($limit, $index, $parent, $id, $id_not, $title_exact, $title_like, $title_starts_with, $item_frame, $item_root): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataCategory
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$parent = array(56); // int[] | The parental category IDs to receive the children of.
$id = array(56); // int[] | A range of category IDs to receive.
$id_not = array(56); // int[] | A range of category IDs NOT to receive.
$title_exact = array('title_exact_example'); // string[] | Compares the Category.Title using exact search.
$title_like = array('title_like_example'); // string[] | Compares the Category.Title using starts with search. Case Insensitive.
$title_starts_with = array('title_starts_with_example'); // string[] | Compares the Category.Title using DbFunction LIKE. Case Insensitive.
$item_frame = array(56); // int[] | A range of ItemFrame-Ids that need to be inside of the resulting Categories.
$item_root = array(56); // int[] | A range of ItemRoot-Ids that need to be inside of the resulting Categories.

try {
    $result = $apiInstance->itemdataCategoriesGet($limit, $index, $parent, $id, $id_not, $title_exact, $title_like, $title_starts_with, $item_frame, $item_root);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataCategoriesApi->itemdataCategoriesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **parent** | [**int[]**](../Model/int.md)| The parental category IDs to receive the children of. | [optional]
 **id** | [**int[]**](../Model/int.md)| A range of category IDs to receive. | [optional]
 **id_not** | [**int[]**](../Model/int.md)| A range of category IDs NOT to receive. | [optional]
 **title_exact** | [**string[]**](../Model/string.md)| Compares the Category.Title using exact search. | [optional]
 **title_like** | [**string[]**](../Model/string.md)| Compares the Category.Title using starts with search. Case Insensitive. | [optional]
 **title_starts_with** | [**string[]**](../Model/string.md)| Compares the Category.Title using DbFunction LIKE. Case Insensitive. | [optional]
 **item_frame** | [**int[]**](../Model/int.md)| A range of ItemFrame-Ids that need to be inside of the resulting Categories. | [optional]
 **item_root** | [**int[]**](../Model/int.md)| A range of ItemRoot-Ids that need to be inside of the resulting Categories. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataCategory**](../Model/SimplifySoftPecuniariusDataNetItemDataCategory.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataCategoriesIdDelete()`

```php
itemdataCategoriesIdDelete($id)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->itemdataCategoriesIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataCategoriesApi->itemdataCategoriesIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataCategoriesIdGet()`

```php
itemdataCategoriesIdGet($id)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->itemdataCategoriesIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataCategoriesApi->itemdataCategoriesIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataCategoriesIdPut()`

```php
itemdataCategoriesIdPut($id, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->itemdataCategoriesIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataCategoriesApi->itemdataCategoriesIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataCategoriesPost()`

```php
itemdataCategoriesPost($simplify_soft_pecuniarius_data_net_item_data_category): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataCategory
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataCategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_item_data_category = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataCategory(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataCategory

try {
    $result = $apiInstance->itemdataCategoriesPost($simplify_soft_pecuniarius_data_net_item_data_category);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataCategoriesApi->itemdataCategoriesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_item_data_category** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataCategory**](../Model/SimplifySoftPecuniariusDataNetItemDataCategory.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataCategory**](../Model/SimplifySoftPecuniariusDataNetItemDataCategory.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
