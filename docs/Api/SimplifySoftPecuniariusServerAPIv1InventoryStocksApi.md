# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1InventoryStocksApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**warehousedataStockCombinedItemidGet()**](SimplifySoftPecuniariusServerAPIv1InventoryStocksApi.md#warehousedataStockCombinedItemidGet) | **GET** /warehousedata/stock/combined/{itemid} | Allows to receive a single, combined stock.
[**warehousedataStockGet()**](SimplifySoftPecuniariusServerAPIv1InventoryStocksApi.md#warehousedataStockGet) | **GET** /warehousedata/stock | Allows to receive one or more stocks.
[**warehousedataStockIdItemidMinimumPost()**](SimplifySoftPecuniariusServerAPIv1InventoryStocksApi.md#warehousedataStockIdItemidMinimumPost) | **POST** /warehousedata/stock/{id}/{itemid}/minimum | Reserves the Stock for given item.
[**warehousedataStockIdItemidNotePost()**](SimplifySoftPecuniariusServerAPIv1InventoryStocksApi.md#warehousedataStockIdItemidNotePost) | **POST** /warehousedata/stock/{id}/{itemid}/note | Reserves the Stock for given item.
[**warehousedataStockIdMoveStorageLocationPost()**](SimplifySoftPecuniariusServerAPIv1InventoryStocksApi.md#warehousedataStockIdMoveStorageLocationPost) | **POST** /warehousedata/stock/{id}/move/{storageLocation} | Reserves the Stock for given item.
[**warehousedataStockLogGet()**](SimplifySoftPecuniariusServerAPIv1InventoryStocksApi.md#warehousedataStockLogGet) | **GET** /warehousedata/stock/log | Provides an interface to access the stock lock.
[**warehousedataStockLogItemidGet()**](SimplifySoftPecuniariusServerAPIv1InventoryStocksApi.md#warehousedataStockLogItemidGet) | **GET** /warehousedata/stock/log/{itemid} | Provides an interface to access the stock lock.
[**warehousedataStockReserveItemRootIdInStorageLocationIdPost()**](SimplifySoftPecuniariusServerAPIv1InventoryStocksApi.md#warehousedataStockReserveItemRootIdInStorageLocationIdPost) | **POST** /warehousedata/stock/reserve/{itemRootId}/in/{storageLocationId} | Reserves the Stock for given ItemRoot in the given StorageLocation.


## `warehousedataStockCombinedItemidGet()`

```php
warehousedataStockCombinedItemidGet($itemid)
```

Allows to receive a single, combined stock.

Allows to receive a single, combined stock. The Stock-Entries are stripped and put together in this.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$itemid = 56; // int | unavailable

try {
    $apiInstance->warehousedataStockCombinedItemidGet($itemid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStocksApi->warehousedataStockCombinedItemidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `warehousedataStockGet()`

```php
warehousedataStockGet($limit, $index, $itemroot, $itemframe, $warehouses, $storages, $locations, $includelocation, $includestorage, $includewarehouse): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStock
```

Allows to receive one or more stocks.

Allows to receive one or more stocks.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$itemroot = array(56); // int[] | Allows to set the related items by itemroot id. Will be joined together with itemframe[].
$itemframe = array(56); // int[] | Allows to set the related items by itemframe id. Will be joined together with itemroot[].
$warehouses = array(56); // int[] | Allows to set the related warehouses.
$storages = array(56); // int[] | Allows to set the related storages.
$locations = array(56); // int[] | Allows to set the related storage locations. Forces includelocation.
$includelocation = false; // bool | Allows to add the location.
$includestorage = false; // bool | Allows to add the storage. Forces includelocation.
$includewarehouse = false; // bool | Allows to add the warehouse of the storage. Forces includestorage.

try {
    $result = $apiInstance->warehousedataStockGet($limit, $index, $itemroot, $itemframe, $warehouses, $storages, $locations, $includelocation, $includestorage, $includewarehouse);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStocksApi->warehousedataStockGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **itemroot** | [**int[]**](../Model/int.md)| Allows to set the related items by itemroot id. Will be joined together with itemframe[]. | [optional]
 **itemframe** | [**int[]**](../Model/int.md)| Allows to set the related items by itemframe id. Will be joined together with itemroot[]. | [optional]
 **warehouses** | [**int[]**](../Model/int.md)| Allows to set the related warehouses. | [optional]
 **storages** | [**int[]**](../Model/int.md)| Allows to set the related storages. | [optional]
 **locations** | [**int[]**](../Model/int.md)| Allows to set the related storage locations. Forces includelocation. | [optional]
 **includelocation** | **bool**| Allows to add the location. | [optional] [default to false]
 **includestorage** | **bool**| Allows to add the storage. Forces includelocation. | [optional] [default to false]
 **includewarehouse** | **bool**| Allows to add the warehouse of the storage. Forces includestorage. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStock**](../Model/SimplifySoftPecuniariusDataNetInventoryStock.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `warehousedataStockIdItemidMinimumPost()`

```php
warehousedataStockIdItemidMinimumPost($id, $itemid, $unknown_base_type)
```

Reserves the Stock for given item.

Reserves the Stock for given item.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$itemid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->warehousedataStockIdItemidMinimumPost($id, $itemid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStocksApi->warehousedataStockIdItemidMinimumPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **itemid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `warehousedataStockIdItemidNotePost()`

```php
warehousedataStockIdItemidNotePost($id, $itemid, $unknown_base_type)
```

Reserves the Stock for given item.

Reserves the Stock for given item.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$itemid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->warehousedataStockIdItemidNotePost($id, $itemid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStocksApi->warehousedataStockIdItemidNotePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **itemid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `warehousedataStockIdMoveStorageLocationPost()`

```php
warehousedataStockIdMoveStorageLocationPost($id, $storage_location, $reason)
```

Reserves the Stock for given item.

Reserves the Stock for given item.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$storage_location = 56; // int | unavailable
$reason = 56; // int | A reason message to log. If left empty, will contain new StorageLocation title.

try {
    $apiInstance->warehousedataStockIdMoveStorageLocationPost($id, $storage_location, $reason);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStocksApi->warehousedataStockIdMoveStorageLocationPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **storage_location** | **int**| unavailable |
 **reason** | **int**| A reason message to log. If left empty, will contain new StorageLocation title. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `warehousedataStockLogGet()`

```php
warehousedataStockLogGet($limit, $index, $descending, $item, $warehouse, $storage, $location, $min_amount, $max_amount): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStockLog
```

Provides an interface to access the stock lock.

Provides an interface to access the stock lock.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$descending = false; // bool | Wether or not to use descending order.
$item = array(56); // int[] | The item IDs to return.
$warehouse = array(56); // int[] | The warehouse IDs to return.
$storage = array(56); // int[] | The storage IDs to return.
$location = array(56); // int[] | The storage-location IDs to return.
$min_amount = 56; // int | The minimum AmountAvailable to return.
$max_amount = 56; // int | The minimum AmountAvailable to return.

try {
    $result = $apiInstance->warehousedataStockLogGet($limit, $index, $descending, $item, $warehouse, $storage, $location, $min_amount, $max_amount);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStocksApi->warehousedataStockLogGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **descending** | **bool**| Wether or not to use descending order. | [optional] [default to false]
 **item** | [**int[]**](../Model/int.md)| The item IDs to return. | [optional]
 **warehouse** | [**int[]**](../Model/int.md)| The warehouse IDs to return. | [optional]
 **storage** | [**int[]**](../Model/int.md)| The storage IDs to return. | [optional]
 **location** | [**int[]**](../Model/int.md)| The storage-location IDs to return. | [optional]
 **min_amount** | **int**| The minimum AmountAvailable to return. | [optional]
 **max_amount** | **int**| The minimum AmountAvailable to return. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStockLog**](../Model/SimplifySoftPecuniariusDataNetInventoryStockLog.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `warehousedataStockLogItemidGet()`

```php
warehousedataStockLogItemidGet($itemid, $limit, $index, $descending): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStockLog
```

Provides an interface to access the stock lock.

Provides an interface to access the stock lock.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$itemid = 56; // int | unavailable
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$descending = false; // bool | Wether or not to use descending order.

try {
    $result = $apiInstance->warehousedataStockLogItemidGet($itemid, $limit, $index, $descending);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStocksApi->warehousedataStockLogItemidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemid** | **int**| unavailable |
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **descending** | **bool**| Wether or not to use descending order. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStockLog**](../Model/SimplifySoftPecuniariusDataNetInventoryStockLog.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `warehousedataStockReserveItemRootIdInStorageLocationIdPost()`

```php
warehousedataStockReserveItemRootIdInStorageLocationIdPost($item_root_id, $storage_location_id)
```

Reserves the Stock for given ItemRoot in the given StorageLocation.

Reserves the Stock for given ItemRoot in the given StorageLocation.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStocksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$item_root_id = 56; // int | unavailable
$storage_location_id = 56; // int | unavailable

try {
    $apiInstance->warehousedataStockReserveItemRootIdInStorageLocationIdPost($item_root_id, $storage_location_id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStocksApi->warehousedataStockReserveItemRootIdInStorageLocationIdPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **item_root_id** | **int**| unavailable |
 **storage_location_id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
