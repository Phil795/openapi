# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1ContactDataAddressKindsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**contactdataKindAddressGet()**](SimplifySoftPecuniariusServerAPIv1ContactDataAddressKindsApi.md#contactdataKindAddressGet) | **GET** /contactdata/kind/address | unavailable
[**contactdataKindAddressIdDelete()**](SimplifySoftPecuniariusServerAPIv1ContactDataAddressKindsApi.md#contactdataKindAddressIdDelete) | **DELETE** /contactdata/kind/address/{id} | unavailable
[**contactdataKindAddressIdPut()**](SimplifySoftPecuniariusServerAPIv1ContactDataAddressKindsApi.md#contactdataKindAddressIdPut) | **PUT** /contactdata/kind/address/{id} | unavailable
[**contactdataKindAddressPost()**](SimplifySoftPecuniariusServerAPIv1ContactDataAddressKindsApi.md#contactdataKindAddressPost) | **POST** /contactdata/kind/address | unavailable


## `contactdataKindAddressGet()`

```php
contactdataKindAddressGet(): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddressKind
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataAddressKindsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->contactdataKindAddressGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataAddressKindsApi->contactdataKindAddressGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddressKind**](../Model/SimplifySoftPecuniariusDataNetContactDataAddressKind.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataKindAddressIdDelete()`

```php
contactdataKindAddressIdDelete($id)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataAddressKindsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->contactdataKindAddressIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataAddressKindsApi->contactdataKindAddressIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataKindAddressIdPut()`

```php
contactdataKindAddressIdPut($id, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataAddressKindsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->contactdataKindAddressIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataAddressKindsApi->contactdataKindAddressIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataKindAddressPost()`

```php
contactdataKindAddressPost($simplify_soft_pecuniarius_data_net_contact_data_address_kind): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddressKind
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataAddressKindsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_contact_data_address_kind = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddressKind(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddressKind

try {
    $result = $apiInstance->contactdataKindAddressPost($simplify_soft_pecuniarius_data_net_contact_data_address_kind);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataAddressKindsApi->contactdataKindAddressPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_contact_data_address_kind** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddressKind**](../Model/SimplifySoftPecuniariusDataNetContactDataAddressKind.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddressKind**](../Model/SimplifySoftPecuniariusDataNetContactDataAddressKind.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
