# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountingTaxAllCountGet()**](SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi.md#accountingTaxAllCountGet) | **GET** /accounting/tax/all/count | Allows to query the count of all TaxDefinitions available.
[**accountingTaxAllGet()**](SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi.md#accountingTaxAllGet) | **GET** /accounting/tax/all/ | Allows to query all TaxDefinitions available.
[**accountingTaxAllRateCountGet()**](SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi.md#accountingTaxAllRateCountGet) | **GET** /accounting/tax/all/rate/count | Allows to query the count of all Tax available, including end-of-life.
[**accountingTaxAllRateGet()**](SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi.md#accountingTaxAllRateGet) | **GET** /accounting/tax/all/rate | Allows to query all Tax available, including end-of-life.
[**accountingTaxAllRateRateidGet()**](SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi.md#accountingTaxAllRateRateidGet) | **GET** /accounting/tax/all/rate/{rateid} | Allows to receive a single Tax rate with the provided rateid. If no Tax was found with the Id, NetException will be returned.
[**accountingTaxDefaultGet()**](SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi.md#accountingTaxDefaultGet) | **GET** /accounting/tax/default | Allows to receive the default TaxDefinitions if available. If no TaxDefinition has the IsDefault set, NetException will be retur
[**accountingTaxDefinitionidGet()**](SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi.md#accountingTaxDefinitionidGet) | **GET** /accounting/tax/{definitionid} | Allows to receive the TaxDefinitions with the provided definitionid. If no TaxDefinition was found with the Id, NetException wil
[**accountingTaxDefinitionidPut()**](SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi.md#accountingTaxDefinitionidPut) | **PUT** /accounting/tax/{definitionid} | Allows to update a TaxDefinitions with the provided definitionid. If no TaxDefinition was found with the Id, NetException will b
[**accountingTaxDefinitionidRatechangeRequestPost()**](SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi.md#accountingTaxDefinitionidRatechangeRequestPost) | **POST** /accounting/tax/{definitionid}/ratechange/request | Initialized a rate-change request. Will return a confirm token that can be used to confirm the ratechange request.
[**accountingTaxDefinitionidRatechangeTokenPost()**](SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi.md#accountingTaxDefinitionidRatechangeTokenPost) | **POST** /accounting/tax/{definitionid}/ratechange/{token} | Performs the rate-change. Will immediatly return 400 if token is valid and logout ALL users logged in.
[**accountingTaxPost()**](SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi.md#accountingTaxPost) | **POST** /accounting/tax | Allows to create new TaxDefinitions.


## `accountingTaxAllCountGet()`

```php
accountingTaxAllCountGet($index, $limit, $definition_id, $tax_id, $title_like, $chain_using_or)
```

Allows to query the count of all TaxDefinitions available.

Allows to query the count of all TaxDefinitions available.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.
$definition_id = array(56); // int[] | Limits the query to those TaxDefinition with the provided ids.
$tax_id = array(56); // int[] | Limits the query to those TaxDefinition which contain the provided tax in their history or are currently set to.
$title_like = 'title_like_example'; // string | Like check against TaxDefinition.Title
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $apiInstance->accountingTaxAllCountGet($index, $limit, $definition_id, $tax_id, $title_like, $chain_using_or);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi->accountingTaxAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **definition_id** | [**int[]**](../Model/int.md)| Limits the query to those TaxDefinition with the provided ids. | [optional]
 **tax_id** | [**int[]**](../Model/int.md)| Limits the query to those TaxDefinition which contain the provided tax in their history or are currently set to. | [optional]
 **title_like** | **string**| Like check against TaxDefinition.Title | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingTaxAllGet()`

```php
accountingTaxAllGet($index, $limit, $definition_id, $tax_id, $title_like, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTaxDefinition
```

Allows to query all TaxDefinitions available.

Allows to query all TaxDefinitions available. TaxDefinition will contain TaxDefinition.Tax automatically.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.
$definition_id = array(56); // int[] | Limits the query to those TaxDefinition with the provided ids.
$tax_id = array(56); // int[] | Limits the query to those TaxDefinition which contain the provided tax in their history or are currently set to.
$title_like = 'title_like_example'; // string | Like check against TaxDefinition.Title
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->accountingTaxAllGet($index, $limit, $definition_id, $tax_id, $title_like, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi->accountingTaxAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **definition_id** | [**int[]**](../Model/int.md)| Limits the query to those TaxDefinition with the provided ids. | [optional]
 **tax_id** | [**int[]**](../Model/int.md)| Limits the query to those TaxDefinition which contain the provided tax in their history or are currently set to. | [optional]
 **title_like** | **string**| Like check against TaxDefinition.Title | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTaxDefinition**](../Model/SimplifySoftPecuniariusDataNetAccountingTaxDefinition.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingTaxAllRateCountGet()`

```php
accountingTaxAllRateCountGet($index, $limit, $definition_id, $tax_id, $chain_using_or)
```

Allows to query the count of all Tax available, including end-of-life.

Allows to query the count of all Tax available, including end-of-life.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.
$definition_id = array(56); // int[] | Limits the query to those TaxDefinition with the provided ids.
$tax_id = array(56); // int[] | Limits the query to those TaxDefinition which contain the provided tax in their history or are currently set to.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $apiInstance->accountingTaxAllRateCountGet($index, $limit, $definition_id, $tax_id, $chain_using_or);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi->accountingTaxAllRateCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **definition_id** | [**int[]**](../Model/int.md)| Limits the query to those TaxDefinition with the provided ids. | [optional]
 **tax_id** | [**int[]**](../Model/int.md)| Limits the query to those TaxDefinition which contain the provided tax in their history or are currently set to. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingTaxAllRateGet()`

```php
accountingTaxAllRateGet($index, $limit, $definition_id, $tax_id, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTax
```

Allows to query all Tax available, including end-of-life.

Allows to query all Tax available, including end-of-life.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.
$definition_id = array(56); // int[] | Limits the query to those TaxDefinition with the provided ids.
$tax_id = array(56); // int[] | Limits the query to those TaxDefinition which contain the provided tax in their history or are currently set to.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->accountingTaxAllRateGet($index, $limit, $definition_id, $tax_id, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi->accountingTaxAllRateGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **definition_id** | [**int[]**](../Model/int.md)| Limits the query to those TaxDefinition with the provided ids. | [optional]
 **tax_id** | [**int[]**](../Model/int.md)| Limits the query to those TaxDefinition which contain the provided tax in their history or are currently set to. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTax**](../Model/SimplifySoftPecuniariusDataNetAccountingTax.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingTaxAllRateRateidGet()`

```php
accountingTaxAllRateRateidGet($rateid)
```

Allows to receive a single Tax rate with the provided rateid. If no Tax was found with the Id, NetException will be returned.

Allows to receive a single Tax rate with the provided rateid. If no Tax was found with the Id, NetException will be returned.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$rateid = 56; // int | unavailable

try {
    $apiInstance->accountingTaxAllRateRateidGet($rateid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi->accountingTaxAllRateRateidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rateid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingTaxDefaultGet()`

```php
accountingTaxDefaultGet()
```

Allows to receive the default TaxDefinitions if available. If no TaxDefinition has the IsDefault set, NetException will be retur

Allows to receive the default TaxDefinitions if available. If no TaxDefinition has the IsDefault set, NetException will be returned. TaxDefinition will contain TaxDefinition.Tax automatically.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $apiInstance->accountingTaxDefaultGet();
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi->accountingTaxDefaultGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingTaxDefinitionidGet()`

```php
accountingTaxDefinitionidGet($definitionid)
```

Allows to receive the TaxDefinitions with the provided definitionid. If no TaxDefinition was found with the Id, NetException wil

Allows to receive the TaxDefinitions with the provided definitionid. If no TaxDefinition was found with the Id, NetException will be returned. TaxDefinition will contain TaxDefinition.Tax automatically.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$definitionid = 56; // int | unavailable

try {
    $apiInstance->accountingTaxDefinitionidGet($definitionid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi->accountingTaxDefinitionidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **definitionid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingTaxDefinitionidPut()`

```php
accountingTaxDefinitionidPut($definitionid, $unknown_base_type)
```

Allows to update a TaxDefinitions with the provided definitionid. If no TaxDefinition was found with the Id, NetException will b

Allows to update a TaxDefinitions with the provided definitionid. If no TaxDefinition was found with the Id, NetException will be returned. Changing TaxDefinition.IsDefault will automatically set it to false on any other TaxDefinition.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$definitionid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->accountingTaxDefinitionidPut($definitionid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi->accountingTaxDefinitionidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **definitionid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingTaxDefinitionidRatechangeRequestPost()`

```php
accountingTaxDefinitionidRatechangeRequestPost($definitionid, $rate, $update_net, $update_gross)
```

Initialized a rate-change request. Will return a confirm token that can be used to confirm the ratechange request.

Initialized a rate-change request. Will return a confirm token that can be used to confirm the ratechange request. Only one token may be active at a given time. If two people attempt to change a tax, the older token will be overriden.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$definitionid = 56; // int | unavailable
$rate = 56; // int | Mandatory. The new Tax-Rate for the given TaxDefinition.
$update_net = True; // bool | Converts all prices to Net first and applies Tax change afterwards.
$update_gross = True; // bool | Converts all prices to Gross first and applies Tax change afterwards. Overrides updateNet.

try {
    $apiInstance->accountingTaxDefinitionidRatechangeRequestPost($definitionid, $rate, $update_net, $update_gross);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi->accountingTaxDefinitionidRatechangeRequestPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **definitionid** | **int**| unavailable |
 **rate** | **int**| Mandatory. The new Tax-Rate for the given TaxDefinition. | [optional]
 **update_net** | **bool**| Converts all prices to Net first and applies Tax change afterwards. | [optional]
 **update_gross** | **bool**| Converts all prices to Gross first and applies Tax change afterwards. Overrides updateNet. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingTaxDefinitionidRatechangeTokenPost()`

```php
accountingTaxDefinitionidRatechangeTokenPost($definitionid, $token)
```

Performs the rate-change. Will immediatly return 400 if token is valid and logout ALL users logged in.

Performs the rate-change. Will immediatly return 400 if token is valid and logout ALL users logged in.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$definitionid = 56; // int | unavailable
$token = 'token_example'; // string | unavailable

try {
    $apiInstance->accountingTaxDefinitionidRatechangeTokenPost($definitionid, $token);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi->accountingTaxDefinitionidRatechangeTokenPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **definitionid** | **int**| unavailable |
 **token** | **string**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingTaxPost()`

```php
accountingTaxPost($simplify_soft_pecuniarius_data_net_accounting_tax_definition): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTaxDefinition
```

Allows to create new TaxDefinitions.

Allows to create new TaxDefinitions. TaxDefinition may contain TaxDefinition.Tax already. If no Tax is present, a new one will be created with zero tax rate.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_accounting_tax_definition = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTaxDefinition(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTaxDefinition

try {
    $result = $apiInstance->accountingTaxPost($simplify_soft_pecuniarius_data_net_accounting_tax_definition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingTaxDefinitionsApi->accountingTaxPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_accounting_tax_definition** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTaxDefinition**](../Model/SimplifySoftPecuniariusDataNetAccountingTaxDefinition.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTaxDefinition**](../Model/SimplifySoftPecuniariusDataNetAccountingTaxDefinition.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
