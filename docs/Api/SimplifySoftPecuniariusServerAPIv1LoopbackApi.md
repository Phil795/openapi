# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1LoopbackApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**createadminuserGet()**](SimplifySoftPecuniariusServerAPIv1LoopbackApi.md#createadminuserGet) | **GET** /createadminuser | unavailable
[**documentationGet()**](SimplifySoftPecuniariusServerAPIv1LoopbackApi.md#documentationGet) | **GET** /documentation | unavailable
[**exportGet()**](SimplifySoftPecuniariusServerAPIv1LoopbackApi.md#exportGet) | **GET** /export | unavailable
[**localsettingsGet()**](SimplifySoftPecuniariusServerAPIv1LoopbackApi.md#localsettingsGet) | **GET** /localsettings | unavailable


## `createadminuserGet()`

```php
createadminuserGet($username)
```

unavailable

Creates a new, local user. If a user with provided name already exists, it will fail.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1LoopbackApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$username = 'username_example'; // string | unavailable

try {
    $apiInstance->createadminuserGet($username);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1LoopbackApi->createadminuserGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| unavailable | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `documentationGet()`

```php
documentationGet()
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1LoopbackApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $apiInstance->documentationGet();
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1LoopbackApi->documentationGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `exportGet()`

```php
exportGet($type, $namespace)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1LoopbackApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$type = 'type_example'; // string | unavailable
$namespace = 'namespace_example'; // string | Allows to alter the output-namespace for some generators.

try {
    $apiInstance->exportGet($type, $namespace);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1LoopbackApi->exportGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **string**| unavailable | [optional]
 **namespace** | **string**| Allows to alter the output-namespace for some generators. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `localsettingsGet()`

```php
localsettingsGet($host, $username, $password, $database, $port, $readonly_username, $readonly_password)
```

unavailable

Allows to configure the database settings of the server.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1LoopbackApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$host = 'host_example'; // string | unavailable
$username = 'username_example'; // string | unavailable
$password = 'password_example'; // string | unavailable
$database = 'database_example'; // string | unavailable
$port = 56; // int | unavailable
$readonly_username = 'readonly_username_example'; // string | unavailable
$readonly_password = 'readonly_password_example'; // string | unavailable

try {
    $apiInstance->localsettingsGet($host, $username, $password, $database, $port, $readonly_username, $readonly_password);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1LoopbackApi->localsettingsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host** | **string**| unavailable | [optional]
 **username** | **string**| unavailable | [optional]
 **password** | **string**| unavailable | [optional]
 **database** | **string**| unavailable | [optional]
 **port** | **int**| unavailable | [optional]
 **readonly_username** | **string**| unavailable | [optional]
 **readonly_password** | **string**| unavailable | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
