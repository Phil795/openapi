# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1InventoryStorageLocationsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**locationLocationsGet()**](SimplifySoftPecuniariusServerAPIv1InventoryStorageLocationsApi.md#locationLocationsGet) | **GET** /location/locations | unavailable
[**locationLocationsIdDelete()**](SimplifySoftPecuniariusServerAPIv1InventoryStorageLocationsApi.md#locationLocationsIdDelete) | **DELETE** /location/locations/{id} | unavailable
[**locationLocationsIdGet()**](SimplifySoftPecuniariusServerAPIv1InventoryStorageLocationsApi.md#locationLocationsIdGet) | **GET** /location/locations/{id} | unavailable
[**locationLocationsIdPut()**](SimplifySoftPecuniariusServerAPIv1InventoryStorageLocationsApi.md#locationLocationsIdPut) | **PUT** /location/locations/{id} | unavailable
[**locationLocationsPost()**](SimplifySoftPecuniariusServerAPIv1InventoryStorageLocationsApi.md#locationLocationsPost) | **POST** /location/locations | unavailable


## `locationLocationsGet()`

```php
locationLocationsGet($index, $limit, $title, $title_starts_with, $title_like, $dim_width, $dim_height, $dim_dephts, $dim_mul, $itemroot, $warehouse, $storage, $location, $location_parent, $has_children, $has_parent, $is_empty): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorageLocation
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStorageLocationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 1000; // int | The amount of records to take.
$title = array('title_example'); // string[] | Looks up the StorageLocation.Title property, equality check.
$title_starts_with = array('title_starts_with_example'); // string[] | Looks up the StorageLocation.Title property, using starts-with check. Case Invariant.
$title_like = 'title_like_example'; // string | Looks up the StorageLocation.Title property, using DbFunction.Like. Case Invariant.
$dim_width = 3.4; // float | The Width the storage location must support. Requires dim-height & dim-dephts.
$dim_height = 3.4; // float | The Height the storage location must support. Requires dim-width & dim-dephts.
$dim_dephts = 3.4; // float | The Depth the storage location must support. Requires dim-width & dim-height.
$dim_mul = 1; // float | Multiplicator for ItemFrame dimensions. Used in conjunction with other dim- parameters.
$itemroot = array(56); // int[] | Filters for StorageLocations with the provided item in stock.
$warehouse = array(56); // int[] | Allows to only receive the children of provided parent storage location.
$storage = array(56); // int[] | Allows to only receive the children of provided parent storages.
$location = array(56); // int[] | List of StorageLocation.Id's to receive.
$location_parent = array(56); // int[] | List of StorageLocation.Id's which are parent to the ones to receive.
$has_children = true; // bool | Applies a filter, checking wether a given StorageLocation has (true) or has not (false) children.
$has_parent = true; // bool | Applies a filter, checking wether a given StorageLocations parent is existing (true) or not (false).
$is_empty = True; // bool | Returns either only empty (true) storage locations (empty also includes locations with Stock.AmountAvailable == 0) or full ones (excluding Stock.AmountAvailable == 0)

try {
    $result = $apiInstance->locationLocationsGet($index, $limit, $title, $title_starts_with, $title_like, $dim_width, $dim_height, $dim_dephts, $dim_mul, $itemroot, $warehouse, $storage, $location, $location_parent, $has_children, $has_parent, $is_empty);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStorageLocationsApi->locationLocationsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 1000]
 **title** | [**string[]**](../Model/string.md)| Looks up the StorageLocation.Title property, equality check. | [optional]
 **title_starts_with** | [**string[]**](../Model/string.md)| Looks up the StorageLocation.Title property, using starts-with check. Case Invariant. | [optional]
 **title_like** | **string**| Looks up the StorageLocation.Title property, using DbFunction.Like. Case Invariant. | [optional]
 **dim_width** | **float**| The Width the storage location must support. Requires dim-height &amp; dim-dephts. | [optional]
 **dim_height** | **float**| The Height the storage location must support. Requires dim-width &amp; dim-dephts. | [optional]
 **dim_dephts** | **float**| The Depth the storage location must support. Requires dim-width &amp; dim-height. | [optional]
 **dim_mul** | **float**| Multiplicator for ItemFrame dimensions. Used in conjunction with other dim- parameters. | [optional] [default to 1]
 **itemroot** | [**int[]**](../Model/int.md)| Filters for StorageLocations with the provided item in stock. | [optional]
 **warehouse** | [**int[]**](../Model/int.md)| Allows to only receive the children of provided parent storage location. | [optional]
 **storage** | [**int[]**](../Model/int.md)| Allows to only receive the children of provided parent storages. | [optional]
 **location** | [**int[]**](../Model/int.md)| List of StorageLocation.Id&#39;s to receive. | [optional]
 **location_parent** | [**int[]**](../Model/int.md)| List of StorageLocation.Id&#39;s which are parent to the ones to receive. | [optional]
 **has_children** | **bool**| Applies a filter, checking wether a given StorageLocation has (true) or has not (false) children. | [optional] [default to true]
 **has_parent** | **bool**| Applies a filter, checking wether a given StorageLocations parent is existing (true) or not (false). | [optional] [default to true]
 **is_empty** | **bool**| Returns either only empty (true) storage locations (empty also includes locations with Stock.AmountAvailable &#x3D;&#x3D; 0) or full ones (excluding Stock.AmountAvailable &#x3D;&#x3D; 0) | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorageLocation**](../Model/SimplifySoftPecuniariusDataNetInventoryStorageLocation.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `locationLocationsIdDelete()`

```php
locationLocationsIdDelete($id)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStorageLocationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->locationLocationsIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStorageLocationsApi->locationLocationsIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `locationLocationsIdGet()`

```php
locationLocationsIdGet($id)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStorageLocationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->locationLocationsIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStorageLocationsApi->locationLocationsIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `locationLocationsIdPut()`

```php
locationLocationsIdPut($id, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStorageLocationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->locationLocationsIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStorageLocationsApi->locationLocationsIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `locationLocationsPost()`

```php
locationLocationsPost($simplify_soft_pecuniarius_data_net_inventory_storage_location): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorageLocation
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStorageLocationsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_inventory_storage_location = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorageLocation(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorageLocation

try {
    $result = $apiInstance->locationLocationsPost($simplify_soft_pecuniarius_data_net_inventory_storage_location);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStorageLocationsApi->locationLocationsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_inventory_storage_location** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorageLocation**](../Model/SimplifySoftPecuniariusDataNetInventoryStorageLocation.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorageLocation**](../Model/SimplifySoftPecuniariusDataNetInventoryStorageLocation.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
