# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1GenericApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**genericQuicknavSearchAllGet()**](SimplifySoftPecuniariusServerAPIv1GenericApi.md#genericQuicknavSearchAllGet) | **GET** /generic/quicknav/search-all | Allows to search the whole database using DbLike, looking up certain properties in all objects searched and returning the type a


## `genericQuicknavSearchAllGet()`

```php
genericQuicknavSearchAllGet($index, $limit, $like): object
```

Allows to search the whole database using DbLike, looking up certain properties in all objects searched and returning the type a

Allows to search the whole database using DbLike, looking up certain properties in all objects searched and returning the type and key of matches

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1GenericApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.
$like = 'like_example'; // string | The like expression

try {
    $result = $apiInstance->genericQuicknavSearchAllGet($index, $limit, $like);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1GenericApi->genericQuicknavSearchAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **like** | **string**| The like expression | [optional]

### Return type

**object**

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
