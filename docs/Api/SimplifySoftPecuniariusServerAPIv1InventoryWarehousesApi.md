# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**locationWarehousesGet()**](SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi.md#locationWarehousesGet) | **GET** /location/warehouses | Receives a list of warehouses according to provided filtering provided via GET parameters.
[**locationWarehousesIdDelete()**](SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi.md#locationWarehousesIdDelete) | **DELETE** /location/warehouses/{id} | Endpoint to remove a warehouse. Note that this will throw errors unless ensured that the warehouse is empty and not refered to f
[**locationWarehousesIdGet()**](SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi.md#locationWarehousesIdGet) | **GET** /location/warehouses/{id} | Endpoint allowing to receive a single warehouses according to the Id provided.
[**locationWarehousesIdOwnerGet()**](SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi.md#locationWarehousesIdOwnerGet) | **GET** /location/warehouses/{id}/owner | Returns the owner of a warehouse designated by its Id.
[**locationWarehousesIdPut()**](SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi.md#locationWarehousesIdPut) | **PUT** /location/warehouses/{id} | Allows to update a given warehouse via its Id.
[**locationWarehousesIdStoragesGet()**](SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi.md#locationWarehousesIdStoragesGet) | **GET** /location/warehouses/{id}/storages | Receives a list of storages owned by the warehouse designated by its Id.
[**locationWarehousesPost()**](SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi.md#locationWarehousesPost) | **POST** /location/warehouses | Provides the ability to create a new warehouse.


## `locationWarehousesGet()`

```php
locationWarehousesGet($limit, $index, $names): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryWarehouse
```

Receives a list of warehouses according to provided filtering provided via GET parameters.

Receives a list of warehouses according to provided filtering provided via GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 1000; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$names = array('names_example'); // string[] | unavailable

try {
    $result = $apiInstance->locationWarehousesGet($limit, $index, $names);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi->locationWarehousesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 1000]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **names** | [**string[]**](../Model/string.md)| unavailable | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryWarehouse**](../Model/SimplifySoftPecuniariusDataNetInventoryWarehouse.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `locationWarehousesIdDelete()`

```php
locationWarehousesIdDelete($id)
```

Endpoint to remove a warehouse. Note that this will throw errors unless ensured that the warehouse is empty and not refered to f

Endpoint to remove a warehouse. Note that this will throw errors unless ensured that the warehouse is empty and not refered to from anywhere.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->locationWarehousesIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi->locationWarehousesIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `locationWarehousesIdGet()`

```php
locationWarehousesIdGet($id)
```

Endpoint allowing to receive a single warehouses according to the Id provided.

Endpoint allowing to receive a single warehouses according to the Id provided.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->locationWarehousesIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi->locationWarehousesIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `locationWarehousesIdOwnerGet()`

```php
locationWarehousesIdOwnerGet($id)
```

Returns the owner of a warehouse designated by its Id.

Returns the owner of a warehouse designated by its Id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->locationWarehousesIdOwnerGet($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi->locationWarehousesIdOwnerGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `locationWarehousesIdPut()`

```php
locationWarehousesIdPut($id, $unknown_base_type)
```

Allows to update a given warehouse via its Id.

Allows to update a given warehouse via its Id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->locationWarehousesIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi->locationWarehousesIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `locationWarehousesIdStoragesGet()`

```php
locationWarehousesIdStoragesGet($id, $limit, $index): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorage
```

Receives a list of storages owned by the warehouse designated by its Id.

Receives a list of storages owned by the warehouse designated by its Id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$limit = 1000; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.

try {
    $result = $apiInstance->locationWarehousesIdStoragesGet($id, $limit, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi->locationWarehousesIdStoragesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **limit** | **int**| The amount of records to take. | [optional] [default to 1000]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorage**](../Model/SimplifySoftPecuniariusDataNetInventoryStorage.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `locationWarehousesPost()`

```php
locationWarehousesPost($simplify_soft_pecuniarius_data_net_inventory_warehouse): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryWarehouse
```

Provides the ability to create a new warehouse.

Provides the ability to create a new warehouse.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_inventory_warehouse = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryWarehouse(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryWarehouse

try {
    $result = $apiInstance->locationWarehousesPost($simplify_soft_pecuniarius_data_net_inventory_warehouse);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryWarehousesApi->locationWarehousesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_inventory_warehouse** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryWarehouse**](../Model/SimplifySoftPecuniariusDataNetInventoryWarehouse.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryWarehouse**](../Model/SimplifySoftPecuniariusDataNetInventoryWarehouse.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
