# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1AccountingAccountingItemsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountingItemsCountGet()**](SimplifySoftPecuniariusServerAPIv1AccountingAccountingItemsApi.md#accountingItemsCountGet) | **GET** /accounting/items/count | unavailable
[**accountingItemsGet()**](SimplifySoftPecuniariusServerAPIv1AccountingAccountingItemsApi.md#accountingItemsGet) | **GET** /accounting/items | unavailable
[**accountingItemsIdEventsGet()**](SimplifySoftPecuniariusServerAPIv1AccountingAccountingItemsApi.md#accountingItemsIdEventsGet) | **GET** /accounting/items/{id}/events | unavailable
[**accountingItemsIdGet()**](SimplifySoftPecuniariusServerAPIv1AccountingAccountingItemsApi.md#accountingItemsIdGet) | **GET** /accounting/items/{id} | unavailable


## `accountingItemsCountGet()`

```php
accountingItemsCountGet($limit, $index, $receipt, $id, $include_events)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingAccountingItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$receipt = array(56); // int[] | A collection of receipt ids to filter the accounting items for.
$id = array(56); // int[] | A collection of accounting item ids to receive.
$include_events = false; // bool | Wether to include the events, listed in a single accounting item.

try {
    $apiInstance->accountingItemsCountGet($limit, $index, $receipt, $id, $include_events);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingAccountingItemsApi->accountingItemsCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **receipt** | [**int[]**](../Model/int.md)| A collection of receipt ids to filter the accounting items for. | [optional]
 **id** | [**int[]**](../Model/int.md)| A collection of accounting item ids to receive. | [optional]
 **include_events** | **bool**| Wether to include the events, listed in a single accounting item. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingItemsGet()`

```php
accountingItemsGet($limit, $index, $receipt, $id, $include_events): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingAccountingItem
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingAccountingItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$receipt = array(56); // int[] | A collection of receipt ids to filter the accounting items for.
$id = array(56); // int[] | A collection of accounting item ids to receive.
$include_events = false; // bool | Wether to include the events, listed in a single accounting item.

try {
    $result = $apiInstance->accountingItemsGet($limit, $index, $receipt, $id, $include_events);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingAccountingItemsApi->accountingItemsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **receipt** | [**int[]**](../Model/int.md)| A collection of receipt ids to filter the accounting items for. | [optional]
 **id** | [**int[]**](../Model/int.md)| A collection of accounting item ids to receive. | [optional]
 **include_events** | **bool**| Wether to include the events, listed in a single accounting item. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingAccountingItem**](../Model/SimplifySoftPecuniariusDataNetAccountingAccountingItem.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingItemsIdEventsGet()`

```php
accountingItemsIdEventsGet($id, $limit, $index): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTransactionEvent
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingAccountingItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.

try {
    $result = $apiInstance->accountingItemsIdEventsGet($id, $limit, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingAccountingItemsApi->accountingItemsIdEventsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTransactionEvent**](../Model/SimplifySoftPecuniariusDataNetAccountingTransactionEvent.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingItemsIdGet()`

```php
accountingItemsIdGet($id)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingAccountingItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->accountingItemsIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingAccountingItemsApi->accountingItemsIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
