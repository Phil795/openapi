# SimplifySoft\Pecuniarius\Api\ExampleRestServiceMyRestPathApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**pluginExampleRestServiceArg1Get()**](ExampleRestServiceMyRestPathApi.md#pluginExampleRestServiceArg1Get) | **GET** /plugin/example/rest-service/{arg1} | unavailable


## `pluginExampleRestServiceArg1Get()`

```php
pluginExampleRestServiceArg1Get($arg1, $test)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\ExampleRestServiceMyRestPathApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$arg1 = 56; // int | unavailable
$test = 'something'; // string | Just some describing GET parameter.

try {
    $apiInstance->pluginExampleRestServiceArg1Get($arg1, $test);
} catch (Exception $e) {
    echo 'Exception when calling ExampleRestServiceMyRestPathApi->pluginExampleRestServiceArg1Get: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **arg1** | **int**| unavailable |
 **test** | **string**| Just some describing GET parameter. | [optional] [default to &#39;something&#39;]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
