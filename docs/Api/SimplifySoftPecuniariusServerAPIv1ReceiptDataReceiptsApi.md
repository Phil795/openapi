# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**receiptDataCaseAllCountGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseAllCountGet) | **GET** /receipt-data/case/all/count | unavailable
[**receiptDataCaseAllGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseAllGet) | **GET** /receipt-data/case/all | unavailable
[**receiptDataCaseAllOptionAllCountGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseAllOptionAllCountGet) | **GET** /receipt-data/case/all/option/all/count | unavailable
[**receiptDataCaseAllOptionAllGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseAllOptionAllGet) | **GET** /receipt-data/case/all/option/all | unavailable
[**receiptDataCaseAllReceiptAllCountGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseAllReceiptAllCountGet) | **GET** /receipt-data/case/all/receipt/all/count | unavailable
[**receiptDataCaseAllReceiptAllDeliveryGoalAllCountGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseAllReceiptAllDeliveryGoalAllCountGet) | **GET** /receipt-data/case/all/receipt/all/delivery/goal/all/count | unavailable
[**receiptDataCaseAllReceiptAllDeliveryGoalAllGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseAllReceiptAllDeliveryGoalAllGet) | **GET** /receipt-data/case/all/receipt/all/delivery/goal/all | unavailable
[**receiptDataCaseAllReceiptAllGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseAllReceiptAllGet) | **GET** /receipt-data/case/all/receipt/all | unavailable
[**receiptDataCaseAllReceiptAllOptionAllCountGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseAllReceiptAllOptionAllCountGet) | **GET** /receipt-data/case/all/receipt/all/option/all/count | unavailable
[**receiptDataCaseAllReceiptAllOptionAllGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseAllReceiptAllOptionAllGet) | **GET** /receipt-data/case/all/receipt/all/option/all | unavailable
[**receiptDataCaseAllReceiptAllSearchGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseAllReceiptAllSearchGet) | **GET** /receipt-data/case/all/receipt/all/search | unavailable
[**receiptDataCaseCaseidDelete()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidDelete) | **DELETE** /receipt-data/case/{caseid} | unavailable
[**receiptDataCaseCaseidGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidGet) | **GET** /receipt-data/case/{caseid} | unavailable
[**receiptDataCaseCaseidOptionAllGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidOptionAllGet) | **GET** /receipt-data/case/{caseid}/option/all | unavailable
[**receiptDataCaseCaseidOptionOptidDelete()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidOptionOptidDelete) | **DELETE** /receipt-data/case/{caseid}/option/{optid} | unavailable
[**receiptDataCaseCaseidOptionOptidGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidOptionOptidGet) | **GET** /receipt-data/case/{caseid}/option/{optid} | unavailable
[**receiptDataCaseCaseidOptionOptidPut()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidOptionOptidPut) | **PUT** /receipt-data/case/{caseid}/option/{optid} | unavailable
[**receiptDataCaseCaseidOptionPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidOptionPost) | **POST** /receipt-data/case/{caseid}/option | unavailable
[**receiptDataCaseCaseidPut()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidPut) | **PUT** /receipt-data/case/{caseid} | unavailable
[**receiptDataCaseCaseidReceiptAllGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptAllGet) | **GET** /receipt-data/case/{caseid}/receipt/all | unavailable
[**receiptDataCaseCaseidReceiptPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptPost) | **POST** /receipt-data/case/{caseid}/receipt | unavailable
[**receiptDataCaseCaseidReceiptReceiptidDelete()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidDelete) | **DELETE** /receipt-data/case/{caseid}/receipt/{receiptid} | unavailable
[**receiptDataCaseCaseidReceiptReceiptidDeliveryGoalAllGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidDeliveryGoalAllGet) | **GET** /receipt-data/case/{caseid}/receipt/{receiptid}/delivery/goal/all | unavailable
[**receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidDelete()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidDelete) | **DELETE** /receipt-data/case/{caseid}/receipt/{receiptid}/delivery/goal/{goalid} | unavailable
[**receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidGet) | **GET** /receipt-data/case/{caseid}/receipt/{receiptid}/delivery/goal/{goalid} | unavailable
[**receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionIdsGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionIdsGet) | **GET** /receipt-data/case/{caseid}/receipt/{receiptid}/delivery/goal/{goalid}/position/ids | unavailable
[**receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionPosidLinkPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionPosidLinkPost) | **POST** /receipt-data/case/{caseid}/receipt/{receiptid}/delivery/goal/{goalid}/position/{posid}/link | unavailable
[**receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionPosidUnlinkPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionPosidUnlinkPost) | **POST** /receipt-data/case/{caseid}/receipt/{receiptid}/delivery/goal/{goalid}/position/{posid}/unlink | unavailable
[**receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPut()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPut) | **PUT** /receipt-data/case/{caseid}/receipt/{receiptid}/delivery/goal/{goalid} | unavailable
[**receiptDataCaseCaseidReceiptReceiptidDeliveryGoalPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidDeliveryGoalPost) | **POST** /receipt-data/case/{caseid}/receipt/{receiptid}/delivery/goal | unavailable
[**receiptDataCaseCaseidReceiptReceiptidFileFileidLinkPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidFileFileidLinkPost) | **POST** /receipt-data/case/{caseid}/receipt/{receiptid}/file/{fileid}/link | unavailable
[**receiptDataCaseCaseidReceiptReceiptidFileFileidUnlinkPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidFileFileidUnlinkPost) | **POST** /receipt-data/case/{caseid}/receipt/{receiptid}/file/{fileid}/unlink | unavailable
[**receiptDataCaseCaseidReceiptReceiptidGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidGet) | **GET** /receipt-data/case/{caseid}/receipt/{receiptid} | unavailable
[**receiptDataCaseCaseidReceiptReceiptidOptionAllGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidOptionAllGet) | **GET** /receipt-data/case/{caseid}/receipt/{receiptid}/option/all | unavailable
[**receiptDataCaseCaseidReceiptReceiptidOptionOptidDelete()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidOptionOptidDelete) | **DELETE** /receipt-data/case/{caseid}/receipt/{receiptid}/option/{optid} | unavailable
[**receiptDataCaseCaseidReceiptReceiptidOptionOptidGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidOptionOptidGet) | **GET** /receipt-data/case/{caseid}/receipt/{receiptid}/option/{optid} | unavailable
[**receiptDataCaseCaseidReceiptReceiptidOptionOptidPut()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidOptionOptidPut) | **PUT** /receipt-data/case/{caseid}/receipt/{receiptid}/option/{optid} | unavailable
[**receiptDataCaseCaseidReceiptReceiptidOptionPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidOptionPost) | **POST** /receipt-data/case/{caseid}/receipt/{receiptid}/option | unavailable
[**receiptDataCaseCaseidReceiptReceiptidPathsGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidPathsGet) | **GET** /receipt-data/case/{caseid}/receipt/{receiptid}/paths | unavailable
[**receiptDataCaseCaseidReceiptReceiptidPathsPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidPathsPost) | **POST** /receipt-data/case/{caseid}/receipt/{receiptid}/paths | unavailable
[**receiptDataCaseCaseidReceiptReceiptidPositionAllGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidPositionAllGet) | **GET** /receipt-data/case/{caseid}/receipt/{receiptid}/position/all | unavailable
[**receiptDataCaseCaseidReceiptReceiptidPositionIdsGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidPositionIdsGet) | **GET** /receipt-data/case/{caseid}/receipt/{receiptid}/position/ids | unavailable
[**receiptDataCaseCaseidReceiptReceiptidPut()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidPut) | **PUT** /receipt-data/case/{caseid}/receipt/{receiptid} | unavailable
[**receiptDataCaseCaseidReceiptReceiptidShipmentsGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidReceiptReceiptidShipmentsGet) | **GET** /receipt-data/case/{caseid}/receipt/{receiptid}/shipments | unavailable
[**receiptDataCaseCaseidShipmentsGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCaseCaseidShipmentsGet) | **GET** /receipt-data/case/{caseid}/shipments | unavailable
[**receiptDataCasePost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi.md#receiptDataCasePost) | **POST** /receipt-data/case | unavailable


## `receiptDataCaseAllCountGet()`

```php
receiptDataCaseAllCountGet($index, $limit, $case_id, $chain_using_or)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.
$case_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $apiInstance->receiptDataCaseAllCountGet($index, $limit, $case_id, $chain_using_or);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **case_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseAllGet()`

```php
receiptDataCaseAllGet($index, $limit, $case_id, $chain_using_or): object
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.
$case_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->receiptDataCaseAllGet($index, $limit, $case_id, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **case_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

**object**

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseAllOptionAllCountGet()`

```php
receiptDataCaseAllOptionAllCountGet($index, $limit, $case_id, $case_option_id, $chain_using_or)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.
$case_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$case_option_id = array(56); // int[] | The case options to receive. If empty, all cases will be received.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $apiInstance->receiptDataCaseAllOptionAllCountGet($index, $limit, $case_id, $case_option_id, $chain_using_or);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseAllOptionAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **case_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **case_option_id** | [**int[]**](../Model/int.md)| The case options to receive. If empty, all cases will be received. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseAllOptionAllGet()`

```php
receiptDataCaseAllOptionAllGet($index, $limit, $case_id, $case_option_id, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataCaseOption
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.
$case_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$case_option_id = array(56); // int[] | The case options to receive. If empty, all cases will be received.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->receiptDataCaseAllOptionAllGet($index, $limit, $case_id, $case_option_id, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseAllOptionAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **case_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **case_option_id** | [**int[]**](../Model/int.md)| The case options to receive. If empty, all cases will be received. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataCaseOption**](../Model/SimplifySoftPecuniariusDataNetReceiptDataCaseOption.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseAllReceiptAllCountGet()`

```php
receiptDataCaseAllReceiptAllCountGet($case_id, $continues_id, $receipt_id, $position_id, $self_id, $self_street_exact, $self_street_starts_with, $self_street_like, $self_town_exact, $self_town_starts_with, $self_town_like, $self_country_exact, $self_country_starts_with, $self_country_like, $self_namea_exact, $self_namea_starts_with, $self_namea_like, $self_nameb_exact, $self_nameb_starts_with, $self_nameb_like, $self_namec_exact, $self_namec_starts_with, $self_namec_like, $self_street_number_exact, $self_street_number_starts_with, $self_street_number_like, $self_zip_exact, $self_zip_starts_with, $self_zip_like, $other_id, $other_street_exact, $other_street_starts_with, $other_street_like, $other_town_exact, $other_town_starts_with, $other_town_like, $other_country_exact, $other_country_starts_with, $other_country_like, $other_namea_exact, $other_namea_starts_with, $other_namea_like, $other_nameb_exact, $other_nameb_starts_with, $other_nameb_like, $other_namec_exact, $other_namec_starts_with, $other_namec_like, $other_street_number_exact, $other_street_number_starts_with, $other_street_number_like, $other_zip_exact, $other_zip_starts_with, $other_zip_like, $accounting_item_id, $picklist_id, $state_group_kind, $state_id, $item_root_id, $item_frame_id, $not_item_root_id, $not_item_frame_id, $direction, $state_group_exact, $state_group_starts_with, $state_group_like, $state_title_exact, $state_title_starts_with, $state_title_like, $index, $limit, $identifier_exact, $identifier_starts_with, $identifier_like, $extref_exact, $extref_starts_with, $extref_like, $note_exact, $note_starts_with, $note_like, $has_accounting_item, $chain_using_or, $force_id, $include_entries, $include_billing, $include_contact, $in_ext_auth, $not_in_ext_auth, $has_ext_auth)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$case_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$continues_id = array(56); // int[] | The Receipt IDs that preceeded the searched receipt, limiting the search to. If empty, no filter for Receipt will be set.
$receipt_id = array(56); // int[] | The ReceiptEntry IDs to limit the search to. If empty, no filter for ReceiptEntries will be set.
$position_id = array(56); // int[] | The ReceiptEntry IDs to limit the search to. If empty, no filter for ReceiptEntries will be set.
$self_id = array(56); // int[] | Compares the provided range of Ids against Receipt.Self.Id and returns only those matching.
$self_street_exact = array('self_street_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].Street against the provided strings using exact match.
$self_street_starts_with = array('self_street_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].Street against the provided strings using starts with. Case Invariant.
$self_street_like = 'self_street_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].Street for provided content. Case Invariant.
$self_town_exact = array('self_town_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].Town against the provided strings using exact match.
$self_town_starts_with = array('self_town_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].Town against the provided strings using starts with. Case Invariant.
$self_town_like = 'self_town_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].Town for provided content. Case Invariant.
$self_country_exact = array('self_country_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].Country against the provided strings using exact match.
$self_country_starts_with = array('self_country_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].Country against the provided strings using starts with. Case Invariant.
$self_country_like = 'self_country_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].Country for provided content. Case Invariant.
$self_namea_exact = array('self_namea_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].NameA against the provided strings using exact match.
$self_namea_starts_with = array('self_namea_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].NameA against the provided strings using starts with. Case Invariant.
$self_namea_like = 'self_namea_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].NameA for provided content. Case Invariant.
$self_nameb_exact = array('self_nameb_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].NameB against the provided strings using exact match.
$self_nameb_starts_with = array('self_nameb_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].NameB against the provided strings using starts with. Case Invariant.
$self_nameb_like = 'self_nameb_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].NameB for provided content. Case Invariant.
$self_namec_exact = array('self_namec_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].NameC against the provided strings using exact match.
$self_namec_starts_with = array('self_namec_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].NameC against the provided strings using starts with. Case Invariant.
$self_namec_like = 'self_namec_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].NameC for provided content. Case Invariant.
$self_street_number_exact = array('self_street_number_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].StreetNumber against the provided strings using exact match.
$self_street_number_starts_with = array('self_street_number_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].StreetNumber against the provided strings using starts with. Case Invariant.
$self_street_number_like = 'self_street_number_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].StreetNumber for provided content. Case Invariant.
$self_zip_exact = array('self_zip_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].PostalCode against the provided strings using exact match.
$self_zip_starts_with = array('self_zip_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].PostalCode against the provided strings using starts with. Case Invariant.
$self_zip_like = 'self_zip_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].PostalCode for provided content. Case Invariant.
$other_id = array(56); // int[] | Compares the provided range of Ids against Receipt.ContractualParty.Id and returns only those matching.
$other_street_exact = array('other_street_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Street against the provided strings using exact match.
$other_street_starts_with = array('other_street_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Street against the provided strings using starts with. Case Invariant.
$other_street_like = 'other_street_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Street for provided content. Case Invariant.
$other_town_exact = array('other_town_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Town against the provided strings using exact match.
$other_town_starts_with = array('other_town_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Town against the provided strings using starts with. Case Invariant.
$other_town_like = 'other_town_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Town for provided content. Case Invariant.
$other_country_exact = array('other_country_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Country against the provided strings using exact match.
$other_country_starts_with = array('other_country_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Country against the provided strings using starts with. Case Invariant.
$other_country_like = 'other_country_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Country for provided content. Case Invariant.
$other_namea_exact = array('other_namea_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameA against the provided strings using exact match.
$other_namea_starts_with = array('other_namea_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameA against the provided strings using starts with. Case Invariant.
$other_namea_like = 'other_namea_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameA for provided content. Case Invariant.
$other_nameb_exact = array('other_nameb_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameB against the provided strings using exact match.
$other_nameb_starts_with = array('other_nameb_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameB against the provided strings using starts with. Case Invariant.
$other_nameb_like = 'other_nameb_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameB for provided content. Case Invariant.
$other_namec_exact = array('other_namec_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameC against the provided strings using exact match.
$other_namec_starts_with = array('other_namec_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameC against the provided strings using starts with. Case Invariant.
$other_namec_like = 'other_namec_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameC for provided content. Case Invariant.
$other_street_number_exact = array('other_street_number_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].StreetNumber against the provided strings using exact match.
$other_street_number_starts_with = array('other_street_number_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].StreetNumber against the provided strings using starts with. Case Invariant.
$other_street_number_like = 'other_street_number_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].StreetNumber for provided content. Case Invariant.
$other_zip_exact = array('other_zip_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].PostalCode against the provided strings using exact match.
$other_zip_starts_with = array('other_zip_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].PostalCode against the provided strings using starts with. Case Invariant.
$other_zip_like = 'other_zip_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].PostalCode for provided content. Case Invariant.
$accounting_item_id = array(56); // int[] | Filters for receipts where the provided accounting item id is being used.
$picklist_id = array(56); // int[] | The picklists the receipts should contain to receive. If empty, all picklists will be received.
$state_group_kind = array('state_group_kind_example'); // string[] | The state IDs to receive. If empty, all states will be received.
$state_id = array(56); // int[] | The state IDs to receive. If empty, all states will be received.
$item_root_id = array(56); // int[] | Looks up all receipts, with positions having the provided item linked.
$item_frame_id = array(56); // int[] | Looks up all receipts, with positions having the provided item linked.
$not_item_root_id = array(56); // int[] | Looks up all receipts, without positions having the provided item linked.
$not_item_frame_id = array(56); // int[] | Looks up all receipts, without positions having the provided item linked.
$direction = array('direction_example'); // string[] | The directions to receive. If empty, all directions will be received.
$state_group_exact = array('state_group_exact_example'); // string[] | Takes the StateNode.GroupName and compares it against the provided names using exact match.
$state_group_starts_with = array('state_group_starts_with_example'); // string[] | Takes the StateNode.GroupName and compares it against the provided names using starts with match. Case Invariant.
$state_group_like = 'state_group_like_example'; // string | Takes the StateNode.GroupName and compares it against the provided names using DBFunction.Like match. Case Invariant.
$state_title_exact = array('state_title_exact_example'); // string[] | Takes the StateNode.Title and compares it against the provided names using exact match.
$state_title_starts_with = array('state_title_starts_with_example'); // string[] | Takes the StateNode.Title and compares it against the provided names using starts with match. Case Invariant.
$state_title_like = 'state_title_like_example'; // string | Takes the StateNode.Title and compares it against the provided names using DBFunction.Like match. Case Invariant.
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.
$identifier_exact = array('identifier_exact_example'); // string[] | Checks the identifier against the provided strings using exact match.
$identifier_starts_with = array('identifier_starts_with_example'); // string[] | Checks the identifier against the provided strings using starts with. Case Invariant.
$identifier_like = 'identifier_like_example'; // string | Uses LIKE expression to check the identifiers for provided content. Case Invariant.
$extref_exact = array('extref_exact_example'); // string[] | Checks the external reference against the provided strings using exact match.
$extref_starts_with = array('extref_starts_with_example'); // string[] | Checks the external reference against the provided strings using starts with. Case Invariant.
$extref_like = 'extref_like_example'; // string | Uses LIKE expression to check the external reference for provided content. Case Invariant.
$note_exact = array('note_exact_example'); // string[] | Checks the Receipt.Note against the provided strings using exact match.
$note_starts_with = array('note_starts_with_example'); // string[] | Checks the Receipt.Note against the provided strings using starts with. Case Invariant.
$note_like = 'note_like_example'; // string | Uses LIKE expression to check the Receipt.Note for provided content. Case Invariant.
$has_accounting_item = True; // bool | Wether or not an AccountingItem should be present.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.
$force_id = false; // bool | Enforces AND linking, regardless of chain-using-or, for '*-id' parameters.
$include_entries = false; // bool | Forces the inclusion of the linked receipt entries to be inside the result query.
$include_billing = false; // bool | Forces the inclusion of the linked billing address to be inside the result query.
$include_contact = false; // bool | Forces the inclusion of the linked contacts to be inside the result query.
$in_ext_auth = array(56); // int[] | Looks up only Receipts which are inside of the provided ExternAuthorities.
$not_in_ext_auth = array(56); // int[] | Looks up only Receipts which are not inside of the provided ExternAuthorities.
$has_ext_auth = True; // bool | If true, filters for Receipts with no linked ExternAuthority. If False, filters for Receipts with linked ExternAuthorities.

try {
    $apiInstance->receiptDataCaseAllReceiptAllCountGet($case_id, $continues_id, $receipt_id, $position_id, $self_id, $self_street_exact, $self_street_starts_with, $self_street_like, $self_town_exact, $self_town_starts_with, $self_town_like, $self_country_exact, $self_country_starts_with, $self_country_like, $self_namea_exact, $self_namea_starts_with, $self_namea_like, $self_nameb_exact, $self_nameb_starts_with, $self_nameb_like, $self_namec_exact, $self_namec_starts_with, $self_namec_like, $self_street_number_exact, $self_street_number_starts_with, $self_street_number_like, $self_zip_exact, $self_zip_starts_with, $self_zip_like, $other_id, $other_street_exact, $other_street_starts_with, $other_street_like, $other_town_exact, $other_town_starts_with, $other_town_like, $other_country_exact, $other_country_starts_with, $other_country_like, $other_namea_exact, $other_namea_starts_with, $other_namea_like, $other_nameb_exact, $other_nameb_starts_with, $other_nameb_like, $other_namec_exact, $other_namec_starts_with, $other_namec_like, $other_street_number_exact, $other_street_number_starts_with, $other_street_number_like, $other_zip_exact, $other_zip_starts_with, $other_zip_like, $accounting_item_id, $picklist_id, $state_group_kind, $state_id, $item_root_id, $item_frame_id, $not_item_root_id, $not_item_frame_id, $direction, $state_group_exact, $state_group_starts_with, $state_group_like, $state_title_exact, $state_title_starts_with, $state_title_like, $index, $limit, $identifier_exact, $identifier_starts_with, $identifier_like, $extref_exact, $extref_starts_with, $extref_like, $note_exact, $note_starts_with, $note_like, $has_accounting_item, $chain_using_or, $force_id, $include_entries, $include_billing, $include_contact, $in_ext_auth, $not_in_ext_auth, $has_ext_auth);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseAllReceiptAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **case_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **continues_id** | [**int[]**](../Model/int.md)| The Receipt IDs that preceeded the searched receipt, limiting the search to. If empty, no filter for Receipt will be set. | [optional]
 **receipt_id** | [**int[]**](../Model/int.md)| The ReceiptEntry IDs to limit the search to. If empty, no filter for ReceiptEntries will be set. | [optional]
 **position_id** | [**int[]**](../Model/int.md)| The ReceiptEntry IDs to limit the search to. If empty, no filter for ReceiptEntries will be set. | [optional]
 **self_id** | [**int[]**](../Model/int.md)| Compares the provided range of Ids against Receipt.Self.Id and returns only those matching. | [optional]
 **self_street_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Street against the provided strings using exact match. | [optional]
 **self_street_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Street against the provided strings using starts with. Case Invariant. | [optional]
 **self_street_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].Street for provided content. Case Invariant. | [optional]
 **self_town_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Town against the provided strings using exact match. | [optional]
 **self_town_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Town against the provided strings using starts with. Case Invariant. | [optional]
 **self_town_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].Town for provided content. Case Invariant. | [optional]
 **self_country_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Country against the provided strings using exact match. | [optional]
 **self_country_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Country against the provided strings using starts with. Case Invariant. | [optional]
 **self_country_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].Country for provided content. Case Invariant. | [optional]
 **self_namea_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameA against the provided strings using exact match. | [optional]
 **self_namea_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameA against the provided strings using starts with. Case Invariant. | [optional]
 **self_namea_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].NameA for provided content. Case Invariant. | [optional]
 **self_nameb_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameB against the provided strings using exact match. | [optional]
 **self_nameb_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameB against the provided strings using starts with. Case Invariant. | [optional]
 **self_nameb_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].NameB for provided content. Case Invariant. | [optional]
 **self_namec_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameC against the provided strings using exact match. | [optional]
 **self_namec_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameC against the provided strings using starts with. Case Invariant. | [optional]
 **self_namec_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].NameC for provided content. Case Invariant. | [optional]
 **self_street_number_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].StreetNumber against the provided strings using exact match. | [optional]
 **self_street_number_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].StreetNumber against the provided strings using starts with. Case Invariant. | [optional]
 **self_street_number_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].StreetNumber for provided content. Case Invariant. | [optional]
 **self_zip_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].PostalCode against the provided strings using exact match. | [optional]
 **self_zip_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].PostalCode against the provided strings using starts with. Case Invariant. | [optional]
 **self_zip_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].PostalCode for provided content. Case Invariant. | [optional]
 **other_id** | [**int[]**](../Model/int.md)| Compares the provided range of Ids against Receipt.ContractualParty.Id and returns only those matching. | [optional]
 **other_street_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Street against the provided strings using exact match. | [optional]
 **other_street_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Street against the provided strings using starts with. Case Invariant. | [optional]
 **other_street_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Street for provided content. Case Invariant. | [optional]
 **other_town_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Town against the provided strings using exact match. | [optional]
 **other_town_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Town against the provided strings using starts with. Case Invariant. | [optional]
 **other_town_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Town for provided content. Case Invariant. | [optional]
 **other_country_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Country against the provided strings using exact match. | [optional]
 **other_country_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Country against the provided strings using starts with. Case Invariant. | [optional]
 **other_country_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Country for provided content. Case Invariant. | [optional]
 **other_namea_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameA against the provided strings using exact match. | [optional]
 **other_namea_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameA against the provided strings using starts with. Case Invariant. | [optional]
 **other_namea_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameA for provided content. Case Invariant. | [optional]
 **other_nameb_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameB against the provided strings using exact match. | [optional]
 **other_nameb_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameB against the provided strings using starts with. Case Invariant. | [optional]
 **other_nameb_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameB for provided content. Case Invariant. | [optional]
 **other_namec_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameC against the provided strings using exact match. | [optional]
 **other_namec_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameC against the provided strings using starts with. Case Invariant. | [optional]
 **other_namec_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameC for provided content. Case Invariant. | [optional]
 **other_street_number_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].StreetNumber against the provided strings using exact match. | [optional]
 **other_street_number_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].StreetNumber against the provided strings using starts with. Case Invariant. | [optional]
 **other_street_number_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].StreetNumber for provided content. Case Invariant. | [optional]
 **other_zip_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].PostalCode against the provided strings using exact match. | [optional]
 **other_zip_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].PostalCode against the provided strings using starts with. Case Invariant. | [optional]
 **other_zip_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].PostalCode for provided content. Case Invariant. | [optional]
 **accounting_item_id** | [**int[]**](../Model/int.md)| Filters for receipts where the provided accounting item id is being used. | [optional]
 **picklist_id** | [**int[]**](../Model/int.md)| The picklists the receipts should contain to receive. If empty, all picklists will be received. | [optional]
 **state_group_kind** | [**string[]**](../Model/string.md)| The state IDs to receive. If empty, all states will be received. | [optional]
 **state_id** | [**int[]**](../Model/int.md)| The state IDs to receive. If empty, all states will be received. | [optional]
 **item_root_id** | [**int[]**](../Model/int.md)| Looks up all receipts, with positions having the provided item linked. | [optional]
 **item_frame_id** | [**int[]**](../Model/int.md)| Looks up all receipts, with positions having the provided item linked. | [optional]
 **not_item_root_id** | [**int[]**](../Model/int.md)| Looks up all receipts, without positions having the provided item linked. | [optional]
 **not_item_frame_id** | [**int[]**](../Model/int.md)| Looks up all receipts, without positions having the provided item linked. | [optional]
 **direction** | [**string[]**](../Model/string.md)| The directions to receive. If empty, all directions will be received. | [optional]
 **state_group_exact** | [**string[]**](../Model/string.md)| Takes the StateNode.GroupName and compares it against the provided names using exact match. | [optional]
 **state_group_starts_with** | [**string[]**](../Model/string.md)| Takes the StateNode.GroupName and compares it against the provided names using starts with match. Case Invariant. | [optional]
 **state_group_like** | **string**| Takes the StateNode.GroupName and compares it against the provided names using DBFunction.Like match. Case Invariant. | [optional]
 **state_title_exact** | [**string[]**](../Model/string.md)| Takes the StateNode.Title and compares it against the provided names using exact match. | [optional]
 **state_title_starts_with** | [**string[]**](../Model/string.md)| Takes the StateNode.Title and compares it against the provided names using starts with match. Case Invariant. | [optional]
 **state_title_like** | **string**| Takes the StateNode.Title and compares it against the provided names using DBFunction.Like match. Case Invariant. | [optional]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **identifier_exact** | [**string[]**](../Model/string.md)| Checks the identifier against the provided strings using exact match. | [optional]
 **identifier_starts_with** | [**string[]**](../Model/string.md)| Checks the identifier against the provided strings using starts with. Case Invariant. | [optional]
 **identifier_like** | **string**| Uses LIKE expression to check the identifiers for provided content. Case Invariant. | [optional]
 **extref_exact** | [**string[]**](../Model/string.md)| Checks the external reference against the provided strings using exact match. | [optional]
 **extref_starts_with** | [**string[]**](../Model/string.md)| Checks the external reference against the provided strings using starts with. Case Invariant. | [optional]
 **extref_like** | **string**| Uses LIKE expression to check the external reference for provided content. Case Invariant. | [optional]
 **note_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Note against the provided strings using exact match. | [optional]
 **note_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Note against the provided strings using starts with. Case Invariant. | [optional]
 **note_like** | **string**| Uses LIKE expression to check the Receipt.Note for provided content. Case Invariant. | [optional]
 **has_accounting_item** | **bool**| Wether or not an AccountingItem should be present. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]
 **force_id** | **bool**| Enforces AND linking, regardless of chain-using-or, for &#39;*-id&#39; parameters. | [optional] [default to false]
 **include_entries** | **bool**| Forces the inclusion of the linked receipt entries to be inside the result query. | [optional] [default to false]
 **include_billing** | **bool**| Forces the inclusion of the linked billing address to be inside the result query. | [optional] [default to false]
 **include_contact** | **bool**| Forces the inclusion of the linked contacts to be inside the result query. | [optional] [default to false]
 **in_ext_auth** | [**int[]**](../Model/int.md)| Looks up only Receipts which are inside of the provided ExternAuthorities. | [optional]
 **not_in_ext_auth** | [**int[]**](../Model/int.md)| Looks up only Receipts which are not inside of the provided ExternAuthorities. | [optional]
 **has_ext_auth** | **bool**| If true, filters for Receipts with no linked ExternAuthority. If False, filters for Receipts with linked ExternAuthorities. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseAllReceiptAllDeliveryGoalAllCountGet()`

```php
receiptDataCaseAllReceiptAllDeliveryGoalAllCountGet($index, $limit, $case_id, $receipt_id, $position_id, $goal_id, $chain_using_or)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.
$case_id = array(56); // int[] | Limits the query to those delivery goals which are inside of the provided cases.
$receipt_id = array(56); // int[] | Limits the query to those delivery goals which are inside of the provided receipts.
$position_id = array(56); // int[] | Limits the query to those delivery goals which are containing a specific receipt entry.
$goal_id = array(56); // int[] | Limits the query to those delivery goals which are provided.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $apiInstance->receiptDataCaseAllReceiptAllDeliveryGoalAllCountGet($index, $limit, $case_id, $receipt_id, $position_id, $goal_id, $chain_using_or);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseAllReceiptAllDeliveryGoalAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **case_id** | [**int[]**](../Model/int.md)| Limits the query to those delivery goals which are inside of the provided cases. | [optional]
 **receipt_id** | [**int[]**](../Model/int.md)| Limits the query to those delivery goals which are inside of the provided receipts. | [optional]
 **position_id** | [**int[]**](../Model/int.md)| Limits the query to those delivery goals which are containing a specific receipt entry. | [optional]
 **goal_id** | [**int[]**](../Model/int.md)| Limits the query to those delivery goals which are provided. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseAllReceiptAllDeliveryGoalAllGet()`

```php
receiptDataCaseAllReceiptAllDeliveryGoalAllGet($index, $limit, $case_id, $receipt_id, $position_id, $goal_id, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.
$case_id = array(56); // int[] | Limits the query to those delivery goals which are inside of the provided cases.
$receipt_id = array(56); // int[] | Limits the query to those delivery goals which are inside of the provided receipts.
$position_id = array(56); // int[] | Limits the query to those delivery goals which are containing a specific receipt entry.
$goal_id = array(56); // int[] | Limits the query to those delivery goals which are provided.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->receiptDataCaseAllReceiptAllDeliveryGoalAllGet($index, $limit, $case_id, $receipt_id, $position_id, $goal_id, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseAllReceiptAllDeliveryGoalAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **case_id** | [**int[]**](../Model/int.md)| Limits the query to those delivery goals which are inside of the provided cases. | [optional]
 **receipt_id** | [**int[]**](../Model/int.md)| Limits the query to those delivery goals which are inside of the provided receipts. | [optional]
 **position_id** | [**int[]**](../Model/int.md)| Limits the query to those delivery goals which are containing a specific receipt entry. | [optional]
 **goal_id** | [**int[]**](../Model/int.md)| Limits the query to those delivery goals which are provided. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal**](../Model/SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseAllReceiptAllGet()`

```php
receiptDataCaseAllReceiptAllGet($index, $limit, $case_id, $continues_id, $receipt_id, $position_id, $self_id, $other_id, $accounting_item_id, $picklist_id, $state_id, $item_root_id, $item_frame_id, $not_item_root_id, $not_item_frame_id, $self_street_exact, $self_street_starts_with, $self_street_like, $self_town_exact, $self_town_starts_with, $self_town_like, $self_country_exact, $self_country_starts_with, $self_country_like, $self_namea_exact, $self_namea_starts_with, $self_namea_like, $self_nameb_exact, $self_nameb_starts_with, $self_nameb_like, $self_namec_exact, $self_namec_starts_with, $self_namec_like, $self_street_number_exact, $self_street_number_starts_with, $self_street_number_like, $self_zip_exact, $self_zip_starts_with, $self_zip_like, $other_street_exact, $other_street_starts_with, $other_street_like, $other_town_exact, $other_town_starts_with, $other_town_like, $other_country_exact, $other_country_starts_with, $other_country_like, $other_namea_exact, $other_namea_starts_with, $other_namea_like, $other_nameb_exact, $other_nameb_starts_with, $other_nameb_like, $other_namec_exact, $other_namec_starts_with, $other_namec_like, $other_street_number_exact, $other_street_number_starts_with, $other_street_number_like, $other_zip_exact, $other_zip_starts_with, $other_zip_like, $state_group_kind, $direction, $state_group_exact, $state_group_starts_with, $state_group_like, $state_title_exact, $state_title_starts_with, $state_title_like, $identifier_exact, $identifier_starts_with, $identifier_like, $extrefa_exact, $extrefa_starts_with, $extrefa_like, $extrefb_exact, $extrefb_starts_with, $extrefb_like, $extrefc_exact, $extrefc_starts_with, $extrefc_like, $note_exact, $note_starts_with, $note_like, $has_accounting_item, $include_entries, $include_billing, $include_contact, $in_ext_auth, $not_in_ext_auth, $has_ext_auth, $chain_using_or, $force_id): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.
$case_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$continues_id = array(56); // int[] | The Receipt IDs that preceeded the searched receipt, limiting the search to. If empty, no filter for Receipt will be set.
$receipt_id = array(56); // int[] | The Receipt IDs to limit the search to. If empty, no filter for Receipt will be set.
$position_id = array(56); // int[] | The Position IDs to limit the search to. If empty, no filter for Position will be set.
$self_id = array(56); // int[] | Compares the provided range of Ids against Receipt.Self.Id and returns only those matching.
$other_id = array(56); // int[] | Compares the provided range of Ids against Receipt.ContractualParty.Id and returns only those matching.
$accounting_item_id = array(56); // int[] | Filters for receipts where the provided accounting item id is being used.
$picklist_id = array(56); // int[] | The picklists the receipts should contain to receive. If empty, all picklists will be received.
$state_id = array(56); // int[] | The state IDs to receive. If empty, all states will be received.
$item_root_id = array(56); // int[] | Looks up all receipts, with positions having the provided item linked.
$item_frame_id = array(56); // int[] | Looks up all receipts, with positions having the provided item linked.
$not_item_root_id = array(56); // int[] | Looks up all receipts, without positions having the provided item linked.
$not_item_frame_id = array(56); // int[] | Looks up all receipts, without positions having the provided item linked.
$self_street_exact = array('self_street_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].Street against the provided strings using exact match.
$self_street_starts_with = array('self_street_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].Street against the provided strings using starts with. Case Invariant.
$self_street_like = 'self_street_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].Street for provided content. Case Invariant.
$self_town_exact = array('self_town_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].Town against the provided strings using exact match.
$self_town_starts_with = array('self_town_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].Town against the provided strings using starts with. Case Invariant.
$self_town_like = 'self_town_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].Town for provided content. Case Invariant.
$self_country_exact = array('self_country_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].Country against the provided strings using exact match.
$self_country_starts_with = array('self_country_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].Country against the provided strings using starts with. Case Invariant.
$self_country_like = 'self_country_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].Country for provided content. Case Invariant.
$self_namea_exact = array('self_namea_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].NameA against the provided strings using exact match.
$self_namea_starts_with = array('self_namea_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].NameA against the provided strings using starts with. Case Invariant.
$self_namea_like = 'self_namea_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].NameA for provided content. Case Invariant.
$self_nameb_exact = array('self_nameb_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].NameB against the provided strings using exact match.
$self_nameb_starts_with = array('self_nameb_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].NameB against the provided strings using starts with. Case Invariant.
$self_nameb_like = 'self_nameb_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].NameB for provided content. Case Invariant.
$self_namec_exact = array('self_namec_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].NameC against the provided strings using exact match.
$self_namec_starts_with = array('self_namec_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].NameC against the provided strings using starts with. Case Invariant.
$self_namec_like = 'self_namec_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].NameC for provided content. Case Invariant.
$self_street_number_exact = array('self_street_number_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].StreetNumber against the provided strings using exact match.
$self_street_number_starts_with = array('self_street_number_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].StreetNumber against the provided strings using starts with. Case Invariant.
$self_street_number_like = 'self_street_number_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].StreetNumber for provided content. Case Invariant.
$self_zip_exact = array('self_zip_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].PostalCode against the provided strings using exact match.
$self_zip_starts_with = array('self_zip_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].PostalCode against the provided strings using starts with. Case Invariant.
$self_zip_like = 'self_zip_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].PostalCode for provided content. Case Invariant.
$other_street_exact = array('other_street_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Street against the provided strings using exact match.
$other_street_starts_with = array('other_street_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Street against the provided strings using starts with. Case Invariant.
$other_street_like = 'other_street_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Street for provided content. Case Invariant.
$other_town_exact = array('other_town_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Town against the provided strings using exact match.
$other_town_starts_with = array('other_town_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Town against the provided strings using starts with. Case Invariant.
$other_town_like = 'other_town_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Town for provided content. Case Invariant.
$other_country_exact = array('other_country_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Country against the provided strings using exact match.
$other_country_starts_with = array('other_country_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Country against the provided strings using starts with. Case Invariant.
$other_country_like = 'other_country_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Country for provided content. Case Invariant.
$other_namea_exact = array('other_namea_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameA against the provided strings using exact match.
$other_namea_starts_with = array('other_namea_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameA against the provided strings using starts with. Case Invariant.
$other_namea_like = 'other_namea_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameA for provided content. Case Invariant.
$other_nameb_exact = array('other_nameb_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameB against the provided strings using exact match.
$other_nameb_starts_with = array('other_nameb_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameB against the provided strings using starts with. Case Invariant.
$other_nameb_like = 'other_nameb_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameB for provided content. Case Invariant.
$other_namec_exact = array('other_namec_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameC against the provided strings using exact match.
$other_namec_starts_with = array('other_namec_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameC against the provided strings using starts with. Case Invariant.
$other_namec_like = 'other_namec_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameC for provided content. Case Invariant.
$other_street_number_exact = array('other_street_number_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].StreetNumber against the provided strings using exact match.
$other_street_number_starts_with = array('other_street_number_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].StreetNumber against the provided strings using starts with. Case Invariant.
$other_street_number_like = 'other_street_number_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].StreetNumber for provided content. Case Invariant.
$other_zip_exact = array('other_zip_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].PostalCode against the provided strings using exact match.
$other_zip_starts_with = array('other_zip_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].PostalCode against the provided strings using starts with. Case Invariant.
$other_zip_like = 'other_zip_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].PostalCode for provided content. Case Invariant.
$state_group_kind = array('state_group_kind_example'); // string[] | The state IDs to receive. If empty, all states will be received.
$direction = array('direction_example'); // string[] | The directions to receive. If empty, all directions will be received.
$state_group_exact = array('state_group_exact_example'); // string[] | Takes the StateNode.GroupName and compares it against the provided names using exact match.
$state_group_starts_with = array('state_group_starts_with_example'); // string[] | Takes the StateNode.GroupName and compares it against the provided names using starts with match. Case Invariant.
$state_group_like = 'state_group_like_example'; // string | Takes the StateNode.GroupName and compares it against the provided names using DBFunction.Like match. Case Invariant.
$state_title_exact = array('state_title_exact_example'); // string[] | Takes the StateNode.Title and compares it against the provided names using exact match.
$state_title_starts_with = array('state_title_starts_with_example'); // string[] | Takes the StateNode.Title and compares it against the provided names using starts with match. Case Invariant.
$state_title_like = 'state_title_like_example'; // string | Takes the StateNode.Title and compares it against the provided names using DBFunction.Like match. Case Invariant.
$identifier_exact = array('identifier_exact_example'); // string[] | Checks the identifier against the provided strings using exact match.
$identifier_starts_with = array('identifier_starts_with_example'); // string[] | Checks the identifier against the provided strings using starts with. Case Invariant.
$identifier_like = 'identifier_like_example'; // string | Uses LIKE expression to check the identifiers for provided content. Case Invariant.
$extrefa_exact = array('extrefa_exact_example'); // string[] | Checks the Receipt.ExternReferenceA against the provided strings using exact match.
$extrefa_starts_with = array('extrefa_starts_with_example'); // string[] | Checks the Receipt.ExternReferenceA against the provided strings using starts with. Case Invariant.
$extrefa_like = 'extrefa_like_example'; // string | Uses LIKE expression to check the Receipt.ExternReferenceA for provided content. Case Invariant.
$extrefb_exact = array('extrefb_exact_example'); // string[] | Checks the Receipt.ExternReferenceB against the provided strings using exact match.
$extrefb_starts_with = array('extrefb_starts_with_example'); // string[] | Checks the Receipt.ExternReferenceB against the provided strings using starts with. Case Invariant.
$extrefb_like = 'extrefb_like_example'; // string | Uses LIKE expression to check the Receipt.ExternReferenceB for provided content. Case Invariant.
$extrefc_exact = array('extrefc_exact_example'); // string[] | Checks the Receipt.ExternReferenceC against the provided strings using exact match.
$extrefc_starts_with = array('extrefc_starts_with_example'); // string[] | Checks the Receipt.ExternReferenceC against the provided strings using starts with. Case Invariant.
$extrefc_like = 'extrefc_like_example'; // string | Uses LIKE expression to check the Receipt.ExternReferenceC for provided content. Case Invariant.
$note_exact = array('note_exact_example'); // string[] | Checks the Receipt.Note against the provided strings using exact match.
$note_starts_with = array('note_starts_with_example'); // string[] | Checks the Receipt.Note against the provided strings using starts with. Case Invariant.
$note_like = 'note_like_example'; // string | Uses LIKE expression to check the Receipt.Note for provided content. Case Invariant.
$has_accounting_item = True; // bool | Wether or not an AccountingItem should be present.
$include_entries = false; // bool | Forces the inclusion of the linked receipt entries to be inside the result query.
$include_billing = false; // bool | Forces the inclusion of the linked billing address to be inside the result query.
$include_contact = false; // bool | Forces the inclusion of the linked contacts to be inside the result query.
$in_ext_auth = array(56); // int[] | Looks up only Receipts which are inside of the provided ExternAuthorities.
$not_in_ext_auth = array(56); // int[] | Looks up only Receipts which are not inside of the provided ExternAuthorities.
$has_ext_auth = True; // bool | If true, filters for Receipts with linked ExternAuthority. If False, filters for Receipts with no linked ExternAuthorities.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.
$force_id = false; // bool | Enforces AND linking, regardless of chain-using-or, for '*-id' parameters.

try {
    $result = $apiInstance->receiptDataCaseAllReceiptAllGet($index, $limit, $case_id, $continues_id, $receipt_id, $position_id, $self_id, $other_id, $accounting_item_id, $picklist_id, $state_id, $item_root_id, $item_frame_id, $not_item_root_id, $not_item_frame_id, $self_street_exact, $self_street_starts_with, $self_street_like, $self_town_exact, $self_town_starts_with, $self_town_like, $self_country_exact, $self_country_starts_with, $self_country_like, $self_namea_exact, $self_namea_starts_with, $self_namea_like, $self_nameb_exact, $self_nameb_starts_with, $self_nameb_like, $self_namec_exact, $self_namec_starts_with, $self_namec_like, $self_street_number_exact, $self_street_number_starts_with, $self_street_number_like, $self_zip_exact, $self_zip_starts_with, $self_zip_like, $other_street_exact, $other_street_starts_with, $other_street_like, $other_town_exact, $other_town_starts_with, $other_town_like, $other_country_exact, $other_country_starts_with, $other_country_like, $other_namea_exact, $other_namea_starts_with, $other_namea_like, $other_nameb_exact, $other_nameb_starts_with, $other_nameb_like, $other_namec_exact, $other_namec_starts_with, $other_namec_like, $other_street_number_exact, $other_street_number_starts_with, $other_street_number_like, $other_zip_exact, $other_zip_starts_with, $other_zip_like, $state_group_kind, $direction, $state_group_exact, $state_group_starts_with, $state_group_like, $state_title_exact, $state_title_starts_with, $state_title_like, $identifier_exact, $identifier_starts_with, $identifier_like, $extrefa_exact, $extrefa_starts_with, $extrefa_like, $extrefb_exact, $extrefb_starts_with, $extrefb_like, $extrefc_exact, $extrefc_starts_with, $extrefc_like, $note_exact, $note_starts_with, $note_like, $has_accounting_item, $include_entries, $include_billing, $include_contact, $in_ext_auth, $not_in_ext_auth, $has_ext_auth, $chain_using_or, $force_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseAllReceiptAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **case_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **continues_id** | [**int[]**](../Model/int.md)| The Receipt IDs that preceeded the searched receipt, limiting the search to. If empty, no filter for Receipt will be set. | [optional]
 **receipt_id** | [**int[]**](../Model/int.md)| The Receipt IDs to limit the search to. If empty, no filter for Receipt will be set. | [optional]
 **position_id** | [**int[]**](../Model/int.md)| The Position IDs to limit the search to. If empty, no filter for Position will be set. | [optional]
 **self_id** | [**int[]**](../Model/int.md)| Compares the provided range of Ids against Receipt.Self.Id and returns only those matching. | [optional]
 **other_id** | [**int[]**](../Model/int.md)| Compares the provided range of Ids against Receipt.ContractualParty.Id and returns only those matching. | [optional]
 **accounting_item_id** | [**int[]**](../Model/int.md)| Filters for receipts where the provided accounting item id is being used. | [optional]
 **picklist_id** | [**int[]**](../Model/int.md)| The picklists the receipts should contain to receive. If empty, all picklists will be received. | [optional]
 **state_id** | [**int[]**](../Model/int.md)| The state IDs to receive. If empty, all states will be received. | [optional]
 **item_root_id** | [**int[]**](../Model/int.md)| Looks up all receipts, with positions having the provided item linked. | [optional]
 **item_frame_id** | [**int[]**](../Model/int.md)| Looks up all receipts, with positions having the provided item linked. | [optional]
 **not_item_root_id** | [**int[]**](../Model/int.md)| Looks up all receipts, without positions having the provided item linked. | [optional]
 **not_item_frame_id** | [**int[]**](../Model/int.md)| Looks up all receipts, without positions having the provided item linked. | [optional]
 **self_street_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Street against the provided strings using exact match. | [optional]
 **self_street_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Street against the provided strings using starts with. Case Invariant. | [optional]
 **self_street_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].Street for provided content. Case Invariant. | [optional]
 **self_town_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Town against the provided strings using exact match. | [optional]
 **self_town_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Town against the provided strings using starts with. Case Invariant. | [optional]
 **self_town_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].Town for provided content. Case Invariant. | [optional]
 **self_country_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Country against the provided strings using exact match. | [optional]
 **self_country_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Country against the provided strings using starts with. Case Invariant. | [optional]
 **self_country_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].Country for provided content. Case Invariant. | [optional]
 **self_namea_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameA against the provided strings using exact match. | [optional]
 **self_namea_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameA against the provided strings using starts with. Case Invariant. | [optional]
 **self_namea_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].NameA for provided content. Case Invariant. | [optional]
 **self_nameb_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameB against the provided strings using exact match. | [optional]
 **self_nameb_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameB against the provided strings using starts with. Case Invariant. | [optional]
 **self_nameb_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].NameB for provided content. Case Invariant. | [optional]
 **self_namec_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameC against the provided strings using exact match. | [optional]
 **self_namec_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameC against the provided strings using starts with. Case Invariant. | [optional]
 **self_namec_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].NameC for provided content. Case Invariant. | [optional]
 **self_street_number_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].StreetNumber against the provided strings using exact match. | [optional]
 **self_street_number_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].StreetNumber against the provided strings using starts with. Case Invariant. | [optional]
 **self_street_number_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].StreetNumber for provided content. Case Invariant. | [optional]
 **self_zip_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].PostalCode against the provided strings using exact match. | [optional]
 **self_zip_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].PostalCode against the provided strings using starts with. Case Invariant. | [optional]
 **self_zip_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].PostalCode for provided content. Case Invariant. | [optional]
 **other_street_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Street against the provided strings using exact match. | [optional]
 **other_street_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Street against the provided strings using starts with. Case Invariant. | [optional]
 **other_street_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Street for provided content. Case Invariant. | [optional]
 **other_town_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Town against the provided strings using exact match. | [optional]
 **other_town_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Town against the provided strings using starts with. Case Invariant. | [optional]
 **other_town_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Town for provided content. Case Invariant. | [optional]
 **other_country_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Country against the provided strings using exact match. | [optional]
 **other_country_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Country against the provided strings using starts with. Case Invariant. | [optional]
 **other_country_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Country for provided content. Case Invariant. | [optional]
 **other_namea_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameA against the provided strings using exact match. | [optional]
 **other_namea_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameA against the provided strings using starts with. Case Invariant. | [optional]
 **other_namea_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameA for provided content. Case Invariant. | [optional]
 **other_nameb_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameB against the provided strings using exact match. | [optional]
 **other_nameb_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameB against the provided strings using starts with. Case Invariant. | [optional]
 **other_nameb_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameB for provided content. Case Invariant. | [optional]
 **other_namec_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameC against the provided strings using exact match. | [optional]
 **other_namec_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameC against the provided strings using starts with. Case Invariant. | [optional]
 **other_namec_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameC for provided content. Case Invariant. | [optional]
 **other_street_number_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].StreetNumber against the provided strings using exact match. | [optional]
 **other_street_number_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].StreetNumber against the provided strings using starts with. Case Invariant. | [optional]
 **other_street_number_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].StreetNumber for provided content. Case Invariant. | [optional]
 **other_zip_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].PostalCode against the provided strings using exact match. | [optional]
 **other_zip_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].PostalCode against the provided strings using starts with. Case Invariant. | [optional]
 **other_zip_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].PostalCode for provided content. Case Invariant. | [optional]
 **state_group_kind** | [**string[]**](../Model/string.md)| The state IDs to receive. If empty, all states will be received. | [optional]
 **direction** | [**string[]**](../Model/string.md)| The directions to receive. If empty, all directions will be received. | [optional]
 **state_group_exact** | [**string[]**](../Model/string.md)| Takes the StateNode.GroupName and compares it against the provided names using exact match. | [optional]
 **state_group_starts_with** | [**string[]**](../Model/string.md)| Takes the StateNode.GroupName and compares it against the provided names using starts with match. Case Invariant. | [optional]
 **state_group_like** | **string**| Takes the StateNode.GroupName and compares it against the provided names using DBFunction.Like match. Case Invariant. | [optional]
 **state_title_exact** | [**string[]**](../Model/string.md)| Takes the StateNode.Title and compares it against the provided names using exact match. | [optional]
 **state_title_starts_with** | [**string[]**](../Model/string.md)| Takes the StateNode.Title and compares it against the provided names using starts with match. Case Invariant. | [optional]
 **state_title_like** | **string**| Takes the StateNode.Title and compares it against the provided names using DBFunction.Like match. Case Invariant. | [optional]
 **identifier_exact** | [**string[]**](../Model/string.md)| Checks the identifier against the provided strings using exact match. | [optional]
 **identifier_starts_with** | [**string[]**](../Model/string.md)| Checks the identifier against the provided strings using starts with. Case Invariant. | [optional]
 **identifier_like** | **string**| Uses LIKE expression to check the identifiers for provided content. Case Invariant. | [optional]
 **extrefa_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ExternReferenceA against the provided strings using exact match. | [optional]
 **extrefa_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ExternReferenceA against the provided strings using starts with. Case Invariant. | [optional]
 **extrefa_like** | **string**| Uses LIKE expression to check the Receipt.ExternReferenceA for provided content. Case Invariant. | [optional]
 **extrefb_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ExternReferenceB against the provided strings using exact match. | [optional]
 **extrefb_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ExternReferenceB against the provided strings using starts with. Case Invariant. | [optional]
 **extrefb_like** | **string**| Uses LIKE expression to check the Receipt.ExternReferenceB for provided content. Case Invariant. | [optional]
 **extrefc_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ExternReferenceC against the provided strings using exact match. | [optional]
 **extrefc_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ExternReferenceC against the provided strings using starts with. Case Invariant. | [optional]
 **extrefc_like** | **string**| Uses LIKE expression to check the Receipt.ExternReferenceC for provided content. Case Invariant. | [optional]
 **note_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Note against the provided strings using exact match. | [optional]
 **note_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Note against the provided strings using starts with. Case Invariant. | [optional]
 **note_like** | **string**| Uses LIKE expression to check the Receipt.Note for provided content. Case Invariant. | [optional]
 **has_accounting_item** | **bool**| Wether or not an AccountingItem should be present. | [optional]
 **include_entries** | **bool**| Forces the inclusion of the linked receipt entries to be inside the result query. | [optional] [default to false]
 **include_billing** | **bool**| Forces the inclusion of the linked billing address to be inside the result query. | [optional] [default to false]
 **include_contact** | **bool**| Forces the inclusion of the linked contacts to be inside the result query. | [optional] [default to false]
 **in_ext_auth** | [**int[]**](../Model/int.md)| Looks up only Receipts which are inside of the provided ExternAuthorities. | [optional]
 **not_in_ext_auth** | [**int[]**](../Model/int.md)| Looks up only Receipts which are not inside of the provided ExternAuthorities. | [optional]
 **has_ext_auth** | **bool**| If true, filters for Receipts with linked ExternAuthority. If False, filters for Receipts with no linked ExternAuthorities. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]
 **force_id** | **bool**| Enforces AND linking, regardless of chain-using-or, for &#39;*-id&#39; parameters. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt**](../Model/SimplifySoftPecuniariusDataNetReceiptDataReceipt.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseAllReceiptAllOptionAllCountGet()`

```php
receiptDataCaseAllReceiptAllOptionAllCountGet($case_id, $receipt_id, $receipt_option_id, $index, $limit, $chain_using_or)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$case_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$receipt_id = array(56); // int[] | The ReceiptEntry IDs to limit the search to. If empty, no filter for ReceiptEntries will be set.
$receipt_option_id = array(56); // int[] | The receipt options to receive. If empty, all cases will be received.
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $apiInstance->receiptDataCaseAllReceiptAllOptionAllCountGet($case_id, $receipt_id, $receipt_option_id, $index, $limit, $chain_using_or);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseAllReceiptAllOptionAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **case_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **receipt_id** | [**int[]**](../Model/int.md)| The ReceiptEntry IDs to limit the search to. If empty, no filter for ReceiptEntries will be set. | [optional]
 **receipt_option_id** | [**int[]**](../Model/int.md)| The receipt options to receive. If empty, all cases will be received. | [optional]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseAllReceiptAllOptionAllGet()`

```php
receiptDataCaseAllReceiptAllOptionAllGet($case_id, $receipt_id, $receipt_option_id, $index, $limit, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceiptOption
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$case_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$receipt_id = array(56); // int[] | The ReceiptEntry IDs to limit the search to. If empty, no filter for ReceiptEntries will be set.
$receipt_option_id = array(56); // int[] | The receipt options to receive. If empty, all cases will be received.
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->receiptDataCaseAllReceiptAllOptionAllGet($case_id, $receipt_id, $receipt_option_id, $index, $limit, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseAllReceiptAllOptionAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **case_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **receipt_id** | [**int[]**](../Model/int.md)| The ReceiptEntry IDs to limit the search to. If empty, no filter for ReceiptEntries will be set. | [optional]
 **receipt_option_id** | [**int[]**](../Model/int.md)| The receipt options to receive. If empty, all cases will be received. | [optional]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceiptOption**](../Model/SimplifySoftPecuniariusDataNetReceiptDataReceiptOption.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseAllReceiptAllSearchGet()`

```php
receiptDataCaseAllReceiptAllSearchGet($case_id, $continues_id, $receipt_id, $position_id, $self_id, $self_street_exact, $self_street_starts_with, $self_street_like, $self_town_exact, $self_town_starts_with, $self_town_like, $self_country_exact, $self_country_starts_with, $self_country_like, $self_namea_exact, $self_namea_starts_with, $self_namea_like, $self_nameb_exact, $self_nameb_starts_with, $self_nameb_like, $self_namec_exact, $self_namec_starts_with, $self_namec_like, $self_street_number_exact, $self_street_number_starts_with, $self_street_number_like, $self_zip_exact, $self_zip_starts_with, $self_zip_like, $other_id, $other_street_exact, $other_street_starts_with, $other_street_like, $other_town_exact, $other_town_starts_with, $other_town_like, $other_country_exact, $other_country_starts_with, $other_country_like, $other_namea_exact, $other_namea_starts_with, $other_namea_like, $other_nameb_exact, $other_nameb_starts_with, $other_nameb_like, $other_namec_exact, $other_namec_starts_with, $other_namec_like, $other_street_number_exact, $other_street_number_starts_with, $other_street_number_like, $other_zip_exact, $other_zip_starts_with, $other_zip_like, $accounting_item_id, $picklist_id, $state_id, $item_root_id, $item_frame_id, $not_item_root_id, $not_item_frame_id, $state_group_kind, $direction, $state_group_exact, $state_group_starts_with, $state_group_like, $state_title_exact, $state_title_starts_with, $state_title_like, $index, $limit, $identifier_exact, $identifier_starts_with, $identifier_like, $extref_exact, $extref_starts_with, $extref_like, $note_exact, $note_starts_with, $note_like, $has_accounting_item, $chain_using_or, $force_id, $in_ext_auth, $not_in_ext_auth, $has_ext_auth): object
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$case_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$continues_id = array(56); // int[] | The Receipt IDs that preceeded the searched receipt, limiting the search to. If empty, no filter for Receipt will be set.
$receipt_id = array(56); // int[] | The Receipt IDs to limit the search to. If empty, no filter for Receipt will be set.
$position_id = array(56); // int[] | The Position IDs to limit the search to. If empty, no filter for Position will be set.
$self_id = array(56); // int[] | Compares the provided range of Ids against Receipt.Self.Id and returns only those matching.
$self_street_exact = array('self_street_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].Street against the provided strings using exact match.
$self_street_starts_with = array('self_street_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].Street against the provided strings using starts with. Case Invariant.
$self_street_like = 'self_street_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].Street for provided content. Case Invariant.
$self_town_exact = array('self_town_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].Town against the provided strings using exact match.
$self_town_starts_with = array('self_town_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].Town against the provided strings using starts with. Case Invariant.
$self_town_like = 'self_town_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].Town for provided content. Case Invariant.
$self_country_exact = array('self_country_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].Country against the provided strings using exact match.
$self_country_starts_with = array('self_country_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].Country against the provided strings using starts with. Case Invariant.
$self_country_like = 'self_country_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].Country for provided content. Case Invariant.
$self_namea_exact = array('self_namea_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].NameA against the provided strings using exact match.
$self_namea_starts_with = array('self_namea_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].NameA against the provided strings using starts with. Case Invariant.
$self_namea_like = 'self_namea_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].NameA for provided content. Case Invariant.
$self_nameb_exact = array('self_nameb_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].NameB against the provided strings using exact match.
$self_nameb_starts_with = array('self_nameb_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].NameB against the provided strings using starts with. Case Invariant.
$self_nameb_like = 'self_nameb_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].NameB for provided content. Case Invariant.
$self_namec_exact = array('self_namec_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].NameC against the provided strings using exact match.
$self_namec_starts_with = array('self_namec_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].NameC against the provided strings using starts with. Case Invariant.
$self_namec_like = 'self_namec_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].NameC for provided content. Case Invariant.
$self_street_number_exact = array('self_street_number_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].StreetNumber against the provided strings using exact match.
$self_street_number_starts_with = array('self_street_number_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].StreetNumber against the provided strings using starts with. Case Invariant.
$self_street_number_like = 'self_street_number_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].StreetNumber for provided content. Case Invariant.
$self_zip_exact = array('self_zip_exact_example'); // string[] | Checks the Receipt.Self.Addresses[].PostalCode against the provided strings using exact match.
$self_zip_starts_with = array('self_zip_starts_with_example'); // string[] | Checks the Receipt.Self.Addresses[].PostalCode against the provided strings using starts with. Case Invariant.
$self_zip_like = 'self_zip_like_example'; // string | Uses LIKE expression to check the Receipt.Self.Addresses[].PostalCode for provided content. Case Invariant.
$other_id = array(56); // int[] | Compares the provided range of Ids against Receipt.ContractualParty.Id and returns only those matching.
$other_street_exact = array('other_street_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Street against the provided strings using exact match.
$other_street_starts_with = array('other_street_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Street against the provided strings using starts with. Case Invariant.
$other_street_like = 'other_street_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Street for provided content. Case Invariant.
$other_town_exact = array('other_town_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Town against the provided strings using exact match.
$other_town_starts_with = array('other_town_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Town against the provided strings using starts with. Case Invariant.
$other_town_like = 'other_town_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Town for provided content. Case Invariant.
$other_country_exact = array('other_country_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Country against the provided strings using exact match.
$other_country_starts_with = array('other_country_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].Country against the provided strings using starts with. Case Invariant.
$other_country_like = 'other_country_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Country for provided content. Case Invariant.
$other_namea_exact = array('other_namea_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameA against the provided strings using exact match.
$other_namea_starts_with = array('other_namea_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameA against the provided strings using starts with. Case Invariant.
$other_namea_like = 'other_namea_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameA for provided content. Case Invariant.
$other_nameb_exact = array('other_nameb_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameB against the provided strings using exact match.
$other_nameb_starts_with = array('other_nameb_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameB against the provided strings using starts with. Case Invariant.
$other_nameb_like = 'other_nameb_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameB for provided content. Case Invariant.
$other_namec_exact = array('other_namec_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameC against the provided strings using exact match.
$other_namec_starts_with = array('other_namec_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].NameC against the provided strings using starts with. Case Invariant.
$other_namec_like = 'other_namec_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameC for provided content. Case Invariant.
$other_street_number_exact = array('other_street_number_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].StreetNumber against the provided strings using exact match.
$other_street_number_starts_with = array('other_street_number_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].StreetNumber against the provided strings using starts with. Case Invariant.
$other_street_number_like = 'other_street_number_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].StreetNumber for provided content. Case Invariant.
$other_zip_exact = array('other_zip_exact_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].PostalCode against the provided strings using exact match.
$other_zip_starts_with = array('other_zip_starts_with_example'); // string[] | Checks the Receipt.ContractualParty.Addresses[].PostalCode against the provided strings using starts with. Case Invariant.
$other_zip_like = 'other_zip_like_example'; // string | Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].PostalCode for provided content. Case Invariant.
$accounting_item_id = array(56); // int[] | Filters for receipts where the provided accounting item id is being used.
$picklist_id = array(56); // int[] | The picklists the receipts should contain to receive. If empty, all picklists will be received.
$state_id = array(56); // int[] | The state IDs to receive. If empty, all states will be received.
$item_root_id = array(56); // int[] | Looks up all receipts, with positions having the provided item linked.
$item_frame_id = array(56); // int[] | Looks up all receipts, with positions having the provided item linked.
$not_item_root_id = array(56); // int[] | Looks up all receipts, without positions having the provided item linked.
$not_item_frame_id = array(56); // int[] | Looks up all receipts, without positions having the provided item linked.
$state_group_kind = array('state_group_kind_example'); // string[] | The state IDs to receive. If empty, all states will be received.
$direction = array('direction_example'); // string[] | The directions to receive. If empty, all directions will be received.
$state_group_exact = array('state_group_exact_example'); // string[] | Takes the StateNode.GroupName and compares it against the provided names using exact match.
$state_group_starts_with = array('state_group_starts_with_example'); // string[] | Takes the StateNode.GroupName and compares it against the provided names using starts with match. Case Invariant.
$state_group_like = 'state_group_like_example'; // string | Takes the StateNode.GroupName and compares it against the provided names using DBFunction.Like match. Case Invariant.
$state_title_exact = array('state_title_exact_example'); // string[] | Takes the StateNode.Title and compares it against the provided names using exact match.
$state_title_starts_with = array('state_title_starts_with_example'); // string[] | Takes the StateNode.Title and compares it against the provided names using starts with match. Case Invariant.
$state_title_like = 'state_title_like_example'; // string | Takes the StateNode.Title and compares it against the provided names using DBFunction.Like match. Case Invariant.
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.
$identifier_exact = array('identifier_exact_example'); // string[] | Checks the identifier against the provided strings using exact match.
$identifier_starts_with = array('identifier_starts_with_example'); // string[] | Checks the identifier against the provided strings using starts with. Case Invariant.
$identifier_like = 'identifier_like_example'; // string | Uses LIKE expression to check the identifiers for provided content. Case Invariant.
$extref_exact = array('extref_exact_example'); // string[] | Checks the external reference against the provided strings using exact match.
$extref_starts_with = array('extref_starts_with_example'); // string[] | Checks the external reference against the provided strings using starts with. Case Invariant.
$extref_like = 'extref_like_example'; // string | Uses LIKE expression to check the external reference for provided content. Case Invariant.
$note_exact = array('note_exact_example'); // string[] | Checks the Receipt.Note against the provided strings using exact match.
$note_starts_with = array('note_starts_with_example'); // string[] | Checks the Receipt.Note against the provided strings using starts with. Case Invariant.
$note_like = 'note_like_example'; // string | Uses LIKE expression to check the Receipt.Note for provided content. Case Invariant.
$has_accounting_item = True; // bool | Wether or not an AccountingItem should be present.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.
$force_id = false; // bool | Enforces AND linking, regardless of chain-using-or, for '*-id' parameters.
$in_ext_auth = array(56); // int[] | Looks up only Receipts which are inside of the provided ExternAuthorities.
$not_in_ext_auth = array(56); // int[] | Looks up only Receipts which are not inside of the provided ExternAuthorities.
$has_ext_auth = True; // bool | If true, filters for Receipts with no linked ExternAuthority. If False, filters for Receipts with linked ExternAuthorities.

try {
    $result = $apiInstance->receiptDataCaseAllReceiptAllSearchGet($case_id, $continues_id, $receipt_id, $position_id, $self_id, $self_street_exact, $self_street_starts_with, $self_street_like, $self_town_exact, $self_town_starts_with, $self_town_like, $self_country_exact, $self_country_starts_with, $self_country_like, $self_namea_exact, $self_namea_starts_with, $self_namea_like, $self_nameb_exact, $self_nameb_starts_with, $self_nameb_like, $self_namec_exact, $self_namec_starts_with, $self_namec_like, $self_street_number_exact, $self_street_number_starts_with, $self_street_number_like, $self_zip_exact, $self_zip_starts_with, $self_zip_like, $other_id, $other_street_exact, $other_street_starts_with, $other_street_like, $other_town_exact, $other_town_starts_with, $other_town_like, $other_country_exact, $other_country_starts_with, $other_country_like, $other_namea_exact, $other_namea_starts_with, $other_namea_like, $other_nameb_exact, $other_nameb_starts_with, $other_nameb_like, $other_namec_exact, $other_namec_starts_with, $other_namec_like, $other_street_number_exact, $other_street_number_starts_with, $other_street_number_like, $other_zip_exact, $other_zip_starts_with, $other_zip_like, $accounting_item_id, $picklist_id, $state_id, $item_root_id, $item_frame_id, $not_item_root_id, $not_item_frame_id, $state_group_kind, $direction, $state_group_exact, $state_group_starts_with, $state_group_like, $state_title_exact, $state_title_starts_with, $state_title_like, $index, $limit, $identifier_exact, $identifier_starts_with, $identifier_like, $extref_exact, $extref_starts_with, $extref_like, $note_exact, $note_starts_with, $note_like, $has_accounting_item, $chain_using_or, $force_id, $in_ext_auth, $not_in_ext_auth, $has_ext_auth);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseAllReceiptAllSearchGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **case_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **continues_id** | [**int[]**](../Model/int.md)| The Receipt IDs that preceeded the searched receipt, limiting the search to. If empty, no filter for Receipt will be set. | [optional]
 **receipt_id** | [**int[]**](../Model/int.md)| The Receipt IDs to limit the search to. If empty, no filter for Receipt will be set. | [optional]
 **position_id** | [**int[]**](../Model/int.md)| The Position IDs to limit the search to. If empty, no filter for Position will be set. | [optional]
 **self_id** | [**int[]**](../Model/int.md)| Compares the provided range of Ids against Receipt.Self.Id and returns only those matching. | [optional]
 **self_street_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Street against the provided strings using exact match. | [optional]
 **self_street_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Street against the provided strings using starts with. Case Invariant. | [optional]
 **self_street_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].Street for provided content. Case Invariant. | [optional]
 **self_town_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Town against the provided strings using exact match. | [optional]
 **self_town_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Town against the provided strings using starts with. Case Invariant. | [optional]
 **self_town_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].Town for provided content. Case Invariant. | [optional]
 **self_country_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Country against the provided strings using exact match. | [optional]
 **self_country_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].Country against the provided strings using starts with. Case Invariant. | [optional]
 **self_country_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].Country for provided content. Case Invariant. | [optional]
 **self_namea_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameA against the provided strings using exact match. | [optional]
 **self_namea_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameA against the provided strings using starts with. Case Invariant. | [optional]
 **self_namea_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].NameA for provided content. Case Invariant. | [optional]
 **self_nameb_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameB against the provided strings using exact match. | [optional]
 **self_nameb_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameB against the provided strings using starts with. Case Invariant. | [optional]
 **self_nameb_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].NameB for provided content. Case Invariant. | [optional]
 **self_namec_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameC against the provided strings using exact match. | [optional]
 **self_namec_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].NameC against the provided strings using starts with. Case Invariant. | [optional]
 **self_namec_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].NameC for provided content. Case Invariant. | [optional]
 **self_street_number_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].StreetNumber against the provided strings using exact match. | [optional]
 **self_street_number_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].StreetNumber against the provided strings using starts with. Case Invariant. | [optional]
 **self_street_number_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].StreetNumber for provided content. Case Invariant. | [optional]
 **self_zip_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].PostalCode against the provided strings using exact match. | [optional]
 **self_zip_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Self.Addresses[].PostalCode against the provided strings using starts with. Case Invariant. | [optional]
 **self_zip_like** | **string**| Uses LIKE expression to check the Receipt.Self.Addresses[].PostalCode for provided content. Case Invariant. | [optional]
 **other_id** | [**int[]**](../Model/int.md)| Compares the provided range of Ids against Receipt.ContractualParty.Id and returns only those matching. | [optional]
 **other_street_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Street against the provided strings using exact match. | [optional]
 **other_street_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Street against the provided strings using starts with. Case Invariant. | [optional]
 **other_street_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Street for provided content. Case Invariant. | [optional]
 **other_town_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Town against the provided strings using exact match. | [optional]
 **other_town_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Town against the provided strings using starts with. Case Invariant. | [optional]
 **other_town_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Town for provided content. Case Invariant. | [optional]
 **other_country_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Country against the provided strings using exact match. | [optional]
 **other_country_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].Country against the provided strings using starts with. Case Invariant. | [optional]
 **other_country_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].Country for provided content. Case Invariant. | [optional]
 **other_namea_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameA against the provided strings using exact match. | [optional]
 **other_namea_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameA against the provided strings using starts with. Case Invariant. | [optional]
 **other_namea_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameA for provided content. Case Invariant. | [optional]
 **other_nameb_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameB against the provided strings using exact match. | [optional]
 **other_nameb_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameB against the provided strings using starts with. Case Invariant. | [optional]
 **other_nameb_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameB for provided content. Case Invariant. | [optional]
 **other_namec_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameC against the provided strings using exact match. | [optional]
 **other_namec_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].NameC against the provided strings using starts with. Case Invariant. | [optional]
 **other_namec_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].NameC for provided content. Case Invariant. | [optional]
 **other_street_number_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].StreetNumber against the provided strings using exact match. | [optional]
 **other_street_number_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].StreetNumber against the provided strings using starts with. Case Invariant. | [optional]
 **other_street_number_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].StreetNumber for provided content. Case Invariant. | [optional]
 **other_zip_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].PostalCode against the provided strings using exact match. | [optional]
 **other_zip_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.ContractualParty.Addresses[].PostalCode against the provided strings using starts with. Case Invariant. | [optional]
 **other_zip_like** | **string**| Uses LIKE expression to check the Receipt.ContractualParty.Addresses[].PostalCode for provided content. Case Invariant. | [optional]
 **accounting_item_id** | [**int[]**](../Model/int.md)| Filters for receipts where the provided accounting item id is being used. | [optional]
 **picklist_id** | [**int[]**](../Model/int.md)| The picklists the receipts should contain to receive. If empty, all picklists will be received. | [optional]
 **state_id** | [**int[]**](../Model/int.md)| The state IDs to receive. If empty, all states will be received. | [optional]
 **item_root_id** | [**int[]**](../Model/int.md)| Looks up all receipts, with positions having the provided item linked. | [optional]
 **item_frame_id** | [**int[]**](../Model/int.md)| Looks up all receipts, with positions having the provided item linked. | [optional]
 **not_item_root_id** | [**int[]**](../Model/int.md)| Looks up all receipts, without positions having the provided item linked. | [optional]
 **not_item_frame_id** | [**int[]**](../Model/int.md)| Looks up all receipts, without positions having the provided item linked. | [optional]
 **state_group_kind** | [**string[]**](../Model/string.md)| The state IDs to receive. If empty, all states will be received. | [optional]
 **direction** | [**string[]**](../Model/string.md)| The directions to receive. If empty, all directions will be received. | [optional]
 **state_group_exact** | [**string[]**](../Model/string.md)| Takes the StateNode.GroupName and compares it against the provided names using exact match. | [optional]
 **state_group_starts_with** | [**string[]**](../Model/string.md)| Takes the StateNode.GroupName and compares it against the provided names using starts with match. Case Invariant. | [optional]
 **state_group_like** | **string**| Takes the StateNode.GroupName and compares it against the provided names using DBFunction.Like match. Case Invariant. | [optional]
 **state_title_exact** | [**string[]**](../Model/string.md)| Takes the StateNode.Title and compares it against the provided names using exact match. | [optional]
 **state_title_starts_with** | [**string[]**](../Model/string.md)| Takes the StateNode.Title and compares it against the provided names using starts with match. Case Invariant. | [optional]
 **state_title_like** | **string**| Takes the StateNode.Title and compares it against the provided names using DBFunction.Like match. Case Invariant. | [optional]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **identifier_exact** | [**string[]**](../Model/string.md)| Checks the identifier against the provided strings using exact match. | [optional]
 **identifier_starts_with** | [**string[]**](../Model/string.md)| Checks the identifier against the provided strings using starts with. Case Invariant. | [optional]
 **identifier_like** | **string**| Uses LIKE expression to check the identifiers for provided content. Case Invariant. | [optional]
 **extref_exact** | [**string[]**](../Model/string.md)| Checks the external reference against the provided strings using exact match. | [optional]
 **extref_starts_with** | [**string[]**](../Model/string.md)| Checks the external reference against the provided strings using starts with. Case Invariant. | [optional]
 **extref_like** | **string**| Uses LIKE expression to check the external reference for provided content. Case Invariant. | [optional]
 **note_exact** | [**string[]**](../Model/string.md)| Checks the Receipt.Note against the provided strings using exact match. | [optional]
 **note_starts_with** | [**string[]**](../Model/string.md)| Checks the Receipt.Note against the provided strings using starts with. Case Invariant. | [optional]
 **note_like** | **string**| Uses LIKE expression to check the Receipt.Note for provided content. Case Invariant. | [optional]
 **has_accounting_item** | **bool**| Wether or not an AccountingItem should be present. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]
 **force_id** | **bool**| Enforces AND linking, regardless of chain-using-or, for &#39;*-id&#39; parameters. | [optional] [default to false]
 **in_ext_auth** | [**int[]**](../Model/int.md)| Looks up only Receipts which are inside of the provided ExternAuthorities. | [optional]
 **not_in_ext_auth** | [**int[]**](../Model/int.md)| Looks up only Receipts which are not inside of the provided ExternAuthorities. | [optional]
 **has_ext_auth** | **bool**| If true, filters for Receipts with no linked ExternAuthority. If False, filters for Receipts with linked ExternAuthorities. | [optional]

### Return type

**object**

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidDelete()`

```php
receiptDataCaseCaseidDelete($caseid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable

try {
    $apiInstance->receiptDataCaseCaseidDelete($caseid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidGet()`

```php
receiptDataCaseCaseidGet($caseid, $include_options, $include_receipt, $include_receipt_self, $include_receipt_contract, $include_receipt_billing, $include_receipt_accountingitem, $include_receipt_state, $include_receipt_state_localization, $include_receipt_paymentoption, $include_receipt_options, $include_receipt_positions, $include_receipt_positions_tax, $include_receipt_positions_options, $include_receipt_positions_itemframe, $include_receipt_positions_itemframe_parent, $include_receipt_positions_itemframe_parent_tax, $include_receipt_positions_itemframe_unit, $include_receipt_positions_itemframe_defaultfile, $include_receipt_positions_deliverygoals, $include_receipt_positions_deliverygoals_address, $include_receipt_positions_deliverygoals_method, $no_include)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$include_options = true; // bool | Sets wether returning the Case.Options[] is desired.
$include_receipt = true; // bool | Sets wether returning the Case.Receipts[] is desired.
$include_receipt_self = true; // bool | Sets wether returning the Case.Receipts[].Self is desired.
$include_receipt_contract = true; // bool | Sets wether returning the Case.Receipts[].ContractualParty is desired.
$include_receipt_billing = true; // bool | Sets wether returning the Case.Receipts[].Billing is desired.
$include_receipt_accountingitem = true; // bool | Sets wether returning the Case.Receipts[].AccountingItem is desired.
$include_receipt_state = true; // bool | Sets wether returning the Case.Receipts[].CurrentState is desired.
$include_receipt_state_localization = true; // bool | Sets wether returning the Case.Receipts[].CurrentState.Localizations is desired.
$include_receipt_paymentoption = true; // bool | Sets wether returning the Case.Receipts[].PaymentOption is desired.
$include_receipt_options = true; // bool | Sets wether returning the Case.Receipts[].Options is desired.
$include_receipt_positions = true; // bool | Sets wether returning the Case.Receipts[].Positions[] is desired.
$include_receipt_positions_tax = true; // bool | Sets wether returning the Case.Receipts[].Positions[] is desired.
$include_receipt_positions_options = true; // bool | Sets wether returning the Case.Receipts[].Positions[].Options is desired.
$include_receipt_positions_itemframe = true; // bool | Sets wether returning the Case.Receipts[].Positions[].ItemFrame is desired.
$include_receipt_positions_itemframe_parent = true; // bool | Sets wether returning the Case.Receipts[].Positions[].ItemFrame.Parent is desired.
$include_receipt_positions_itemframe_parent_tax = true; // bool | Sets wether returning the Case.Receipts[].Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired.
$include_receipt_positions_itemframe_unit = true; // bool | Sets wether returning the Case.Receipts[].Positions[].ItemFrame.Unit is desired.
$include_receipt_positions_itemframe_defaultfile = true; // bool | Sets wether returning the Case.Receipts[].Positions[].ItemFrame.DefaultFile is desired.
$include_receipt_positions_deliverygoals = true; // bool | Sets wether returning the Case.Receipts[].Positions[].DeliveryGoals[] is desired.
$include_receipt_positions_deliverygoals_address = true; // bool | Sets wether returning the Case.Receipts[].Positions[].DeliveryGoals[].Address is desired.
$include_receipt_positions_deliverygoals_method = true; // bool | Sets wether returning the Case.Receipts[].Positions[].DeliveryGoals[].Method is desired.
$no_include = false; // bool | Sets that no inclusion of anything is wanted.

try {
    $apiInstance->receiptDataCaseCaseidGet($caseid, $include_options, $include_receipt, $include_receipt_self, $include_receipt_contract, $include_receipt_billing, $include_receipt_accountingitem, $include_receipt_state, $include_receipt_state_localization, $include_receipt_paymentoption, $include_receipt_options, $include_receipt_positions, $include_receipt_positions_tax, $include_receipt_positions_options, $include_receipt_positions_itemframe, $include_receipt_positions_itemframe_parent, $include_receipt_positions_itemframe_parent_tax, $include_receipt_positions_itemframe_unit, $include_receipt_positions_itemframe_defaultfile, $include_receipt_positions_deliverygoals, $include_receipt_positions_deliverygoals_address, $include_receipt_positions_deliverygoals_method, $no_include);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **include_options** | **bool**| Sets wether returning the Case.Options[] is desired. | [optional] [default to true]
 **include_receipt** | **bool**| Sets wether returning the Case.Receipts[] is desired. | [optional] [default to true]
 **include_receipt_self** | **bool**| Sets wether returning the Case.Receipts[].Self is desired. | [optional] [default to true]
 **include_receipt_contract** | **bool**| Sets wether returning the Case.Receipts[].ContractualParty is desired. | [optional] [default to true]
 **include_receipt_billing** | **bool**| Sets wether returning the Case.Receipts[].Billing is desired. | [optional] [default to true]
 **include_receipt_accountingitem** | **bool**| Sets wether returning the Case.Receipts[].AccountingItem is desired. | [optional] [default to true]
 **include_receipt_state** | **bool**| Sets wether returning the Case.Receipts[].CurrentState is desired. | [optional] [default to true]
 **include_receipt_state_localization** | **bool**| Sets wether returning the Case.Receipts[].CurrentState.Localizations is desired. | [optional] [default to true]
 **include_receipt_paymentoption** | **bool**| Sets wether returning the Case.Receipts[].PaymentOption is desired. | [optional] [default to true]
 **include_receipt_options** | **bool**| Sets wether returning the Case.Receipts[].Options is desired. | [optional] [default to true]
 **include_receipt_positions** | **bool**| Sets wether returning the Case.Receipts[].Positions[] is desired. | [optional] [default to true]
 **include_receipt_positions_tax** | **bool**| Sets wether returning the Case.Receipts[].Positions[] is desired. | [optional] [default to true]
 **include_receipt_positions_options** | **bool**| Sets wether returning the Case.Receipts[].Positions[].Options is desired. | [optional] [default to true]
 **include_receipt_positions_itemframe** | **bool**| Sets wether returning the Case.Receipts[].Positions[].ItemFrame is desired. | [optional] [default to true]
 **include_receipt_positions_itemframe_parent** | **bool**| Sets wether returning the Case.Receipts[].Positions[].ItemFrame.Parent is desired. | [optional] [default to true]
 **include_receipt_positions_itemframe_parent_tax** | **bool**| Sets wether returning the Case.Receipts[].Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired. | [optional] [default to true]
 **include_receipt_positions_itemframe_unit** | **bool**| Sets wether returning the Case.Receipts[].Positions[].ItemFrame.Unit is desired. | [optional] [default to true]
 **include_receipt_positions_itemframe_defaultfile** | **bool**| Sets wether returning the Case.Receipts[].Positions[].ItemFrame.DefaultFile is desired. | [optional] [default to true]
 **include_receipt_positions_deliverygoals** | **bool**| Sets wether returning the Case.Receipts[].Positions[].DeliveryGoals[] is desired. | [optional] [default to true]
 **include_receipt_positions_deliverygoals_address** | **bool**| Sets wether returning the Case.Receipts[].Positions[].DeliveryGoals[].Address is desired. | [optional] [default to true]
 **include_receipt_positions_deliverygoals_method** | **bool**| Sets wether returning the Case.Receipts[].Positions[].DeliveryGoals[].Method is desired. | [optional] [default to true]
 **no_include** | **bool**| Sets that no inclusion of anything is wanted. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidOptionAllGet()`

```php
receiptDataCaseCaseidOptionAllGet($caseid, $include_definition, $include_tax, $no_include): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataCaseOption
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$include_definition = true; // bool | Sets wether returning the OptionDefinition is desired.
$include_tax = true; // bool | Sets wether returning the Tax is desired.
$no_include = false; // bool | Sets that no inclusion of anything is wanted.

try {
    $result = $apiInstance->receiptDataCaseCaseidOptionAllGet($caseid, $include_definition, $include_tax, $no_include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidOptionAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **include_definition** | **bool**| Sets wether returning the OptionDefinition is desired. | [optional] [default to true]
 **include_tax** | **bool**| Sets wether returning the Tax is desired. | [optional] [default to true]
 **no_include** | **bool**| Sets that no inclusion of anything is wanted. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataCaseOption**](../Model/SimplifySoftPecuniariusDataNetReceiptDataCaseOption.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidOptionOptidDelete()`

```php
receiptDataCaseCaseidOptionOptidDelete($caseid, $optid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$optid = 56; // int | unavailable

try {
    $apiInstance->receiptDataCaseCaseidOptionOptidDelete($caseid, $optid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidOptionOptidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **optid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidOptionOptidGet()`

```php
receiptDataCaseCaseidOptionOptidGet($caseid, $optid, $include_definition, $include_tax, $no_include)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$optid = 56; // int | unavailable
$include_definition = true; // bool | Sets wether returning the OptionDefinition is desired.
$include_tax = true; // bool | Sets wether returning the Tax is desired.
$no_include = false; // bool | Sets that no inclusion of anything is wanted.

try {
    $apiInstance->receiptDataCaseCaseidOptionOptidGet($caseid, $optid, $include_definition, $include_tax, $no_include);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidOptionOptidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **optid** | **int**| unavailable |
 **include_definition** | **bool**| Sets wether returning the OptionDefinition is desired. | [optional] [default to true]
 **include_tax** | **bool**| Sets wether returning the Tax is desired. | [optional] [default to true]
 **no_include** | **bool**| Sets that no inclusion of anything is wanted. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidOptionOptidPut()`

```php
receiptDataCaseCaseidOptionOptidPut($caseid, $optid, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$optid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->receiptDataCaseCaseidOptionOptidPut($caseid, $optid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidOptionOptidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **optid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidOptionPost()`

```php
receiptDataCaseCaseidOptionPost($caseid, $simplify_soft_pecuniarius_data_net_receipt_data_case_option): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataCaseOption
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$simplify_soft_pecuniarius_data_net_receipt_data_case_option = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataCaseOption(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataCaseOption

try {
    $result = $apiInstance->receiptDataCaseCaseidOptionPost($caseid, $simplify_soft_pecuniarius_data_net_receipt_data_case_option);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidOptionPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **simplify_soft_pecuniarius_data_net_receipt_data_case_option** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataCaseOption**](../Model/SimplifySoftPecuniariusDataNetReceiptDataCaseOption.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataCaseOption**](../Model/SimplifySoftPecuniariusDataNetReceiptDataCaseOption.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidPut()`

```php
receiptDataCaseCaseidPut($caseid, $body)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$body = new \stdClass; // object

try {
    $apiInstance->receiptDataCaseCaseidPut($caseid, $body);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **body** | **object**|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptAllGet()`

```php
receiptDataCaseCaseidReceiptAllGet($caseid, $include_self, $include_contract, $include_billing, $include_accountingitem, $include_state, $include_state_localization, $include_paymentoption, $include_options, $include_positions, $include_positions_tax, $include_positions_options, $include_positions_itemframe, $include_positions_itemframe_parent, $include_positions_itemframe_parent_tax, $include_positions_itemframe_unit, $include_positions_itemframe_defaultfile, $include_positions_deliverygoals, $include_positions_deliverygoals_address, $include_positions_deliverygoals_method, $no_include): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$include_self = true; // bool | Sets wether returning the Receipt.Self is desired.
$include_contract = true; // bool | Sets wether returning the Receipt.ContractualParty is desired.
$include_billing = true; // bool | Sets wether returning the Receipt.Billing is desired.
$include_accountingitem = true; // bool | Sets wether returning the Receipt.AccountingItem is desired.
$include_state = true; // bool | Sets wether returning the Receipt.CurrentState is desired.
$include_state_localization = true; // bool | Sets wether returning the Receipt.CurrentState.Localizations is desired.
$include_paymentoption = true; // bool | Sets wether returning the Receipt.PaymentOption is desired.
$include_options = true; // bool | Sets wether returning the Receipt.Options is desired.
$include_positions = true; // bool | Sets wether returning the Receipt.Positions[] is desired.
$include_positions_tax = true; // bool | Sets wether returning the Receipt.Positions[] is desired.
$include_positions_options = true; // bool | Sets wether returning the Receipt.Positions[].Options is desired.
$include_positions_itemframe = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame is desired.
$include_positions_itemframe_parent = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.Parent is desired.
$include_positions_itemframe_parent_tax = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired.
$include_positions_itemframe_unit = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.Unit is desired.
$include_positions_itemframe_defaultfile = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.DefaultFile is desired.
$include_positions_deliverygoals = true; // bool | Sets wether returning the Receipt.Positions[].DeliveryGoals[] is desired.
$include_positions_deliverygoals_address = true; // bool | Sets wether returning the Receipt.Positions[].DeliveryGoals[].Address is desired.
$include_positions_deliverygoals_method = true; // bool | Sets wether returning the Receipt.Positions[].DeliveryGoals[].Method is desired.
$no_include = false; // bool | Sets that no inclusion of anything is wanted.

try {
    $result = $apiInstance->receiptDataCaseCaseidReceiptAllGet($caseid, $include_self, $include_contract, $include_billing, $include_accountingitem, $include_state, $include_state_localization, $include_paymentoption, $include_options, $include_positions, $include_positions_tax, $include_positions_options, $include_positions_itemframe, $include_positions_itemframe_parent, $include_positions_itemframe_parent_tax, $include_positions_itemframe_unit, $include_positions_itemframe_defaultfile, $include_positions_deliverygoals, $include_positions_deliverygoals_address, $include_positions_deliverygoals_method, $no_include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **include_self** | **bool**| Sets wether returning the Receipt.Self is desired. | [optional] [default to true]
 **include_contract** | **bool**| Sets wether returning the Receipt.ContractualParty is desired. | [optional] [default to true]
 **include_billing** | **bool**| Sets wether returning the Receipt.Billing is desired. | [optional] [default to true]
 **include_accountingitem** | **bool**| Sets wether returning the Receipt.AccountingItem is desired. | [optional] [default to true]
 **include_state** | **bool**| Sets wether returning the Receipt.CurrentState is desired. | [optional] [default to true]
 **include_state_localization** | **bool**| Sets wether returning the Receipt.CurrentState.Localizations is desired. | [optional] [default to true]
 **include_paymentoption** | **bool**| Sets wether returning the Receipt.PaymentOption is desired. | [optional] [default to true]
 **include_options** | **bool**| Sets wether returning the Receipt.Options is desired. | [optional] [default to true]
 **include_positions** | **bool**| Sets wether returning the Receipt.Positions[] is desired. | [optional] [default to true]
 **include_positions_tax** | **bool**| Sets wether returning the Receipt.Positions[] is desired. | [optional] [default to true]
 **include_positions_options** | **bool**| Sets wether returning the Receipt.Positions[].Options is desired. | [optional] [default to true]
 **include_positions_itemframe** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame is desired. | [optional] [default to true]
 **include_positions_itemframe_parent** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.Parent is desired. | [optional] [default to true]
 **include_positions_itemframe_parent_tax** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired. | [optional] [default to true]
 **include_positions_itemframe_unit** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.Unit is desired. | [optional] [default to true]
 **include_positions_itemframe_defaultfile** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.DefaultFile is desired. | [optional] [default to true]
 **include_positions_deliverygoals** | **bool**| Sets wether returning the Receipt.Positions[].DeliveryGoals[] is desired. | [optional] [default to true]
 **include_positions_deliverygoals_address** | **bool**| Sets wether returning the Receipt.Positions[].DeliveryGoals[].Address is desired. | [optional] [default to true]
 **include_positions_deliverygoals_method** | **bool**| Sets wether returning the Receipt.Positions[].DeliveryGoals[].Method is desired. | [optional] [default to true]
 **no_include** | **bool**| Sets that no inclusion of anything is wanted. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt**](../Model/SimplifySoftPecuniariusDataNetReceiptDataReceipt.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptPost()`

```php
receiptDataCaseCaseidReceiptPost($caseid, $entry, $simplify_soft_pecuniarius_data_net_receipt_data_receipt): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$entry = 56; // int | The entry to use.
$simplify_soft_pecuniarius_data_net_receipt_data_receipt = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt

try {
    $result = $apiInstance->receiptDataCaseCaseidReceiptPost($caseid, $entry, $simplify_soft_pecuniarius_data_net_receipt_data_receipt);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **entry** | **int**| The entry to use. | [optional]
 **simplify_soft_pecuniarius_data_net_receipt_data_receipt** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt**](../Model/SimplifySoftPecuniariusDataNetReceiptDataReceipt.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt**](../Model/SimplifySoftPecuniariusDataNetReceiptDataReceipt.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidDelete()`

```php
receiptDataCaseCaseidReceiptReceiptidDelete($caseid, $receiptid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable

try {
    $apiInstance->receiptDataCaseCaseidReceiptReceiptidDelete($caseid, $receiptid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidDeliveryGoalAllGet()`

```php
receiptDataCaseCaseidReceiptReceiptidDeliveryGoalAllGet($caseid, $receiptid, $include_address, $include_method, $include_method_tax, $include_entries, $include_entries_tax, $include_entries_options, $include_entries_itemframe, $include_entries_itemframe_parent, $include_entries_itemframe_parent_tax, $include_entries_itemframe_unit, $include_entries_itemframe_defaultfile, $include_entries_deliverygoals, $include_entries_deliverygoals_address, $include_entries_deliverygoals_method, $include_shipments, $include_shipments_tax, $include_shipments_entries, $include_shipments_entries_itemframe, $include_shipments_entries_itemframe_parent, $include_shipments_entries_itemframe_parent_tax, $include_shipments_entries_itemframe_unit, $include_shipments_entries_itemframe_defaultfile, $no_include): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$include_address = true; // bool | Sets wether returning the DeliveryGoal.Address is desired.
$include_method = true; // bool | Sets wether returning the DeliveryGoal.Method is desired.
$include_method_tax = true; // bool | Sets wether returning the DeliveryGoal.Method.TaxDefinition and -.Tax is desired.
$include_entries = true; // bool | Sets wether returning the DeliveryGoal.Positions[] is desired.
$include_entries_tax = true; // bool | Sets wether returning the DeliveryGoal.Positions[] is desired.
$include_entries_options = true; // bool | Sets wether returning the DeliveryGoal.Positions[].Options is desired.
$include_entries_itemframe = true; // bool | Sets wether returning the DeliveryGoal.Positions[].ItemFrame is desired.
$include_entries_itemframe_parent = true; // bool | Sets wether returning the DeliveryGoal.Positions[].ItemFrame.Parent is desired.
$include_entries_itemframe_parent_tax = true; // bool | Sets wether returning the DeliveryGoal.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired.
$include_entries_itemframe_unit = true; // bool | Sets wether returning the DeliveryGoal.Positions[].ItemFrame.Unit is desired.
$include_entries_itemframe_defaultfile = true; // bool | Sets wether returning the DeliveryGoal.Positions[].ItemFrame.DefaultFile is desired.
$include_entries_deliverygoals = true; // bool | Sets wether returning the DeliveryGoal.Positions[].DeliveryGoals[] is desired.
$include_entries_deliverygoals_address = true; // bool | Sets wether returning the DeliveryGoal.Positions[].DeliveryGoals[].Address is desired.
$include_entries_deliverygoals_method = true; // bool | Sets wether returning the DeliveryGoal.Positions[].DeliveryGoals[].Method is desired.
$include_shipments = true; // bool | Sets wether returning the DeliveryGoal.Shipments is desired.
$include_shipments_tax = true; // bool | Sets wether returning the DeliveryGoal.Shipments[].Tax is desired.
$include_shipments_entries = true; // bool | Sets wether returning the DeliveryGoal.Shipments[].Tax is desired.
$include_shipments_entries_itemframe = true; // bool | Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame is desired.
$include_shipments_entries_itemframe_parent = true; // bool | Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame.Parent is desired.
$include_shipments_entries_itemframe_parent_tax = true; // bool | Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame.Parent.TaxDefinition and -.Tax is desired.
$include_shipments_entries_itemframe_unit = true; // bool | Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame.Unit is desired.
$include_shipments_entries_itemframe_defaultfile = true; // bool | Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame.DefaultFile is desired.
$no_include = false; // bool | Sets that no inclusion of anything is wanted.

try {
    $result = $apiInstance->receiptDataCaseCaseidReceiptReceiptidDeliveryGoalAllGet($caseid, $receiptid, $include_address, $include_method, $include_method_tax, $include_entries, $include_entries_tax, $include_entries_options, $include_entries_itemframe, $include_entries_itemframe_parent, $include_entries_itemframe_parent_tax, $include_entries_itemframe_unit, $include_entries_itemframe_defaultfile, $include_entries_deliverygoals, $include_entries_deliverygoals_address, $include_entries_deliverygoals_method, $include_shipments, $include_shipments_tax, $include_shipments_entries, $include_shipments_entries_itemframe, $include_shipments_entries_itemframe_parent, $include_shipments_entries_itemframe_parent_tax, $include_shipments_entries_itemframe_unit, $include_shipments_entries_itemframe_defaultfile, $no_include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidDeliveryGoalAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **include_address** | **bool**| Sets wether returning the DeliveryGoal.Address is desired. | [optional] [default to true]
 **include_method** | **bool**| Sets wether returning the DeliveryGoal.Method is desired. | [optional] [default to true]
 **include_method_tax** | **bool**| Sets wether returning the DeliveryGoal.Method.TaxDefinition and -.Tax is desired. | [optional] [default to true]
 **include_entries** | **bool**| Sets wether returning the DeliveryGoal.Positions[] is desired. | [optional] [default to true]
 **include_entries_tax** | **bool**| Sets wether returning the DeliveryGoal.Positions[] is desired. | [optional] [default to true]
 **include_entries_options** | **bool**| Sets wether returning the DeliveryGoal.Positions[].Options is desired. | [optional] [default to true]
 **include_entries_itemframe** | **bool**| Sets wether returning the DeliveryGoal.Positions[].ItemFrame is desired. | [optional] [default to true]
 **include_entries_itemframe_parent** | **bool**| Sets wether returning the DeliveryGoal.Positions[].ItemFrame.Parent is desired. | [optional] [default to true]
 **include_entries_itemframe_parent_tax** | **bool**| Sets wether returning the DeliveryGoal.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired. | [optional] [default to true]
 **include_entries_itemframe_unit** | **bool**| Sets wether returning the DeliveryGoal.Positions[].ItemFrame.Unit is desired. | [optional] [default to true]
 **include_entries_itemframe_defaultfile** | **bool**| Sets wether returning the DeliveryGoal.Positions[].ItemFrame.DefaultFile is desired. | [optional] [default to true]
 **include_entries_deliverygoals** | **bool**| Sets wether returning the DeliveryGoal.Positions[].DeliveryGoals[] is desired. | [optional] [default to true]
 **include_entries_deliverygoals_address** | **bool**| Sets wether returning the DeliveryGoal.Positions[].DeliveryGoals[].Address is desired. | [optional] [default to true]
 **include_entries_deliverygoals_method** | **bool**| Sets wether returning the DeliveryGoal.Positions[].DeliveryGoals[].Method is desired. | [optional] [default to true]
 **include_shipments** | **bool**| Sets wether returning the DeliveryGoal.Shipments is desired. | [optional] [default to true]
 **include_shipments_tax** | **bool**| Sets wether returning the DeliveryGoal.Shipments[].Tax is desired. | [optional] [default to true]
 **include_shipments_entries** | **bool**| Sets wether returning the DeliveryGoal.Shipments[].Tax is desired. | [optional] [default to true]
 **include_shipments_entries_itemframe** | **bool**| Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame is desired. | [optional] [default to true]
 **include_shipments_entries_itemframe_parent** | **bool**| Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame.Parent is desired. | [optional] [default to true]
 **include_shipments_entries_itemframe_parent_tax** | **bool**| Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame.Parent.TaxDefinition and -.Tax is desired. | [optional] [default to true]
 **include_shipments_entries_itemframe_unit** | **bool**| Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame.Unit is desired. | [optional] [default to true]
 **include_shipments_entries_itemframe_defaultfile** | **bool**| Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame.DefaultFile is desired. | [optional] [default to true]
 **no_include** | **bool**| Sets that no inclusion of anything is wanted. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal**](../Model/SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidDelete()`

```php
receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidDelete($caseid, $receiptid, $goalid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$goalid = 56; // int | unavailable

try {
    $apiInstance->receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidDelete($caseid, $receiptid, $goalid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **goalid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidGet()`

```php
receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidGet($caseid, $receiptid, $goalid, $include_address, $include_method, $include_method_tax, $include_entries, $include_entries_tax, $include_entries_options, $include_entries_itemframe, $include_entries_itemframe_parent, $include_entries_itemframe_parent_tax, $include_entries_itemframe_unit, $include_entries_itemframe_defaultfile, $include_entries_deliverygoals, $include_entries_deliverygoals_address, $include_entries_deliverygoals_method, $include_shipments, $include_shipments_tax, $include_shipments_entries, $include_shipments_entries_itemframe, $include_shipments_entries_itemframe_parent, $include_shipments_entries_itemframe_parent_tax, $include_shipments_entries_itemframe_unit, $include_shipments_entries_itemframe_defaultfile, $no_include)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$goalid = 56; // int | unavailable
$include_address = true; // bool | Sets wether returning the DeliveryGoal.Address is desired.
$include_method = true; // bool | Sets wether returning the DeliveryGoal.Method is desired.
$include_method_tax = true; // bool | Sets wether returning the DeliveryGoal.Method.TaxDefinition and -.Tax is desired.
$include_entries = true; // bool | Sets wether returning the DeliveryGoal.Positions[] is desired.
$include_entries_tax = true; // bool | Sets wether returning the DeliveryGoal.Positions[] is desired.
$include_entries_options = true; // bool | Sets wether returning the DeliveryGoal.Positions[].Options is desired.
$include_entries_itemframe = true; // bool | Sets wether returning the DeliveryGoal.Positions[].ItemFrame is desired.
$include_entries_itemframe_parent = true; // bool | Sets wether returning the DeliveryGoal.Positions[].ItemFrame.Parent is desired.
$include_entries_itemframe_parent_tax = true; // bool | Sets wether returning the DeliveryGoal.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired.
$include_entries_itemframe_unit = true; // bool | Sets wether returning the DeliveryGoal.Positions[].ItemFrame.Unit is desired.
$include_entries_itemframe_defaultfile = true; // bool | Sets wether returning the DeliveryGoal.Positions[].ItemFrame.DefaultFile is desired.
$include_entries_deliverygoals = true; // bool | Sets wether returning the DeliveryGoal.Positions[].DeliveryGoals[] is desired.
$include_entries_deliverygoals_address = true; // bool | Sets wether returning the DeliveryGoal.Positions[].DeliveryGoals[].Address is desired.
$include_entries_deliverygoals_method = true; // bool | Sets wether returning the DeliveryGoal.Positions[].DeliveryGoals[].Method is desired.
$include_shipments = true; // bool | Sets wether returning the DeliveryGoal.Shipments is desired.
$include_shipments_tax = true; // bool | Sets wether returning the DeliveryGoal.Shipments[].Tax is desired.
$include_shipments_entries = true; // bool | Sets wether returning the DeliveryGoal.Shipments[].Tax is desired.
$include_shipments_entries_itemframe = true; // bool | Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame is desired.
$include_shipments_entries_itemframe_parent = true; // bool | Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame.Parent is desired.
$include_shipments_entries_itemframe_parent_tax = true; // bool | Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame.Parent.TaxDefinition and -.Tax is desired.
$include_shipments_entries_itemframe_unit = true; // bool | Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame.Unit is desired.
$include_shipments_entries_itemframe_defaultfile = true; // bool | Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame.DefaultFile is desired.
$no_include = false; // bool | Sets that no inclusion of anything is wanted.

try {
    $apiInstance->receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidGet($caseid, $receiptid, $goalid, $include_address, $include_method, $include_method_tax, $include_entries, $include_entries_tax, $include_entries_options, $include_entries_itemframe, $include_entries_itemframe_parent, $include_entries_itemframe_parent_tax, $include_entries_itemframe_unit, $include_entries_itemframe_defaultfile, $include_entries_deliverygoals, $include_entries_deliverygoals_address, $include_entries_deliverygoals_method, $include_shipments, $include_shipments_tax, $include_shipments_entries, $include_shipments_entries_itemframe, $include_shipments_entries_itemframe_parent, $include_shipments_entries_itemframe_parent_tax, $include_shipments_entries_itemframe_unit, $include_shipments_entries_itemframe_defaultfile, $no_include);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **goalid** | **int**| unavailable |
 **include_address** | **bool**| Sets wether returning the DeliveryGoal.Address is desired. | [optional] [default to true]
 **include_method** | **bool**| Sets wether returning the DeliveryGoal.Method is desired. | [optional] [default to true]
 **include_method_tax** | **bool**| Sets wether returning the DeliveryGoal.Method.TaxDefinition and -.Tax is desired. | [optional] [default to true]
 **include_entries** | **bool**| Sets wether returning the DeliveryGoal.Positions[] is desired. | [optional] [default to true]
 **include_entries_tax** | **bool**| Sets wether returning the DeliveryGoal.Positions[] is desired. | [optional] [default to true]
 **include_entries_options** | **bool**| Sets wether returning the DeliveryGoal.Positions[].Options is desired. | [optional] [default to true]
 **include_entries_itemframe** | **bool**| Sets wether returning the DeliveryGoal.Positions[].ItemFrame is desired. | [optional] [default to true]
 **include_entries_itemframe_parent** | **bool**| Sets wether returning the DeliveryGoal.Positions[].ItemFrame.Parent is desired. | [optional] [default to true]
 **include_entries_itemframe_parent_tax** | **bool**| Sets wether returning the DeliveryGoal.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired. | [optional] [default to true]
 **include_entries_itemframe_unit** | **bool**| Sets wether returning the DeliveryGoal.Positions[].ItemFrame.Unit is desired. | [optional] [default to true]
 **include_entries_itemframe_defaultfile** | **bool**| Sets wether returning the DeliveryGoal.Positions[].ItemFrame.DefaultFile is desired. | [optional] [default to true]
 **include_entries_deliverygoals** | **bool**| Sets wether returning the DeliveryGoal.Positions[].DeliveryGoals[] is desired. | [optional] [default to true]
 **include_entries_deliverygoals_address** | **bool**| Sets wether returning the DeliveryGoal.Positions[].DeliveryGoals[].Address is desired. | [optional] [default to true]
 **include_entries_deliverygoals_method** | **bool**| Sets wether returning the DeliveryGoal.Positions[].DeliveryGoals[].Method is desired. | [optional] [default to true]
 **include_shipments** | **bool**| Sets wether returning the DeliveryGoal.Shipments is desired. | [optional] [default to true]
 **include_shipments_tax** | **bool**| Sets wether returning the DeliveryGoal.Shipments[].Tax is desired. | [optional] [default to true]
 **include_shipments_entries** | **bool**| Sets wether returning the DeliveryGoal.Shipments[].Tax is desired. | [optional] [default to true]
 **include_shipments_entries_itemframe** | **bool**| Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame is desired. | [optional] [default to true]
 **include_shipments_entries_itemframe_parent** | **bool**| Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame.Parent is desired. | [optional] [default to true]
 **include_shipments_entries_itemframe_parent_tax** | **bool**| Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame.Parent.TaxDefinition and -.Tax is desired. | [optional] [default to true]
 **include_shipments_entries_itemframe_unit** | **bool**| Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame.Unit is desired. | [optional] [default to true]
 **include_shipments_entries_itemframe_defaultfile** | **bool**| Sets wether returning the DeliveryGoal.Shipments[].Entries[].ItemFrame.DefaultFile is desired. | [optional] [default to true]
 **no_include** | **bool**| Sets that no inclusion of anything is wanted. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionIdsGet()`

```php
receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionIdsGet($caseid, $receiptid, $goalid): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$goalid = 56; // int | unavailable

try {
    $result = $apiInstance->receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionIdsGet($caseid, $receiptid, $goalid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionIdsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **goalid** | **int**| unavailable |

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32**](../Model/SimplifySoftPecuniariusDataNetPrimitivesNetInt32.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionPosidLinkPost()`

```php
receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionPosidLinkPost($caseid, $receiptid, $goalid, $posid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$goalid = 56; // int | unavailable
$posid = 56; // int | unavailable

try {
    $apiInstance->receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionPosidLinkPost($caseid, $receiptid, $goalid, $posid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionPosidLinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **goalid** | **int**| unavailable |
 **posid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionPosidUnlinkPost()`

```php
receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionPosidUnlinkPost($caseid, $receiptid, $goalid, $posid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$goalid = 56; // int | unavailable
$posid = 56; // int | unavailable

try {
    $apiInstance->receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionPosidUnlinkPost($caseid, $receiptid, $goalid, $posid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPositionPosidUnlinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **goalid** | **int**| unavailable |
 **posid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPut()`

```php
receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPut($caseid, $receiptid, $goalid, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$goalid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPut($caseid, $receiptid, $goalid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidDeliveryGoalGoalidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **goalid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidDeliveryGoalPost()`

```php
receiptDataCaseCaseidReceiptReceiptidDeliveryGoalPost($caseid, $receiptid, $simplify_soft_pecuniarius_data_net_receipt_data_delivery_goal): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$simplify_soft_pecuniarius_data_net_receipt_data_delivery_goal = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal

try {
    $result = $apiInstance->receiptDataCaseCaseidReceiptReceiptidDeliveryGoalPost($caseid, $receiptid, $simplify_soft_pecuniarius_data_net_receipt_data_delivery_goal);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidDeliveryGoalPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **simplify_soft_pecuniarius_data_net_receipt_data_delivery_goal** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal**](../Model/SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal**](../Model/SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidFileFileidLinkPost()`

```php
receiptDataCaseCaseidReceiptReceiptidFileFileidLinkPost($caseid, $receiptid, $fileid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$fileid = 56; // int | unavailable

try {
    $apiInstance->receiptDataCaseCaseidReceiptReceiptidFileFileidLinkPost($caseid, $receiptid, $fileid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidFileFileidLinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **fileid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidFileFileidUnlinkPost()`

```php
receiptDataCaseCaseidReceiptReceiptidFileFileidUnlinkPost($caseid, $receiptid, $fileid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$fileid = 56; // int | unavailable

try {
    $apiInstance->receiptDataCaseCaseidReceiptReceiptidFileFileidUnlinkPost($caseid, $receiptid, $fileid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidFileFileidUnlinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **fileid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidGet()`

```php
receiptDataCaseCaseidReceiptReceiptidGet($caseid, $receiptid, $include_self, $include_self_contactpossibilities, $include_self_addresses, $include_contract, $include_contract_contactpossibilities, $include_contract_addresses, $include_billing, $include_accountingitem, $include_state, $include_state_localization, $include_paymentoption, $include_options, $include_deliverygoals, $include_deliverygoals_address, $include_deliverygoals_method, $include_positions, $include_positions_tax, $include_positions_options, $include_positions_itemframe, $include_positions_itemframe_linkeditems, $include_positions_itemframe_linkeditems_secondary, $include_positions_itemframe_linkeditems_secondary_tax, $include_positions_itemframe_linkeditems_secondary_frame, $include_positions_itemframe_linkeditems_secondary_frame_unit, $include_positions_itemframe_linkeditems_secondary_stocks, $include_positions_itemframe_linkeditems_secondary_stocks_location, $include_positions_itemframe_linkeditems_secondary_stocks_location_storage, $include_positions_itemframe_linkeditems_secondary_stocks_location_storage_warehouse, $include_positions_itemframe_parent, $include_positions_itemframe_parent_tax, $include_positions_itemframe_parent_stocks, $include_positions_itemframe_parent_stocks_location, $include_positions_itemframe_parent_stocks_location_storage, $include_positions_itemframe_parent_stocks_location_storage_warehouse, $include_positions_itemframe_unit, $include_positions_deliverygoals, $include_positions_deliverygoals_address, $include_positions_deliverygoals_method, $default_include)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$include_self = true; // bool | Sets wether returning the Receipt.Self is desired.
$include_self_contactpossibilities = true; // bool | Sets wether returning the Receipt.Self.ContactPossibilities is desired.
$include_self_addresses = true; // bool | Sets wether returning the Receipt.Self.Addresses is desired.
$include_contract = true; // bool | Sets wether returning the Receipt.ContractualParty is desired.
$include_contract_contactpossibilities = true; // bool | Sets wether returning the Receipt.ContractualParty.ContactPossibilities is desired.
$include_contract_addresses = true; // bool | Sets wether returning the Receipt.ContractualParty.Addresses is desired.
$include_billing = true; // bool | Sets wether returning the Receipt.Billing is desired.
$include_accountingitem = true; // bool | Sets wether returning the Receipt.AccountingItem is desired.
$include_state = true; // bool | Sets wether returning the Receipt.CurrentState is desired.
$include_state_localization = true; // bool | Sets wether returning the Receipt.CurrentState.Localizations is desired.
$include_paymentoption = true; // bool | Sets wether returning the Receipt.PaymentOption is desired.
$include_options = true; // bool | Sets wether returning the Receipt.Options is desired.
$include_deliverygoals = true; // bool | Sets wether returning the Receipt.Deliveries[] is desired.
$include_deliverygoals_address = true; // bool | Sets wether returning the Receipt.Deliveries[].Address is desired.
$include_deliverygoals_method = true; // bool | Sets wether returning the Receipt.Deliveries[].Method is desired.
$include_positions = true; // bool | Sets wether returning the Receipt.Positions[] is desired.
$include_positions_tax = true; // bool | Sets wether returning the Receipt.Positions[].Tax is desired.
$include_positions_options = true; // bool | Sets wether returning the Receipt.Positions[].Options is desired.
$include_positions_itemframe = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame is desired.
$include_positions_itemframe_linkeditems = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[] is desired.
$include_positions_itemframe_linkeditems_secondary = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[].Secondary is desired.
$include_positions_itemframe_linkeditems_secondary_tax = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[].Secondary.TaxDefinition and -.Tax is desired.
$include_positions_itemframe_linkeditems_secondary_frame = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[].Secondary.Frame is desired.
$include_positions_itemframe_linkeditems_secondary_frame_unit = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[].Secondary.Frame.Unit is desired.
$include_positions_itemframe_linkeditems_secondary_stocks = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[].Secondary.Stocks[] is desired.
$include_positions_itemframe_linkeditems_secondary_stocks_location = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[].Secondary.Stocks[].StorageLocation is desired.
$include_positions_itemframe_linkeditems_secondary_stocks_location_storage = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[].Secondary.Stocks[].StorageLocation.Storage is desired.
$include_positions_itemframe_linkeditems_secondary_stocks_location_storage_warehouse = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[].Secondary.Stocks[].StorageLocation.Storage.Warehouse is desired.
$include_positions_itemframe_parent = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.Parent is desired.
$include_positions_itemframe_parent_tax = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired.
$include_positions_itemframe_parent_stocks = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.Parent.Stocks[] is desired.
$include_positions_itemframe_parent_stocks_location = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.Parent.Stocks[].StorageLocation is desired.
$include_positions_itemframe_parent_stocks_location_storage = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.Parent.Stocks[].StorageLocation.Storage is desired.
$include_positions_itemframe_parent_stocks_location_storage_warehouse = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.Parent.Stocks[].StorageLocation.Storage.Warehouse is desired.
$include_positions_itemframe_unit = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.Unit is desired.
$include_positions_deliverygoals = true; // bool | Sets wether returning the Receipt.Positions[].DeliveryGoals[] is desired.
$include_positions_deliverygoals_address = true; // bool | Sets wether returning the Receipt.Positions[].DeliveryGoals[].Address is desired.
$include_positions_deliverygoals_method = true; // bool | Sets wether returning the Receipt.Positions[].DeliveryGoals[].Method is desired.
$default_include = false; // bool | Sets that no inclusion of anything is wanted.

try {
    $apiInstance->receiptDataCaseCaseidReceiptReceiptidGet($caseid, $receiptid, $include_self, $include_self_contactpossibilities, $include_self_addresses, $include_contract, $include_contract_contactpossibilities, $include_contract_addresses, $include_billing, $include_accountingitem, $include_state, $include_state_localization, $include_paymentoption, $include_options, $include_deliverygoals, $include_deliverygoals_address, $include_deliverygoals_method, $include_positions, $include_positions_tax, $include_positions_options, $include_positions_itemframe, $include_positions_itemframe_linkeditems, $include_positions_itemframe_linkeditems_secondary, $include_positions_itemframe_linkeditems_secondary_tax, $include_positions_itemframe_linkeditems_secondary_frame, $include_positions_itemframe_linkeditems_secondary_frame_unit, $include_positions_itemframe_linkeditems_secondary_stocks, $include_positions_itemframe_linkeditems_secondary_stocks_location, $include_positions_itemframe_linkeditems_secondary_stocks_location_storage, $include_positions_itemframe_linkeditems_secondary_stocks_location_storage_warehouse, $include_positions_itemframe_parent, $include_positions_itemframe_parent_tax, $include_positions_itemframe_parent_stocks, $include_positions_itemframe_parent_stocks_location, $include_positions_itemframe_parent_stocks_location_storage, $include_positions_itemframe_parent_stocks_location_storage_warehouse, $include_positions_itemframe_unit, $include_positions_deliverygoals, $include_positions_deliverygoals_address, $include_positions_deliverygoals_method, $default_include);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **include_self** | **bool**| Sets wether returning the Receipt.Self is desired. | [optional] [default to true]
 **include_self_contactpossibilities** | **bool**| Sets wether returning the Receipt.Self.ContactPossibilities is desired. | [optional] [default to true]
 **include_self_addresses** | **bool**| Sets wether returning the Receipt.Self.Addresses is desired. | [optional] [default to true]
 **include_contract** | **bool**| Sets wether returning the Receipt.ContractualParty is desired. | [optional] [default to true]
 **include_contract_contactpossibilities** | **bool**| Sets wether returning the Receipt.ContractualParty.ContactPossibilities is desired. | [optional] [default to true]
 **include_contract_addresses** | **bool**| Sets wether returning the Receipt.ContractualParty.Addresses is desired. | [optional] [default to true]
 **include_billing** | **bool**| Sets wether returning the Receipt.Billing is desired. | [optional] [default to true]
 **include_accountingitem** | **bool**| Sets wether returning the Receipt.AccountingItem is desired. | [optional] [default to true]
 **include_state** | **bool**| Sets wether returning the Receipt.CurrentState is desired. | [optional] [default to true]
 **include_state_localization** | **bool**| Sets wether returning the Receipt.CurrentState.Localizations is desired. | [optional] [default to true]
 **include_paymentoption** | **bool**| Sets wether returning the Receipt.PaymentOption is desired. | [optional] [default to true]
 **include_options** | **bool**| Sets wether returning the Receipt.Options is desired. | [optional] [default to true]
 **include_deliverygoals** | **bool**| Sets wether returning the Receipt.Deliveries[] is desired. | [optional] [default to true]
 **include_deliverygoals_address** | **bool**| Sets wether returning the Receipt.Deliveries[].Address is desired. | [optional] [default to true]
 **include_deliverygoals_method** | **bool**| Sets wether returning the Receipt.Deliveries[].Method is desired. | [optional] [default to true]
 **include_positions** | **bool**| Sets wether returning the Receipt.Positions[] is desired. | [optional] [default to true]
 **include_positions_tax** | **bool**| Sets wether returning the Receipt.Positions[].Tax is desired. | [optional] [default to true]
 **include_positions_options** | **bool**| Sets wether returning the Receipt.Positions[].Options is desired. | [optional] [default to true]
 **include_positions_itemframe** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame is desired. | [optional] [default to true]
 **include_positions_itemframe_linkeditems** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[] is desired. | [optional] [default to true]
 **include_positions_itemframe_linkeditems_secondary** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[].Secondary is desired. | [optional] [default to true]
 **include_positions_itemframe_linkeditems_secondary_tax** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[].Secondary.TaxDefinition and -.Tax is desired. | [optional] [default to true]
 **include_positions_itemframe_linkeditems_secondary_frame** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[].Secondary.Frame is desired. | [optional] [default to true]
 **include_positions_itemframe_linkeditems_secondary_frame_unit** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[].Secondary.Frame.Unit is desired. | [optional] [default to true]
 **include_positions_itemframe_linkeditems_secondary_stocks** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[].Secondary.Stocks[] is desired. | [optional] [default to true]
 **include_positions_itemframe_linkeditems_secondary_stocks_location** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[].Secondary.Stocks[].StorageLocation is desired. | [optional] [default to true]
 **include_positions_itemframe_linkeditems_secondary_stocks_location_storage** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[].Secondary.Stocks[].StorageLocation.Storage is desired. | [optional] [default to true]
 **include_positions_itemframe_linkeditems_secondary_stocks_location_storage_warehouse** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.LinkedItems[].Secondary.Stocks[].StorageLocation.Storage.Warehouse is desired. | [optional] [default to true]
 **include_positions_itemframe_parent** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.Parent is desired. | [optional] [default to true]
 **include_positions_itemframe_parent_tax** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired. | [optional] [default to true]
 **include_positions_itemframe_parent_stocks** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.Parent.Stocks[] is desired. | [optional] [default to true]
 **include_positions_itemframe_parent_stocks_location** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.Parent.Stocks[].StorageLocation is desired. | [optional] [default to true]
 **include_positions_itemframe_parent_stocks_location_storage** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.Parent.Stocks[].StorageLocation.Storage is desired. | [optional] [default to true]
 **include_positions_itemframe_parent_stocks_location_storage_warehouse** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.Parent.Stocks[].StorageLocation.Storage.Warehouse is desired. | [optional] [default to true]
 **include_positions_itemframe_unit** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.Unit is desired. | [optional] [default to true]
 **include_positions_deliverygoals** | **bool**| Sets wether returning the Receipt.Positions[].DeliveryGoals[] is desired. | [optional] [default to true]
 **include_positions_deliverygoals_address** | **bool**| Sets wether returning the Receipt.Positions[].DeliveryGoals[].Address is desired. | [optional] [default to true]
 **include_positions_deliverygoals_method** | **bool**| Sets wether returning the Receipt.Positions[].DeliveryGoals[].Method is desired. | [optional] [default to true]
 **default_include** | **bool**| Sets that no inclusion of anything is wanted. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidOptionAllGet()`

```php
receiptDataCaseCaseidReceiptReceiptidOptionAllGet($caseid, $receiptid, $include_definition, $include_tax, $no_include): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceiptOption
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$include_definition = true; // bool | Sets wether returning the OptionDefinition is desired.
$include_tax = true; // bool | Sets wether returning the Tax is desired.
$no_include = false; // bool | Sets that no inclusion of anything is wanted.

try {
    $result = $apiInstance->receiptDataCaseCaseidReceiptReceiptidOptionAllGet($caseid, $receiptid, $include_definition, $include_tax, $no_include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidOptionAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **include_definition** | **bool**| Sets wether returning the OptionDefinition is desired. | [optional] [default to true]
 **include_tax** | **bool**| Sets wether returning the Tax is desired. | [optional] [default to true]
 **no_include** | **bool**| Sets that no inclusion of anything is wanted. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceiptOption**](../Model/SimplifySoftPecuniariusDataNetReceiptDataReceiptOption.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidOptionOptidDelete()`

```php
receiptDataCaseCaseidReceiptReceiptidOptionOptidDelete($caseid, $receiptid, $optid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$optid = 56; // int | unavailable

try {
    $apiInstance->receiptDataCaseCaseidReceiptReceiptidOptionOptidDelete($caseid, $receiptid, $optid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidOptionOptidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **optid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidOptionOptidGet()`

```php
receiptDataCaseCaseidReceiptReceiptidOptionOptidGet($caseid, $receiptid, $optid, $include_definition, $include_tax, $no_include)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$optid = 56; // int | unavailable
$include_definition = true; // bool | Sets wether returning the OptionDefinition is desired.
$include_tax = true; // bool | Sets wether returning the Tax is desired.
$no_include = false; // bool | Sets that no inclusion of anything is wanted.

try {
    $apiInstance->receiptDataCaseCaseidReceiptReceiptidOptionOptidGet($caseid, $receiptid, $optid, $include_definition, $include_tax, $no_include);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidOptionOptidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **optid** | **int**| unavailable |
 **include_definition** | **bool**| Sets wether returning the OptionDefinition is desired. | [optional] [default to true]
 **include_tax** | **bool**| Sets wether returning the Tax is desired. | [optional] [default to true]
 **no_include** | **bool**| Sets that no inclusion of anything is wanted. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidOptionOptidPut()`

```php
receiptDataCaseCaseidReceiptReceiptidOptionOptidPut($caseid, $receiptid, $optid, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$optid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->receiptDataCaseCaseidReceiptReceiptidOptionOptidPut($caseid, $receiptid, $optid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidOptionOptidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **optid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidOptionPost()`

```php
receiptDataCaseCaseidReceiptReceiptidOptionPost($caseid, $receiptid, $simplify_soft_pecuniarius_data_net_receipt_data_receipt_option): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceiptOption
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$simplify_soft_pecuniarius_data_net_receipt_data_receipt_option = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceiptOption(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceiptOption

try {
    $result = $apiInstance->receiptDataCaseCaseidReceiptReceiptidOptionPost($caseid, $receiptid, $simplify_soft_pecuniarius_data_net_receipt_data_receipt_option);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidOptionPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **simplify_soft_pecuniarius_data_net_receipt_data_receipt_option** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceiptOption**](../Model/SimplifySoftPecuniariusDataNetReceiptDataReceiptOption.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceiptOption**](../Model/SimplifySoftPecuniariusDataNetReceiptDataReceiptOption.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidPathsGet()`

```php
receiptDataCaseCaseidReceiptReceiptidPathsGet($caseid, $receiptid, $language): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPackagesTransitionPath
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$language = 'language_example'; // string | The 2 letter ISO code of the language you want the response to contain.

try {
    $result = $apiInstance->receiptDataCaseCaseidReceiptReceiptidPathsGet($caseid, $receiptid, $language);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidPathsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **language** | **string**| The 2 letter ISO code of the language you want the response to contain. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPackagesTransitionPath**](../Model/SimplifySoftPecuniariusDataNetReceiptDataPackagesTransitionPath.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidPathsPost()`

```php
receiptDataCaseCaseidReceiptReceiptidPathsPost($caseid, $receiptid, $transition_id)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$transition_id = 56; // int | Allows to follow an Interaction by refering to it via its ID. Interaction must be exposed to current state.

try {
    $apiInstance->receiptDataCaseCaseidReceiptReceiptidPathsPost($caseid, $receiptid, $transition_id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidPathsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **transition_id** | **int**| Allows to follow an Interaction by refering to it via its ID. Interaction must be exposed to current state. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidPositionAllGet()`

```php
receiptDataCaseCaseidReceiptReceiptidPositionAllGet($caseid, $receiptid, $include_tax, $include_options, $include_itemframe, $include_itemframe_parent, $include_itemframe_parent_tax, $include_itemframe_unit, $include_itemframe_defaultfile, $include_deliverygoal, $include_deliverygoal_address, $include_deliverygoal_method, $no_include): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPosition
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$include_tax = true; // bool | Sets wether returning the Receipt.Positions[] is desired.
$include_options = true; // bool | Sets wether returning the Receipt.Positions[].Options is desired.
$include_itemframe = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame is desired.
$include_itemframe_parent = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.Parent is desired.
$include_itemframe_parent_tax = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired.
$include_itemframe_unit = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.Unit is desired.
$include_itemframe_defaultfile = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.DefaultFile is desired.
$include_deliverygoal = true; // bool | Sets wether returning the Receipt.Positions[].DeliveryGoals[] is desired.
$include_deliverygoal_address = true; // bool | Sets wether returning the Receipt.Positions[].DeliveryGoals[].Address is desired.
$include_deliverygoal_method = true; // bool | Sets wether returning the Receipt.Positions[].DeliveryGoals[].Method is desired.
$no_include = false; // bool | Sets that no inclusion of anything is wanted.

try {
    $result = $apiInstance->receiptDataCaseCaseidReceiptReceiptidPositionAllGet($caseid, $receiptid, $include_tax, $include_options, $include_itemframe, $include_itemframe_parent, $include_itemframe_parent_tax, $include_itemframe_unit, $include_itemframe_defaultfile, $include_deliverygoal, $include_deliverygoal_address, $include_deliverygoal_method, $no_include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidPositionAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **include_tax** | **bool**| Sets wether returning the Receipt.Positions[] is desired. | [optional] [default to true]
 **include_options** | **bool**| Sets wether returning the Receipt.Positions[].Options is desired. | [optional] [default to true]
 **include_itemframe** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame is desired. | [optional] [default to true]
 **include_itemframe_parent** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.Parent is desired. | [optional] [default to true]
 **include_itemframe_parent_tax** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired. | [optional] [default to true]
 **include_itemframe_unit** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.Unit is desired. | [optional] [default to true]
 **include_itemframe_defaultfile** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.DefaultFile is desired. | [optional] [default to true]
 **include_deliverygoal** | **bool**| Sets wether returning the Receipt.Positions[].DeliveryGoals[] is desired. | [optional] [default to true]
 **include_deliverygoal_address** | **bool**| Sets wether returning the Receipt.Positions[].DeliveryGoals[].Address is desired. | [optional] [default to true]
 **include_deliverygoal_method** | **bool**| Sets wether returning the Receipt.Positions[].DeliveryGoals[].Method is desired. | [optional] [default to true]
 **no_include** | **bool**| Sets that no inclusion of anything is wanted. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPosition**](../Model/SimplifySoftPecuniariusDataNetReceiptDataPosition.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidPositionIdsGet()`

```php
receiptDataCaseCaseidReceiptReceiptidPositionIdsGet($caseid, $receiptid): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable

try {
    $result = $apiInstance->receiptDataCaseCaseidReceiptReceiptidPositionIdsGet($caseid, $receiptid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidPositionIdsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32**](../Model/SimplifySoftPecuniariusDataNetPrimitivesNetInt32.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidPut()`

```php
receiptDataCaseCaseidReceiptReceiptidPut($caseid, $receiptid, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->receiptDataCaseCaseidReceiptReceiptidPut($caseid, $receiptid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidReceiptReceiptidShipmentsGet()`

```php
receiptDataCaseCaseidReceiptReceiptidShipmentsGet($caseid, $receiptid, $index, $limit): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataShipment
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.

try {
    $result = $apiInstance->receiptDataCaseCaseidReceiptReceiptidShipmentsGet($caseid, $receiptid, $index, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidReceiptReceiptidShipmentsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataShipment**](../Model/SimplifySoftPecuniariusDataNetReceiptDataShipment.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCaseCaseidShipmentsGet()`

```php
receiptDataCaseCaseidShipmentsGet($caseid, $index, $limit): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataShipment
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$caseid = 56; // int | unavailable
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.

try {
    $result = $apiInstance->receiptDataCaseCaseidShipmentsGet($caseid, $index, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCaseCaseidShipmentsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caseid** | **int**| unavailable |
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataShipment**](../Model/SimplifySoftPecuniariusDataNetReceiptDataShipment.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataCasePost()`

```php
receiptDataCasePost($body): object
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \stdClass; // object

try {
    $result = $apiInstance->receiptDataCasePost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataReceiptsApi->receiptDataCasePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **object**|  | [optional]

### Return type

**object**

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
