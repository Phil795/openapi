# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1FilesApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**filesGet()**](SimplifySoftPecuniariusServerAPIv1FilesApi.md#filesGet) | **GET** /files | Returns a list of files (no content).
[**filesIdClonePost()**](SimplifySoftPecuniariusServerAPIv1FilesApi.md#filesIdClonePost) | **POST** /files/{id}/clone | Clones provided file id. Only the new ID will be returned.
[**filesIdDelete()**](SimplifySoftPecuniariusServerAPIv1FilesApi.md#filesIdDelete) | **DELETE** /files/{id} | Allows to remove a file entirely.
[**filesIdGet()**](SimplifySoftPecuniariusServerAPIv1FilesApi.md#filesIdGet) | **GET** /files/{id} | Returns a single, full file.
[**filesIdPut()**](SimplifySoftPecuniariusServerAPIv1FilesApi.md#filesIdPut) | **PUT** /files/{id} | Provides an interface to update a file.
[**filesPost()**](SimplifySoftPecuniariusServerAPIv1FilesApi.md#filesPost) | **POST** /files | Allows to create new files.


## `filesGet()`

```php
filesGet($index, $limit, $mime, $item_root, $case, $receipt, $title, $size_gt, $size_lt, $file): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetFileInfo
```

Returns a list of files (no content).

Returns a list of files (no content).

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1FilesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | unavailable
$limit = 100; // int | unavailable
$mime = array('mime_example'); // string[] | Allows to limit the query to certain mime types.
$item_root = array(56); // int[] | Allows to limit the query to certain linked ItemRoot.Id's.
$case = array(56); // int[] | Allows to limit the query to certain linked Case.Id's.
$receipt = array(56); // int[] | Allows to limit the query to certain linked Receipt.Id's.
$title = array('title_example'); // string[] | Looks up if any title matches the provided strings using DBLike.
$size_gt = 56; // int | Looks up files with a filesize (in bytes) greater then or equal provided.
$size_lt = 56; // int | Looks up files with a filesize (in bytes) less then or equal provided.
$file = array(56); // int[] | List of File-IDs to get.

try {
    $result = $apiInstance->filesGet($index, $limit, $mime, $item_root, $case, $receipt, $title, $size_gt, $size_lt, $file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1FilesApi->filesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| unavailable | [optional] [default to 0]
 **limit** | **int**| unavailable | [optional] [default to 100]
 **mime** | [**string[]**](../Model/string.md)| Allows to limit the query to certain mime types. | [optional]
 **item_root** | [**int[]**](../Model/int.md)| Allows to limit the query to certain linked ItemRoot.Id&#39;s. | [optional]
 **case** | [**int[]**](../Model/int.md)| Allows to limit the query to certain linked Case.Id&#39;s. | [optional]
 **receipt** | [**int[]**](../Model/int.md)| Allows to limit the query to certain linked Receipt.Id&#39;s. | [optional]
 **title** | [**string[]**](../Model/string.md)| Looks up if any title matches the provided strings using DBLike. | [optional]
 **size_gt** | **int**| Looks up files with a filesize (in bytes) greater then or equal provided. | [optional]
 **size_lt** | **int**| Looks up files with a filesize (in bytes) less then or equal provided. | [optional]
 **file** | [**int[]**](../Model/int.md)| List of File-IDs to get. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetFileInfo**](../Model/SimplifySoftPecuniariusDataNetFileInfo.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `filesIdClonePost()`

```php
filesIdClonePost($id)
```

Clones provided file id. Only the new ID will be returned.

Clones provided file id. Only the new ID will be returned.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1FilesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->filesIdClonePost($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1FilesApi->filesIdClonePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `filesIdDelete()`

```php
filesIdDelete($id)
```

Allows to remove a file entirely.

Allows to remove a file entirely.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1FilesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->filesIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1FilesApi->filesIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `filesIdGet()`

```php
filesIdGet($id, $preview_only)
```

Returns a single, full file.

Returns a single, full file.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1FilesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$preview_only = false; // bool | Will only receive the preview of the file, not the actual file.

try {
    $apiInstance->filesIdGet($id, $preview_only);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1FilesApi->filesIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **preview_only** | **bool**| Will only receive the preview of the file, not the actual file. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `filesIdPut()`

```php
filesIdPut($id, $unknown_base_type)
```

Provides an interface to update a file.

Provides an interface to update a file.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1FilesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->filesIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1FilesApi->filesIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `filesPost()`

```php
filesPost($simplify_soft_pecuniarius_data_net_file): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetFile
```

Allows to create new files.

Allows to create new files.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1FilesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_file = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetFile(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetFile

try {
    $result = $apiInstance->filesPost($simplify_soft_pecuniarius_data_net_file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1FilesApi->filesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_file** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetFile**](../Model/SimplifySoftPecuniariusDataNetFile.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetFile**](../Model/SimplifySoftPecuniariusDataNetFile.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
