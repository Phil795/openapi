# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**receiptDataInvoiceAllCountGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi.md#receiptDataInvoiceAllCountGet) | **GET** /receipt-data/invoice/all/count | unavailable
[**receiptDataInvoiceAllGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi.md#receiptDataInvoiceAllGet) | **GET** /receipt-data/invoice/all | unavailable
[**receiptDataInvoiceInvoiceidDelete()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi.md#receiptDataInvoiceInvoiceidDelete) | **DELETE** /receipt-data/invoice/{invoiceid} | unavailable
[**receiptDataInvoiceInvoiceidEntryGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi.md#receiptDataInvoiceInvoiceidEntryGet) | **GET** /receipt-data/invoice/{invoiceid}/entry | unavailable
[**receiptDataInvoiceInvoiceidGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi.md#receiptDataInvoiceInvoiceidGet) | **GET** /receipt-data/invoice/{invoiceid} | unavailable
[**receiptDataInvoiceInvoiceidPositionPosidLinkPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi.md#receiptDataInvoiceInvoiceidPositionPosidLinkPost) | **POST** /receipt-data/invoice/{invoiceid}/position/{posid}/link | unavailable
[**receiptDataInvoiceInvoiceidPositionPosidUnlinkPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi.md#receiptDataInvoiceInvoiceidPositionPosidUnlinkPost) | **POST** /receipt-data/invoice/{invoiceid}/position/{posid}/unlink | unavailable
[**receiptDataInvoiceInvoiceidPut()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi.md#receiptDataInvoiceInvoiceidPut) | **PUT** /receipt-data/invoice/{invoiceid} | unavailable
[**receiptDataInvoiceInvoiceidReceiptReceiptidLinkPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi.md#receiptDataInvoiceInvoiceidReceiptReceiptidLinkPost) | **POST** /receipt-data/invoice/{invoiceid}/receipt/{receiptid}/link | unavailable
[**receiptDataInvoiceInvoiceidReceiptReceiptidUnlinkPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi.md#receiptDataInvoiceInvoiceidReceiptReceiptidUnlinkPost) | **POST** /receipt-data/invoice/{invoiceid}/receipt/{receiptid}/unlink | unavailable
[**receiptDataInvoicePost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi.md#receiptDataInvoicePost) | **POST** /receipt-data/invoice | unavailable


## `receiptDataInvoiceAllCountGet()`

```php
receiptDataInvoiceAllCountGet($index, $limit, $case_id, $receipt_id, $position_id, $goal_id, $chain_using_or, $include_address, $include_entries, $include_entries_tax, $include_entries_options, $include_entries_itemframe, $include_entries_itemframe_parent, $include_entries_itemframe_parent_tax, $include_entries_itemframe_unit, $include_entries_itemframe_defaultfile, $include_entries_deliverygoal, $include_entries_deliverygoal_address, $include_entries_deliverygoal_method, $include_receipts, $no_include)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.
$case_id = array(56); // int[] | Limits the query to those delivery goals which are inside of the provided cases.
$receipt_id = array(56); // int[] | Limits the query to those delivery goals which are inside of the provided receipts.
$position_id = array(56); // int[] | Limits the query to those delivery goals which are containing a specific receipt entry.
$goal_id = array(56); // int[] | Limits the query to those delivery goals which are provided.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.
$include_address = true; // bool | Sets wether returning the Invoice.Address is desired.
$include_entries = true; // bool | Sets wether returning the Invoice.Positions[] is desired.
$include_entries_tax = true; // bool | Sets wether returning the Invoice.Positions[] is desired.
$include_entries_options = true; // bool | Sets wether returning the Invoice.Positions[].Options is desired.
$include_entries_itemframe = true; // bool | Sets wether returning the Invoice.Positions[].ItemFrame is desired.
$include_entries_itemframe_parent = true; // bool | Sets wether returning the Invoice.Positions[].ItemFrame.Parent is desired.
$include_entries_itemframe_parent_tax = true; // bool | Sets wether returning the Invoice.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired.
$include_entries_itemframe_unit = true; // bool | Sets wether returning the Invoice.Positions[].ItemFrame.Unit is desired.
$include_entries_itemframe_defaultfile = true; // bool | Sets wether returning the Invoice.Positions[].ItemFrame.DefaultFile is desired.
$include_entries_deliverygoal = true; // bool | Sets wether returning the Invoice.Positions[].DeliveryGoals[] is desired.
$include_entries_deliverygoal_address = true; // bool | Sets wether returning the Invoice.Positions[].DeliveryGoals[].Address[] is desired.
$include_entries_deliverygoal_method = true; // bool | Sets wether returning the Invoice.Positions[].DeliveryGoals[].Method[] is desired.
$include_receipts = true; // bool | Sets wether returning the Invoice.Receipts is desired.
$no_include = false; // bool | Sets that no inclusion of anything is wanted.

try {
    $apiInstance->receiptDataInvoiceAllCountGet($index, $limit, $case_id, $receipt_id, $position_id, $goal_id, $chain_using_or, $include_address, $include_entries, $include_entries_tax, $include_entries_options, $include_entries_itemframe, $include_entries_itemframe_parent, $include_entries_itemframe_parent_tax, $include_entries_itemframe_unit, $include_entries_itemframe_defaultfile, $include_entries_deliverygoal, $include_entries_deliverygoal_address, $include_entries_deliverygoal_method, $include_receipts, $no_include);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi->receiptDataInvoiceAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **case_id** | [**int[]**](../Model/int.md)| Limits the query to those delivery goals which are inside of the provided cases. | [optional]
 **receipt_id** | [**int[]**](../Model/int.md)| Limits the query to those delivery goals which are inside of the provided receipts. | [optional]
 **position_id** | [**int[]**](../Model/int.md)| Limits the query to those delivery goals which are containing a specific receipt entry. | [optional]
 **goal_id** | [**int[]**](../Model/int.md)| Limits the query to those delivery goals which are provided. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]
 **include_address** | **bool**| Sets wether returning the Invoice.Address is desired. | [optional] [default to true]
 **include_entries** | **bool**| Sets wether returning the Invoice.Positions[] is desired. | [optional] [default to true]
 **include_entries_tax** | **bool**| Sets wether returning the Invoice.Positions[] is desired. | [optional] [default to true]
 **include_entries_options** | **bool**| Sets wether returning the Invoice.Positions[].Options is desired. | [optional] [default to true]
 **include_entries_itemframe** | **bool**| Sets wether returning the Invoice.Positions[].ItemFrame is desired. | [optional] [default to true]
 **include_entries_itemframe_parent** | **bool**| Sets wether returning the Invoice.Positions[].ItemFrame.Parent is desired. | [optional] [default to true]
 **include_entries_itemframe_parent_tax** | **bool**| Sets wether returning the Invoice.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired. | [optional] [default to true]
 **include_entries_itemframe_unit** | **bool**| Sets wether returning the Invoice.Positions[].ItemFrame.Unit is desired. | [optional] [default to true]
 **include_entries_itemframe_defaultfile** | **bool**| Sets wether returning the Invoice.Positions[].ItemFrame.DefaultFile is desired. | [optional] [default to true]
 **include_entries_deliverygoal** | **bool**| Sets wether returning the Invoice.Positions[].DeliveryGoals[] is desired. | [optional] [default to true]
 **include_entries_deliverygoal_address** | **bool**| Sets wether returning the Invoice.Positions[].DeliveryGoals[].Address[] is desired. | [optional] [default to true]
 **include_entries_deliverygoal_method** | **bool**| Sets wether returning the Invoice.Positions[].DeliveryGoals[].Method[] is desired. | [optional] [default to true]
 **include_receipts** | **bool**| Sets wether returning the Invoice.Receipts is desired. | [optional] [default to true]
 **no_include** | **bool**| Sets that no inclusion of anything is wanted. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataInvoiceAllGet()`

```php
receiptDataInvoiceAllGet($index, $limit, $case_id, $receipt_id, $position_id, $goal_id, $chain_using_or, $include_address, $include_entries, $include_entries_tax, $include_entries_options, $include_entries_itemframe, $include_entries_itemframe_parent, $include_entries_itemframe_parent_tax, $include_entries_itemframe_unit, $include_entries_itemframe_defaultfile, $include_entries_deliverygoal, $include_entries_deliverygoal_address, $include_entries_deliverygoal_method, $include_receipts, $no_include): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataInvoice
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.
$case_id = array(56); // int[] | Limits the query to those delivery goals which are inside of the provided cases.
$receipt_id = array(56); // int[] | Limits the query to those delivery goals which are inside of the provided receipts.
$position_id = array(56); // int[] | Limits the query to those delivery goals which are containing a specific receipt entry.
$goal_id = array(56); // int[] | Limits the query to those delivery goals which are provided.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.
$include_address = true; // bool | Sets wether returning the Invoice.Address is desired.
$include_entries = true; // bool | Sets wether returning the Invoice.Positions[] is desired.
$include_entries_tax = true; // bool | Sets wether returning the Invoice.Positions[] is desired.
$include_entries_options = true; // bool | Sets wether returning the Invoice.Positions[].Options is desired.
$include_entries_itemframe = true; // bool | Sets wether returning the Invoice.Positions[].ItemFrame is desired.
$include_entries_itemframe_parent = true; // bool | Sets wether returning the Invoice.Positions[].ItemFrame.Parent is desired.
$include_entries_itemframe_parent_tax = true; // bool | Sets wether returning the Invoice.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired.
$include_entries_itemframe_unit = true; // bool | Sets wether returning the Invoice.Positions[].ItemFrame.Unit is desired.
$include_entries_itemframe_defaultfile = true; // bool | Sets wether returning the Invoice.Positions[].ItemFrame.DefaultFile is desired.
$include_entries_deliverygoal = true; // bool | Sets wether returning the Invoice.Positions[].DeliveryGoals[] is desired.
$include_entries_deliverygoal_address = true; // bool | Sets wether returning the Invoice.Positions[].DeliveryGoals[].Address is desired.
$include_entries_deliverygoal_method = true; // bool | Sets wether returning the Invoice.Positions[].DeliveryGoals[].Method is desired.
$include_receipts = true; // bool | Sets wether returning the Invoice.Receipts is desired.
$no_include = false; // bool | Sets that no inclusion of anything is wanted.

try {
    $result = $apiInstance->receiptDataInvoiceAllGet($index, $limit, $case_id, $receipt_id, $position_id, $goal_id, $chain_using_or, $include_address, $include_entries, $include_entries_tax, $include_entries_options, $include_entries_itemframe, $include_entries_itemframe_parent, $include_entries_itemframe_parent_tax, $include_entries_itemframe_unit, $include_entries_itemframe_defaultfile, $include_entries_deliverygoal, $include_entries_deliverygoal_address, $include_entries_deliverygoal_method, $include_receipts, $no_include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi->receiptDataInvoiceAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **case_id** | [**int[]**](../Model/int.md)| Limits the query to those delivery goals which are inside of the provided cases. | [optional]
 **receipt_id** | [**int[]**](../Model/int.md)| Limits the query to those delivery goals which are inside of the provided receipts. | [optional]
 **position_id** | [**int[]**](../Model/int.md)| Limits the query to those delivery goals which are containing a specific receipt entry. | [optional]
 **goal_id** | [**int[]**](../Model/int.md)| Limits the query to those delivery goals which are provided. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]
 **include_address** | **bool**| Sets wether returning the Invoice.Address is desired. | [optional] [default to true]
 **include_entries** | **bool**| Sets wether returning the Invoice.Positions[] is desired. | [optional] [default to true]
 **include_entries_tax** | **bool**| Sets wether returning the Invoice.Positions[] is desired. | [optional] [default to true]
 **include_entries_options** | **bool**| Sets wether returning the Invoice.Positions[].Options is desired. | [optional] [default to true]
 **include_entries_itemframe** | **bool**| Sets wether returning the Invoice.Positions[].ItemFrame is desired. | [optional] [default to true]
 **include_entries_itemframe_parent** | **bool**| Sets wether returning the Invoice.Positions[].ItemFrame.Parent is desired. | [optional] [default to true]
 **include_entries_itemframe_parent_tax** | **bool**| Sets wether returning the Invoice.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired. | [optional] [default to true]
 **include_entries_itemframe_unit** | **bool**| Sets wether returning the Invoice.Positions[].ItemFrame.Unit is desired. | [optional] [default to true]
 **include_entries_itemframe_defaultfile** | **bool**| Sets wether returning the Invoice.Positions[].ItemFrame.DefaultFile is desired. | [optional] [default to true]
 **include_entries_deliverygoal** | **bool**| Sets wether returning the Invoice.Positions[].DeliveryGoals[] is desired. | [optional] [default to true]
 **include_entries_deliverygoal_address** | **bool**| Sets wether returning the Invoice.Positions[].DeliveryGoals[].Address is desired. | [optional] [default to true]
 **include_entries_deliverygoal_method** | **bool**| Sets wether returning the Invoice.Positions[].DeliveryGoals[].Method is desired. | [optional] [default to true]
 **include_receipts** | **bool**| Sets wether returning the Invoice.Receipts is desired. | [optional] [default to true]
 **no_include** | **bool**| Sets that no inclusion of anything is wanted. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataInvoice**](../Model/SimplifySoftPecuniariusDataNetReceiptDataInvoice.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataInvoiceInvoiceidDelete()`

```php
receiptDataInvoiceInvoiceidDelete($invoiceid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoiceid = 56; // int | unavailable

try {
    $apiInstance->receiptDataInvoiceInvoiceidDelete($invoiceid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi->receiptDataInvoiceInvoiceidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataInvoiceInvoiceidEntryGet()`

```php
receiptDataInvoiceInvoiceidEntryGet($invoiceid): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoiceid = 56; // int | unavailable

try {
    $result = $apiInstance->receiptDataInvoiceInvoiceidEntryGet($invoiceid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi->receiptDataInvoiceInvoiceidEntryGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceid** | **int**| unavailable |

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32**](../Model/SimplifySoftPecuniariusDataNetPrimitivesNetInt32.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataInvoiceInvoiceidGet()`

```php
receiptDataInvoiceInvoiceidGet($invoiceid, $include_address, $include_entries, $include_entries_tax, $include_entries_options, $include_entries_itemframe, $include_entries_itemframe_parent, $include_entries_itemframe_parent_tax, $include_entries_itemframe_unit, $include_entries_itemframe_defaultfile, $include_entries_deliverygoals, $include_entries_deliverygoals_address, $include_entries_deliverygoals_method, $include_receipts, $no_include)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoiceid = 56; // int | unavailable
$include_address = true; // bool | Sets wether returning the Invoice.Address is desired.
$include_entries = true; // bool | Sets wether returning the Invoice.Positions[] is desired.
$include_entries_tax = true; // bool | Sets wether returning the Invoice.Positions[] is desired.
$include_entries_options = true; // bool | Sets wether returning the Invoice.Positions[].Options is desired.
$include_entries_itemframe = true; // bool | Sets wether returning the Invoice.Positions[].ItemFrame is desired.
$include_entries_itemframe_parent = true; // bool | Sets wether returning the Invoice.Positions[].ItemFrame.Parent is desired.
$include_entries_itemframe_parent_tax = true; // bool | Sets wether returning the Invoice.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired.
$include_entries_itemframe_unit = true; // bool | Sets wether returning the Invoice.Positions[].ItemFrame.Unit is desired.
$include_entries_itemframe_defaultfile = true; // bool | Sets wether returning the Invoice.Positions[].ItemFrame.DefaultFile is desired.
$include_entries_deliverygoals = true; // bool | Sets wether returning the Invoice.Positions[].DeliveryGoals[] is desired.
$include_entries_deliverygoals_address = true; // bool | Sets wether returning the Invoice.Positions[].DeliveryGoals[].Address is desired.
$include_entries_deliverygoals_method = true; // bool | Sets wether returning the Invoice.Positions[].DeliveryGoals[].Method is desired.
$include_receipts = true; // bool | Sets wether returning the Invoice.Receipts is desired.
$no_include = false; // bool | Sets that no inclusion of anything is wanted.

try {
    $apiInstance->receiptDataInvoiceInvoiceidGet($invoiceid, $include_address, $include_entries, $include_entries_tax, $include_entries_options, $include_entries_itemframe, $include_entries_itemframe_parent, $include_entries_itemframe_parent_tax, $include_entries_itemframe_unit, $include_entries_itemframe_defaultfile, $include_entries_deliverygoals, $include_entries_deliverygoals_address, $include_entries_deliverygoals_method, $include_receipts, $no_include);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi->receiptDataInvoiceInvoiceidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceid** | **int**| unavailable |
 **include_address** | **bool**| Sets wether returning the Invoice.Address is desired. | [optional] [default to true]
 **include_entries** | **bool**| Sets wether returning the Invoice.Positions[] is desired. | [optional] [default to true]
 **include_entries_tax** | **bool**| Sets wether returning the Invoice.Positions[] is desired. | [optional] [default to true]
 **include_entries_options** | **bool**| Sets wether returning the Invoice.Positions[].Options is desired. | [optional] [default to true]
 **include_entries_itemframe** | **bool**| Sets wether returning the Invoice.Positions[].ItemFrame is desired. | [optional] [default to true]
 **include_entries_itemframe_parent** | **bool**| Sets wether returning the Invoice.Positions[].ItemFrame.Parent is desired. | [optional] [default to true]
 **include_entries_itemframe_parent_tax** | **bool**| Sets wether returning the Invoice.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired. | [optional] [default to true]
 **include_entries_itemframe_unit** | **bool**| Sets wether returning the Invoice.Positions[].ItemFrame.Unit is desired. | [optional] [default to true]
 **include_entries_itemframe_defaultfile** | **bool**| Sets wether returning the Invoice.Positions[].ItemFrame.DefaultFile is desired. | [optional] [default to true]
 **include_entries_deliverygoals** | **bool**| Sets wether returning the Invoice.Positions[].DeliveryGoals[] is desired. | [optional] [default to true]
 **include_entries_deliverygoals_address** | **bool**| Sets wether returning the Invoice.Positions[].DeliveryGoals[].Address is desired. | [optional] [default to true]
 **include_entries_deliverygoals_method** | **bool**| Sets wether returning the Invoice.Positions[].DeliveryGoals[].Method is desired. | [optional] [default to true]
 **include_receipts** | **bool**| Sets wether returning the Invoice.Receipts is desired. | [optional] [default to true]
 **no_include** | **bool**| Sets that no inclusion of anything is wanted. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataInvoiceInvoiceidPositionPosidLinkPost()`

```php
receiptDataInvoiceInvoiceidPositionPosidLinkPost($invoiceid, $posid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoiceid = 56; // int | unavailable
$posid = 56; // int | unavailable

try {
    $apiInstance->receiptDataInvoiceInvoiceidPositionPosidLinkPost($invoiceid, $posid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi->receiptDataInvoiceInvoiceidPositionPosidLinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceid** | **int**| unavailable |
 **posid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataInvoiceInvoiceidPositionPosidUnlinkPost()`

```php
receiptDataInvoiceInvoiceidPositionPosidUnlinkPost($invoiceid, $posid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoiceid = 56; // int | unavailable
$posid = 56; // int | unavailable

try {
    $apiInstance->receiptDataInvoiceInvoiceidPositionPosidUnlinkPost($invoiceid, $posid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi->receiptDataInvoiceInvoiceidPositionPosidUnlinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceid** | **int**| unavailable |
 **posid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataInvoiceInvoiceidPut()`

```php
receiptDataInvoiceInvoiceidPut($invoiceid, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoiceid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->receiptDataInvoiceInvoiceidPut($invoiceid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi->receiptDataInvoiceInvoiceidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataInvoiceInvoiceidReceiptReceiptidLinkPost()`

```php
receiptDataInvoiceInvoiceidReceiptReceiptidLinkPost($invoiceid, $receiptid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoiceid = 56; // int | unavailable
$receiptid = 56; // int | unavailable

try {
    $apiInstance->receiptDataInvoiceInvoiceidReceiptReceiptidLinkPost($invoiceid, $receiptid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi->receiptDataInvoiceInvoiceidReceiptReceiptidLinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataInvoiceInvoiceidReceiptReceiptidUnlinkPost()`

```php
receiptDataInvoiceInvoiceidReceiptReceiptidUnlinkPost($invoiceid, $receiptid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$invoiceid = 56; // int | unavailable
$receiptid = 56; // int | unavailable

try {
    $apiInstance->receiptDataInvoiceInvoiceidReceiptReceiptidUnlinkPost($invoiceid, $receiptid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi->receiptDataInvoiceInvoiceidReceiptReceiptidUnlinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invoiceid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataInvoicePost()`

```php
receiptDataInvoicePost($simplify_soft_pecuniarius_data_net_receipt_data_invoice): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataInvoice
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_receipt_data_invoice = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataInvoice(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataInvoice

try {
    $result = $apiInstance->receiptDataInvoicePost($simplify_soft_pecuniarius_data_net_receipt_data_invoice);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataInvoicesApi->receiptDataInvoicePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_receipt_data_invoice** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataInvoice**](../Model/SimplifySoftPecuniariusDataNetReceiptDataInvoice.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataInvoice**](../Model/SimplifySoftPecuniariusDataNetReceiptDataInvoice.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
