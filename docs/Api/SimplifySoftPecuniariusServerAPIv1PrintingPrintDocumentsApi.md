# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**printDocumentsDidConditionsCidDelete()**](SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi.md#printDocumentsDidConditionsCidDelete) | **DELETE** /print/documents/{did}/conditions/{cid} | unavailable
[**printDocumentsDidConditionsCidPut()**](SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi.md#printDocumentsDidConditionsCidPut) | **PUT** /print/documents/{did}/conditions/{cid} | unavailable
[**printDocumentsDidConditionsGet()**](SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi.md#printDocumentsDidConditionsGet) | **GET** /print/documents/{did}/conditions | unavailable
[**printDocumentsDidConditionsPost()**](SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi.md#printDocumentsDidConditionsPost) | **POST** /print/documents/{did}/conditions | unavailable
[**printDocumentsGet()**](SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi.md#printDocumentsGet) | **GET** /print/documents | unavailable
[**printDocumentsIdDelete()**](SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi.md#printDocumentsIdDelete) | **DELETE** /print/documents/{id} | unavailable
[**printDocumentsIdGet()**](SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi.md#printDocumentsIdGet) | **GET** /print/documents/{id} | unavailable
[**printDocumentsIdPut()**](SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi.md#printDocumentsIdPut) | **PUT** /print/documents/{id} | unavailable
[**printDocumentsPost()**](SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi.md#printDocumentsPost) | **POST** /print/documents | unavailable


## `printDocumentsDidConditionsCidDelete()`

```php
printDocumentsDidConditionsCidDelete($did, $cid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$did = 56; // int | unavailable
$cid = 56; // int | unavailable

try {
    $apiInstance->printDocumentsDidConditionsCidDelete($did, $cid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi->printDocumentsDidConditionsCidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **did** | **int**| unavailable |
 **cid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `printDocumentsDidConditionsCidPut()`

```php
printDocumentsDidConditionsCidPut($did, $cid, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$did = 56; // int | unavailable
$cid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->printDocumentsDidConditionsCidPut($did, $cid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi->printDocumentsDidConditionsCidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **did** | **int**| unavailable |
 **cid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `printDocumentsDidConditionsGet()`

```php
printDocumentsDidConditionsGet($did, $limit, $index, $stategroup): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrintingPrintCondition
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$did = 56; // int | unavailable
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$stategroup = array('stategroup_example'); // string[] | StateGroup names that the conditions should be applied on. Executed using LIKE '%value%'.

try {
    $result = $apiInstance->printDocumentsDidConditionsGet($did, $limit, $index, $stategroup);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi->printDocumentsDidConditionsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **did** | **int**| unavailable |
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **stategroup** | [**string[]**](../Model/string.md)| StateGroup names that the conditions should be applied on. Executed using LIKE &#39;%value%&#39;. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrintingPrintCondition**](../Model/SimplifySoftPecuniariusDataNetPrintingPrintCondition.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `printDocumentsDidConditionsPost()`

```php
printDocumentsDidConditionsPost($did, $simplify_soft_pecuniarius_data_net_printing_print_condition): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrintingPrintCondition
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$did = 56; // int | unavailable
$simplify_soft_pecuniarius_data_net_printing_print_condition = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrintingPrintCondition(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrintingPrintCondition

try {
    $result = $apiInstance->printDocumentsDidConditionsPost($did, $simplify_soft_pecuniarius_data_net_printing_print_condition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi->printDocumentsDidConditionsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **did** | **int**| unavailable |
 **simplify_soft_pecuniarius_data_net_printing_print_condition** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrintingPrintCondition**](../Model/SimplifySoftPecuniariusDataNetPrintingPrintCondition.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrintingPrintCondition**](../Model/SimplifySoftPecuniariusDataNetPrintingPrintCondition.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `printDocumentsGet()`

```php
printDocumentsGet($limit, $index, $title_exact, $title_starts_with, $title_like, $include_conditions, $document_type, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrintingPrintDocument
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$title_exact = array('title_exact_example'); // string[] | Checks the PrintDocument.Title field on Item using exact comparison.
$title_starts_with = array('title_starts_with_example'); // string[] | Checks the PrintDocument.Title field on Item using StartsWith. Case Invariant.
$title_like = 'title_like_example'; // string | Checks the PrintDocument.Title field on Item using the DB-Function LIKE. Case Invariant.
$include_conditions = false; // bool | If true, will include conditions.
$document_type = array('document_type_example'); // string[] | Applies a filter, checking wether PrintDocument.PrintDocumentType is contained in the provided range.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->printDocumentsGet($limit, $index, $title_exact, $title_starts_with, $title_like, $include_conditions, $document_type, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi->printDocumentsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **title_exact** | [**string[]**](../Model/string.md)| Checks the PrintDocument.Title field on Item using exact comparison. | [optional]
 **title_starts_with** | [**string[]**](../Model/string.md)| Checks the PrintDocument.Title field on Item using StartsWith. Case Invariant. | [optional]
 **title_like** | **string**| Checks the PrintDocument.Title field on Item using the DB-Function LIKE. Case Invariant. | [optional]
 **include_conditions** | **bool**| If true, will include conditions. | [optional] [default to false]
 **document_type** | [**string[]**](../Model/string.md)| Applies a filter, checking wether PrintDocument.PrintDocumentType is contained in the provided range. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrintingPrintDocument**](../Model/SimplifySoftPecuniariusDataNetPrintingPrintDocument.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `printDocumentsIdDelete()`

```php
printDocumentsIdDelete($id)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->printDocumentsIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi->printDocumentsIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `printDocumentsIdGet()`

```php
printDocumentsIdGet($id)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->printDocumentsIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi->printDocumentsIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `printDocumentsIdPut()`

```php
printDocumentsIdPut($id, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->printDocumentsIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi->printDocumentsIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `printDocumentsPost()`

```php
printDocumentsPost($simplify_soft_pecuniarius_data_net_printing_print_document): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrintingPrintDocument
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_printing_print_document = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrintingPrintDocument(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrintingPrintDocument

try {
    $result = $apiInstance->printDocumentsPost($simplify_soft_pecuniarius_data_net_printing_print_document);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApi->printDocumentsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_printing_print_document** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrintingPrintDocument**](../Model/SimplifySoftPecuniariusDataNetPrintingPrintDocument.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrintingPrintDocument**](../Model/SimplifySoftPecuniariusDataNetPrintingPrintDocument.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
