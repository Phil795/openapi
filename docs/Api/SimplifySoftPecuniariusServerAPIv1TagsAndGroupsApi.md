# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**taggingGroupAllCountGet()**](SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi.md#taggingGroupAllCountGet) | **GET** /tagging/group/all/count | Allows to poll the count of all TagGroups available.
[**taggingGroupAllGet()**](SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi.md#taggingGroupAllGet) | **GET** /tagging/group/all | Allows to poll all TagGroups available.
[**taggingGroupAllIdsGet()**](SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi.md#taggingGroupAllIdsGet) | **GET** /tagging/group/all/ids | Allows to poll the ids of all TagGroups available.
[**taggingGroupAllTagAllCountGet()**](SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi.md#taggingGroupAllTagAllCountGet) | **GET** /tagging/group/all/tag/all/count | Allows to poll the count of all Tags available.
[**taggingGroupAllTagAllGet()**](SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi.md#taggingGroupAllTagAllGet) | **GET** /tagging/group/all/tag/all | Allows to poll all Tags available.
[**taggingGroupAllTagAllIdsGet()**](SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi.md#taggingGroupAllTagAllIdsGet) | **GET** /tagging/group/all/tag/all/ids | Allows to poll the count of all Tags available.
[**taggingGroupGroupidDelete()**](SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi.md#taggingGroupGroupidDelete) | **DELETE** /tagging/group/{groupid} | Deletes a single TagGroup.
[**taggingGroupGroupidGet()**](SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi.md#taggingGroupGroupidGet) | **GET** /tagging/group/{groupid} | unavailable
[**taggingGroupGroupidPut()**](SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi.md#taggingGroupGroupidPut) | **PUT** /tagging/group/{groupid} | Updates a single TagGroup.
[**taggingGroupGroupidTagPost()**](SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi.md#taggingGroupGroupidTagPost) | **POST** /tagging/group/{groupid}/tag | Creates new Tags.
[**taggingGroupGroupidTagTagidDelete()**](SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi.md#taggingGroupGroupidTagTagidDelete) | **DELETE** /tagging/group/{groupid}/tag/{tagid} | Deletes a single Tag.
[**taggingGroupGroupidTagTagidGet()**](SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi.md#taggingGroupGroupidTagTagidGet) | **GET** /tagging/group/{groupid}/tag/{tagid} | Receives a single Tag.
[**taggingGroupGroupidTagTagidPut()**](SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi.md#taggingGroupGroupidTagTagidPut) | **PUT** /tagging/group/{groupid}/tag/{tagid} | Updates a single Tag.
[**taggingGroupPost()**](SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi.md#taggingGroupPost) | **POST** /tagging/group | Creates new TagGroups.


## `taggingGroupAllCountGet()`

```php
taggingGroupAllCountGet($index, $limit, $group_id, $in_group, $no_group, $not_group, $in_ext_auth, $no_ext_auth, $not_ext_auth, $title_exact, $title_like, $title_starts_with, $chain_using_or)
```

Allows to poll the count of all TagGroups available.

Allows to poll the count of all TagGroups available.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 1000; // int | The amount of records to take.
$group_id = array(56); // int[] | Only query those TagGroups, where the Id matches one of the provided.
$in_group = array(56); // int[] | Only query those TagGroups, where the ParentFK matches one of the provided.
$no_group = array(56); // int[] | Only query those TagGroups, not inside of any TagGroup.
$not_group = array(56); // int[] | Only query those TagGroups, where the ParentFK matches none of the provided.
$in_ext_auth = array(56); // int[] | Only query those TagGroups, linking (active) to a ExternAuthority of the provided.
$no_ext_auth = array(56); // int[] | Only query those TagGroups, not linking (active) to any ExternAuthoritys.
$not_ext_auth = array(56); // int[] | Only query those TagGroups, not linking (active) to a ExternAuthority of the provided.
$title_exact = array('title_exact_example'); // string[] | Compares the TagGroup.Title using exact search.
$title_like = array('title_like_example'); // string[] | Compares the TagGroup.Title using starts with search. Case Insensitive.
$title_starts_with = array('title_starts_with_example'); // string[] | Compares the TagGroup.Title using DbFunction LIKE. Case Insensitive.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $apiInstance->taggingGroupAllCountGet($index, $limit, $group_id, $in_group, $no_group, $not_group, $in_ext_auth, $no_ext_auth, $not_ext_auth, $title_exact, $title_like, $title_starts_with, $chain_using_or);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi->taggingGroupAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 1000]
 **group_id** | [**int[]**](../Model/int.md)| Only query those TagGroups, where the Id matches one of the provided. | [optional]
 **in_group** | [**int[]**](../Model/int.md)| Only query those TagGroups, where the ParentFK matches one of the provided. | [optional]
 **no_group** | [**int[]**](../Model/int.md)| Only query those TagGroups, not inside of any TagGroup. | [optional]
 **not_group** | [**int[]**](../Model/int.md)| Only query those TagGroups, where the ParentFK matches none of the provided. | [optional]
 **in_ext_auth** | [**int[]**](../Model/int.md)| Only query those TagGroups, linking (active) to a ExternAuthority of the provided. | [optional]
 **no_ext_auth** | [**int[]**](../Model/int.md)| Only query those TagGroups, not linking (active) to any ExternAuthoritys. | [optional]
 **not_ext_auth** | [**int[]**](../Model/int.md)| Only query those TagGroups, not linking (active) to a ExternAuthority of the provided. | [optional]
 **title_exact** | [**string[]**](../Model/string.md)| Compares the TagGroup.Title using exact search. | [optional]
 **title_like** | [**string[]**](../Model/string.md)| Compares the TagGroup.Title using starts with search. Case Insensitive. | [optional]
 **title_starts_with** | [**string[]**](../Model/string.md)| Compares the TagGroup.Title using DbFunction LIKE. Case Insensitive. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `taggingGroupAllGet()`

```php
taggingGroupAllGet($index, $limit, $group_id, $in_group, $no_group, $not_group, $in_ext_auth, $no_ext_auth, $not_ext_auth, $title_exact, $title_like, $title_starts_with, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTagGroup
```

Allows to poll all TagGroups available.

Allows to poll all TagGroups available.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 1000; // int | The amount of records to take.
$group_id = array(56); // int[] | Only query those TagGroups, where the Id matches one of the provided.
$in_group = array(56); // int[] | Only query those TagGroups, where the ParentFK matches one of the provided.
$no_group = array(56); // int[] | Only query those TagGroups, not inside of any TagGroup.
$not_group = array(56); // int[] | Only query those TagGroups, where the ParentFK matches none of the provided.
$in_ext_auth = array(56); // int[] | Only query those TagGroups, linking (active) to a ExternAuthority of the provided.
$no_ext_auth = array(56); // int[] | Only query those TagGroups, not linking (active) to any ExternAuthoritys.
$not_ext_auth = array(56); // int[] | Only query those TagGroups, not linking (active) to a ExternAuthority of the provided.
$title_exact = array('title_exact_example'); // string[] | Compares the TagGroup.Title using exact search.
$title_like = array('title_like_example'); // string[] | Compares the TagGroup.Title using starts with search. Case Insensitive.
$title_starts_with = array('title_starts_with_example'); // string[] | Compares the TagGroup.Title using DbFunction LIKE. Case Insensitive.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->taggingGroupAllGet($index, $limit, $group_id, $in_group, $no_group, $not_group, $in_ext_auth, $no_ext_auth, $not_ext_auth, $title_exact, $title_like, $title_starts_with, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi->taggingGroupAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 1000]
 **group_id** | [**int[]**](../Model/int.md)| Only query those TagGroups, where the Id matches one of the provided. | [optional]
 **in_group** | [**int[]**](../Model/int.md)| Only query those TagGroups, where the ParentFK matches one of the provided. | [optional]
 **no_group** | [**int[]**](../Model/int.md)| Only query those TagGroups, not inside of any TagGroup. | [optional]
 **not_group** | [**int[]**](../Model/int.md)| Only query those TagGroups, where the ParentFK matches none of the provided. | [optional]
 **in_ext_auth** | [**int[]**](../Model/int.md)| Only query those TagGroups, linking (active) to a ExternAuthority of the provided. | [optional]
 **no_ext_auth** | [**int[]**](../Model/int.md)| Only query those TagGroups, not linking (active) to any ExternAuthoritys. | [optional]
 **not_ext_auth** | [**int[]**](../Model/int.md)| Only query those TagGroups, not linking (active) to a ExternAuthority of the provided. | [optional]
 **title_exact** | [**string[]**](../Model/string.md)| Compares the TagGroup.Title using exact search. | [optional]
 **title_like** | [**string[]**](../Model/string.md)| Compares the TagGroup.Title using starts with search. Case Insensitive. | [optional]
 **title_starts_with** | [**string[]**](../Model/string.md)| Compares the TagGroup.Title using DbFunction LIKE. Case Insensitive. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTagGroup**](../Model/SimplifySoftPecuniariusDataNetTagGroup.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `taggingGroupAllIdsGet()`

```php
taggingGroupAllIdsGet($index, $limit, $group_id, $in_group, $no_group, $not_group, $in_ext_auth, $no_ext_auth, $not_ext_auth, $title_exact, $title_like, $title_starts_with, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32
```

Allows to poll the ids of all TagGroups available.

Allows to poll the ids of all TagGroups available.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 1000; // int | The amount of records to take.
$group_id = array(56); // int[] | Only query those TagGroups, where the Id matches one of the provided.
$in_group = array(56); // int[] | Only query those TagGroups, where the ParentFK matches one of the provided.
$no_group = array(56); // int[] | Only query those TagGroups, not inside of any TagGroup.
$not_group = array(56); // int[] | Only query those TagGroups, where the ParentFK matches none of the provided.
$in_ext_auth = array(56); // int[] | Only query those TagGroups, linking (active) to a ExternAuthority of the provided.
$no_ext_auth = array(56); // int[] | Only query those TagGroups, not linking (active) to any ExternAuthoritys.
$not_ext_auth = array(56); // int[] | Only query those TagGroups, not linking (active) to a ExternAuthority of the provided.
$title_exact = array('title_exact_example'); // string[] | Compares the TagGroup.Title using exact search.
$title_like = array('title_like_example'); // string[] | Compares the TagGroup.Title using starts with search. Case Insensitive.
$title_starts_with = array('title_starts_with_example'); // string[] | Compares the TagGroup.Title using DbFunction LIKE. Case Insensitive.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->taggingGroupAllIdsGet($index, $limit, $group_id, $in_group, $no_group, $not_group, $in_ext_auth, $no_ext_auth, $not_ext_auth, $title_exact, $title_like, $title_starts_with, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi->taggingGroupAllIdsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 1000]
 **group_id** | [**int[]**](../Model/int.md)| Only query those TagGroups, where the Id matches one of the provided. | [optional]
 **in_group** | [**int[]**](../Model/int.md)| Only query those TagGroups, where the ParentFK matches one of the provided. | [optional]
 **no_group** | [**int[]**](../Model/int.md)| Only query those TagGroups, not inside of any TagGroup. | [optional]
 **not_group** | [**int[]**](../Model/int.md)| Only query those TagGroups, where the ParentFK matches none of the provided. | [optional]
 **in_ext_auth** | [**int[]**](../Model/int.md)| Only query those TagGroups, linking (active) to a ExternAuthority of the provided. | [optional]
 **no_ext_auth** | [**int[]**](../Model/int.md)| Only query those TagGroups, not linking (active) to any ExternAuthoritys. | [optional]
 **not_ext_auth** | [**int[]**](../Model/int.md)| Only query those TagGroups, not linking (active) to a ExternAuthority of the provided. | [optional]
 **title_exact** | [**string[]**](../Model/string.md)| Compares the TagGroup.Title using exact search. | [optional]
 **title_like** | [**string[]**](../Model/string.md)| Compares the TagGroup.Title using starts with search. Case Insensitive. | [optional]
 **title_starts_with** | [**string[]**](../Model/string.md)| Compares the TagGroup.Title using DbFunction LIKE. Case Insensitive. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32**](../Model/SimplifySoftPecuniariusDataNetPrimitivesNetInt32.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `taggingGroupAllTagAllCountGet()`

```php
taggingGroupAllTagAllCountGet($index, $limit, $not_tag_id, $tag_id, $in_group, $no_group, $not_group, $in_ext_auth, $no_ext_auth, $not_ext_auth, $title_exact, $title_like, $title_starts_with, $chain_using_or)
```

Allows to poll the count of all Tags available.

Allows to poll the count of all Tags available.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 1000; // int | The amount of records to take.
$not_tag_id = array(56); // int[] | Only query those Tags, where the Id matches none of the provided.
$tag_id = array(56); // int[] | Only query those Tags, where the Id matches one of the provided.
$in_group = array(56); // int[] | Only query those Tags, where the ParentFK matches one of the provided.
$no_group = array(56); // int[] | Only query those Tags, not inside of any TagGroup.
$not_group = array(56); // int[] | Only query those Tags, where the ParentFK matches none of the provided.
$in_ext_auth = array(56); // int[] | Only query those Tags, linking (active) to a ExternAuthority of the provided.
$no_ext_auth = array(56); // int[] | Only query those Tags, not linking (active) to any ExternAuthoritys.
$not_ext_auth = array(56); // int[] | Only query those Tags, not linking (active) to a ExternAuthority of the provided.
$title_exact = array('title_exact_example'); // string[] | Compares the Tag.Title using exact search.
$title_like = array('title_like_example'); // string[] | Compares the Tag.Title using starts with search. Case Insensitive.
$title_starts_with = array('title_starts_with_example'); // string[] | Compares the Tag.Title using DbFunction LIKE. Case Insensitive.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $apiInstance->taggingGroupAllTagAllCountGet($index, $limit, $not_tag_id, $tag_id, $in_group, $no_group, $not_group, $in_ext_auth, $no_ext_auth, $not_ext_auth, $title_exact, $title_like, $title_starts_with, $chain_using_or);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi->taggingGroupAllTagAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 1000]
 **not_tag_id** | [**int[]**](../Model/int.md)| Only query those Tags, where the Id matches none of the provided. | [optional]
 **tag_id** | [**int[]**](../Model/int.md)| Only query those Tags, where the Id matches one of the provided. | [optional]
 **in_group** | [**int[]**](../Model/int.md)| Only query those Tags, where the ParentFK matches one of the provided. | [optional]
 **no_group** | [**int[]**](../Model/int.md)| Only query those Tags, not inside of any TagGroup. | [optional]
 **not_group** | [**int[]**](../Model/int.md)| Only query those Tags, where the ParentFK matches none of the provided. | [optional]
 **in_ext_auth** | [**int[]**](../Model/int.md)| Only query those Tags, linking (active) to a ExternAuthority of the provided. | [optional]
 **no_ext_auth** | [**int[]**](../Model/int.md)| Only query those Tags, not linking (active) to any ExternAuthoritys. | [optional]
 **not_ext_auth** | [**int[]**](../Model/int.md)| Only query those Tags, not linking (active) to a ExternAuthority of the provided. | [optional]
 **title_exact** | [**string[]**](../Model/string.md)| Compares the Tag.Title using exact search. | [optional]
 **title_like** | [**string[]**](../Model/string.md)| Compares the Tag.Title using starts with search. Case Insensitive. | [optional]
 **title_starts_with** | [**string[]**](../Model/string.md)| Compares the Tag.Title using DbFunction LIKE. Case Insensitive. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `taggingGroupAllTagAllGet()`

```php
taggingGroupAllTagAllGet($index, $limit, $not_tag_id, $tag_id, $in_group, $no_group, $not_group, $in_ext_auth, $no_ext_auth, $not_ext_auth, $title_exact, $title_like, $title_starts_with, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTag
```

Allows to poll all Tags available.

Allows to poll all Tags available.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 1000; // int | The amount of records to take.
$not_tag_id = array(56); // int[] | Only query those Tags, where the Id matches none of the provided.
$tag_id = array(56); // int[] | Only query those Tags, where the Id matches one of the provided.
$in_group = array(56); // int[] | Only query those Tags, where the ParentFK matches one of the provided.
$no_group = array(56); // int[] | Only query those Tags, not inside of any TagGroup.
$not_group = array(56); // int[] | Only query those Tags, where the ParentFK matches none of the provided.
$in_ext_auth = array(56); // int[] | Only query those Tags, linking (active) to a ExternAuthority of the provided.
$no_ext_auth = array(56); // int[] | Only query those Tags, not linking (active) to any ExternAuthoritys.
$not_ext_auth = array(56); // int[] | Only query those Tags, not linking (active) to a ExternAuthority of the provided.
$title_exact = array('title_exact_example'); // string[] | Compares the Tag.Title using exact search.
$title_like = array('title_like_example'); // string[] | Compares the Tag.Title using starts with search. Case Insensitive.
$title_starts_with = array('title_starts_with_example'); // string[] | Compares the Tag.Title using DbFunction LIKE. Case Insensitive.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->taggingGroupAllTagAllGet($index, $limit, $not_tag_id, $tag_id, $in_group, $no_group, $not_group, $in_ext_auth, $no_ext_auth, $not_ext_auth, $title_exact, $title_like, $title_starts_with, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi->taggingGroupAllTagAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 1000]
 **not_tag_id** | [**int[]**](../Model/int.md)| Only query those Tags, where the Id matches none of the provided. | [optional]
 **tag_id** | [**int[]**](../Model/int.md)| Only query those Tags, where the Id matches one of the provided. | [optional]
 **in_group** | [**int[]**](../Model/int.md)| Only query those Tags, where the ParentFK matches one of the provided. | [optional]
 **no_group** | [**int[]**](../Model/int.md)| Only query those Tags, not inside of any TagGroup. | [optional]
 **not_group** | [**int[]**](../Model/int.md)| Only query those Tags, where the ParentFK matches none of the provided. | [optional]
 **in_ext_auth** | [**int[]**](../Model/int.md)| Only query those Tags, linking (active) to a ExternAuthority of the provided. | [optional]
 **no_ext_auth** | [**int[]**](../Model/int.md)| Only query those Tags, not linking (active) to any ExternAuthoritys. | [optional]
 **not_ext_auth** | [**int[]**](../Model/int.md)| Only query those Tags, not linking (active) to a ExternAuthority of the provided. | [optional]
 **title_exact** | [**string[]**](../Model/string.md)| Compares the Tag.Title using exact search. | [optional]
 **title_like** | [**string[]**](../Model/string.md)| Compares the Tag.Title using starts with search. Case Insensitive. | [optional]
 **title_starts_with** | [**string[]**](../Model/string.md)| Compares the Tag.Title using DbFunction LIKE. Case Insensitive. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTag**](../Model/SimplifySoftPecuniariusDataNetTag.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `taggingGroupAllTagAllIdsGet()`

```php
taggingGroupAllTagAllIdsGet($index, $limit, $not_tag_id, $tag_id, $in_group, $no_group, $not_group, $in_ext_auth, $no_ext_auth, $not_ext_auth, $title_exact, $title_like, $title_starts_with, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32
```

Allows to poll the count of all Tags available.

Allows to poll the count of all Tags available.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 1000; // int | The amount of records to take.
$not_tag_id = array(56); // int[] | Only query those Tags, where the Id matches none of the provided.
$tag_id = array(56); // int[] | Only query those Tags, where the Id matches one of the provided.
$in_group = array(56); // int[] | Only query those Tags, where the ParentFK matches one of the provided.
$no_group = array(56); // int[] | Only query those Tags, not inside of any TagGroup.
$not_group = array(56); // int[] | Only query those Tags, where the ParentFK matches none of the provided.
$in_ext_auth = array(56); // int[] | Only query those Tags, linking (active) to a ExternAuthority of the provided.
$no_ext_auth = array(56); // int[] | Only query those Tags, not linking (active) to any ExternAuthoritys.
$not_ext_auth = array(56); // int[] | Only query those Tags, not linking (active) to a ExternAuthority of the provided.
$title_exact = array('title_exact_example'); // string[] | Compares the Tag.Title using exact search.
$title_like = array('title_like_example'); // string[] | Compares the Tag.Title using starts with search. Case Insensitive.
$title_starts_with = array('title_starts_with_example'); // string[] | Compares the Tag.Title using DbFunction LIKE. Case Insensitive.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->taggingGroupAllTagAllIdsGet($index, $limit, $not_tag_id, $tag_id, $in_group, $no_group, $not_group, $in_ext_auth, $no_ext_auth, $not_ext_auth, $title_exact, $title_like, $title_starts_with, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi->taggingGroupAllTagAllIdsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 1000]
 **not_tag_id** | [**int[]**](../Model/int.md)| Only query those Tags, where the Id matches none of the provided. | [optional]
 **tag_id** | [**int[]**](../Model/int.md)| Only query those Tags, where the Id matches one of the provided. | [optional]
 **in_group** | [**int[]**](../Model/int.md)| Only query those Tags, where the ParentFK matches one of the provided. | [optional]
 **no_group** | [**int[]**](../Model/int.md)| Only query those Tags, not inside of any TagGroup. | [optional]
 **not_group** | [**int[]**](../Model/int.md)| Only query those Tags, where the ParentFK matches none of the provided. | [optional]
 **in_ext_auth** | [**int[]**](../Model/int.md)| Only query those Tags, linking (active) to a ExternAuthority of the provided. | [optional]
 **no_ext_auth** | [**int[]**](../Model/int.md)| Only query those Tags, not linking (active) to any ExternAuthoritys. | [optional]
 **not_ext_auth** | [**int[]**](../Model/int.md)| Only query those Tags, not linking (active) to a ExternAuthority of the provided. | [optional]
 **title_exact** | [**string[]**](../Model/string.md)| Compares the Tag.Title using exact search. | [optional]
 **title_like** | [**string[]**](../Model/string.md)| Compares the Tag.Title using starts with search. Case Insensitive. | [optional]
 **title_starts_with** | [**string[]**](../Model/string.md)| Compares the Tag.Title using DbFunction LIKE. Case Insensitive. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32**](../Model/SimplifySoftPecuniariusDataNetPrimitivesNetInt32.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `taggingGroupGroupidDelete()`

```php
taggingGroupGroupidDelete($groupid)
```

Deletes a single TagGroup.

Deletes a single TagGroup.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$groupid = 56; // int | unavailable

try {
    $apiInstance->taggingGroupGroupidDelete($groupid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi->taggingGroupGroupidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `taggingGroupGroupidGet()`

```php
taggingGroupGroupidGet($groupid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$groupid = 56; // int | unavailable

try {
    $apiInstance->taggingGroupGroupidGet($groupid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi->taggingGroupGroupidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `taggingGroupGroupidPut()`

```php
taggingGroupGroupidPut($groupid, $unknown_base_type)
```

Updates a single TagGroup.

Updates a single TagGroup.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$groupid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->taggingGroupGroupidPut($groupid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi->taggingGroupGroupidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `taggingGroupGroupidTagPost()`

```php
taggingGroupGroupidTagPost($groupid, $simplify_soft_pecuniarius_data_net_tag): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTag
```

Creates new Tags.

Creates new Tags. Provide '0' for {groupid} if the Tags getting created have no TagGroup.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$groupid = 56; // int | unavailable
$simplify_soft_pecuniarius_data_net_tag = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTag(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTag

try {
    $result = $apiInstance->taggingGroupGroupidTagPost($groupid, $simplify_soft_pecuniarius_data_net_tag);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi->taggingGroupGroupidTagPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupid** | **int**| unavailable |
 **simplify_soft_pecuniarius_data_net_tag** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTag**](../Model/SimplifySoftPecuniariusDataNetTag.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTag**](../Model/SimplifySoftPecuniariusDataNetTag.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `taggingGroupGroupidTagTagidDelete()`

```php
taggingGroupGroupidTagTagidDelete($groupid, $tagid)
```

Deletes a single Tag.

Deletes a single Tag. Provide '0' for {groupid} if the Tag desired either has no TagGroup or the groupid is not known.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$groupid = 56; // int | unavailable
$tagid = 56; // int | unavailable

try {
    $apiInstance->taggingGroupGroupidTagTagidDelete($groupid, $tagid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi->taggingGroupGroupidTagTagidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupid** | **int**| unavailable |
 **tagid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `taggingGroupGroupidTagTagidGet()`

```php
taggingGroupGroupidTagTagidGet($groupid, $tagid)
```

Receives a single Tag.

Receives a single Tag. Provide '0' for {groupid} if the Tag desired either has no group or the groupid is not known.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$groupid = 56; // int | unavailable
$tagid = 56; // int | unavailable

try {
    $apiInstance->taggingGroupGroupidTagTagidGet($groupid, $tagid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi->taggingGroupGroupidTagTagidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupid** | **int**| unavailable |
 **tagid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `taggingGroupGroupidTagTagidPut()`

```php
taggingGroupGroupidTagTagidPut($groupid, $tagid, $unknown_base_type)
```

Updates a single Tag.

Updates a single Tag. Provide '0' for {groupid} if the Tag desired either has no TagGroup or the groupid is not known.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$groupid = 56; // int | unavailable
$tagid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->taggingGroupGroupidTagTagidPut($groupid, $tagid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi->taggingGroupGroupidTagTagidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupid** | **int**| unavailable |
 **tagid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `taggingGroupPost()`

```php
taggingGroupPost($simplify_soft_pecuniarius_data_net_tag_group): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTagGroup
```

Creates new TagGroups.

Creates new TagGroups.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_tag_group = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTagGroup(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTagGroup

try {
    $result = $apiInstance->taggingGroupPost($simplify_soft_pecuniarius_data_net_tag_group);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1TagsAndGroupsApi->taggingGroupPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_tag_group** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTagGroup**](../Model/SimplifySoftPecuniariusDataNetTagGroup.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTagGroup**](../Model/SimplifySoftPecuniariusDataNetTagGroup.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
