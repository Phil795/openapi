# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**externAuthorityGet()**](SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi.md#externAuthorityGet) | **GET** /extern/authority | Receives a list of all available extern/authoritys.
[**externAuthorityIdClearlogPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi.md#externAuthorityIdClearlogPost) | **POST** /extern/authority/{id}/clearlog | Clears the log of the provided extern authority.
[**externAuthorityIdDelete()**](SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi.md#externAuthorityIdDelete) | **DELETE** /extern/authority/{id} | Deletes a extern/authority.
[**externAuthorityIdGet()**](SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi.md#externAuthorityIdGet) | **GET** /extern/authority/{id} | Receives a single extern/authority.
[**externAuthorityIdLogGet()**](SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi.md#externAuthorityIdLogGet) | **GET** /extern/authority/{id}/log | Receives a single extern/authority.
[**externAuthorityIdPausePost()**](SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi.md#externAuthorityIdPausePost) | **POST** /extern/authority/{id}/pause | Forces the Extern Authority to halt its execution until it is unpaused by calling this endpoint again. With bad timing, server m
[**externAuthorityIdPropertyDelete()**](SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi.md#externAuthorityIdPropertyDelete) | **DELETE** /extern/authority/{id}/{property} | unavailable
[**externAuthorityIdPropertyPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi.md#externAuthorityIdPropertyPost) | **POST** /extern/authority/{id}/{property} | unavailable
[**externAuthorityIdPut()**](SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi.md#externAuthorityIdPut) | **PUT** /extern/authority/{id} | Updates a extern/authority.
[**externAuthorityIdRunPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi.md#externAuthorityIdRunPost) | **POST** /extern/authority/{id}/run | Forces the Extern Authority to be executed right now. Will do nothing if Extern Authority is running right now. Does not guarant
[**externAuthorityPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi.md#externAuthorityPost) | **POST** /extern/authority | Creates one or more new extern/authoritys.


## `externAuthorityGet()`

```php
externAuthorityGet($limit, $index, $name): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternAuthority
```

Receives a list of all available extern/authoritys.

Receives a list of all available extern/authoritys.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 10; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$name = 'name_example'; // string | The name of the extern authority.

try {
    $result = $apiInstance->externAuthorityGet($limit, $index, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi->externAuthorityGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **name** | **string**| The name of the extern authority. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternAuthority**](../Model/SimplifySoftPecuniariusDataNetExternExternAuthority.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externAuthorityIdClearlogPost()`

```php
externAuthorityIdClearlogPost($id)
```

Clears the log of the provided extern authority.

Clears the log of the provided extern authority.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->externAuthorityIdClearlogPost($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi->externAuthorityIdClearlogPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externAuthorityIdDelete()`

```php
externAuthorityIdDelete($id)
```

Deletes a extern/authority.

Deletes a extern/authority.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->externAuthorityIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi->externAuthorityIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externAuthorityIdGet()`

```php
externAuthorityIdGet($id)
```

Receives a single extern/authority.

Receives a single extern/authority.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->externAuthorityIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi->externAuthorityIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externAuthorityIdLogGet()`

```php
externAuthorityIdLogGet($id, $limit, $index): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternAuthorityLogMessage
```

Receives a single extern/authority.

Receives a single extern/authority.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$limit = 10; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.

try {
    $result = $apiInstance->externAuthorityIdLogGet($id, $limit, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi->externAuthorityIdLogGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternAuthorityLogMessage**](../Model/SimplifySoftPecuniariusDataNetExternExternAuthorityLogMessage.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externAuthorityIdPausePost()`

```php
externAuthorityIdPausePost($id)
```

Forces the Extern Authority to halt its execution until it is unpaused by calling this endpoint again. With bad timing, server m

Forces the Extern Authority to halt its execution until it is unpaused by calling this endpoint again. With bad timing, server may start spinning up an instance undoing this call after the instance finished running.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->externAuthorityIdPausePost($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi->externAuthorityIdPausePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externAuthorityIdPropertyDelete()`

```php
externAuthorityIdPropertyDelete($id, $property)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$property = 'property_example'; // string | unavailable

try {
    $apiInstance->externAuthorityIdPropertyDelete($id, $property);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi->externAuthorityIdPropertyDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **property** | **string**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externAuthorityIdPropertyPost()`

```php
externAuthorityIdPropertyPost($id, $property, $string, $int32, $int64, $double, $boolean)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$property = 'property_example'; // string | unavailable
$string = 'string_example'; // string | The string value to set.
$int32 = 56; // int | The int value to set.
$int64 = 56; // int | The long value to set.
$double = 3.4; // double | The double value to set.
$boolean = True; // bool | The boolean value to set.

try {
    $apiInstance->externAuthorityIdPropertyPost($id, $property, $string, $int32, $int64, $double, $boolean);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi->externAuthorityIdPropertyPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **property** | **string**| unavailable |
 **string** | **string**| The string value to set. | [optional]
 **int32** | **int**| The int value to set. | [optional]
 **int64** | **int**| The long value to set. | [optional]
 **double** | **double**| The double value to set. | [optional]
 **boolean** | **bool**| The boolean value to set. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externAuthorityIdPut()`

```php
externAuthorityIdPut($id, $unknown_base_type)
```

Updates a extern/authority.

Updates a extern/authority.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->externAuthorityIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi->externAuthorityIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externAuthorityIdRunPost()`

```php
externAuthorityIdRunPost($id)
```

Forces the Extern Authority to be executed right now. Will do nothing if Extern Authority is running right now. Does not guarant

Forces the Extern Authority to be executed right now. Will do nothing if Extern Authority is running right now. Does not guarantees an exclusive 'run' (that is: With bad timing, server may also start spinning up an instance making two run at the same time.)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->externAuthorityIdRunPost($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi->externAuthorityIdRunPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externAuthorityPost()`

```php
externAuthorityPost($simplify_soft_pecuniarius_data_net_extern_extern_authority): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternAuthority
```

Creates one or more new extern/authoritys.

Creates one or more new extern/authoritys.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_extern_extern_authority = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternAuthority(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternAuthority

try {
    $result = $apiInstance->externAuthorityPost($simplify_soft_pecuniarius_data_net_extern_extern_authority);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternAuthoritiesApi->externAuthorityPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_extern_extern_authority** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternAuthority**](../Model/SimplifySoftPecuniariusDataNetExternExternAuthority.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternAuthority**](../Model/SimplifySoftPecuniariusDataNetExternExternAuthority.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
