# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1SQLRawApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**rawIdDelete()**](SimplifySoftPecuniariusServerAPIv1SQLRawApi.md#rawIdDelete) | **DELETE** /raw/{id} | Deletes (closes) the query with provided id.
[**rawIdGet()**](SimplifySoftPecuniariusServerAPIv1SQLRawApi.md#rawIdGet) | **GET** /raw/{id} | Allows to receive a raw sql query prepared via POST &#39;/raw&#39;.
[**rawPost()**](SimplifySoftPecuniariusServerAPIv1SQLRawApi.md#rawPost) | **POST** /raw | Creates a new raw sql query. Queries are available for maximum of 00:05:00 until they time out.


## `rawIdDelete()`

```php
rawIdDelete($id)
```

Deletes (closes) the query with provided id.

Deletes (closes) the query with provided id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1SQLRawApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string | unavailable

try {
    $apiInstance->rawIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1SQLRawApi->rawIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `rawIdGet()`

```php
rawIdGet($id, $limit)
```

Allows to receive a raw sql query prepared via POST '/raw'.

Allows to receive a raw sql query prepared via POST '/raw'.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1SQLRawApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string | unavailable
$limit = 10; // int | The amount of records to take.

try {
    $apiInstance->rawIdGet($id, $limit);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1SQLRawApi->rawIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| unavailable |
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `rawPost()`

```php
rawPost($unknown_base_type)
```

Creates a new raw sql query. Queries are available for maximum of 00:05:00 until they time out.

Creates a new raw sql query. Queries are available for maximum of 00:05:00 until they time out.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1SQLRawApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->rawPost($unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1SQLRawApi->rawPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
