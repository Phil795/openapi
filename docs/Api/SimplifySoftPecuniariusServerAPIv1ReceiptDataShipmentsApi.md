# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**receiptDataShipmentsAllCountGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi.md#receiptDataShipmentsAllCountGet) | **GET** /receipt-data/shipments/all/count | Endpoint to poll for available shipments count.
[**receiptDataShipmentsAllGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi.md#receiptDataShipmentsAllGet) | **GET** /receipt-data/shipments/all | Endpoint to poll for available shipments.
[**receiptDataShipmentsDeliveryGoalidCreatePost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi.md#receiptDataShipmentsDeliveryGoalidCreatePost) | **POST** /receipt-data/shipments/delivery/{goalid}/create | Creates a shipment including a corresponding label for the given Receipt.
[**receiptDataShipmentsDeliveryGoalidPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi.md#receiptDataShipmentsDeliveryGoalidPost) | **POST** /receipt-data/shipments/delivery/{goalid} | Allows creating shipment informations of any given case.
[**receiptDataShipmentsDeliveryGoalidPreviewPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi.md#receiptDataShipmentsDeliveryGoalidPreviewPost) | **POST** /receipt-data/shipments/delivery/{goalid}/preview | Creates a preview of the shipments to be created.
[**receiptDataShipmentsShipmentidCancelGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi.md#receiptDataShipmentsShipmentidCancelGet) | **GET** /receipt-data/shipments/{shipmentid}/cancel | Asks the server to use the corresponding DeliveryProviderContext to cancel a given shipment.
[**receiptDataShipmentsShipmentidDelete()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi.md#receiptDataShipmentsShipmentidDelete) | **DELETE** /receipt-data/shipments/{shipmentid} | Provides the ability to remove shipments from a given case.
[**receiptDataShipmentsShipmentidFilesGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi.md#receiptDataShipmentsShipmentidFilesGet) | **GET** /receipt-data/shipments/{shipmentid}/files | Endpoint to poll for available shipments.
[**receiptDataShipmentsShipmentidPut()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi.md#receiptDataShipmentsShipmentidPut) | **PUT** /receipt-data/shipments/{shipmentid} | Allows updating shipment informations of any given case.
[**receiptDataShipmentsShipmentidStatusGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi.md#receiptDataShipmentsShipmentidStatusGet) | **GET** /receipt-data/shipments/{shipmentid}/status | Asks the server to use the corresponding DeliveryProviderContext to poll for the status of a given shipment.


## `receiptDataShipmentsAllCountGet()`

```php
receiptDataShipmentsAllCountGet($index, $limit, $shipment_id, $shipment_value_equals, $shipment_value_start_with, $shipment_value_like, $method, $receipt, $case, $chain_using_or)
```

Endpoint to poll for available shipments count.

Endpoint to poll for available shipments count.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.
$shipment_id = array(56); // int[] | The IDs of the delivery method to get the shipments for.
$shipment_value_equals = array('shipment_value_equals_example'); // string[] | Limits search to the shipment label value using equals check.
$shipment_value_start_with = array('shipment_value_start_with_example'); // string[] | Limits search to the shipment label value using starts with check. Case Invariant.
$shipment_value_like = 'shipment_value_like_example'; // string | Limits search to the shipment label value using DbFunction LIKE check. Case Invariant.
$method = array(56); // int[] | The IDs of the delivery method to get the shipments for.
$receipt = array(56); // int[] | The receipt ids to get the shipment infos for.
$case = array(56); // int[] | The case ids to get the shipment infos for.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $apiInstance->receiptDataShipmentsAllCountGet($index, $limit, $shipment_id, $shipment_value_equals, $shipment_value_start_with, $shipment_value_like, $method, $receipt, $case, $chain_using_or);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi->receiptDataShipmentsAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **shipment_id** | [**int[]**](../Model/int.md)| The IDs of the delivery method to get the shipments for. | [optional]
 **shipment_value_equals** | [**string[]**](../Model/string.md)| Limits search to the shipment label value using equals check. | [optional]
 **shipment_value_start_with** | [**string[]**](../Model/string.md)| Limits search to the shipment label value using starts with check. Case Invariant. | [optional]
 **shipment_value_like** | **string**| Limits search to the shipment label value using DbFunction LIKE check. Case Invariant. | [optional]
 **method** | [**int[]**](../Model/int.md)| The IDs of the delivery method to get the shipments for. | [optional]
 **receipt** | [**int[]**](../Model/int.md)| The receipt ids to get the shipment infos for. | [optional]
 **case** | [**int[]**](../Model/int.md)| The case ids to get the shipment infos for. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataShipmentsAllGet()`

```php
receiptDataShipmentsAllGet($index, $limit, $shipmentid, $shipment_value_exact, $shipment_value_start_with, $shipment_value_like, $delivery_method, $delivery_goal, $receipt, $case, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataShipment
```

Endpoint to poll for available shipments.

Endpoint to poll for available shipments.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.
$shipmentid = array(56); // int[] | The IDs of the delivery method to get the shipments for.
$shipment_value_exact = array('shipment_value_exact_example'); // string[] | Limits search to the shipment label value using equals check.
$shipment_value_start_with = array('shipment_value_start_with_example'); // string[] | Limits search to the shipment label value using starts with check. Case Invariant.
$shipment_value_like = 'shipment_value_like_example'; // string | Limits search to the shipment label value using DbFunction LIKE check. Case Invariant.
$delivery_method = array(56); // int[] | The IDs of the delivery method to get the shipments for.
$delivery_goal = array(56); // int[] | The IDs of the delivery goal to get the shipments for.
$receipt = array(56); // int[] | The receipt ids to get the shipment infos for.
$case = array(56); // int[] | The case ids to get the shipment infos for.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->receiptDataShipmentsAllGet($index, $limit, $shipmentid, $shipment_value_exact, $shipment_value_start_with, $shipment_value_like, $delivery_method, $delivery_goal, $receipt, $case, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi->receiptDataShipmentsAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **shipmentid** | [**int[]**](../Model/int.md)| The IDs of the delivery method to get the shipments for. | [optional]
 **shipment_value_exact** | [**string[]**](../Model/string.md)| Limits search to the shipment label value using equals check. | [optional]
 **shipment_value_start_with** | [**string[]**](../Model/string.md)| Limits search to the shipment label value using starts with check. Case Invariant. | [optional]
 **shipment_value_like** | **string**| Limits search to the shipment label value using DbFunction LIKE check. Case Invariant. | [optional]
 **delivery_method** | [**int[]**](../Model/int.md)| The IDs of the delivery method to get the shipments for. | [optional]
 **delivery_goal** | [**int[]**](../Model/int.md)| The IDs of the delivery goal to get the shipments for. | [optional]
 **receipt** | [**int[]**](../Model/int.md)| The receipt ids to get the shipment infos for. | [optional]
 **case** | [**int[]**](../Model/int.md)| The case ids to get the shipment infos for. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataShipment**](../Model/SimplifySoftPecuniariusDataNetReceiptDataShipment.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataShipmentsDeliveryGoalidCreatePost()`

```php
receiptDataShipmentsDeliveryGoalidCreatePost($goalid): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataShipment
```

Creates a shipment including a corresponding label for the given Receipt.

Creates a shipment including a corresponding label for the given Receipt. Unless the used addresses are marked as validated, this method will fail.This method also will fail unless the chosen DeliveryOption has a DeliveryProviderContext assigned.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$goalid = 56; // int | unavailable

try {
    $result = $apiInstance->receiptDataShipmentsDeliveryGoalidCreatePost($goalid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi->receiptDataShipmentsDeliveryGoalidCreatePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **goalid** | **int**| unavailable |

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataShipment**](../Model/SimplifySoftPecuniariusDataNetReceiptDataShipment.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataShipmentsDeliveryGoalidPost()`

```php
receiptDataShipmentsDeliveryGoalidPost($goalid, $simplify_soft_pecuniarius_data_net_receipt_data_shipment): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataShipment
```

Allows creating shipment informations of any given case.

Allows creating shipment informations of any given case. Case will be forced onto the shipments overriding any values provided with the POST. Due to the nature of a shipment being mostly mutable, all informations should be provided with this post.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$goalid = 56; // int | unavailable
$simplify_soft_pecuniarius_data_net_receipt_data_shipment = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataShipment(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataShipment

try {
    $result = $apiInstance->receiptDataShipmentsDeliveryGoalidPost($goalid, $simplify_soft_pecuniarius_data_net_receipt_data_shipment);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi->receiptDataShipmentsDeliveryGoalidPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **goalid** | **int**| unavailable |
 **simplify_soft_pecuniarius_data_net_receipt_data_shipment** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataShipment**](../Model/SimplifySoftPecuniariusDataNetReceiptDataShipment.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataShipment**](../Model/SimplifySoftPecuniariusDataNetReceiptDataShipment.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataShipmentsDeliveryGoalidPreviewPost()`

```php
receiptDataShipmentsDeliveryGoalidPreviewPost($goalid): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataShipment
```

Creates a preview of the shipments to be created.

Creates a preview of the shipments to be created. Will not validate addresses or store anything on the server.This method also will fail unless the chosen DeliveryOption has a DeliveryProviderContext assigned.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$goalid = 56; // int | unavailable

try {
    $result = $apiInstance->receiptDataShipmentsDeliveryGoalidPreviewPost($goalid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi->receiptDataShipmentsDeliveryGoalidPreviewPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **goalid** | **int**| unavailable |

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataShipment**](../Model/SimplifySoftPecuniariusDataNetReceiptDataShipment.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataShipmentsShipmentidCancelGet()`

```php
receiptDataShipmentsShipmentidCancelGet($shipmentid)
```

Asks the server to use the corresponding DeliveryProviderContext to cancel a given shipment.

Asks the server to use the corresponding DeliveryProviderContext to cancel a given shipment. If no DeliveryProviderContext is available, NetException will be raised.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$shipmentid = 56; // int | unavailable

try {
    $apiInstance->receiptDataShipmentsShipmentidCancelGet($shipmentid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi->receiptDataShipmentsShipmentidCancelGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shipmentid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataShipmentsShipmentidDelete()`

```php
receiptDataShipmentsShipmentidDelete($shipmentid)
```

Provides the ability to remove shipments from a given case.

Provides the ability to remove shipments from a given case.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$shipmentid = 56; // int | unavailable

try {
    $apiInstance->receiptDataShipmentsShipmentidDelete($shipmentid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi->receiptDataShipmentsShipmentidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shipmentid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataShipmentsShipmentidFilesGet()`

```php
receiptDataShipmentsShipmentidFilesGet($shipmentid): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetFile
```

Endpoint to poll for available shipments.

Endpoint to poll for available shipments.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$shipmentid = 56; // int | unavailable

try {
    $result = $apiInstance->receiptDataShipmentsShipmentidFilesGet($shipmentid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi->receiptDataShipmentsShipmentidFilesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shipmentid** | **int**| unavailable |

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetFile**](../Model/SimplifySoftPecuniariusDataNetFile.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataShipmentsShipmentidPut()`

```php
receiptDataShipmentsShipmentidPut($shipmentid, $unknown_base_type)
```

Allows updating shipment informations of any given case.

Allows updating shipment informations of any given case. Cannot alter key-properties (like Case, Value, Timestamps). ShipmentEntries cannot be altered after creation.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$shipmentid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->receiptDataShipmentsShipmentidPut($shipmentid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi->receiptDataShipmentsShipmentidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shipmentid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataShipmentsShipmentidStatusGet()`

```php
receiptDataShipmentsShipmentidStatusGet($shipmentid): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPackagesShipmentStatus
```

Asks the server to use the corresponding DeliveryProviderContext to poll for the status of a given shipment.

Asks the server to use the corresponding DeliveryProviderContext to poll for the status of a given shipment. If no DeliveryProviderContext is available, NetException will be raised.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$shipmentid = 56; // int | unavailable

try {
    $result = $apiInstance->receiptDataShipmentsShipmentidStatusGet($shipmentid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataShipmentsApi->receiptDataShipmentsShipmentidStatusGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **shipmentid** | **int**| unavailable |

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPackagesShipmentStatus**](../Model/SimplifySoftPecuniariusDataNetReceiptDataPackagesShipmentStatus.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
