# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1StateMachinesApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**stateMachineGet()**](SimplifySoftPecuniariusServerAPIv1StateMachinesApi.md#stateMachineGet) | **GET** /state/machine | Receives a list of all available state-machines.
[**stateMachineIdCodePageCodepageidLinkPost()**](SimplifySoftPecuniariusServerAPIv1StateMachinesApi.md#stateMachineIdCodePageCodepageidLinkPost) | **POST** /state/machine/{id}/code-page/{codepageid}/link | Allows to link a state-node to a state-machine.
[**stateMachineIdCodePageCodepageidUnlinkPost()**](SimplifySoftPecuniariusServerAPIv1StateMachinesApi.md#stateMachineIdCodePageCodepageidUnlinkPost) | **POST** /state/machine/{id}/code-page/{codepageid}/unlink | Allows to unlink a code-page from a state-machine.
[**stateMachineIdDelete()**](SimplifySoftPecuniariusServerAPIv1StateMachinesApi.md#stateMachineIdDelete) | **DELETE** /state/machine/{id} | Deletes a state-machine.
[**stateMachineIdEntrystatesGet()**](SimplifySoftPecuniariusServerAPIv1StateMachinesApi.md#stateMachineIdEntrystatesGet) | **GET** /state/machine/{id}/entrystates | Receives a list of all available entry states.
[**stateMachineIdGet()**](SimplifySoftPecuniariusServerAPIv1StateMachinesApi.md#stateMachineIdGet) | **GET** /state/machine/{id} | Receives a single state-machine.
[**stateMachineIdInteractionInteractionidLinkPost()**](SimplifySoftPecuniariusServerAPIv1StateMachinesApi.md#stateMachineIdInteractionInteractionidLinkPost) | **POST** /state/machine/{id}/interaction/{interactionid}/link | Allows to link a state-node to a state-machine.
[**stateMachineIdInteractionInteractionidUnlinkPost()**](SimplifySoftPecuniariusServerAPIv1StateMachinesApi.md#stateMachineIdInteractionInteractionidUnlinkPost) | **POST** /state/machine/{id}/interaction/{interactionid}/unlink | Allows to unlink a state-node from a state-machine.
[**stateMachineIdNodeNodeidLinkPost()**](SimplifySoftPecuniariusServerAPIv1StateMachinesApi.md#stateMachineIdNodeNodeidLinkPost) | **POST** /state/machine/{id}/node/{nodeid}/link | Allows to link a state-node to a state-machine.
[**stateMachineIdNodeNodeidUnlinkPost()**](SimplifySoftPecuniariusServerAPIv1StateMachinesApi.md#stateMachineIdNodeNodeidUnlinkPost) | **POST** /state/machine/{id}/node/{nodeid}/unlink | Allows to unlink a state-node from a state-machine.
[**stateMachineIdPut()**](SimplifySoftPecuniariusServerAPIv1StateMachinesApi.md#stateMachineIdPut) | **PUT** /state/machine/{id} | Updates an attribute definition.
[**stateMachinePost()**](SimplifySoftPecuniariusServerAPIv1StateMachinesApi.md#stateMachinePost) | **POST** /state/machine | Creates one or more new state-machines.


## `stateMachineGet()`

```php
stateMachineGet($limit, $index): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateMachine
```

Receives a list of all available state-machines.

Receives a list of all available state-machines.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateMachinesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 10; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.

try {
    $result = $apiInstance->stateMachineGet($limit, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateMachinesApi->stateMachineGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateMachine**](../Model/SimplifySoftPecuniariusDataNetStateMachineStateMachine.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateMachineIdCodePageCodepageidLinkPost()`

```php
stateMachineIdCodePageCodepageidLinkPost($id, $codepageid)
```

Allows to link a state-node to a state-machine.

Allows to link a state-node to a state-machine.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateMachinesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$codepageid = 56; // int | unavailable

try {
    $apiInstance->stateMachineIdCodePageCodepageidLinkPost($id, $codepageid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateMachinesApi->stateMachineIdCodePageCodepageidLinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **codepageid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateMachineIdCodePageCodepageidUnlinkPost()`

```php
stateMachineIdCodePageCodepageidUnlinkPost($id, $codepageid)
```

Allows to unlink a code-page from a state-machine.

Allows to unlink a code-page from a state-machine.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateMachinesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$codepageid = 56; // int | unavailable

try {
    $apiInstance->stateMachineIdCodePageCodepageidUnlinkPost($id, $codepageid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateMachinesApi->stateMachineIdCodePageCodepageidUnlinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **codepageid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateMachineIdDelete()`

```php
stateMachineIdDelete($id)
```

Deletes a state-machine.

Deletes a state-machine.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateMachinesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->stateMachineIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateMachinesApi->stateMachineIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateMachineIdEntrystatesGet()`

```php
stateMachineIdEntrystatesGet($id, $includenext): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineInteractionsEnterInteraction
```

Receives a list of all available entry states.

Receives a list of all available entry states.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateMachinesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$includenext = false; // bool | If set, will return the next state too.

try {
    $result = $apiInstance->stateMachineIdEntrystatesGet($id, $includenext);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateMachinesApi->stateMachineIdEntrystatesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **includenext** | **bool**| If set, will return the next state too. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineInteractionsEnterInteraction**](../Model/SimplifySoftPecuniariusDataNetStateMachineInteractionsEnterInteraction.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateMachineIdGet()`

```php
stateMachineIdGet($id, $limit, $index)
```

Receives a single state-machine.

Receives a single state-machine.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateMachinesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$limit = 10; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.

try {
    $apiInstance->stateMachineIdGet($id, $limit, $index);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateMachinesApi->stateMachineIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateMachineIdInteractionInteractionidLinkPost()`

```php
stateMachineIdInteractionInteractionidLinkPost($id, $interactionid)
```

Allows to link a state-node to a state-machine.

Allows to link a state-node to a state-machine.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateMachinesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$interactionid = 56; // int | unavailable

try {
    $apiInstance->stateMachineIdInteractionInteractionidLinkPost($id, $interactionid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateMachinesApi->stateMachineIdInteractionInteractionidLinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **interactionid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateMachineIdInteractionInteractionidUnlinkPost()`

```php
stateMachineIdInteractionInteractionidUnlinkPost($id, $interactionid)
```

Allows to unlink a state-node from a state-machine.

Allows to unlink a state-node from a state-machine.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateMachinesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$interactionid = 56; // int | unavailable

try {
    $apiInstance->stateMachineIdInteractionInteractionidUnlinkPost($id, $interactionid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateMachinesApi->stateMachineIdInteractionInteractionidUnlinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **interactionid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateMachineIdNodeNodeidLinkPost()`

```php
stateMachineIdNodeNodeidLinkPost($id, $nodeid)
```

Allows to link a state-node to a state-machine.

Allows to link a state-node to a state-machine.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateMachinesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$nodeid = 56; // int | unavailable

try {
    $apiInstance->stateMachineIdNodeNodeidLinkPost($id, $nodeid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateMachinesApi->stateMachineIdNodeNodeidLinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **nodeid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateMachineIdNodeNodeidUnlinkPost()`

```php
stateMachineIdNodeNodeidUnlinkPost($id, $nodeid)
```

Allows to unlink a state-node from a state-machine.

Allows to unlink a state-node from a state-machine.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateMachinesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$nodeid = 56; // int | unavailable

try {
    $apiInstance->stateMachineIdNodeNodeidUnlinkPost($id, $nodeid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateMachinesApi->stateMachineIdNodeNodeidUnlinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **nodeid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateMachineIdPut()`

```php
stateMachineIdPut($id, $unknown_base_type)
```

Updates an attribute definition.

Updates an attribute definition.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateMachinesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->stateMachineIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateMachinesApi->stateMachineIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateMachinePost()`

```php
stateMachinePost($simplify_soft_pecuniarius_data_net_state_machine_state_machine): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateMachine
```

Creates one or more new state-machines.

Creates one or more new state-machines.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateMachinesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_state_machine_state_machine = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateMachine(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateMachine

try {
    $result = $apiInstance->stateMachinePost($simplify_soft_pecuniarius_data_net_state_machine_state_machine);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateMachinesApi->stateMachinePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_state_machine_state_machine** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateMachine**](../Model/SimplifySoftPecuniariusDataNetStateMachineStateMachine.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateMachine**](../Model/SimplifySoftPecuniariusDataNetStateMachineStateMachine.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
