# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1StateInteractionsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**stateInteractionGet()**](SimplifySoftPecuniariusServerAPIv1StateInteractionsApi.md#stateInteractionGet) | **GET** /state/interaction | Receives a list of all available interactions.
[**stateInteractionIdConditionCodepageidLinkPost()**](SimplifySoftPecuniariusServerAPIv1StateInteractionsApi.md#stateInteractionIdConditionCodepageidLinkPost) | **POST** /state/interaction/{id}/condition/{codepageid}/link | Allows to link a code-page to a conditional-interactions conditions.
[**stateInteractionIdConditionCodepageidUnlinkPost()**](SimplifySoftPecuniariusServerAPIv1StateInteractionsApi.md#stateInteractionIdConditionCodepageidUnlinkPost) | **POST** /state/interaction/{id}/condition/{codepageid}/unlink | Allows to unlink a code-page to a conditional-interactions conditions.
[**stateInteractionIdDelete()**](SimplifySoftPecuniariusServerAPIv1StateInteractionsApi.md#stateInteractionIdDelete) | **DELETE** /state/interaction/{id} | Deletes an interaction.
[**stateInteractionIdGet()**](SimplifySoftPecuniariusServerAPIv1StateInteractionsApi.md#stateInteractionIdGet) | **GET** /state/interaction/{id} | Receives a single interaction.
[**stateInteractionIdPut()**](SimplifySoftPecuniariusServerAPIv1StateInteractionsApi.md#stateInteractionIdPut) | **PUT** /state/interaction/{id} | Updates an interaction.
[**stateInteractionPost()**](SimplifySoftPecuniariusServerAPIv1StateInteractionsApi.md#stateInteractionPost) | **POST** /state/interaction | Creates one or more new interactions.


## `stateInteractionGet()`

```php
stateInteractionGet($limit, $index): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineInteractionsInteraction
```

Receives a list of all available interactions.

Receives a list of all available interactions.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateInteractionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 10; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.

try {
    $result = $apiInstance->stateInteractionGet($limit, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateInteractionsApi->stateInteractionGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineInteractionsInteraction**](../Model/SimplifySoftPecuniariusDataNetStateMachineInteractionsInteraction.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateInteractionIdConditionCodepageidLinkPost()`

```php
stateInteractionIdConditionCodepageidLinkPost($id, $codepageid)
```

Allows to link a code-page to a conditional-interactions conditions.

Allows to link a code-page to a conditional-interactions conditions.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateInteractionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$codepageid = 56; // int | unavailable

try {
    $apiInstance->stateInteractionIdConditionCodepageidLinkPost($id, $codepageid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateInteractionsApi->stateInteractionIdConditionCodepageidLinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **codepageid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateInteractionIdConditionCodepageidUnlinkPost()`

```php
stateInteractionIdConditionCodepageidUnlinkPost($id, $codepageid)
```

Allows to unlink a code-page to a conditional-interactions conditions.

Allows to unlink a code-page to a conditional-interactions conditions.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateInteractionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$codepageid = 56; // int | unavailable

try {
    $apiInstance->stateInteractionIdConditionCodepageidUnlinkPost($id, $codepageid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateInteractionsApi->stateInteractionIdConditionCodepageidUnlinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **codepageid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateInteractionIdDelete()`

```php
stateInteractionIdDelete($id)
```

Deletes an interaction.

Deletes an interaction.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateInteractionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->stateInteractionIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateInteractionsApi->stateInteractionIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateInteractionIdGet()`

```php
stateInteractionIdGet($id, $limit, $index)
```

Receives a single interaction.

Receives a single interaction.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateInteractionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$limit = 10; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.

try {
    $apiInstance->stateInteractionIdGet($id, $limit, $index);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateInteractionsApi->stateInteractionIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateInteractionIdPut()`

```php
stateInteractionIdPut($id, $unknown_base_type)
```

Updates an interaction.

Updates an interaction.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateInteractionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->stateInteractionIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateInteractionsApi->stateInteractionIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateInteractionPost()`

```php
stateInteractionPost($simplify_soft_pecuniarius_data_net_state_machine_interactions_interaction): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineInteractionsInteraction
```

Creates one or more new interactions.

Creates one or more new interactions.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateInteractionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_state_machine_interactions_interaction = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineInteractionsInteraction(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineInteractionsInteraction

try {
    $result = $apiInstance->stateInteractionPost($simplify_soft_pecuniarius_data_net_state_machine_interactions_interaction);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateInteractionsApi->stateInteractionPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_state_machine_interactions_interaction** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineInteractionsInteraction**](../Model/SimplifySoftPecuniariusDataNetStateMachineInteractionsInteraction.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineInteractionsInteraction**](../Model/SimplifySoftPecuniariusDataNetStateMachineInteractionsInteraction.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
