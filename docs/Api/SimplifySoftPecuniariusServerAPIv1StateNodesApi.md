# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1StateNodesApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**stateNodeGet()**](SimplifySoftPecuniariusServerAPIv1StateNodesApi.md#stateNodeGet) | **GET** /state/node | Receives a list of all available state nodes.
[**stateNodeGroupsGet()**](SimplifySoftPecuniariusServerAPIv1StateNodesApi.md#stateNodeGroupsGet) | **GET** /state/node/groups | Receives a list of all available state node groups.
[**stateNodeIdDelete()**](SimplifySoftPecuniariusServerAPIv1StateNodesApi.md#stateNodeIdDelete) | **DELETE** /state/node/{id} | Deletes a state node.
[**stateNodeIdEnterCodepageidLinkPost()**](SimplifySoftPecuniariusServerAPIv1StateNodesApi.md#stateNodeIdEnterCodepageidLinkPost) | **POST** /state/node/{id}/enter/{codepageid}/link | Allows to link a code-page to a state-nodes enter actions.
[**stateNodeIdEnterCodepageidUnlinkPost()**](SimplifySoftPecuniariusServerAPIv1StateNodesApi.md#stateNodeIdEnterCodepageidUnlinkPost) | **POST** /state/node/{id}/enter/{codepageid}/unlink | Allows to unlink a code-page from a state-nodes enter actions.
[**stateNodeIdEnterGet()**](SimplifySoftPecuniariusServerAPIv1StateNodesApi.md#stateNodeIdEnterGet) | **GET** /state/node/{id}/enter | Receives a list of all available enter Code Pages.
[**stateNodeIdExitCodepageidLinkPost()**](SimplifySoftPecuniariusServerAPIv1StateNodesApi.md#stateNodeIdExitCodepageidLinkPost) | **POST** /state/node/{id}/exit/{codepageid}/link | Allows to link a code-page to a state-nodes exit actions.
[**stateNodeIdExitCodepageidUnlinkPost()**](SimplifySoftPecuniariusServerAPIv1StateNodesApi.md#stateNodeIdExitCodepageidUnlinkPost) | **POST** /state/node/{id}/exit/{codepageid}/unlink | Allows to unlink a code-page from a state-nodes exit actions.
[**stateNodeIdExitGet()**](SimplifySoftPecuniariusServerAPIv1StateNodesApi.md#stateNodeIdExitGet) | **GET** /state/node/{id}/exit | Receives a list of all available exit Code Pages.
[**stateNodeIdGet()**](SimplifySoftPecuniariusServerAPIv1StateNodesApi.md#stateNodeIdGet) | **GET** /state/node/{id} | Receives a single state node.
[**stateNodeIdPut()**](SimplifySoftPecuniariusServerAPIv1StateNodesApi.md#stateNodeIdPut) | **PUT** /state/node/{id} | Updates a state node.
[**stateNodePost()**](SimplifySoftPecuniariusServerAPIv1StateNodesApi.md#stateNodePost) | **POST** /state/node | Creates one or more new state nodes.
[**stateNodeTitlesGet()**](SimplifySoftPecuniariusServerAPIv1StateNodesApi.md#stateNodeTitlesGet) | **GET** /state/node/titles | Receives a list of all available state node titles.


## `stateNodeGet()`

```php
stateNodeGet($limit, $index, $id, $parent, $title, $includeemptytitle): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateNode
```

Receives a list of all available state nodes.

Receives a list of all available state nodes.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateNodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 10; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$id = array(56); // int[] | The IDs to receive.
$parent = array(56); // int[] | The Parent IDs to receive.
$title = array('title_example'); // string[] | Looks up nodes with the given name using DbFunction.Like.
$includeemptytitle = true; // bool | Wether to include empty-titled nodes.

try {
    $result = $apiInstance->stateNodeGet($limit, $index, $id, $parent, $title, $includeemptytitle);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateNodesApi->stateNodeGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **id** | [**int[]**](../Model/int.md)| The IDs to receive. | [optional]
 **parent** | [**int[]**](../Model/int.md)| The Parent IDs to receive. | [optional]
 **title** | [**string[]**](../Model/string.md)| Looks up nodes with the given name using DbFunction.Like. | [optional]
 **includeemptytitle** | **bool**| Wether to include empty-titled nodes. | [optional] [default to true]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateNode**](../Model/SimplifySoftPecuniariusDataNetStateMachineStateNode.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateNodeGroupsGet()`

```php
stateNodeGroupsGet($limit, $index): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetString
```

Receives a list of all available state node groups.

Receives a list of all available state node groups.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateNodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 10; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.

try {
    $result = $apiInstance->stateNodeGroupsGet($limit, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateNodesApi->stateNodeGroupsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetString**](../Model/SimplifySoftPecuniariusDataNetPrimitivesNetString.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateNodeIdDelete()`

```php
stateNodeIdDelete($id)
```

Deletes a state node.

Deletes a state node.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateNodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->stateNodeIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateNodesApi->stateNodeIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateNodeIdEnterCodepageidLinkPost()`

```php
stateNodeIdEnterCodepageidLinkPost($id, $codepageid)
```

Allows to link a code-page to a state-nodes enter actions.

Allows to link a code-page to a state-nodes enter actions.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateNodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$codepageid = 56; // int | unavailable

try {
    $apiInstance->stateNodeIdEnterCodepageidLinkPost($id, $codepageid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateNodesApi->stateNodeIdEnterCodepageidLinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **codepageid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateNodeIdEnterCodepageidUnlinkPost()`

```php
stateNodeIdEnterCodepageidUnlinkPost($id, $codepageid)
```

Allows to unlink a code-page from a state-nodes enter actions.

Allows to unlink a code-page from a state-nodes enter actions.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateNodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$codepageid = 56; // int | unavailable

try {
    $apiInstance->stateNodeIdEnterCodepageidUnlinkPost($id, $codepageid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateNodesApi->stateNodeIdEnterCodepageidUnlinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **codepageid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateNodeIdEnterGet()`

```php
stateNodeIdEnterGet($id): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineCodePage
```

Receives a list of all available enter Code Pages.

Receives a list of all available enter Code Pages.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateNodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $result = $apiInstance->stateNodeIdEnterGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateNodesApi->stateNodeIdEnterGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineCodePage**](../Model/SimplifySoftPecuniariusDataNetStateMachineCodePage.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateNodeIdExitCodepageidLinkPost()`

```php
stateNodeIdExitCodepageidLinkPost($id, $codepageid)
```

Allows to link a code-page to a state-nodes exit actions.

Allows to link a code-page to a state-nodes exit actions.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateNodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$codepageid = 56; // int | unavailable

try {
    $apiInstance->stateNodeIdExitCodepageidLinkPost($id, $codepageid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateNodesApi->stateNodeIdExitCodepageidLinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **codepageid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateNodeIdExitCodepageidUnlinkPost()`

```php
stateNodeIdExitCodepageidUnlinkPost($id, $codepageid)
```

Allows to unlink a code-page from a state-nodes exit actions.

Allows to unlink a code-page from a state-nodes exit actions.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateNodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$codepageid = 56; // int | unavailable

try {
    $apiInstance->stateNodeIdExitCodepageidUnlinkPost($id, $codepageid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateNodesApi->stateNodeIdExitCodepageidUnlinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **codepageid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateNodeIdExitGet()`

```php
stateNodeIdExitGet($id): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineCodePage
```

Receives a list of all available exit Code Pages.

Receives a list of all available exit Code Pages.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateNodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $result = $apiInstance->stateNodeIdExitGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateNodesApi->stateNodeIdExitGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineCodePage**](../Model/SimplifySoftPecuniariusDataNetStateMachineCodePage.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateNodeIdGet()`

```php
stateNodeIdGet($id)
```

Receives a single state node.

Receives a single state node.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateNodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->stateNodeIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateNodesApi->stateNodeIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateNodeIdPut()`

```php
stateNodeIdPut($id, $unknown_base_type)
```

Updates a state node.

Updates a state node.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateNodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->stateNodeIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateNodesApi->stateNodeIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateNodePost()`

```php
stateNodePost($simplify_soft_pecuniarius_data_net_state_machine_state_node): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateNode
```

Creates one or more new state nodes.

Creates one or more new state nodes.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateNodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_state_machine_state_node = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateNode(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateNode

try {
    $result = $apiInstance->stateNodePost($simplify_soft_pecuniarius_data_net_state_machine_state_node);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateNodesApi->stateNodePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_state_machine_state_node** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateNode**](../Model/SimplifySoftPecuniariusDataNetStateMachineStateNode.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateNode**](../Model/SimplifySoftPecuniariusDataNetStateMachineStateNode.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateNodeTitlesGet()`

```php
stateNodeTitlesGet($limit, $index, $exact, $starts_with, $like, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetString
```

Receives a list of all available state node titles.

Receives a list of all available state node titles.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateNodesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 10; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$exact = array('exact_example'); // string[] | Filters the results against the provided inputs.
$starts_with = array('starts_with_example'); // string[] | Filters the results against the provided inputs using StartsWith comparison. Case Invariant.
$like = 'like_example'; // string | Filters the results against the provided inputs using DbFunction.Like comparison. Case Invariant.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->stateNodeTitlesGet($limit, $index, $exact, $starts_with, $like, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateNodesApi->stateNodeTitlesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **exact** | [**string[]**](../Model/string.md)| Filters the results against the provided inputs. | [optional]
 **starts_with** | [**string[]**](../Model/string.md)| Filters the results against the provided inputs using StartsWith comparison. Case Invariant. | [optional]
 **like** | **string**| Filters the results against the provided inputs using DbFunction.Like comparison. Case Invariant. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetString**](../Model/SimplifySoftPecuniariusDataNetPrimitivesNetString.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
