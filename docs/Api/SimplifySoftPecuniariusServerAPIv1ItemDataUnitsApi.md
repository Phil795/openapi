# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**itemdataUnitsGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi.md#itemdataUnitsGet) | **GET** /itemdata/units | Receives a list of all available unit-groups.
[**itemdataUnitsGroupDelete()**](SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi.md#itemdataUnitsGroupDelete) | **DELETE** /itemdata/units/{group} | Receives a list of all available gradiations inside of the provided group.
[**itemdataUnitsGroupPut()**](SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi.md#itemdataUnitsGroupPut) | **PUT** /itemdata/units/{group} | Allows to update a single Unit-Group.
[**itemdataUnitsGroupTierGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi.md#itemdataUnitsGroupTierGet) | **GET** /itemdata/units/{group}/tier | Receives a list of all available gradiations inside of the provided group.
[**itemdataUnitsGroupTierPost()**](SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi.md#itemdataUnitsGroupTierPost) | **POST** /itemdata/units/{group}/tier | Allows to create a new Gradiation inside a Unit-Group.
[**itemdataUnitsGroupTierTierDelete()**](SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi.md#itemdataUnitsGroupTierTierDelete) | **DELETE** /itemdata/units/{group}/tier/{tier} | Receives a list of all available gradiations inside of the provided group.
[**itemdataUnitsGroupTierTierPut()**](SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi.md#itemdataUnitsGroupTierTierPut) | **PUT** /itemdata/units/{group}/tier/{tier} | Allows to update a single Tier.
[**itemdataUnitsGroupUnitsGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi.md#itemdataUnitsGroupUnitsGet) | **GET** /itemdata/units/{group}/units | Receives a list of all available units inside of the provided group.
[**itemdataUnitsGroupUnitsPost()**](SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi.md#itemdataUnitsGroupUnitsPost) | **POST** /itemdata/units/{group}/units | Allows to create a new Unit inside a Unit-Group.
[**itemdataUnitsGroupUnitsUnitDelete()**](SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi.md#itemdataUnitsGroupUnitsUnitDelete) | **DELETE** /itemdata/units/{group}/units/{unit} | Receives a list of all available gradiations inside of the provided group.
[**itemdataUnitsGroupUnitsUnitPut()**](SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi.md#itemdataUnitsGroupUnitsUnitPut) | **PUT** /itemdata/units/{group}/units/{unit} | Allows to update a single Unit.
[**itemdataUnitsPost()**](SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi.md#itemdataUnitsPost) | **POST** /itemdata/units | Allows to create a new Unit-Group.


## `itemdataUnitsGet()`

```php
itemdataUnitsGet($index, $limit): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnitGroup
```

Receives a list of all available unit-groups.

Receives a list of all available unit-groups.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.

try {
    $result = $apiInstance->itemdataUnitsGet($index, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi->itemdataUnitsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnitGroup**](../Model/SimplifySoftPecuniariusDataNetItemDataUnitGroup.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataUnitsGroupDelete()`

```php
itemdataUnitsGroupDelete($group)
```

Receives a list of all available gradiations inside of the provided group.

Receives a list of all available gradiations inside of the provided group.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group = 56; // int | unavailable

try {
    $apiInstance->itemdataUnitsGroupDelete($group);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi->itemdataUnitsGroupDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataUnitsGroupPut()`

```php
itemdataUnitsGroupPut($group, $unknown_base_type)
```

Allows to update a single Unit-Group.

Allows to update a single Unit-Group.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->itemdataUnitsGroupPut($group, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi->itemdataUnitsGroupPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataUnitsGroupTierGet()`

```php
itemdataUnitsGroupTierGet($group, $index, $limit): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnitTier
```

Receives a list of all available gradiations inside of the provided group.

Receives a list of all available gradiations inside of the provided group.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group = 56; // int | unavailable
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.

try {
    $result = $apiInstance->itemdataUnitsGroupTierGet($group, $index, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi->itemdataUnitsGroupTierGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **int**| unavailable |
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnitTier**](../Model/SimplifySoftPecuniariusDataNetItemDataUnitTier.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataUnitsGroupTierPost()`

```php
itemdataUnitsGroupTierPost($group, $simplify_soft_pecuniarius_data_net_item_data_unit_tier): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnitTier
```

Allows to create a new Gradiation inside a Unit-Group.

Allows to create a new Gradiation inside a Unit-Group.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group = 56; // int | unavailable
$simplify_soft_pecuniarius_data_net_item_data_unit_tier = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnitTier(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnitTier

try {
    $result = $apiInstance->itemdataUnitsGroupTierPost($group, $simplify_soft_pecuniarius_data_net_item_data_unit_tier);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi->itemdataUnitsGroupTierPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **int**| unavailable |
 **simplify_soft_pecuniarius_data_net_item_data_unit_tier** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnitTier**](../Model/SimplifySoftPecuniariusDataNetItemDataUnitTier.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnitTier**](../Model/SimplifySoftPecuniariusDataNetItemDataUnitTier.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataUnitsGroupTierTierDelete()`

```php
itemdataUnitsGroupTierTierDelete($group, $tier)
```

Receives a list of all available gradiations inside of the provided group.

Receives a list of all available gradiations inside of the provided group.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group = 56; // int | unavailable
$tier = 56; // int | unavailable

try {
    $apiInstance->itemdataUnitsGroupTierTierDelete($group, $tier);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi->itemdataUnitsGroupTierTierDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **int**| unavailable |
 **tier** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataUnitsGroupTierTierPut()`

```php
itemdataUnitsGroupTierTierPut($group, $tier, $unknown_base_type)
```

Allows to update a single Tier.

Allows to update a single Tier.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group = 56; // int | unavailable
$tier = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->itemdataUnitsGroupTierTierPut($group, $tier, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi->itemdataUnitsGroupTierTierPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **int**| unavailable |
 **tier** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataUnitsGroupUnitsGet()`

```php
itemdataUnitsGroupUnitsGet($group, $index, $limit): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnit
```

Receives a list of all available units inside of the provided group.

Receives a list of all available units inside of the provided group.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group = 56; // int | unavailable
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.

try {
    $result = $apiInstance->itemdataUnitsGroupUnitsGet($group, $index, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi->itemdataUnitsGroupUnitsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **int**| unavailable |
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnit**](../Model/SimplifySoftPecuniariusDataNetItemDataUnit.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataUnitsGroupUnitsPost()`

```php
itemdataUnitsGroupUnitsPost($group, $simplify_soft_pecuniarius_data_net_item_data_unit): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnit
```

Allows to create a new Unit inside a Unit-Group.

Allows to create a new Unit inside a Unit-Group.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group = 56; // int | unavailable
$simplify_soft_pecuniarius_data_net_item_data_unit = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnit(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnit

try {
    $result = $apiInstance->itemdataUnitsGroupUnitsPost($group, $simplify_soft_pecuniarius_data_net_item_data_unit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi->itemdataUnitsGroupUnitsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **int**| unavailable |
 **simplify_soft_pecuniarius_data_net_item_data_unit** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnit**](../Model/SimplifySoftPecuniariusDataNetItemDataUnit.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnit**](../Model/SimplifySoftPecuniariusDataNetItemDataUnit.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataUnitsGroupUnitsUnitDelete()`

```php
itemdataUnitsGroupUnitsUnitDelete($group, $unit)
```

Receives a list of all available gradiations inside of the provided group.

Receives a list of all available gradiations inside of the provided group.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group = 56; // int | unavailable
$unit = 56; // int | unavailable

try {
    $apiInstance->itemdataUnitsGroupUnitsUnitDelete($group, $unit);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi->itemdataUnitsGroupUnitsUnitDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **int**| unavailable |
 **unit** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataUnitsGroupUnitsUnitPut()`

```php
itemdataUnitsGroupUnitsUnitPut($group, $unit, $unknown_base_type)
```

Allows to update a single Unit.

Allows to update a single Unit.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$group = 56; // int | unavailable
$unit = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->itemdataUnitsGroupUnitsUnitPut($group, $unit, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi->itemdataUnitsGroupUnitsUnitPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **int**| unavailable |
 **unit** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataUnitsPost()`

```php
itemdataUnitsPost($simplify_soft_pecuniarius_data_net_item_data_unit_group): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnitGroup
```

Allows to create a new Unit-Group.

Allows to create a new Unit-Group.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_item_data_unit_group = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnitGroup(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnitGroup

try {
    $result = $apiInstance->itemdataUnitsPost($simplify_soft_pecuniarius_data_net_item_data_unit_group);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataUnitsApi->itemdataUnitsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_item_data_unit_group** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnitGroup**](../Model/SimplifySoftPecuniariusDataNetItemDataUnitGroup.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnitGroup**](../Model/SimplifySoftPecuniariusDataNetItemDataUnitGroup.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
