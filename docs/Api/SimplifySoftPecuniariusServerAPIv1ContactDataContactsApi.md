# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**contactdataAddressesAddidPut()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataAddressesAddidPut) | **PUT** /contactdata/addresses/{addid} | Provides an interface to update a single address on a contact.
[**contactdataContactsAddressesAddidGet()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsAddressesAddidGet) | **GET** /contactdata/contacts/addresses/{addid} | Returns requested address.
[**contactdataContactsAddressesAddidValidPost()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsAddressesAddidValidPost) | **POST** /contactdata/contacts/addresses/{addid}/valid | Marks the given address as valid.
[**contactdataContactsAddressesAddidValidateDeliveryOptionIdPost()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsAddressesAddidValidateDeliveryOptionIdPost) | **POST** /contactdata/contacts/addresses/{addid}/validate/{deliveryOptionId} | Attempts to validate a given address using the given DeliveryOptions ServiceProvider.
[**contactdataContactsAllAddressesAllCountGet()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsAllAddressesAllCountGet) | **GET** /contactdata/contacts/all/addresses/all/count | Allows polling the count of all addresses.
[**contactdataContactsAllAddressesAllGet()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsAllAddressesAllGet) | **GET** /contactdata/contacts/all/addresses/all | Allows polling all addresses.
[**contactdataContactsAllAddressesAllIdsGet()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsAllAddressesAllIdsGet) | **GET** /contactdata/contacts/all/addresses/all/ids | Allows polling the count of all addresses.
[**contactdataContactsAllCountGet()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsAllCountGet) | **GET** /contactdata/contacts/all/count | Allows to return a range of contacts, filtered according to provided GET parameters.
[**contactdataContactsAllGet()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsAllGet) | **GET** /contactdata/contacts/all | Allows to return a range of contacts, filtered according to provided GET parameters.
[**contactdataContactsAllIdsGet()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsAllIdsGet) | **GET** /contactdata/contacts/all/ids | Allows to return a range of contacts, filtered according to provided GET parameters.
[**contactdataContactsContactpossibilitiesIdGet()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsContactpossibilitiesIdGet) | **GET** /contactdata/contacts/contactpossibilities/{id} | Returns requested contactpossibility.
[**contactdataContactsIdTagsGet()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsIdTagsGet) | **GET** /contactdata/contacts/{id}/tags | Receives the tags this contact is linked with via its ID.
[**contactdataContactsIdTagsTagidDelete()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsIdTagsTagidDelete) | **DELETE** /contactdata/contacts/{id}/tags/{tagid} | Allows to remove a tag relation from a contact via its ID.
[**contactdataContactsIdTagsTagidPost()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsIdTagsTagidPost) | **POST** /contactdata/contacts/{id}/tags/{tagid} | Allows to add a tag relation onto a contact via its ID.
[**contactdataContactsIdentAddressesAddidDelete()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsIdentAddressesAddidDelete) | **DELETE** /contactdata/contacts/{ident}/addresses/{addid} | Allows to remove a single address from a given contact.
[**contactdataContactsIdentAddressesAddidPut()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsIdentAddressesAddidPut) | **PUT** /contactdata/contacts/{ident}/addresses/{addid} | Provides an interface to update a single address on a contact.
[**contactdataContactsIdentAddressesGet()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsIdentAddressesGet) | **GET** /contactdata/contacts/{ident}/addresses | Returns a list of addresses for given contact.
[**contactdataContactsIdentAddressesPost()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsIdentAddressesPost) | **POST** /contactdata/contacts/{ident}/addresses | Allows to create new addresses in given contact.
[**contactdataContactsIdentContactpossibilitiesCpidDelete()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsIdentContactpossibilitiesCpidDelete) | **DELETE** /contactdata/contacts/{ident}/contactpossibilities/{cpid} | Allows to remove a single contact possibility from a given contact.
[**contactdataContactsIdentContactpossibilitiesCpidPut()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsIdentContactpossibilitiesCpidPut) | **PUT** /contactdata/contacts/{ident}/contactpossibilities/{cpid} | Provides an interface to update a single contact possibility on a contact.
[**contactdataContactsIdentContactpossibilitiesGet()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsIdentContactpossibilitiesGet) | **GET** /contactdata/contacts/{ident}/contactpossibilities | Returns a list of contact possibilities for given contact.
[**contactdataContactsIdentContactpossibilitiesPost()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsIdentContactpossibilitiesPost) | **POST** /contactdata/contacts/{ident}/contactpossibilities | Allows to create new contact possibility in given contact.
[**contactdataContactsIdentGet()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsIdentGet) | **GET** /contactdata/contacts/{ident} | Returns a single contact.
[**contactdataContactsIdentPut()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsIdentPut) | **PUT** /contactdata/contacts/{ident} | Provides an interface to update a single contact.
[**contactdataContactsPost()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi.md#contactdataContactsPost) | **POST** /contactdata/contacts | Allows to create new contacts.


## `contactdataAddressesAddidPut()`

```php
contactdataAddressesAddidPut($addid, $unknown_base_type)
```

Provides an interface to update a single address on a contact.

Provides an interface to update a single address on a contact.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$addid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->contactdataAddressesAddidPut($addid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataAddressesAddidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **addid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsAddressesAddidGet()`

```php
contactdataContactsAddressesAddidGet($addid)
```

Returns requested address.

Returns requested address.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$addid = 56; // int | unavailable

try {
    $apiInstance->contactdataContactsAddressesAddidGet($addid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsAddressesAddidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **addid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsAddressesAddidValidPost()`

```php
contactdataContactsAddressesAddidValidPost($addid, $revoke)
```

Marks the given address as valid.

Marks the given address as valid.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$addid = 56; // int | unavailable
$revoke = True; // bool | Inverts this operation. Provided address afterwards will no longer be valid.

try {
    $apiInstance->contactdataContactsAddressesAddidValidPost($addid, $revoke);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsAddressesAddidValidPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **addid** | **int**| unavailable |
 **revoke** | **bool**| Inverts this operation. Provided address afterwards will no longer be valid. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsAddressesAddidValidateDeliveryOptionIdPost()`

```php
contactdataContactsAddressesAddidValidateDeliveryOptionIdPost($addid, $delivery_option_id)
```

Attempts to validate a given address using the given DeliveryOptions ServiceProvider.

Attempts to validate a given address using the given DeliveryOptions ServiceProvider.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$addid = 56; // int | unavailable
$delivery_option_id = 56; // int | unavailable

try {
    $apiInstance->contactdataContactsAddressesAddidValidateDeliveryOptionIdPost($addid, $delivery_option_id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsAddressesAddidValidateDeliveryOptionIdPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **addid** | **int**| unavailable |
 **delivery_option_id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsAllAddressesAllCountGet()`

```php
contactdataContactsAllAddressesAllCountGet($index, $limit, $address_id, $contact_id, $street_exact, $street_starts_with, $street_like, $town_exact, $town_starts_with, $town_like, $country_exact, $country_starts_with, $country_like, $namea_exact, $namea_starts_with, $namea_like, $nameb_exact, $nameb_starts_with, $nameb_like, $namec_exact, $namec_starts_with, $namec_like, $street_number_exact, $street_number_starts_with, $street_number_like, $zip_exact, $zip_starts_with, $zip_like, $chain_using_or, $force_id)
```

Allows polling the count of all addresses.

Allows polling the count of all addresses.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.
$address_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$contact_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$street_exact = array('street_exact_example'); // string[] | Checks the Address.Street against the provided strings using exact match.
$street_starts_with = array('street_starts_with_example'); // string[] | Checks the Address.Street against the provided strings using starts with. Case Invariant.
$street_like = 'street_like_example'; // string | Uses LIKE expression to check the Address.Street for provided content. Case Invariant.
$town_exact = array('town_exact_example'); // string[] | Checks the Address.Town against the provided strings using exact match.
$town_starts_with = array('town_starts_with_example'); // string[] | Checks the Address.Town against the provided strings using starts with. Case Invariant.
$town_like = 'town_like_example'; // string | Uses LIKE expression to check the Address.Town for provided content. Case Invariant.
$country_exact = array('country_exact_example'); // string[] | Checks the Address.Country against the provided strings using exact match.
$country_starts_with = array('country_starts_with_example'); // string[] | Checks the Address.Country against the provided strings using starts with. Case Invariant.
$country_like = 'country_like_example'; // string | Uses LIKE expression to check the Address.Country for provided content. Case Invariant.
$namea_exact = array('namea_exact_example'); // string[] | Checks the Address.NameA against the provided strings using exact match.
$namea_starts_with = array('namea_starts_with_example'); // string[] | Checks the Address.NameA against the provided strings using starts with. Case Invariant.
$namea_like = 'namea_like_example'; // string | Uses LIKE expression to check the Address.NameA for provided content. Case Invariant.
$nameb_exact = array('nameb_exact_example'); // string[] | Checks the Address.NameB against the provided strings using exact match.
$nameb_starts_with = array('nameb_starts_with_example'); // string[] | Checks the Address.NameB against the provided strings using starts with. Case Invariant.
$nameb_like = 'nameb_like_example'; // string | Uses LIKE expression to check the Address.NameB for provided content. Case Invariant.
$namec_exact = array('namec_exact_example'); // string[] | Checks the Address.NameC against the provided strings using exact match.
$namec_starts_with = array('namec_starts_with_example'); // string[] | Checks the Address.NameC against the provided strings using starts with. Case Invariant.
$namec_like = 'namec_like_example'; // string | Uses LIKE expression to check the Address.NameC for provided content. Case Invariant.
$street_number_exact = array('street_number_exact_example'); // string[] | Checks the Address.StreetNumber against the provided strings using exact match.
$street_number_starts_with = array('street_number_starts_with_example'); // string[] | Checks the Address.StreetNumber against the provided strings using starts with. Case Invariant.
$street_number_like = 'street_number_like_example'; // string | Uses LIKE expression to check the Address.StreetNumber for provided content. Case Invariant.
$zip_exact = array('zip_exact_example'); // string[] | Checks the Address.PostalCode against the provided strings using exact match.
$zip_starts_with = array('zip_starts_with_example'); // string[] | Checks the Address.PostalCode against the provided strings using starts with. Case Invariant.
$zip_like = 'zip_like_example'; // string | Uses LIKE expression to check the Address.PostalCode for provided content. Case Invariant.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.
$force_id = false; // bool | Enforces AND linking, regardless of chain-using-or, for '*-id' parameters.

try {
    $apiInstance->contactdataContactsAllAddressesAllCountGet($index, $limit, $address_id, $contact_id, $street_exact, $street_starts_with, $street_like, $town_exact, $town_starts_with, $town_like, $country_exact, $country_starts_with, $country_like, $namea_exact, $namea_starts_with, $namea_like, $nameb_exact, $nameb_starts_with, $nameb_like, $namec_exact, $namec_starts_with, $namec_like, $street_number_exact, $street_number_starts_with, $street_number_like, $zip_exact, $zip_starts_with, $zip_like, $chain_using_or, $force_id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsAllAddressesAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **address_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **contact_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **street_exact** | [**string[]**](../Model/string.md)| Checks the Address.Street against the provided strings using exact match. | [optional]
 **street_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.Street against the provided strings using starts with. Case Invariant. | [optional]
 **street_like** | **string**| Uses LIKE expression to check the Address.Street for provided content. Case Invariant. | [optional]
 **town_exact** | [**string[]**](../Model/string.md)| Checks the Address.Town against the provided strings using exact match. | [optional]
 **town_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.Town against the provided strings using starts with. Case Invariant. | [optional]
 **town_like** | **string**| Uses LIKE expression to check the Address.Town for provided content. Case Invariant. | [optional]
 **country_exact** | [**string[]**](../Model/string.md)| Checks the Address.Country against the provided strings using exact match. | [optional]
 **country_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.Country against the provided strings using starts with. Case Invariant. | [optional]
 **country_like** | **string**| Uses LIKE expression to check the Address.Country for provided content. Case Invariant. | [optional]
 **namea_exact** | [**string[]**](../Model/string.md)| Checks the Address.NameA against the provided strings using exact match. | [optional]
 **namea_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.NameA against the provided strings using starts with. Case Invariant. | [optional]
 **namea_like** | **string**| Uses LIKE expression to check the Address.NameA for provided content. Case Invariant. | [optional]
 **nameb_exact** | [**string[]**](../Model/string.md)| Checks the Address.NameB against the provided strings using exact match. | [optional]
 **nameb_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.NameB against the provided strings using starts with. Case Invariant. | [optional]
 **nameb_like** | **string**| Uses LIKE expression to check the Address.NameB for provided content. Case Invariant. | [optional]
 **namec_exact** | [**string[]**](../Model/string.md)| Checks the Address.NameC against the provided strings using exact match. | [optional]
 **namec_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.NameC against the provided strings using starts with. Case Invariant. | [optional]
 **namec_like** | **string**| Uses LIKE expression to check the Address.NameC for provided content. Case Invariant. | [optional]
 **street_number_exact** | [**string[]**](../Model/string.md)| Checks the Address.StreetNumber against the provided strings using exact match. | [optional]
 **street_number_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.StreetNumber against the provided strings using starts with. Case Invariant. | [optional]
 **street_number_like** | **string**| Uses LIKE expression to check the Address.StreetNumber for provided content. Case Invariant. | [optional]
 **zip_exact** | [**string[]**](../Model/string.md)| Checks the Address.PostalCode against the provided strings using exact match. | [optional]
 **zip_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.PostalCode against the provided strings using starts with. Case Invariant. | [optional]
 **zip_like** | **string**| Uses LIKE expression to check the Address.PostalCode for provided content. Case Invariant. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]
 **force_id** | **bool**| Enforces AND linking, regardless of chain-using-or, for &#39;*-id&#39; parameters. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsAllAddressesAllGet()`

```php
contactdataContactsAllAddressesAllGet($index, $limit, $address_id, $contact_id, $street_exact, $street_starts_with, $street_like, $town_exact, $town_starts_with, $town_like, $country_exact, $country_starts_with, $country_like, $namea_exact, $namea_starts_with, $namea_like, $nameb_exact, $nameb_starts_with, $nameb_like, $namec_exact, $namec_starts_with, $namec_like, $street_number_exact, $street_number_starts_with, $street_number_like, $zip_exact, $zip_starts_with, $zip_like, $chain_using_or, $force_id): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddress
```

Allows polling all addresses.

Allows polling all addresses.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.
$address_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$contact_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$street_exact = array('street_exact_example'); // string[] | Checks the Address.Street against the provided strings using exact match.
$street_starts_with = array('street_starts_with_example'); // string[] | Checks the Address.Street against the provided strings using starts with. Case Invariant.
$street_like = 'street_like_example'; // string | Uses LIKE expression to check the Address.Street for provided content. Case Invariant.
$town_exact = array('town_exact_example'); // string[] | Checks the Address.Town against the provided strings using exact match.
$town_starts_with = array('town_starts_with_example'); // string[] | Checks the Address.Town against the provided strings using starts with. Case Invariant.
$town_like = 'town_like_example'; // string | Uses LIKE expression to check the Address.Town for provided content. Case Invariant.
$country_exact = array('country_exact_example'); // string[] | Checks the Address.Country against the provided strings using exact match.
$country_starts_with = array('country_starts_with_example'); // string[] | Checks the Address.Country against the provided strings using starts with. Case Invariant.
$country_like = 'country_like_example'; // string | Uses LIKE expression to check the Address.Country for provided content. Case Invariant.
$namea_exact = array('namea_exact_example'); // string[] | Checks the Address.NameA against the provided strings using exact match.
$namea_starts_with = array('namea_starts_with_example'); // string[] | Checks the Address.NameA against the provided strings using starts with. Case Invariant.
$namea_like = 'namea_like_example'; // string | Uses LIKE expression to check the Address.NameA for provided content. Case Invariant.
$nameb_exact = array('nameb_exact_example'); // string[] | Checks the Address.NameB against the provided strings using exact match.
$nameb_starts_with = array('nameb_starts_with_example'); // string[] | Checks the Address.NameB against the provided strings using starts with. Case Invariant.
$nameb_like = 'nameb_like_example'; // string | Uses LIKE expression to check the Address.NameB for provided content. Case Invariant.
$namec_exact = array('namec_exact_example'); // string[] | Checks the Address.NameC against the provided strings using exact match.
$namec_starts_with = array('namec_starts_with_example'); // string[] | Checks the Address.NameC against the provided strings using starts with. Case Invariant.
$namec_like = 'namec_like_example'; // string | Uses LIKE expression to check the Address.NameC for provided content. Case Invariant.
$street_number_exact = array('street_number_exact_example'); // string[] | Checks the Address.StreetNumber against the provided strings using exact match.
$street_number_starts_with = array('street_number_starts_with_example'); // string[] | Checks the Address.StreetNumber against the provided strings using starts with. Case Invariant.
$street_number_like = 'street_number_like_example'; // string | Uses LIKE expression to check the Address.StreetNumber for provided content. Case Invariant.
$zip_exact = array('zip_exact_example'); // string[] | Checks the Address.PostalCode against the provided strings using exact match.
$zip_starts_with = array('zip_starts_with_example'); // string[] | Checks the Address.PostalCode against the provided strings using starts with. Case Invariant.
$zip_like = 'zip_like_example'; // string | Uses LIKE expression to check the Address.PostalCode for provided content. Case Invariant.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.
$force_id = false; // bool | Enforces AND linking, regardless of chain-using-or, for '*-id' parameters.

try {
    $result = $apiInstance->contactdataContactsAllAddressesAllGet($index, $limit, $address_id, $contact_id, $street_exact, $street_starts_with, $street_like, $town_exact, $town_starts_with, $town_like, $country_exact, $country_starts_with, $country_like, $namea_exact, $namea_starts_with, $namea_like, $nameb_exact, $nameb_starts_with, $nameb_like, $namec_exact, $namec_starts_with, $namec_like, $street_number_exact, $street_number_starts_with, $street_number_like, $zip_exact, $zip_starts_with, $zip_like, $chain_using_or, $force_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsAllAddressesAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **address_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **contact_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **street_exact** | [**string[]**](../Model/string.md)| Checks the Address.Street against the provided strings using exact match. | [optional]
 **street_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.Street against the provided strings using starts with. Case Invariant. | [optional]
 **street_like** | **string**| Uses LIKE expression to check the Address.Street for provided content. Case Invariant. | [optional]
 **town_exact** | [**string[]**](../Model/string.md)| Checks the Address.Town against the provided strings using exact match. | [optional]
 **town_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.Town against the provided strings using starts with. Case Invariant. | [optional]
 **town_like** | **string**| Uses LIKE expression to check the Address.Town for provided content. Case Invariant. | [optional]
 **country_exact** | [**string[]**](../Model/string.md)| Checks the Address.Country against the provided strings using exact match. | [optional]
 **country_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.Country against the provided strings using starts with. Case Invariant. | [optional]
 **country_like** | **string**| Uses LIKE expression to check the Address.Country for provided content. Case Invariant. | [optional]
 **namea_exact** | [**string[]**](../Model/string.md)| Checks the Address.NameA against the provided strings using exact match. | [optional]
 **namea_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.NameA against the provided strings using starts with. Case Invariant. | [optional]
 **namea_like** | **string**| Uses LIKE expression to check the Address.NameA for provided content. Case Invariant. | [optional]
 **nameb_exact** | [**string[]**](../Model/string.md)| Checks the Address.NameB against the provided strings using exact match. | [optional]
 **nameb_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.NameB against the provided strings using starts with. Case Invariant. | [optional]
 **nameb_like** | **string**| Uses LIKE expression to check the Address.NameB for provided content. Case Invariant. | [optional]
 **namec_exact** | [**string[]**](../Model/string.md)| Checks the Address.NameC against the provided strings using exact match. | [optional]
 **namec_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.NameC against the provided strings using starts with. Case Invariant. | [optional]
 **namec_like** | **string**| Uses LIKE expression to check the Address.NameC for provided content. Case Invariant. | [optional]
 **street_number_exact** | [**string[]**](../Model/string.md)| Checks the Address.StreetNumber against the provided strings using exact match. | [optional]
 **street_number_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.StreetNumber against the provided strings using starts with. Case Invariant. | [optional]
 **street_number_like** | **string**| Uses LIKE expression to check the Address.StreetNumber for provided content. Case Invariant. | [optional]
 **zip_exact** | [**string[]**](../Model/string.md)| Checks the Address.PostalCode against the provided strings using exact match. | [optional]
 **zip_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.PostalCode against the provided strings using starts with. Case Invariant. | [optional]
 **zip_like** | **string**| Uses LIKE expression to check the Address.PostalCode for provided content. Case Invariant. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]
 **force_id** | **bool**| Enforces AND linking, regardless of chain-using-or, for &#39;*-id&#39; parameters. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddress**](../Model/SimplifySoftPecuniariusDataNetContactDataAddress.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsAllAddressesAllIdsGet()`

```php
contactdataContactsAllAddressesAllIdsGet($index, $limit, $address_id, $contact_id, $street_exact, $street_starts_with, $street_like, $town_exact, $town_starts_with, $town_like, $country_exact, $country_starts_with, $country_like, $namea_exact, $namea_starts_with, $namea_like, $nameb_exact, $nameb_starts_with, $nameb_like, $namec_exact, $namec_starts_with, $namec_like, $street_number_exact, $street_number_starts_with, $street_number_like, $zip_exact, $zip_starts_with, $zip_like, $chain_using_or, $force_id): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32
```

Allows polling the count of all addresses.

Allows polling the count of all addresses.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.
$address_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$contact_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$street_exact = array('street_exact_example'); // string[] | Checks the Address.Street against the provided strings using exact match.
$street_starts_with = array('street_starts_with_example'); // string[] | Checks the Address.Street against the provided strings using starts with. Case Invariant.
$street_like = 'street_like_example'; // string | Uses LIKE expression to check the Address.Street for provided content. Case Invariant.
$town_exact = array('town_exact_example'); // string[] | Checks the Address.Town against the provided strings using exact match.
$town_starts_with = array('town_starts_with_example'); // string[] | Checks the Address.Town against the provided strings using starts with. Case Invariant.
$town_like = 'town_like_example'; // string | Uses LIKE expression to check the Address.Town for provided content. Case Invariant.
$country_exact = array('country_exact_example'); // string[] | Checks the Address.Country against the provided strings using exact match.
$country_starts_with = array('country_starts_with_example'); // string[] | Checks the Address.Country against the provided strings using starts with. Case Invariant.
$country_like = 'country_like_example'; // string | Uses LIKE expression to check the Address.Country for provided content. Case Invariant.
$namea_exact = array('namea_exact_example'); // string[] | Checks the Address.NameA against the provided strings using exact match.
$namea_starts_with = array('namea_starts_with_example'); // string[] | Checks the Address.NameA against the provided strings using starts with. Case Invariant.
$namea_like = 'namea_like_example'; // string | Uses LIKE expression to check the Address.NameA for provided content. Case Invariant.
$nameb_exact = array('nameb_exact_example'); // string[] | Checks the Address.NameB against the provided strings using exact match.
$nameb_starts_with = array('nameb_starts_with_example'); // string[] | Checks the Address.NameB against the provided strings using starts with. Case Invariant.
$nameb_like = 'nameb_like_example'; // string | Uses LIKE expression to check the Address.NameB for provided content. Case Invariant.
$namec_exact = array('namec_exact_example'); // string[] | Checks the Address.NameC against the provided strings using exact match.
$namec_starts_with = array('namec_starts_with_example'); // string[] | Checks the Address.NameC against the provided strings using starts with. Case Invariant.
$namec_like = 'namec_like_example'; // string | Uses LIKE expression to check the Address.NameC for provided content. Case Invariant.
$street_number_exact = array('street_number_exact_example'); // string[] | Checks the Address.StreetNumber against the provided strings using exact match.
$street_number_starts_with = array('street_number_starts_with_example'); // string[] | Checks the Address.StreetNumber against the provided strings using starts with. Case Invariant.
$street_number_like = 'street_number_like_example'; // string | Uses LIKE expression to check the Address.StreetNumber for provided content. Case Invariant.
$zip_exact = array('zip_exact_example'); // string[] | Checks the Address.PostalCode against the provided strings using exact match.
$zip_starts_with = array('zip_starts_with_example'); // string[] | Checks the Address.PostalCode against the provided strings using starts with. Case Invariant.
$zip_like = 'zip_like_example'; // string | Uses LIKE expression to check the Address.PostalCode for provided content. Case Invariant.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.
$force_id = false; // bool | Enforces AND linking, regardless of chain-using-or, for '*-id' parameters.

try {
    $result = $apiInstance->contactdataContactsAllAddressesAllIdsGet($index, $limit, $address_id, $contact_id, $street_exact, $street_starts_with, $street_like, $town_exact, $town_starts_with, $town_like, $country_exact, $country_starts_with, $country_like, $namea_exact, $namea_starts_with, $namea_like, $nameb_exact, $nameb_starts_with, $nameb_like, $namec_exact, $namec_starts_with, $namec_like, $street_number_exact, $street_number_starts_with, $street_number_like, $zip_exact, $zip_starts_with, $zip_like, $chain_using_or, $force_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsAllAddressesAllIdsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **address_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **contact_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **street_exact** | [**string[]**](../Model/string.md)| Checks the Address.Street against the provided strings using exact match. | [optional]
 **street_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.Street against the provided strings using starts with. Case Invariant. | [optional]
 **street_like** | **string**| Uses LIKE expression to check the Address.Street for provided content. Case Invariant. | [optional]
 **town_exact** | [**string[]**](../Model/string.md)| Checks the Address.Town against the provided strings using exact match. | [optional]
 **town_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.Town against the provided strings using starts with. Case Invariant. | [optional]
 **town_like** | **string**| Uses LIKE expression to check the Address.Town for provided content. Case Invariant. | [optional]
 **country_exact** | [**string[]**](../Model/string.md)| Checks the Address.Country against the provided strings using exact match. | [optional]
 **country_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.Country against the provided strings using starts with. Case Invariant. | [optional]
 **country_like** | **string**| Uses LIKE expression to check the Address.Country for provided content. Case Invariant. | [optional]
 **namea_exact** | [**string[]**](../Model/string.md)| Checks the Address.NameA against the provided strings using exact match. | [optional]
 **namea_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.NameA against the provided strings using starts with. Case Invariant. | [optional]
 **namea_like** | **string**| Uses LIKE expression to check the Address.NameA for provided content. Case Invariant. | [optional]
 **nameb_exact** | [**string[]**](../Model/string.md)| Checks the Address.NameB against the provided strings using exact match. | [optional]
 **nameb_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.NameB against the provided strings using starts with. Case Invariant. | [optional]
 **nameb_like** | **string**| Uses LIKE expression to check the Address.NameB for provided content. Case Invariant. | [optional]
 **namec_exact** | [**string[]**](../Model/string.md)| Checks the Address.NameC against the provided strings using exact match. | [optional]
 **namec_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.NameC against the provided strings using starts with. Case Invariant. | [optional]
 **namec_like** | **string**| Uses LIKE expression to check the Address.NameC for provided content. Case Invariant. | [optional]
 **street_number_exact** | [**string[]**](../Model/string.md)| Checks the Address.StreetNumber against the provided strings using exact match. | [optional]
 **street_number_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.StreetNumber against the provided strings using starts with. Case Invariant. | [optional]
 **street_number_like** | **string**| Uses LIKE expression to check the Address.StreetNumber for provided content. Case Invariant. | [optional]
 **zip_exact** | [**string[]**](../Model/string.md)| Checks the Address.PostalCode against the provided strings using exact match. | [optional]
 **zip_starts_with** | [**string[]**](../Model/string.md)| Checks the Address.PostalCode against the provided strings using starts with. Case Invariant. | [optional]
 **zip_like** | **string**| Uses LIKE expression to check the Address.PostalCode for provided content. Case Invariant. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]
 **force_id** | **bool**| Enforces AND linking, regardless of chain-using-or, for &#39;*-id&#39; parameters. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32**](../Model/SimplifySoftPecuniariusDataNetPrimitivesNetInt32.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsAllCountGet()`

```php
contactdataContactsAllCountGet($limit, $index, $identifier_exact, $identifier_starts_with, $identifier_like, $addresses_street_exact, $addresses_street_starts_with, $addresses_street_like, $addresses_town_exact, $addresses_town_starts_with, $addresses_town_like, $addresses_country_exact, $addresses_country_starts_with, $addresses_country_like, $addresses_namea_exact, $addresses_namea_starts_with, $addresses_namea_like, $addresses_nameb_exact, $addresses_nameb_starts_with, $addresses_nameb_like, $addresses_namec_exact, $addresses_namec_starts_with, $addresses_namec_like, $contact_id, $tag_id, $price_group_id, $include_addresses, $include_kind, $chain_using_or)
```

Allows to return a range of contacts, filtered according to provided GET parameters.

Allows to return a range of contacts, filtered according to provided GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$identifier_exact = array('identifier_exact_example'); // string[] | Checks the identifier against the provided strings using exact match.
$identifier_starts_with = array('identifier_starts_with_example'); // string[] | Checks the identifier against the provided strings using starts with. Case Invariant.
$identifier_like = 'identifier_like_example'; // string | Uses LIKE expression to check the identifiers for provided content. Case Invariant.
$addresses_street_exact = array('addresses_street_exact_example'); // string[] | Checks the Address[].Street against the provided strings using exact match.
$addresses_street_starts_with = array('addresses_street_starts_with_example'); // string[] | Checks the Address[].Street against the provided strings using starts with. Case Invariant.
$addresses_street_like = 'addresses_street_like_example'; // string | Uses LIKE expression to check the Address[].Street for provided content. Case Invariant.
$addresses_town_exact = array('addresses_town_exact_example'); // string[] | Checks the Address[].Town against the provided strings using exact match.
$addresses_town_starts_with = array('addresses_town_starts_with_example'); // string[] | Checks the Address[].Town against the provided strings using starts with. Case Invariant.
$addresses_town_like = 'addresses_town_like_example'; // string | Uses LIKE expression to check the Address[].Town for provided content. Case Invariant.
$addresses_country_exact = array('addresses_country_exact_example'); // string[] | Checks the Address[].Country against the provided strings using exact match.
$addresses_country_starts_with = array('addresses_country_starts_with_example'); // string[] | Checks the Address[].Country against the provided strings using starts with. Case Invariant.
$addresses_country_like = 'addresses_country_like_example'; // string | Uses LIKE expression to check the Address[].Country for provided content. Case Invariant.
$addresses_namea_exact = array('addresses_namea_exact_example'); // string[] | Checks the Address[].NameA against the provided strings using exact match.
$addresses_namea_starts_with = array('addresses_namea_starts_with_example'); // string[] | Checks the Address[].NameA against the provided strings using starts with. Case Invariant.
$addresses_namea_like = 'addresses_namea_like_example'; // string | Uses LIKE expression to check the Address[].NameA for provided content. Case Invariant.
$addresses_nameb_exact = array('addresses_nameb_exact_example'); // string[] | Checks the Address[].NameB against the provided strings using exact match.
$addresses_nameb_starts_with = array('addresses_nameb_starts_with_example'); // string[] | Checks the Address[].NameB against the provided strings using starts with. Case Invariant.
$addresses_nameb_like = 'addresses_nameb_like_example'; // string | Uses LIKE expression to check the Address[].NameB for provided content. Case Invariant.
$addresses_namec_exact = array('addresses_namec_exact_example'); // string[] | Checks the Address[].NameC against the provided strings using exact match.
$addresses_namec_starts_with = array('addresses_namec_starts_with_example'); // string[] | Checks the Address[].NameC against the provided strings using starts with. Case Invariant.
$addresses_namec_like = 'addresses_namec_like_example'; // string | Uses LIKE expression to check the Address[].NameC for provided content. Case Invariant.
$contact_id = array(56); // int[] | Compares against Contact.Id.
$tag_id = array(56); // int[] | Allows to filter for tags on the different contacts.
$price_group_id = array(56); // int[] | Checks the provided price groups in the list with the contacts.
$include_addresses = false; // bool | Wether to include Contact.Addresses or not.
$include_kind = false; // bool | Wether to include Contact.Kind or not.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $apiInstance->contactdataContactsAllCountGet($limit, $index, $identifier_exact, $identifier_starts_with, $identifier_like, $addresses_street_exact, $addresses_street_starts_with, $addresses_street_like, $addresses_town_exact, $addresses_town_starts_with, $addresses_town_like, $addresses_country_exact, $addresses_country_starts_with, $addresses_country_like, $addresses_namea_exact, $addresses_namea_starts_with, $addresses_namea_like, $addresses_nameb_exact, $addresses_nameb_starts_with, $addresses_nameb_like, $addresses_namec_exact, $addresses_namec_starts_with, $addresses_namec_like, $contact_id, $tag_id, $price_group_id, $include_addresses, $include_kind, $chain_using_or);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **identifier_exact** | [**string[]**](../Model/string.md)| Checks the identifier against the provided strings using exact match. | [optional]
 **identifier_starts_with** | [**string[]**](../Model/string.md)| Checks the identifier against the provided strings using starts with. Case Invariant. | [optional]
 **identifier_like** | **string**| Uses LIKE expression to check the identifiers for provided content. Case Invariant. | [optional]
 **addresses_street_exact** | [**string[]**](../Model/string.md)| Checks the Address[].Street against the provided strings using exact match. | [optional]
 **addresses_street_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].Street against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_street_like** | **string**| Uses LIKE expression to check the Address[].Street for provided content. Case Invariant. | [optional]
 **addresses_town_exact** | [**string[]**](../Model/string.md)| Checks the Address[].Town against the provided strings using exact match. | [optional]
 **addresses_town_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].Town against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_town_like** | **string**| Uses LIKE expression to check the Address[].Town for provided content. Case Invariant. | [optional]
 **addresses_country_exact** | [**string[]**](../Model/string.md)| Checks the Address[].Country against the provided strings using exact match. | [optional]
 **addresses_country_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].Country against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_country_like** | **string**| Uses LIKE expression to check the Address[].Country for provided content. Case Invariant. | [optional]
 **addresses_namea_exact** | [**string[]**](../Model/string.md)| Checks the Address[].NameA against the provided strings using exact match. | [optional]
 **addresses_namea_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].NameA against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_namea_like** | **string**| Uses LIKE expression to check the Address[].NameA for provided content. Case Invariant. | [optional]
 **addresses_nameb_exact** | [**string[]**](../Model/string.md)| Checks the Address[].NameB against the provided strings using exact match. | [optional]
 **addresses_nameb_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].NameB against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_nameb_like** | **string**| Uses LIKE expression to check the Address[].NameB for provided content. Case Invariant. | [optional]
 **addresses_namec_exact** | [**string[]**](../Model/string.md)| Checks the Address[].NameC against the provided strings using exact match. | [optional]
 **addresses_namec_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].NameC against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_namec_like** | **string**| Uses LIKE expression to check the Address[].NameC for provided content. Case Invariant. | [optional]
 **contact_id** | [**int[]**](../Model/int.md)| Compares against Contact.Id. | [optional]
 **tag_id** | [**int[]**](../Model/int.md)| Allows to filter for tags on the different contacts. | [optional]
 **price_group_id** | [**int[]**](../Model/int.md)| Checks the provided price groups in the list with the contacts. | [optional]
 **include_addresses** | **bool**| Wether to include Contact.Addresses or not. | [optional] [default to false]
 **include_kind** | **bool**| Wether to include Contact.Kind or not. | [optional] [default to false]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsAllGet()`

```php
contactdataContactsAllGet($limit, $index, $identifier_exact, $identifier_starts_with, $identifier_like, $addresses_street_exact, $addresses_street_starts_with, $addresses_street_like, $addresses_town_exact, $addresses_town_starts_with, $addresses_town_like, $addresses_country_exact, $addresses_country_starts_with, $addresses_country_like, $addresses_namea_exact, $addresses_namea_starts_with, $addresses_namea_like, $addresses_nameb_exact, $addresses_nameb_starts_with, $addresses_nameb_like, $addresses_namec_exact, $addresses_namec_starts_with, $addresses_namec_like, $contact_id, $tag_id, $price_group_id, $include_addresses, $include_kind, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContact
```

Allows to return a range of contacts, filtered according to provided GET parameters.

Allows to return a range of contacts, filtered according to provided GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$identifier_exact = array('identifier_exact_example'); // string[] | Checks the identifier against the provided strings using exact match.
$identifier_starts_with = array('identifier_starts_with_example'); // string[] | Checks the identifier against the provided strings using starts with. Case Invariant.
$identifier_like = 'identifier_like_example'; // string | Uses LIKE expression to check the identifiers for provided content. Case Invariant.
$addresses_street_exact = array('addresses_street_exact_example'); // string[] | Checks the Address[].Street against the provided strings using exact match.
$addresses_street_starts_with = array('addresses_street_starts_with_example'); // string[] | Checks the Address[].Street against the provided strings using starts with. Case Invariant.
$addresses_street_like = 'addresses_street_like_example'; // string | Uses LIKE expression to check the Address[].Street for provided content. Case Invariant.
$addresses_town_exact = array('addresses_town_exact_example'); // string[] | Checks the Address[].Town against the provided strings using exact match.
$addresses_town_starts_with = array('addresses_town_starts_with_example'); // string[] | Checks the Address[].Town against the provided strings using starts with. Case Invariant.
$addresses_town_like = 'addresses_town_like_example'; // string | Uses LIKE expression to check the Address[].Town for provided content. Case Invariant.
$addresses_country_exact = array('addresses_country_exact_example'); // string[] | Checks the Address[].Country against the provided strings using exact match.
$addresses_country_starts_with = array('addresses_country_starts_with_example'); // string[] | Checks the Address[].Country against the provided strings using starts with. Case Invariant.
$addresses_country_like = 'addresses_country_like_example'; // string | Uses LIKE expression to check the Address[].Country for provided content. Case Invariant.
$addresses_namea_exact = array('addresses_namea_exact_example'); // string[] | Checks the Address[].NameA against the provided strings using exact match.
$addresses_namea_starts_with = array('addresses_namea_starts_with_example'); // string[] | Checks the Address[].NameA against the provided strings using starts with. Case Invariant.
$addresses_namea_like = 'addresses_namea_like_example'; // string | Uses LIKE expression to check the Address[].NameA for provided content. Case Invariant.
$addresses_nameb_exact = array('addresses_nameb_exact_example'); // string[] | Checks the Address[].NameB against the provided strings using exact match.
$addresses_nameb_starts_with = array('addresses_nameb_starts_with_example'); // string[] | Checks the Address[].NameB against the provided strings using starts with. Case Invariant.
$addresses_nameb_like = 'addresses_nameb_like_example'; // string | Uses LIKE expression to check the Address[].NameB for provided content. Case Invariant.
$addresses_namec_exact = array('addresses_namec_exact_example'); // string[] | Checks the Address[].NameC against the provided strings using exact match.
$addresses_namec_starts_with = array('addresses_namec_starts_with_example'); // string[] | Checks the Address[].NameC against the provided strings using starts with. Case Invariant.
$addresses_namec_like = 'addresses_namec_like_example'; // string | Uses LIKE expression to check the Address[].NameC for provided content. Case Invariant.
$contact_id = array(56); // int[] | Compares against Contact.Id.
$tag_id = array(56); // int[] | Allows to filter for tags on the different contacts.
$price_group_id = array(56); // int[] | Checks the provided price groups in the list with the contacts.
$include_addresses = false; // bool | Wether to include Contact.Addresses or not.
$include_kind = false; // bool | Wether to include Contact.Kind or not.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->contactdataContactsAllGet($limit, $index, $identifier_exact, $identifier_starts_with, $identifier_like, $addresses_street_exact, $addresses_street_starts_with, $addresses_street_like, $addresses_town_exact, $addresses_town_starts_with, $addresses_town_like, $addresses_country_exact, $addresses_country_starts_with, $addresses_country_like, $addresses_namea_exact, $addresses_namea_starts_with, $addresses_namea_like, $addresses_nameb_exact, $addresses_nameb_starts_with, $addresses_nameb_like, $addresses_namec_exact, $addresses_namec_starts_with, $addresses_namec_like, $contact_id, $tag_id, $price_group_id, $include_addresses, $include_kind, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **identifier_exact** | [**string[]**](../Model/string.md)| Checks the identifier against the provided strings using exact match. | [optional]
 **identifier_starts_with** | [**string[]**](../Model/string.md)| Checks the identifier against the provided strings using starts with. Case Invariant. | [optional]
 **identifier_like** | **string**| Uses LIKE expression to check the identifiers for provided content. Case Invariant. | [optional]
 **addresses_street_exact** | [**string[]**](../Model/string.md)| Checks the Address[].Street against the provided strings using exact match. | [optional]
 **addresses_street_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].Street against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_street_like** | **string**| Uses LIKE expression to check the Address[].Street for provided content. Case Invariant. | [optional]
 **addresses_town_exact** | [**string[]**](../Model/string.md)| Checks the Address[].Town against the provided strings using exact match. | [optional]
 **addresses_town_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].Town against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_town_like** | **string**| Uses LIKE expression to check the Address[].Town for provided content. Case Invariant. | [optional]
 **addresses_country_exact** | [**string[]**](../Model/string.md)| Checks the Address[].Country against the provided strings using exact match. | [optional]
 **addresses_country_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].Country against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_country_like** | **string**| Uses LIKE expression to check the Address[].Country for provided content. Case Invariant. | [optional]
 **addresses_namea_exact** | [**string[]**](../Model/string.md)| Checks the Address[].NameA against the provided strings using exact match. | [optional]
 **addresses_namea_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].NameA against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_namea_like** | **string**| Uses LIKE expression to check the Address[].NameA for provided content. Case Invariant. | [optional]
 **addresses_nameb_exact** | [**string[]**](../Model/string.md)| Checks the Address[].NameB against the provided strings using exact match. | [optional]
 **addresses_nameb_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].NameB against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_nameb_like** | **string**| Uses LIKE expression to check the Address[].NameB for provided content. Case Invariant. | [optional]
 **addresses_namec_exact** | [**string[]**](../Model/string.md)| Checks the Address[].NameC against the provided strings using exact match. | [optional]
 **addresses_namec_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].NameC against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_namec_like** | **string**| Uses LIKE expression to check the Address[].NameC for provided content. Case Invariant. | [optional]
 **contact_id** | [**int[]**](../Model/int.md)| Compares against Contact.Id. | [optional]
 **tag_id** | [**int[]**](../Model/int.md)| Allows to filter for tags on the different contacts. | [optional]
 **price_group_id** | [**int[]**](../Model/int.md)| Checks the provided price groups in the list with the contacts. | [optional]
 **include_addresses** | **bool**| Wether to include Contact.Addresses or not. | [optional] [default to false]
 **include_kind** | **bool**| Wether to include Contact.Kind or not. | [optional] [default to false]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContact**](../Model/SimplifySoftPecuniariusDataNetContactDataContact.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsAllIdsGet()`

```php
contactdataContactsAllIdsGet($limit, $index, $identifier_exact, $identifier_starts_with, $identifier_like, $addresses_street_exact, $addresses_street_starts_with, $addresses_street_like, $addresses_town_exact, $addresses_town_starts_with, $addresses_town_like, $addresses_country_exact, $addresses_country_starts_with, $addresses_country_like, $addresses_namea_exact, $addresses_namea_starts_with, $addresses_namea_like, $addresses_nameb_exact, $addresses_nameb_starts_with, $addresses_nameb_like, $addresses_namec_exact, $addresses_namec_starts_with, $addresses_namec_like, $contact_id, $tag_id, $price_group_id, $include_addresses, $include_kind, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32
```

Allows to return a range of contacts, filtered according to provided GET parameters.

Allows to return a range of contacts, filtered according to provided GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$identifier_exact = array('identifier_exact_example'); // string[] | Checks the identifier against the provided strings using exact match.
$identifier_starts_with = array('identifier_starts_with_example'); // string[] | Checks the identifier against the provided strings using starts with. Case Invariant.
$identifier_like = 'identifier_like_example'; // string | Uses LIKE expression to check the identifiers for provided content. Case Invariant.
$addresses_street_exact = array('addresses_street_exact_example'); // string[] | Checks the Address[].Street against the provided strings using exact match.
$addresses_street_starts_with = array('addresses_street_starts_with_example'); // string[] | Checks the Address[].Street against the provided strings using starts with. Case Invariant.
$addresses_street_like = 'addresses_street_like_example'; // string | Uses LIKE expression to check the Address[].Street for provided content. Case Invariant.
$addresses_town_exact = array('addresses_town_exact_example'); // string[] | Checks the Address[].Town against the provided strings using exact match.
$addresses_town_starts_with = array('addresses_town_starts_with_example'); // string[] | Checks the Address[].Town against the provided strings using starts with. Case Invariant.
$addresses_town_like = 'addresses_town_like_example'; // string | Uses LIKE expression to check the Address[].Town for provided content. Case Invariant.
$addresses_country_exact = array('addresses_country_exact_example'); // string[] | Checks the Address[].Country against the provided strings using exact match.
$addresses_country_starts_with = array('addresses_country_starts_with_example'); // string[] | Checks the Address[].Country against the provided strings using starts with. Case Invariant.
$addresses_country_like = 'addresses_country_like_example'; // string | Uses LIKE expression to check the Address[].Country for provided content. Case Invariant.
$addresses_namea_exact = array('addresses_namea_exact_example'); // string[] | Checks the Address[].NameA against the provided strings using exact match.
$addresses_namea_starts_with = array('addresses_namea_starts_with_example'); // string[] | Checks the Address[].NameA against the provided strings using starts with. Case Invariant.
$addresses_namea_like = 'addresses_namea_like_example'; // string | Uses LIKE expression to check the Address[].NameA for provided content. Case Invariant.
$addresses_nameb_exact = array('addresses_nameb_exact_example'); // string[] | Checks the Address[].NameB against the provided strings using exact match.
$addresses_nameb_starts_with = array('addresses_nameb_starts_with_example'); // string[] | Checks the Address[].NameB against the provided strings using starts with. Case Invariant.
$addresses_nameb_like = 'addresses_nameb_like_example'; // string | Uses LIKE expression to check the Address[].NameB for provided content. Case Invariant.
$addresses_namec_exact = array('addresses_namec_exact_example'); // string[] | Checks the Address[].NameC against the provided strings using exact match.
$addresses_namec_starts_with = array('addresses_namec_starts_with_example'); // string[] | Checks the Address[].NameC against the provided strings using starts with. Case Invariant.
$addresses_namec_like = 'addresses_namec_like_example'; // string | Uses LIKE expression to check the Address[].NameC for provided content. Case Invariant.
$contact_id = array(56); // int[] | Compares against Contact.Id.
$tag_id = array(56); // int[] | Allows to filter for tags on the different contacts.
$price_group_id = array(56); // int[] | Checks the provided price groups in the list with the contacts.
$include_addresses = false; // bool | Wether to include Contact.Addresses or not.
$include_kind = false; // bool | Wether to include Contact.Kind or not.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->contactdataContactsAllIdsGet($limit, $index, $identifier_exact, $identifier_starts_with, $identifier_like, $addresses_street_exact, $addresses_street_starts_with, $addresses_street_like, $addresses_town_exact, $addresses_town_starts_with, $addresses_town_like, $addresses_country_exact, $addresses_country_starts_with, $addresses_country_like, $addresses_namea_exact, $addresses_namea_starts_with, $addresses_namea_like, $addresses_nameb_exact, $addresses_nameb_starts_with, $addresses_nameb_like, $addresses_namec_exact, $addresses_namec_starts_with, $addresses_namec_like, $contact_id, $tag_id, $price_group_id, $include_addresses, $include_kind, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsAllIdsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **identifier_exact** | [**string[]**](../Model/string.md)| Checks the identifier against the provided strings using exact match. | [optional]
 **identifier_starts_with** | [**string[]**](../Model/string.md)| Checks the identifier against the provided strings using starts with. Case Invariant. | [optional]
 **identifier_like** | **string**| Uses LIKE expression to check the identifiers for provided content. Case Invariant. | [optional]
 **addresses_street_exact** | [**string[]**](../Model/string.md)| Checks the Address[].Street against the provided strings using exact match. | [optional]
 **addresses_street_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].Street against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_street_like** | **string**| Uses LIKE expression to check the Address[].Street for provided content. Case Invariant. | [optional]
 **addresses_town_exact** | [**string[]**](../Model/string.md)| Checks the Address[].Town against the provided strings using exact match. | [optional]
 **addresses_town_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].Town against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_town_like** | **string**| Uses LIKE expression to check the Address[].Town for provided content. Case Invariant. | [optional]
 **addresses_country_exact** | [**string[]**](../Model/string.md)| Checks the Address[].Country against the provided strings using exact match. | [optional]
 **addresses_country_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].Country against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_country_like** | **string**| Uses LIKE expression to check the Address[].Country for provided content. Case Invariant. | [optional]
 **addresses_namea_exact** | [**string[]**](../Model/string.md)| Checks the Address[].NameA against the provided strings using exact match. | [optional]
 **addresses_namea_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].NameA against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_namea_like** | **string**| Uses LIKE expression to check the Address[].NameA for provided content. Case Invariant. | [optional]
 **addresses_nameb_exact** | [**string[]**](../Model/string.md)| Checks the Address[].NameB against the provided strings using exact match. | [optional]
 **addresses_nameb_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].NameB against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_nameb_like** | **string**| Uses LIKE expression to check the Address[].NameB for provided content. Case Invariant. | [optional]
 **addresses_namec_exact** | [**string[]**](../Model/string.md)| Checks the Address[].NameC against the provided strings using exact match. | [optional]
 **addresses_namec_starts_with** | [**string[]**](../Model/string.md)| Checks the Address[].NameC against the provided strings using starts with. Case Invariant. | [optional]
 **addresses_namec_like** | **string**| Uses LIKE expression to check the Address[].NameC for provided content. Case Invariant. | [optional]
 **contact_id** | [**int[]**](../Model/int.md)| Compares against Contact.Id. | [optional]
 **tag_id** | [**int[]**](../Model/int.md)| Allows to filter for tags on the different contacts. | [optional]
 **price_group_id** | [**int[]**](../Model/int.md)| Checks the provided price groups in the list with the contacts. | [optional]
 **include_addresses** | **bool**| Wether to include Contact.Addresses or not. | [optional] [default to false]
 **include_kind** | **bool**| Wether to include Contact.Kind or not. | [optional] [default to false]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32**](../Model/SimplifySoftPecuniariusDataNetPrimitivesNetInt32.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsContactpossibilitiesIdGet()`

```php
contactdataContactsContactpossibilitiesIdGet($id)
```

Returns requested contactpossibility.

Returns requested contactpossibility.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->contactdataContactsContactpossibilitiesIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsContactpossibilitiesIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsIdTagsGet()`

```php
contactdataContactsIdTagsGet($id): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTag
```

Receives the tags this contact is linked with via its ID.

Receives the tags this contact is linked with via its ID.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $result = $apiInstance->contactdataContactsIdTagsGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsIdTagsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTag**](../Model/SimplifySoftPecuniariusDataNetTag.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsIdTagsTagidDelete()`

```php
contactdataContactsIdTagsTagidDelete($id, $tagid)
```

Allows to remove a tag relation from a contact via its ID.

Allows to remove a tag relation from a contact via its ID.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$tagid = 56; // int | unavailable

try {
    $apiInstance->contactdataContactsIdTagsTagidDelete($id, $tagid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsIdTagsTagidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **tagid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsIdTagsTagidPost()`

```php
contactdataContactsIdTagsTagidPost($id, $tagid)
```

Allows to add a tag relation onto a contact via its ID.

Allows to add a tag relation onto a contact via its ID.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$tagid = 56; // int | unavailable

try {
    $apiInstance->contactdataContactsIdTagsTagidPost($id, $tagid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsIdTagsTagidPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **tagid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsIdentAddressesAddidDelete()`

```php
contactdataContactsIdentAddressesAddidDelete($ident, $addid, $forceident)
```

Allows to remove a single address from a given contact.

Allows to remove a single address from a given contact.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$addid = 56; // int | unavailable
$forceident = True; // bool | Allows to force to use the Identifier. Required if the Identifier is all numeric.

try {
    $apiInstance->contactdataContactsIdentAddressesAddidDelete($ident, $addid, $forceident);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsIdentAddressesAddidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **addid** | **int**| unavailable |
 **forceident** | **bool**| Allows to force to use the Identifier. Required if the Identifier is all numeric. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsIdentAddressesAddidPut()`

```php
contactdataContactsIdentAddressesAddidPut($ident, $addid, $forceident, $unknown_base_type)
```

Provides an interface to update a single address on a contact.

Provides an interface to update a single address on a contact.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$addid = 56; // int | unavailable
$forceident = True; // bool | Allows to force to use the Identifier. Required if the Identifier is all numeric.
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->contactdataContactsIdentAddressesAddidPut($ident, $addid, $forceident, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsIdentAddressesAddidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **addid** | **int**| unavailable |
 **forceident** | **bool**| Allows to force to use the Identifier. Required if the Identifier is all numeric. | [optional]
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsIdentAddressesGet()`

```php
contactdataContactsIdentAddressesGet($ident, $forceident): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddress
```

Returns a list of addresses for given contact.

Returns a list of addresses for given contact.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$forceident = True; // bool | Allows to force to use the Identifier. Required if the Identifier is all numeric.

try {
    $result = $apiInstance->contactdataContactsIdentAddressesGet($ident, $forceident);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsIdentAddressesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **forceident** | **bool**| Allows to force to use the Identifier. Required if the Identifier is all numeric. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddress**](../Model/SimplifySoftPecuniariusDataNetContactDataAddress.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsIdentAddressesPost()`

```php
contactdataContactsIdentAddressesPost($ident, $forceident, $simplify_soft_pecuniarius_data_net_contact_data_address): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddress
```

Allows to create new addresses in given contact.

Allows to create new addresses in given contact.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$forceident = True; // bool | Allows to force to use the Identifier. Required if the Identifier is all numeric.
$simplify_soft_pecuniarius_data_net_contact_data_address = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddress(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddress

try {
    $result = $apiInstance->contactdataContactsIdentAddressesPost($ident, $forceident, $simplify_soft_pecuniarius_data_net_contact_data_address);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsIdentAddressesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **forceident** | **bool**| Allows to force to use the Identifier. Required if the Identifier is all numeric. | [optional]
 **simplify_soft_pecuniarius_data_net_contact_data_address** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddress**](../Model/SimplifySoftPecuniariusDataNetContactDataAddress.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddress**](../Model/SimplifySoftPecuniariusDataNetContactDataAddress.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsIdentContactpossibilitiesCpidDelete()`

```php
contactdataContactsIdentContactpossibilitiesCpidDelete($ident, $cpid, $forceident)
```

Allows to remove a single contact possibility from a given contact.

Allows to remove a single contact possibility from a given contact.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$cpid = 56; // int | unavailable
$forceident = True; // bool | Allows to force to use the Identifier. Required if the Identifier is all numeric.

try {
    $apiInstance->contactdataContactsIdentContactpossibilitiesCpidDelete($ident, $cpid, $forceident);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsIdentContactpossibilitiesCpidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **cpid** | **int**| unavailable |
 **forceident** | **bool**| Allows to force to use the Identifier. Required if the Identifier is all numeric. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsIdentContactpossibilitiesCpidPut()`

```php
contactdataContactsIdentContactpossibilitiesCpidPut($ident, $cpid, $forceident, $unknown_base_type)
```

Provides an interface to update a single contact possibility on a contact.

Provides an interface to update a single contact possibility on a contact.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$cpid = 56; // int | unavailable
$forceident = True; // bool | Allows to force to use the Identifier. Required if the Identifier is all numeric.
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->contactdataContactsIdentContactpossibilitiesCpidPut($ident, $cpid, $forceident, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsIdentContactpossibilitiesCpidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **cpid** | **int**| unavailable |
 **forceident** | **bool**| Allows to force to use the Identifier. Required if the Identifier is all numeric. | [optional]
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsIdentContactpossibilitiesGet()`

```php
contactdataContactsIdentContactpossibilitiesGet($ident, $forceident): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContactPossibility
```

Returns a list of contact possibilities for given contact.

Returns a list of contact possibilities for given contact.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$forceident = True; // bool | Allows to force to use the Identifier. Required if the Identifier is all numeric.

try {
    $result = $apiInstance->contactdataContactsIdentContactpossibilitiesGet($ident, $forceident);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsIdentContactpossibilitiesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **forceident** | **bool**| Allows to force to use the Identifier. Required if the Identifier is all numeric. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContactPossibility**](../Model/SimplifySoftPecuniariusDataNetContactDataContactPossibility.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsIdentContactpossibilitiesPost()`

```php
contactdataContactsIdentContactpossibilitiesPost($ident, $forceident, $simplify_soft_pecuniarius_data_net_contact_data_contact_possibility): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContactPossibility
```

Allows to create new contact possibility in given contact.

Allows to create new contact possibility in given contact.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$forceident = True; // bool | Allows to force to use the Identifier. Required if the Identifier is all numeric.
$simplify_soft_pecuniarius_data_net_contact_data_contact_possibility = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContactPossibility(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContactPossibility

try {
    $result = $apiInstance->contactdataContactsIdentContactpossibilitiesPost($ident, $forceident, $simplify_soft_pecuniarius_data_net_contact_data_contact_possibility);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsIdentContactpossibilitiesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **forceident** | **bool**| Allows to force to use the Identifier. Required if the Identifier is all numeric. | [optional]
 **simplify_soft_pecuniarius_data_net_contact_data_contact_possibility** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContactPossibility**](../Model/SimplifySoftPecuniariusDataNetContactDataContactPossibility.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContactPossibility**](../Model/SimplifySoftPecuniariusDataNetContactDataContactPossibility.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsIdentGet()`

```php
contactdataContactsIdentGet($ident, $forceident)
```

Returns a single contact.

Returns a single contact.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$forceident = True; // bool | Allows to force to use the Identifier. Required if the Identifier is all numeric.

try {
    $apiInstance->contactdataContactsIdentGet($ident, $forceident);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsIdentGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **forceident** | **bool**| Allows to force to use the Identifier. Required if the Identifier is all numeric. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsIdentPut()`

```php
contactdataContactsIdentPut($ident, $forceident, $unknown_base_type)
```

Provides an interface to update a single contact.

Provides an interface to update a single contact.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$forceident = True; // bool | Allows to force to use the Identifier. Required if the Identifier is all numeric.
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->contactdataContactsIdentPut($ident, $forceident, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsIdentPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **forceident** | **bool**| Allows to force to use the Identifier. Required if the Identifier is all numeric. | [optional]
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataContactsPost()`

```php
contactdataContactsPost($genident, $simplify_soft_pecuniarius_data_net_contact_data_contact): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContact
```

Allows to create new contacts.

Allows to create new contacts.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$genident = true; // bool | Allows to change wether or not Identifiers should be generated by the system.
$simplify_soft_pecuniarius_data_net_contact_data_contact = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContact(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContact

try {
    $result = $apiInstance->contactdataContactsPost($genident, $simplify_soft_pecuniarius_data_net_contact_data_contact);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactsApi->contactdataContactsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **genident** | **bool**| Allows to change wether or not Identifiers should be generated by the system. | [optional] [default to true]
 **simplify_soft_pecuniarius_data_net_contact_data_contact** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContact**](../Model/SimplifySoftPecuniariusDataNetContactDataContact.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContact**](../Model/SimplifySoftPecuniariusDataNetContactDataContact.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
