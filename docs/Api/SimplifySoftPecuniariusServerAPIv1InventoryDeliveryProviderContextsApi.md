# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**warehousedataDeliveryproviderDeliveryOptionIdGet()**](SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi.md#warehousedataDeliveryproviderDeliveryOptionIdGet) | **GET** /warehousedata/deliveryprovider/{deliveryOptionId} | Allows to receive the Provider of a single DeliveryOption.
[**warehousedataDeliveryproviderDeliveryOptionIdIdDelete()**](SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi.md#warehousedataDeliveryproviderDeliveryOptionIdIdDelete) | **DELETE** /warehousedata/deliveryprovider/{deliveryOptionId}/{id} | Removes a DeliveryProviderContext from a DeliveryOption.
[**warehousedataDeliveryproviderDeliveryOptionIdIdPropertyPost()**](SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi.md#warehousedataDeliveryproviderDeliveryOptionIdIdPropertyPost) | **POST** /warehousedata/deliveryprovider/{deliveryOptionId}/{id}/{property} | Allows to change the CustomProperty values of a DeliveryProviderContext.
[**warehousedataDeliveryproviderDeliveryOptionIdIdPut()**](SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi.md#warehousedataDeliveryproviderDeliveryOptionIdIdPut) | **PUT** /warehousedata/deliveryprovider/{deliveryOptionId}/{id} | Allows to update a DeliveryProviderContext without replacing it.
[**warehousedataDeliveryproviderDeliveryOptionIdPost()**](SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi.md#warehousedataDeliveryproviderDeliveryOptionIdPost) | **POST** /warehousedata/deliveryprovider/{deliveryOptionId} | Replaces the existing DeliveryProviderContext with the new one rovided.
[**warehousedataDeliveryproviderEndpointsGet()**](SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi.md#warehousedataDeliveryproviderEndpointsGet) | **GET** /warehousedata/deliveryprovider/endpoints | Returns a list of all endpoints available.


## `warehousedataDeliveryproviderDeliveryOptionIdGet()`

```php
warehousedataDeliveryproviderDeliveryOptionIdGet($delivery_option_id, $limit, $index): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryDeliveryProviderContext
```

Allows to receive the Provider of a single DeliveryOption.

Allows to receive the Provider of a single DeliveryOption.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$delivery_option_id = 56; // int | unavailable
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.

try {
    $result = $apiInstance->warehousedataDeliveryproviderDeliveryOptionIdGet($delivery_option_id, $limit, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi->warehousedataDeliveryproviderDeliveryOptionIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delivery_option_id** | **int**| unavailable |
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryDeliveryProviderContext**](../Model/SimplifySoftPecuniariusDataNetInventoryDeliveryProviderContext.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `warehousedataDeliveryproviderDeliveryOptionIdIdDelete()`

```php
warehousedataDeliveryproviderDeliveryOptionIdIdDelete($delivery_option_id, $id)
```

Removes a DeliveryProviderContext from a DeliveryOption.

Removes a DeliveryProviderContext from a DeliveryOption.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$delivery_option_id = 56; // int | unavailable
$id = 56; // int | unavailable

try {
    $apiInstance->warehousedataDeliveryproviderDeliveryOptionIdIdDelete($delivery_option_id, $id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi->warehousedataDeliveryproviderDeliveryOptionIdIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delivery_option_id** | **int**| unavailable |
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `warehousedataDeliveryproviderDeliveryOptionIdIdPropertyPost()`

```php
warehousedataDeliveryproviderDeliveryOptionIdIdPropertyPost($delivery_option_id, $id, $property, $string, $int32, $int64, $double, $boolean)
```

Allows to change the CustomProperty values of a DeliveryProviderContext.

Allows to change the CustomProperty values of a DeliveryProviderContext.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$delivery_option_id = 56; // int | unavailable
$id = 56; // int | unavailable
$property = 'property_example'; // string | unavailable
$string = 'string_example'; // string | The string value to set.
$int32 = 56; // int | The int value to set.
$int64 = 56; // int | The long value to set.
$double = 3.4; // double | The double value to set.
$boolean = True; // bool | The boolean value to set.

try {
    $apiInstance->warehousedataDeliveryproviderDeliveryOptionIdIdPropertyPost($delivery_option_id, $id, $property, $string, $int32, $int64, $double, $boolean);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi->warehousedataDeliveryproviderDeliveryOptionIdIdPropertyPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delivery_option_id** | **int**| unavailable |
 **id** | **int**| unavailable |
 **property** | **string**| unavailable |
 **string** | **string**| The string value to set. | [optional]
 **int32** | **int**| The int value to set. | [optional]
 **int64** | **int**| The long value to set. | [optional]
 **double** | **double**| The double value to set. | [optional]
 **boolean** | **bool**| The boolean value to set. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `warehousedataDeliveryproviderDeliveryOptionIdIdPut()`

```php
warehousedataDeliveryproviderDeliveryOptionIdIdPut($delivery_option_id, $id, $unknown_base_type)
```

Allows to update a DeliveryProviderContext without replacing it.

Allows to update a DeliveryProviderContext without replacing it.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$delivery_option_id = 56; // int | unavailable
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->warehousedataDeliveryproviderDeliveryOptionIdIdPut($delivery_option_id, $id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi->warehousedataDeliveryproviderDeliveryOptionIdIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delivery_option_id** | **int**| unavailable |
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `warehousedataDeliveryproviderDeliveryOptionIdPost()`

```php
warehousedataDeliveryproviderDeliveryOptionIdPost($delivery_option_id, $unknown_base_type)
```

Replaces the existing DeliveryProviderContext with the new one rovided.

Replaces the existing DeliveryProviderContext with the new one rovided.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$delivery_option_id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->warehousedataDeliveryproviderDeliveryOptionIdPost($delivery_option_id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi->warehousedataDeliveryproviderDeliveryOptionIdPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delivery_option_id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `warehousedataDeliveryproviderEndpointsGet()`

```php
warehousedataDeliveryproviderEndpointsGet(): object
```

Returns a list of all endpoints available.

Returns a list of all endpoints available.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->warehousedataDeliveryproviderEndpointsGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryDeliveryProviderContextsApi->warehousedataDeliveryproviderEndpointsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
