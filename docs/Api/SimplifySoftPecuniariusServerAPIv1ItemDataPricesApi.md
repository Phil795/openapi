# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**itemdataItemrootPriceContactLogGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi.md#itemdataItemrootPriceContactLogGet) | **GET** /itemdata/{itemroot}/price/{contact}/log | Allows to receive a list of prices. List can be filtered using GET parameters.
[**itemdataItemrootPriceContactLogGet_0()**](SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi.md#itemdataItemrootPriceContactLogGet_0) | **GET** /itemdata/{itemroot}/price/{contact/log} | Allows to receive a list of prices. List can be filtered using GET parameters.
[**itemdataItemrootPriceContactLogIdDelete()**](SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi.md#itemdataItemrootPriceContactLogIdDelete) | **DELETE** /itemdata/{itemroot}/price/{contact}/log/{id} | Allows to receive a list of prices. List can be filtered using GET parameters.
[**itemdataPricesGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi.md#itemdataPricesGet) | **GET** /itemdata/prices | Allows to receive a list of prices. List can be filtered using GET parameters.
[**itemdataPricesIdDelete()**](SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi.md#itemdataPricesIdDelete) | **DELETE** /itemdata/prices/{id} | unavailable
[**itemdataPricesIdGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi.md#itemdataPricesIdGet) | **GET** /itemdata/prices/{id} | Receive a single price by id.
[**itemdataPricesIdPut()**](SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi.md#itemdataPricesIdPut) | **PUT** /itemdata/prices/{id} | unavailable
[**itemdataPricesPost()**](SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi.md#itemdataPricesPost) | **POST** /itemdata/prices | Creates a new price. If the ID is also set, existing price will get marked as EndOfLife.


## `itemdataItemrootPriceContactLogGet()`

```php
itemdataItemrootPriceContactLogGet($itemroot, $contact, $limit, $index, $descending): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPriceLog
```

Allows to receive a list of prices. List can be filtered using GET parameters.

Allows to receive a list of prices. List can be filtered using GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$itemroot = 56; // int | unavailable
$contact = 56; // int | unavailable
$limit = 100; // int | The amount of records to take.
$index = 100; // int | unavailable
$descending = false; // bool | If true, orders by TimeStamp descending. If false, orders by TimeStamp ascending.

try {
    $result = $apiInstance->itemdataItemrootPriceContactLogGet($itemroot, $contact, $limit, $index, $descending);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi->itemdataItemrootPriceContactLogGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemroot** | **int**| unavailable |
 **contact** | **int**| unavailable |
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| unavailable | [optional] [default to 100]
 **descending** | **bool**| If true, orders by TimeStamp descending. If false, orders by TimeStamp ascending. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPriceLog**](../Model/SimplifySoftPecuniariusDataNetItemDataPriceLog.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataItemrootPriceContactLogGet_0()`

```php
itemdataItemrootPriceContactLogGet_0($itemroot, $simplify_soft_pecuniarius_data_net_item_data_price_log): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPriceLog
```

Allows to receive a list of prices. List can be filtered using GET parameters.

Allows to receive a list of prices. List can be filtered using GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$itemroot = 56; // int | unavailable
$simplify_soft_pecuniarius_data_net_item_data_price_log = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPriceLog(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPriceLog

try {
    $result = $apiInstance->itemdataItemrootPriceContactLogGet_0($itemroot, $simplify_soft_pecuniarius_data_net_item_data_price_log);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi->itemdataItemrootPriceContactLogGet_0: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemroot** | **int**| unavailable |
 **simplify_soft_pecuniarius_data_net_item_data_price_log** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPriceLog**](../Model/SimplifySoftPecuniariusDataNetItemDataPriceLog.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPriceLog**](../Model/SimplifySoftPecuniariusDataNetItemDataPriceLog.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataItemrootPriceContactLogIdDelete()`

```php
itemdataItemrootPriceContactLogIdDelete($itemroot, $contact, $id)
```

Allows to receive a list of prices. List can be filtered using GET parameters.

Allows to receive a list of prices. List can be filtered using GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$itemroot = 56; // int | unavailable
$contact = 56; // int | unavailable
$id = 56; // int | unavailable

try {
    $apiInstance->itemdataItemrootPriceContactLogIdDelete($itemroot, $contact, $id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi->itemdataItemrootPriceContactLogIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **itemroot** | **int**| unavailable |
 **contact** | **int**| unavailable |
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataPricesGet()`

```php
itemdataPricesGet($sku, $itemroot, $id, $itemframe, $group, $owner, $limit, $index, $includeitem): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPrice
```

Allows to receive a list of prices. List can be filtered using GET parameters.

Allows to receive a list of prices. List can be filtered using GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sku = array('sku_example'); // string[] | Makes it possible to limit the query to certain SKUs.
$itemroot = array(56); // int[] | Makes it possible to limit the query to certain, owning item-roots IDs.
$id = array(56); // int[] | Makes it possible to limit the query to certain price IDs.
$itemframe = array(56); // int[] | Makes it possible to limit the query to certain, owning item-frames IDs.
$group = array(56); // int[] | Makes it possible to limit the query to certain Price-Group-IDs. OR linked with owner.
$owner = array(56); // int[] | Looks up price groups where the owner is the provided contact (ID). OR linked with group.
$limit = 100; // int | The amount of records to take.
$index = 100; // int | unavailable
$includeitem = false; // bool | Wether the Price.ParentItemRoot & Price.ParentItemRoot.ItemFrame & Price.ParentItemRoot.TaxDefinition shall be included.

try {
    $result = $apiInstance->itemdataPricesGet($sku, $itemroot, $id, $itemframe, $group, $owner, $limit, $index, $includeitem);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi->itemdataPricesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sku** | [**string[]**](../Model/string.md)| Makes it possible to limit the query to certain SKUs. | [optional]
 **itemroot** | [**int[]**](../Model/int.md)| Makes it possible to limit the query to certain, owning item-roots IDs. | [optional]
 **id** | [**int[]**](../Model/int.md)| Makes it possible to limit the query to certain price IDs. | [optional]
 **itemframe** | [**int[]**](../Model/int.md)| Makes it possible to limit the query to certain, owning item-frames IDs. | [optional]
 **group** | [**int[]**](../Model/int.md)| Makes it possible to limit the query to certain Price-Group-IDs. OR linked with owner. | [optional]
 **owner** | [**int[]**](../Model/int.md)| Looks up price groups where the owner is the provided contact (ID). OR linked with group. | [optional]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| unavailable | [optional] [default to 100]
 **includeitem** | **bool**| Wether the Price.ParentItemRoot &amp; Price.ParentItemRoot.ItemFrame &amp; Price.ParentItemRoot.TaxDefinition shall be included. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPrice**](../Model/SimplifySoftPecuniariusDataNetItemDataPrice.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataPricesIdDelete()`

```php
itemdataPricesIdDelete($id)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->itemdataPricesIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi->itemdataPricesIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataPricesIdGet()`

```php
itemdataPricesIdGet($id, $includeitem)
```

Receive a single price by id.

Receive a single price by id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$includeitem = false; // bool | Wether the Price.ParentItemRoot & Price.ParentItemRoot.ItemFrame & Price.ParentItemRoot.TaxDefinition shall be included.

try {
    $apiInstance->itemdataPricesIdGet($id, $includeitem);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi->itemdataPricesIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **includeitem** | **bool**| Wether the Price.ParentItemRoot &amp; Price.ParentItemRoot.ItemFrame &amp; Price.ParentItemRoot.TaxDefinition shall be included. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataPricesIdPut()`

```php
itemdataPricesIdPut($id, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->itemdataPricesIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi->itemdataPricesIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataPricesPost()`

```php
itemdataPricesPost($simplify_soft_pecuniarius_data_net_item_data_price): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPrice
```

Creates a new price. If the ID is also set, existing price will get marked as EndOfLife.

Creates a new price. If the ID is also set, existing price will get marked as EndOfLife.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_item_data_price = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPrice(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPrice

try {
    $result = $apiInstance->itemdataPricesPost($simplify_soft_pecuniarius_data_net_item_data_price);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataPricesApi->itemdataPricesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_item_data_price** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPrice**](../Model/SimplifySoftPecuniariusDataNetItemDataPrice.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPrice**](../Model/SimplifySoftPecuniariusDataNetItemDataPrice.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
