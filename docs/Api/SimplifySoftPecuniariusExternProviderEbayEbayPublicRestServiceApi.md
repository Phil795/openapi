# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusExternProviderEbayEbayPublicRestServiceApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**pluginEbayOauthGuidConfirmPost()**](SimplifySoftPecuniariusExternProviderEbayEbayPublicRestServiceApi.md#pluginEbayOauthGuidConfirmPost) | **POST** /plugin/ebay/oauth/{guid}/confirm | Confirms any given OAuth session.


## `pluginEbayOauthGuidConfirmPost()`

```php
pluginEbayOauthGuidConfirmPost($guid, $extauth)
```

Confirms any given OAuth session.

Confirms any given OAuth session. At any point in time, only one OAuth request shall be active.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusExternProviderEbayEbayPublicRestServiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$guid = 'guid_example'; // string | unavailable
$extauth = 56; // int | ID of the extern authority.

try {
    $apiInstance->pluginEbayOauthGuidConfirmPost($guid, $extauth);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusExternProviderEbayEbayPublicRestServiceApi->pluginEbayOauthGuidConfirmPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **string**| unavailable |
 **extauth** | **int**| ID of the extern authority. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
