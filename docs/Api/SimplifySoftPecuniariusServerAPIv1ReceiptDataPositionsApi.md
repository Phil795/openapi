# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**receiptDataPositionAllCountGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionAllCountGet) | **GET** /receipt-data/position/all/count | unavailable
[**receiptDataPositionAllGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionAllGet) | **GET** /receipt-data/position/all | unavailable
[**receiptDataPositionAllOptionAllCountGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionAllOptionAllCountGet) | **GET** /receipt-data/position/all/option/all/count | unavailable
[**receiptDataPositionAllOptionAllGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionAllOptionAllGet) | **GET** /receipt-data/position/all/option/all | unavailable
[**receiptDataPositionPosidDelete()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionPosidDelete) | **DELETE** /receipt-data/position/{posid} | unavailable
[**receiptDataPositionPosidGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionPosidGet) | **GET** /receipt-data/position/{posid} | unavailable
[**receiptDataPositionPosidGoalGoalidLinkPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionPosidGoalGoalidLinkPost) | **POST** /receipt-data/position/{posid}/goal/{goalid}/link | unavailable
[**receiptDataPositionPosidGoalGoalidUnlinkPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionPosidGoalGoalidUnlinkPost) | **POST** /receipt-data/position/{posid}/goal/{goalid}/unlink | unavailable
[**receiptDataPositionPosidGoalIdsGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionPosidGoalIdsGet) | **GET** /receipt-data/position/{posid}/goal/ids | unavailable
[**receiptDataPositionPosidOptionAllGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionPosidOptionAllGet) | **GET** /receipt-data/position/{posid}/option/all | unavailable
[**receiptDataPositionPosidOptionOptidDelete()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionPosidOptionOptidDelete) | **DELETE** /receipt-data/position/{posid}/option/{optid} | unavailable
[**receiptDataPositionPosidOptionOptidGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionPosidOptionOptidGet) | **GET** /receipt-data/position/{posid}/option/{optid} | unavailable
[**receiptDataPositionPosidOptionOptidPut()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionPosidOptionOptidPut) | **PUT** /receipt-data/position/{posid}/option/{optid} | unavailable
[**receiptDataPositionPosidOptionPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionPosidOptionPost) | **POST** /receipt-data/position/{posid}/option | unavailable
[**receiptDataPositionPosidPut()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionPosidPut) | **PUT** /receipt-data/position/{posid} | unavailable
[**receiptDataPositionPosidReceiptIdsGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionPosidReceiptIdsGet) | **GET** /receipt-data/position/{posid}/receipt/ids | unavailable
[**receiptDataPositionPosidReceiptReceiptidLinkPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionPosidReceiptReceiptidLinkPost) | **POST** /receipt-data/position/{posid}/receipt/{receiptid}/link | unavailable
[**receiptDataPositionPosidReceiptReceiptidUnlinkPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionPosidReceiptReceiptidUnlinkPost) | **POST** /receipt-data/position/{posid}/receipt/{receiptid}/unlink | unavailable
[**receiptDataPositionPosidSplitPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionPosidSplitPost) | **POST** /receipt-data/position/{posid}/split | Splits a non Editible or Booked Position into two separate ones and returns the newly created position. Cannot be undone.
[**receiptDataPositionPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi.md#receiptDataPositionPost) | **POST** /receipt-data/position | unavailable


## `receiptDataPositionAllCountGet()`

```php
receiptDataPositionAllCountGet($index, $limit, $case_id, $receipt_id, $tag_id, $item_root_id, $item_frame_id, $position_id, $chain_using_or)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.
$case_id = array(56); // int[] | Limits the query to those receipt entries which are inside of the provided cases.
$receipt_id = array(56); // int[] | Limits the query to those receipt entries which are inside of the provided receipts.
$tag_id = array(56); // int[] | Limits the query to those receipt entries containing an item refering to the given tag.
$item_root_id = array(56); // int[] | Limits the query to those receipt entries containing the refered item.
$item_frame_id = array(56); // int[] | Limits the query to those receipt entries containing the refered item.
$position_id = array(56); // int[] | Limits the query to those receipt entries which are provided.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $apiInstance->receiptDataPositionAllCountGet($index, $limit, $case_id, $receipt_id, $tag_id, $item_root_id, $item_frame_id, $position_id, $chain_using_or);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **case_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entries which are inside of the provided cases. | [optional]
 **receipt_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entries which are inside of the provided receipts. | [optional]
 **tag_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entries containing an item refering to the given tag. | [optional]
 **item_root_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entries containing the refered item. | [optional]
 **item_frame_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entries containing the refered item. | [optional]
 **position_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entries which are provided. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionAllGet()`

```php
receiptDataPositionAllGet($index, $limit, $case_id, $receipt_id, $tag_id, $item_root_id, $item_frame_id, $position_id, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPosition
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.
$case_id = array(56); // int[] | Limits the query to those receipt entries which are inside of the provided cases.
$receipt_id = array(56); // int[] | Limits the query to those receipt entries which are inside of the provided receipts.
$tag_id = array(56); // int[] | Limits the query to those receipt entries containing an item refering to the given tag.
$item_root_id = array(56); // int[] | Limits the query to those receipt entries containing the refered item.
$item_frame_id = array(56); // int[] | Limits the query to those receipt entries containing the refered item.
$position_id = array(56); // int[] | Limits the query to those receipt entries which are provided.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->receiptDataPositionAllGet($index, $limit, $case_id, $receipt_id, $tag_id, $item_root_id, $item_frame_id, $position_id, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **case_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entries which are inside of the provided cases. | [optional]
 **receipt_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entries which are inside of the provided receipts. | [optional]
 **tag_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entries containing an item refering to the given tag. | [optional]
 **item_root_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entries containing the refered item. | [optional]
 **item_frame_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entries containing the refered item. | [optional]
 **position_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entries which are provided. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPosition**](../Model/SimplifySoftPecuniariusDataNetReceiptDataPosition.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionAllOptionAllCountGet()`

```php
receiptDataPositionAllOptionAllCountGet($index, $limit, $case_id, $receipt_id, $position_id, $position_option_id, $chain_using_or)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.
$case_id = array(56); // int[] | Limits the query to those receipt entriy options inside of the provided cases.
$receipt_id = array(56); // int[] | Limits the query to those receipt entriy options inside of the provided receipts.
$position_id = array(56); // int[] | Limits the query to those receipt entriy options inside of the provided receipt entries.
$position_option_id = array(56); // int[] | Limits the query to those receipt entriy options provided.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $apiInstance->receiptDataPositionAllOptionAllCountGet($index, $limit, $case_id, $receipt_id, $position_id, $position_option_id, $chain_using_or);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionAllOptionAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **case_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entriy options inside of the provided cases. | [optional]
 **receipt_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entriy options inside of the provided receipts. | [optional]
 **position_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entriy options inside of the provided receipt entries. | [optional]
 **position_option_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entriy options provided. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionAllOptionAllGet()`

```php
receiptDataPositionAllOptionAllGet($index, $limit, $case_id, $receipt_id, $position_id, $position_option_id, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPositionOption
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 10; // int | The amount of records to take.
$case_id = array(56); // int[] | Limits the query to those receipt entriy options inside of the provided cases.
$receipt_id = array(56); // int[] | Limits the query to those receipt entriy options inside of the provided receipts.
$position_id = array(56); // int[] | Limits the query to those receipt entriy options inside of the provided receipt entries.
$position_option_id = array(56); // int[] | Limits the query to those receipt entriy options provided.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->receiptDataPositionAllOptionAllGet($index, $limit, $case_id, $receipt_id, $position_id, $position_option_id, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionAllOptionAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **case_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entriy options inside of the provided cases. | [optional]
 **receipt_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entriy options inside of the provided receipts. | [optional]
 **position_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entriy options inside of the provided receipt entries. | [optional]
 **position_option_id** | [**int[]**](../Model/int.md)| Limits the query to those receipt entriy options provided. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPositionOption**](../Model/SimplifySoftPecuniariusDataNetReceiptDataPositionOption.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionPosidDelete()`

```php
receiptDataPositionPosidDelete($posid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$posid = 56; // int | unavailable

try {
    $apiInstance->receiptDataPositionPosidDelete($posid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionPosidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionPosidGet()`

```php
receiptDataPositionPosidGet($posid, $include_tax, $include_options, $include_itemframe, $include_itemframe_parent, $include_itemframe_parent_tax, $include_itemframe_unit, $include_itemframe_defaultfile, $include_deliverygoals, $include_deliverygoals_address, $include_deliverygoals_method, $no_include)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$posid = 56; // int | unavailable
$include_tax = true; // bool | Sets wether returning the Receipt.Positions[] is desired.
$include_options = true; // bool | Sets wether returning the Receipt.Positions[].Options is desired.
$include_itemframe = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame is desired.
$include_itemframe_parent = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.Parent is desired.
$include_itemframe_parent_tax = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired.
$include_itemframe_unit = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.Unit is desired.
$include_itemframe_defaultfile = true; // bool | Sets wether returning the Receipt.Positions[].ItemFrame.DefaultFile is desired.
$include_deliverygoals = true; // bool | Sets wether returning the Receipt.Positions[].DeliveryGoals[] is desired.
$include_deliverygoals_address = true; // bool | Sets wether returning the Receipt.Positions[].DeliveryGoals[].Address is desired.
$include_deliverygoals_method = true; // bool | Sets wether returning the Receipt.Positions[].DeliveryGoals[].Method is desired.
$no_include = false; // bool | Sets that no inclusion of anything is wanted.

try {
    $apiInstance->receiptDataPositionPosidGet($posid, $include_tax, $include_options, $include_itemframe, $include_itemframe_parent, $include_itemframe_parent_tax, $include_itemframe_unit, $include_itemframe_defaultfile, $include_deliverygoals, $include_deliverygoals_address, $include_deliverygoals_method, $no_include);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionPosidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posid** | **int**| unavailable |
 **include_tax** | **bool**| Sets wether returning the Receipt.Positions[] is desired. | [optional] [default to true]
 **include_options** | **bool**| Sets wether returning the Receipt.Positions[].Options is desired. | [optional] [default to true]
 **include_itemframe** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame is desired. | [optional] [default to true]
 **include_itemframe_parent** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.Parent is desired. | [optional] [default to true]
 **include_itemframe_parent_tax** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.Parent.TaxDefinition and -.Tax is desired. | [optional] [default to true]
 **include_itemframe_unit** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.Unit is desired. | [optional] [default to true]
 **include_itemframe_defaultfile** | **bool**| Sets wether returning the Receipt.Positions[].ItemFrame.DefaultFile is desired. | [optional] [default to true]
 **include_deliverygoals** | **bool**| Sets wether returning the Receipt.Positions[].DeliveryGoals[] is desired. | [optional] [default to true]
 **include_deliverygoals_address** | **bool**| Sets wether returning the Receipt.Positions[].DeliveryGoals[].Address is desired. | [optional] [default to true]
 **include_deliverygoals_method** | **bool**| Sets wether returning the Receipt.Positions[].DeliveryGoals[].Method is desired. | [optional] [default to true]
 **no_include** | **bool**| Sets that no inclusion of anything is wanted. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionPosidGoalGoalidLinkPost()`

```php
receiptDataPositionPosidGoalGoalidLinkPost($posid, $goalid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$posid = 56; // int | unavailable
$goalid = 56; // int | unavailable

try {
    $apiInstance->receiptDataPositionPosidGoalGoalidLinkPost($posid, $goalid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionPosidGoalGoalidLinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posid** | **int**| unavailable |
 **goalid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionPosidGoalGoalidUnlinkPost()`

```php
receiptDataPositionPosidGoalGoalidUnlinkPost($posid, $goalid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$posid = 56; // int | unavailable
$goalid = 56; // int | unavailable

try {
    $apiInstance->receiptDataPositionPosidGoalGoalidUnlinkPost($posid, $goalid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionPosidGoalGoalidUnlinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posid** | **int**| unavailable |
 **goalid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionPosidGoalIdsGet()`

```php
receiptDataPositionPosidGoalIdsGet($posid): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$posid = 56; // int | unavailable

try {
    $result = $apiInstance->receiptDataPositionPosidGoalIdsGet($posid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionPosidGoalIdsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posid** | **int**| unavailable |

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32**](../Model/SimplifySoftPecuniariusDataNetPrimitivesNetInt32.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionPosidOptionAllGet()`

```php
receiptDataPositionPosidOptionAllGet($posid, $include_definition, $include_tax, $no_include): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPositionOption
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$posid = 56; // int | unavailable
$include_definition = true; // bool | Sets wether returning the OptionDefinition is desired.
$include_tax = true; // bool | Sets wether returning the Tax is desired.
$no_include = false; // bool | Sets that no inclusion of anything is wanted.

try {
    $result = $apiInstance->receiptDataPositionPosidOptionAllGet($posid, $include_definition, $include_tax, $no_include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionPosidOptionAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posid** | **int**| unavailable |
 **include_definition** | **bool**| Sets wether returning the OptionDefinition is desired. | [optional] [default to true]
 **include_tax** | **bool**| Sets wether returning the Tax is desired. | [optional] [default to true]
 **no_include** | **bool**| Sets that no inclusion of anything is wanted. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPositionOption**](../Model/SimplifySoftPecuniariusDataNetReceiptDataPositionOption.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionPosidOptionOptidDelete()`

```php
receiptDataPositionPosidOptionOptidDelete($posid, $optid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$posid = 56; // int | unavailable
$optid = 56; // int | unavailable

try {
    $apiInstance->receiptDataPositionPosidOptionOptidDelete($posid, $optid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionPosidOptionOptidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posid** | **int**| unavailable |
 **optid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionPosidOptionOptidGet()`

```php
receiptDataPositionPosidOptionOptidGet($posid, $optid, $include_definition, $include_tax, $no_include)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$posid = 56; // int | unavailable
$optid = 56; // int | unavailable
$include_definition = true; // bool | Sets wether returning the OptionDefinition is desired.
$include_tax = true; // bool | Sets wether returning the Tax is desired.
$no_include = false; // bool | Sets that no inclusion of anything is wanted.

try {
    $apiInstance->receiptDataPositionPosidOptionOptidGet($posid, $optid, $include_definition, $include_tax, $no_include);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionPosidOptionOptidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posid** | **int**| unavailable |
 **optid** | **int**| unavailable |
 **include_definition** | **bool**| Sets wether returning the OptionDefinition is desired. | [optional] [default to true]
 **include_tax** | **bool**| Sets wether returning the Tax is desired. | [optional] [default to true]
 **no_include** | **bool**| Sets that no inclusion of anything is wanted. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionPosidOptionOptidPut()`

```php
receiptDataPositionPosidOptionOptidPut($posid, $optid, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$posid = 56; // int | unavailable
$optid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->receiptDataPositionPosidOptionOptidPut($posid, $optid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionPosidOptionOptidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posid** | **int**| unavailable |
 **optid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionPosidOptionPost()`

```php
receiptDataPositionPosidOptionPost($posid, $simplify_soft_pecuniarius_data_net_receipt_data_position_option): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPositionOption
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$posid = 56; // int | unavailable
$simplify_soft_pecuniarius_data_net_receipt_data_position_option = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPositionOption(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPositionOption

try {
    $result = $apiInstance->receiptDataPositionPosidOptionPost($posid, $simplify_soft_pecuniarius_data_net_receipt_data_position_option);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionPosidOptionPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posid** | **int**| unavailable |
 **simplify_soft_pecuniarius_data_net_receipt_data_position_option** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPositionOption**](../Model/SimplifySoftPecuniariusDataNetReceiptDataPositionOption.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPositionOption**](../Model/SimplifySoftPecuniariusDataNetReceiptDataPositionOption.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionPosidPut()`

```php
receiptDataPositionPosidPut($posid, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$posid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->receiptDataPositionPosidPut($posid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionPosidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionPosidReceiptIdsGet()`

```php
receiptDataPositionPosidReceiptIdsGet($posid): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$posid = 56; // int | unavailable

try {
    $result = $apiInstance->receiptDataPositionPosidReceiptIdsGet($posid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionPosidReceiptIdsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posid** | **int**| unavailable |

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32**](../Model/SimplifySoftPecuniariusDataNetPrimitivesNetInt32.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionPosidReceiptReceiptidLinkPost()`

```php
receiptDataPositionPosidReceiptReceiptidLinkPost($posid, $receiptid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$posid = 56; // int | unavailable
$receiptid = 56; // int | unavailable

try {
    $apiInstance->receiptDataPositionPosidReceiptReceiptidLinkPost($posid, $receiptid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionPosidReceiptReceiptidLinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionPosidReceiptReceiptidUnlinkPost()`

```php
receiptDataPositionPosidReceiptReceiptidUnlinkPost($posid, $receiptid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$posid = 56; // int | unavailable
$receiptid = 56; // int | unavailable

try {
    $apiInstance->receiptDataPositionPosidReceiptReceiptidUnlinkPost($posid, $receiptid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionPosidReceiptReceiptidUnlinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionPosidSplitPost()`

```php
receiptDataPositionPosidSplitPost($posid, $amount)
```

Splits a non Editible or Booked Position into two separate ones and returns the newly created position. Cannot be undone.

Splits a non Editible or Booked Position into two separate ones and returns the newly created position. Cannot be undone.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$posid = 56; // int | unavailable
$amount = 3.4; // float | The amount to split. Has to be less then the originals amount

try {
    $apiInstance->receiptDataPositionPosidSplitPost($posid, $amount);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionPosidSplitPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **posid** | **int**| unavailable |
 **amount** | **float**| The amount to split. Has to be less then the originals amount | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataPositionPost()`

```php
receiptDataPositionPost($simplify_soft_pecuniarius_data_net_receipt_data_position): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPosition
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_receipt_data_position = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPosition(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPosition

try {
    $result = $apiInstance->receiptDataPositionPost($simplify_soft_pecuniarius_data_net_receipt_data_position);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataPositionsApi->receiptDataPositionPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_receipt_data_position** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPosition**](../Model/SimplifySoftPecuniariusDataNetReceiptDataPosition.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPosition**](../Model/SimplifySoftPecuniariusDataNetReceiptDataPosition.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
