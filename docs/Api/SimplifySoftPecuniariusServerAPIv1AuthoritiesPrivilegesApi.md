# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1AuthoritiesPrivilegesApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**authorityPrivilegesGet()**](SimplifySoftPecuniariusServerAPIv1AuthoritiesPrivilegesApi.md#authorityPrivilegesGet) | **GET** /authority/privileges | Returns a list of available privilege properties.
[**authorityPrivilegesIdGet()**](SimplifySoftPecuniariusServerAPIv1AuthoritiesPrivilegesApi.md#authorityPrivilegesIdGet) | **GET** /authority/privileges/{id} | Allows to receive the privileges of an existing authority.
[**authorityPrivilegesIdPrivilegePost()**](SimplifySoftPecuniariusServerAPIv1AuthoritiesPrivilegesApi.md#authorityPrivilegesIdPrivilegePost) | **POST** /authority/privileges/{id}/{privilege} | Allows to change the privilege of an existing authority.


## `authorityPrivilegesGet()`

```php
authorityPrivilegesGet(): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityPrivilegeDefinition
```

Returns a list of available privilege properties.

Returns a list of available privilege properties. A NetKeyValuePair<string, EPrivilegeFlag> will be transfered on updates.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AuthoritiesPrivilegesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->authorityPrivilegesGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AuthoritiesPrivilegesApi->authorityPrivilegesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityPrivilegeDefinition**](../Model/SimplifySoftPecuniariusDataNetAuthorityPrivilegeDefinition.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `authorityPrivilegesIdGet()`

```php
authorityPrivilegesIdGet($id, $expand): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityPrivilege
```

Allows to receive the privileges of an existing authority.

Allows to receive the privileges of an existing authority. Privilege is not required, if a user attempts to receive his very own privileges using this.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AuthoritiesPrivilegesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$expand = false; // bool | If set to true, the result will include the collection of privileges instead of only those from the requirested Authority.

try {
    $result = $apiInstance->authorityPrivilegesIdGet($id, $expand);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AuthoritiesPrivilegesApi->authorityPrivilegesIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **expand** | **bool**| If set to true, the result will include the collection of privileges instead of only those from the requirested Authority. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityPrivilege**](../Model/SimplifySoftPecuniariusDataNetAuthorityPrivilege.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `authorityPrivilegesIdPrivilegePost()`

```php
authorityPrivilegesIdPrivilegePost($id, $privilege, $unknown_base_type)
```

Allows to change the privilege of an existing authority.

Allows to change the privilege of an existing authority.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AuthoritiesPrivilegesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$privilege = 'privilege_example'; // string | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->authorityPrivilegesIdPrivilegePost($id, $privilege, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AuthoritiesPrivilegesApi->authorityPrivilegesIdPrivilegePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **privilege** | **string**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
