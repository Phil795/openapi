# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemLinksApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**itemdataLinksGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemLinksApi.md#itemdataLinksGet) | **GET** /itemdata/links | Returns the list of available item links.
[**itemdataLinksIdDelete()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemLinksApi.md#itemdataLinksIdDelete) | **DELETE** /itemdata/links/{id} | Allows to delete a single existing item links.
[**itemdataLinksIdGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemLinksApi.md#itemdataLinksIdGet) | **GET** /itemdata/links/{id} | Returns a single item link.
[**itemdataLinksIdPut()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemLinksApi.md#itemdataLinksIdPut) | **PUT** /itemdata/links/{id} | Interface for updating existing item links.
[**itemdataLinksPost()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemLinksApi.md#itemdataLinksPost) | **POST** /itemdata/links | Provides an interface to create a new item links.


## `itemdataLinksGet()`

```php
itemdataLinksGet($limit, $index, $parent, $secondary): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemLink
```

Returns the list of available item links.

Returns the list of available item links.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 100; // int | unavailable
$parent = array(56); // int[] | Collection of ItemIds that are expected to be in the Primary field.
$secondary = array(56); // int[] | Collection of ItemIds that are expected to be in the Secondary field.

try {
    $result = $apiInstance->itemdataLinksGet($limit, $index, $parent, $secondary);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemLinksApi->itemdataLinksGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| unavailable | [optional] [default to 100]
 **parent** | [**int[]**](../Model/int.md)| Collection of ItemIds that are expected to be in the Primary field. | [optional]
 **secondary** | [**int[]**](../Model/int.md)| Collection of ItemIds that are expected to be in the Secondary field. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemLink**](../Model/SimplifySoftPecuniariusDataNetItemDataItemLink.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataLinksIdDelete()`

```php
itemdataLinksIdDelete($id)
```

Allows to delete a single existing item links.

Allows to delete a single existing item links.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->itemdataLinksIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemLinksApi->itemdataLinksIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataLinksIdGet()`

```php
itemdataLinksIdGet($id, $include_parent, $include_secondary, $include_secondary_frame, $include_secondary_tags)
```

Returns a single item link.

Returns a single item link.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$include_parent = false; // bool | Wether to include the parent ItemFrame in the result.
$include_secondary = false; // bool | Wether to include the secondary ItemRoot in the result.
$include_secondary_frame = false; // bool | Wether to include the secondary ItemRoot.Frame in the result.
$include_secondary_tags = false; // bool | Wether to include the secondary ItemRoot.Tags in the result.

try {
    $apiInstance->itemdataLinksIdGet($id, $include_parent, $include_secondary, $include_secondary_frame, $include_secondary_tags);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemLinksApi->itemdataLinksIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **include_parent** | **bool**| Wether to include the parent ItemFrame in the result. | [optional] [default to false]
 **include_secondary** | **bool**| Wether to include the secondary ItemRoot in the result. | [optional] [default to false]
 **include_secondary_frame** | **bool**| Wether to include the secondary ItemRoot.Frame in the result. | [optional] [default to false]
 **include_secondary_tags** | **bool**| Wether to include the secondary ItemRoot.Tags in the result. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataLinksIdPut()`

```php
itemdataLinksIdPut($id, $unknown_base_type)
```

Interface for updating existing item links.

Interface for updating existing item links.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->itemdataLinksIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemLinksApi->itemdataLinksIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataLinksPost()`

```php
itemdataLinksPost($simplify_soft_pecuniarius_data_net_item_data_item_link): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemLink
```

Provides an interface to create a new item links.

Provides an interface to create a new item links.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_item_data_item_link = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemLink(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemLink

try {
    $result = $apiInstance->itemdataLinksPost($simplify_soft_pecuniarius_data_net_item_data_item_link);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemLinksApi->itemdataLinksPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_item_data_item_link** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemLink**](../Model/SimplifySoftPecuniariusDataNetItemDataItemLink.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemLink**](../Model/SimplifySoftPecuniariusDataNetItemDataItemLink.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
