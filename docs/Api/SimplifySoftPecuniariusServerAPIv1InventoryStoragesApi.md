# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**locationStoragesAllCountGet()**](SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi.md#locationStoragesAllCountGet) | **GET** /location/storages/all/count | Receives a list of storages according to provided filtering provided via GET parameters.
[**locationStoragesAllGet()**](SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi.md#locationStoragesAllGet) | **GET** /location/storages/all | Receives a list of storages according to provided filtering provided via GET parameters.
[**locationStoragesAllIdsGet()**](SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi.md#locationStoragesAllIdsGet) | **GET** /location/storages/all/ids | Receives a list of storages according to provided filtering provided via GET parameters.
[**locationStoragesIdDelete()**](SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi.md#locationStoragesIdDelete) | **DELETE** /location/storages/{id} | Endpoint to remove a storage. Note that this will throw errors unless ensured that the storage is empty and not refered to from
[**locationStoragesIdGet()**](SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi.md#locationStoragesIdGet) | **GET** /location/storages/{id} | Endpoint to receive a single storage according to its Id.
[**locationStoragesIdLocationsGet()**](SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi.md#locationStoragesIdLocationsGet) | **GET** /location/storages/{id}/locations | Receives a list of storage locations owned by the storage designated by its Id.
[**locationStoragesIdPut()**](SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi.md#locationStoragesIdPut) | **PUT** /location/storages/{id} | Allows to update a given storage via its Id.
[**locationStoragesPost()**](SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi.md#locationStoragesPost) | **POST** /location/storages | Provides the ability to create a new storage.


## `locationStoragesAllCountGet()`

```php
locationStoragesAllCountGet($limit, $index, $storage_id, $warehouse_id, $location_id, $title_exact, $title_starts_with, $title_like, $f_is_shipping, $f_auto_assign_empty, $f_is_default_for_new_stock, $chain_using_or, $force_id)
```

Receives a list of storages according to provided filtering provided via GET parameters.

Receives a list of storages according to provided filtering provided via GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$storage_id = array(56); // int[] | Allows to only receive the children of provided parent warehouse.
$warehouse_id = array(56); // int[] | Allows to only receive the children of provided parent warehouse.
$location_id = array(56); // int[] | Allows to only receive the children of provided parent warehouse.
$title_exact = array('title_exact_example'); // string[] | Checks the Storage.Title against the provided strings using exact match.
$title_starts_with = array('title_starts_with_example'); // string[] | Checks the Storage.Title against the provided strings using starts with. Case Invariant.
$title_like = 'title_like_example'; // string | Uses LIKE expression to check the Storage.Title for provided content. Case Invariant.
$f_is_shipping = True; // bool | Filters query, returning only those entries where Storage.IsShipping is the same as the provided value.
$f_auto_assign_empty = True; // bool | Filters query, returning only those entries where Storage.AutoAssignEmpty is the same as the provided value.
$f_is_default_for_new_stock = True; // bool | Filters query, returning only those entries where Storage.IsDefaultForNewStock is the same as the provided value.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.
$force_id = false; // bool | Enforces AND linking, regardless of chain-using-or, for '*-id' and f-* parameters.

try {
    $apiInstance->locationStoragesAllCountGet($limit, $index, $storage_id, $warehouse_id, $location_id, $title_exact, $title_starts_with, $title_like, $f_is_shipping, $f_auto_assign_empty, $f_is_default_for_new_stock, $chain_using_or, $force_id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi->locationStoragesAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **storage_id** | [**int[]**](../Model/int.md)| Allows to only receive the children of provided parent warehouse. | [optional]
 **warehouse_id** | [**int[]**](../Model/int.md)| Allows to only receive the children of provided parent warehouse. | [optional]
 **location_id** | [**int[]**](../Model/int.md)| Allows to only receive the children of provided parent warehouse. | [optional]
 **title_exact** | [**string[]**](../Model/string.md)| Checks the Storage.Title against the provided strings using exact match. | [optional]
 **title_starts_with** | [**string[]**](../Model/string.md)| Checks the Storage.Title against the provided strings using starts with. Case Invariant. | [optional]
 **title_like** | **string**| Uses LIKE expression to check the Storage.Title for provided content. Case Invariant. | [optional]
 **f_is_shipping** | **bool**| Filters query, returning only those entries where Storage.IsShipping is the same as the provided value. | [optional]
 **f_auto_assign_empty** | **bool**| Filters query, returning only those entries where Storage.AutoAssignEmpty is the same as the provided value. | [optional]
 **f_is_default_for_new_stock** | **bool**| Filters query, returning only those entries where Storage.IsDefaultForNewStock is the same as the provided value. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]
 **force_id** | **bool**| Enforces AND linking, regardless of chain-using-or, for &#39;*-id&#39; and f-* parameters. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `locationStoragesAllGet()`

```php
locationStoragesAllGet($limit, $index, $storage_id, $warehouse_id, $location_id, $title_exact, $title_starts_with, $title_like, $f_is_shipping, $f_auto_assign_empty, $f_is_default_for_new_stock, $chain_using_or, $force_id): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorage
```

Receives a list of storages according to provided filtering provided via GET parameters.

Receives a list of storages according to provided filtering provided via GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$storage_id = array(56); // int[] | Allows to only receive the children of provided parent warehouse.
$warehouse_id = array(56); // int[] | Allows to only receive the children of provided parent warehouse.
$location_id = array(56); // int[] | Allows to only receive the children of provided parent warehouse.
$title_exact = array('title_exact_example'); // string[] | Checks the Storage.Title against the provided strings using exact match.
$title_starts_with = array('title_starts_with_example'); // string[] | Checks the Storage.Title against the provided strings using starts with. Case Invariant.
$title_like = 'title_like_example'; // string | Uses LIKE expression to check the Storage.Title for provided content. Case Invariant.
$f_is_shipping = True; // bool | Filters query, returning only those entries where Storage.IsShipping is the same as the provided value.
$f_auto_assign_empty = True; // bool | Filters query, returning only those entries where Storage.AutoAssignEmpty is the same as the provided value.
$f_is_default_for_new_stock = True; // bool | Filters query, returning only those entries where Storage.IsDefaultForNewStock is the same as the provided value.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.
$force_id = false; // bool | Enforces AND linking, regardless of chain-using-or, for '*-id' and f-* parameters.

try {
    $result = $apiInstance->locationStoragesAllGet($limit, $index, $storage_id, $warehouse_id, $location_id, $title_exact, $title_starts_with, $title_like, $f_is_shipping, $f_auto_assign_empty, $f_is_default_for_new_stock, $chain_using_or, $force_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi->locationStoragesAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **storage_id** | [**int[]**](../Model/int.md)| Allows to only receive the children of provided parent warehouse. | [optional]
 **warehouse_id** | [**int[]**](../Model/int.md)| Allows to only receive the children of provided parent warehouse. | [optional]
 **location_id** | [**int[]**](../Model/int.md)| Allows to only receive the children of provided parent warehouse. | [optional]
 **title_exact** | [**string[]**](../Model/string.md)| Checks the Storage.Title against the provided strings using exact match. | [optional]
 **title_starts_with** | [**string[]**](../Model/string.md)| Checks the Storage.Title against the provided strings using starts with. Case Invariant. | [optional]
 **title_like** | **string**| Uses LIKE expression to check the Storage.Title for provided content. Case Invariant. | [optional]
 **f_is_shipping** | **bool**| Filters query, returning only those entries where Storage.IsShipping is the same as the provided value. | [optional]
 **f_auto_assign_empty** | **bool**| Filters query, returning only those entries where Storage.AutoAssignEmpty is the same as the provided value. | [optional]
 **f_is_default_for_new_stock** | **bool**| Filters query, returning only those entries where Storage.IsDefaultForNewStock is the same as the provided value. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]
 **force_id** | **bool**| Enforces AND linking, regardless of chain-using-or, for &#39;*-id&#39; and f-* parameters. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorage**](../Model/SimplifySoftPecuniariusDataNetInventoryStorage.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `locationStoragesAllIdsGet()`

```php
locationStoragesAllIdsGet($limit, $index, $storage_id, $warehouse_id, $location_id, $title_exact, $title_starts_with, $title_like, $f_is_shipping, $f_auto_assign_empty, $f_is_default_for_new_stock, $chain_using_or, $force_id): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32
```

Receives a list of storages according to provided filtering provided via GET parameters.

Receives a list of storages according to provided filtering provided via GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$storage_id = array(56); // int[] | Allows to only receive the children of provided parent warehouse.
$warehouse_id = array(56); // int[] | Allows to only receive the children of provided parent warehouse.
$location_id = array(56); // int[] | Allows to only receive the children of provided parent warehouse.
$title_exact = array('title_exact_example'); // string[] | Checks the Storage.Title against the provided strings using exact match.
$title_starts_with = array('title_starts_with_example'); // string[] | Checks the Storage.Title against the provided strings using starts with. Case Invariant.
$title_like = 'title_like_example'; // string | Uses LIKE expression to check the Storage.Title for provided content. Case Invariant.
$f_is_shipping = True; // bool | Filters query, returning only those entries where Storage.IsShipping is the same as the provided value.
$f_auto_assign_empty = True; // bool | Filters query, returning only those entries where Storage.AutoAssignEmpty is the same as the provided value.
$f_is_default_for_new_stock = True; // bool | Filters query, returning only those entries where Storage.IsDefaultForNewStock is the same as the provided value.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.
$force_id = false; // bool | Enforces AND linking, regardless of chain-using-or, for '*-id' and f-* parameters.

try {
    $result = $apiInstance->locationStoragesAllIdsGet($limit, $index, $storage_id, $warehouse_id, $location_id, $title_exact, $title_starts_with, $title_like, $f_is_shipping, $f_auto_assign_empty, $f_is_default_for_new_stock, $chain_using_or, $force_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi->locationStoragesAllIdsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **storage_id** | [**int[]**](../Model/int.md)| Allows to only receive the children of provided parent warehouse. | [optional]
 **warehouse_id** | [**int[]**](../Model/int.md)| Allows to only receive the children of provided parent warehouse. | [optional]
 **location_id** | [**int[]**](../Model/int.md)| Allows to only receive the children of provided parent warehouse. | [optional]
 **title_exact** | [**string[]**](../Model/string.md)| Checks the Storage.Title against the provided strings using exact match. | [optional]
 **title_starts_with** | [**string[]**](../Model/string.md)| Checks the Storage.Title against the provided strings using starts with. Case Invariant. | [optional]
 **title_like** | **string**| Uses LIKE expression to check the Storage.Title for provided content. Case Invariant. | [optional]
 **f_is_shipping** | **bool**| Filters query, returning only those entries where Storage.IsShipping is the same as the provided value. | [optional]
 **f_auto_assign_empty** | **bool**| Filters query, returning only those entries where Storage.AutoAssignEmpty is the same as the provided value. | [optional]
 **f_is_default_for_new_stock** | **bool**| Filters query, returning only those entries where Storage.IsDefaultForNewStock is the same as the provided value. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]
 **force_id** | **bool**| Enforces AND linking, regardless of chain-using-or, for &#39;*-id&#39; and f-* parameters. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32**](../Model/SimplifySoftPecuniariusDataNetPrimitivesNetInt32.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `locationStoragesIdDelete()`

```php
locationStoragesIdDelete($id)
```

Endpoint to remove a storage. Note that this will throw errors unless ensured that the storage is empty and not refered to from

Endpoint to remove a storage. Note that this will throw errors unless ensured that the storage is empty and not refered to from anywhere.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->locationStoragesIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi->locationStoragesIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `locationStoragesIdGet()`

```php
locationStoragesIdGet($id)
```

Endpoint to receive a single storage according to its Id.

Endpoint to receive a single storage according to its Id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->locationStoragesIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi->locationStoragesIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `locationStoragesIdLocationsGet()`

```php
locationStoragesIdLocationsGet($id, $index, $limit, $item, $dimx, $dimy, $dimz, $count, $allowchildren, $allowparent): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorageLocation
```

Receives a list of storage locations owned by the storage designated by its Id.

Receives a list of storage locations owned by the storage designated by its Id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.
$item = 56; // int | The item ID to look for. If equal to -1 then empty locations will be returned.
$dimx = 3.4; // float | The dimension X (Width) the storage location must support. Requires dimy & dimz.
$dimy = 3.4; // float | The dimension Y (Height) the storage location must support. Requires dimx & dimz.
$dimz = 3.4; // float | The dimension Z (Depth) the storage location must support. Requires dimx & dimy.
$count = 1; // int | Multiplicator for dimensions. Used in conjunction with either item or dimx & dimy & dimz. If 0 value is provided for item, dimension check is skipped.
$allowchildren = true; // bool | When set to false, only Storage-Locations without any children (sub-storage-locations) will be included.
$allowparent = true; // bool | When set to false, only Storage-Locations without any parent (parent-storage-locations) will be included.

try {
    $result = $apiInstance->locationStoragesIdLocationsGet($id, $index, $limit, $item, $dimx, $dimy, $dimz, $count, $allowchildren, $allowparent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi->locationStoragesIdLocationsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **item** | **int**| The item ID to look for. If equal to -1 then empty locations will be returned. | [optional]
 **dimx** | **float**| The dimension X (Width) the storage location must support. Requires dimy &amp; dimz. | [optional]
 **dimy** | **float**| The dimension Y (Height) the storage location must support. Requires dimx &amp; dimz. | [optional]
 **dimz** | **float**| The dimension Z (Depth) the storage location must support. Requires dimx &amp; dimy. | [optional]
 **count** | **int**| Multiplicator for dimensions. Used in conjunction with either item or dimx &amp; dimy &amp; dimz. If 0 value is provided for item, dimension check is skipped. | [optional] [default to 1]
 **allowchildren** | **bool**| When set to false, only Storage-Locations without any children (sub-storage-locations) will be included. | [optional] [default to true]
 **allowparent** | **bool**| When set to false, only Storage-Locations without any parent (parent-storage-locations) will be included. | [optional] [default to true]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorageLocation**](../Model/SimplifySoftPecuniariusDataNetInventoryStorageLocation.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `locationStoragesIdPut()`

```php
locationStoragesIdPut($id, $unknown_base_type)
```

Allows to update a given storage via its Id.

Allows to update a given storage via its Id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->locationStoragesIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi->locationStoragesIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `locationStoragesPost()`

```php
locationStoragesPost($simplify_soft_pecuniarius_data_net_inventory_storage): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorage
```

Provides the ability to create a new storage.

Provides the ability to create a new storage.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_inventory_storage = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorage(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorage

try {
    $result = $apiInstance->locationStoragesPost($simplify_soft_pecuniarius_data_net_inventory_storage);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryStoragesApi->locationStoragesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_inventory_storage** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorage**](../Model/SimplifySoftPecuniariusDataNetInventoryStorage.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorage**](../Model/SimplifySoftPecuniariusDataNetInventoryStorage.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
