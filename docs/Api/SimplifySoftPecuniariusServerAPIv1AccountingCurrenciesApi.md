# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1AccountingCurrenciesApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountingCurrencyGet()**](SimplifySoftPecuniariusServerAPIv1AccountingCurrenciesApi.md#accountingCurrencyGet) | **GET** /accounting/currency | unavailable
[**accountingCurrencyIsonameDelete()**](SimplifySoftPecuniariusServerAPIv1AccountingCurrenciesApi.md#accountingCurrencyIsonameDelete) | **DELETE** /accounting/currency/{isoname} | unavailable
[**accountingCurrencyIsonamePut()**](SimplifySoftPecuniariusServerAPIv1AccountingCurrenciesApi.md#accountingCurrencyIsonamePut) | **PUT** /accounting/currency/{isoname} | unavailable
[**accountingCurrencyPost()**](SimplifySoftPecuniariusServerAPIv1AccountingCurrenciesApi.md#accountingCurrencyPost) | **POST** /accounting/currency | unavailable


## `accountingCurrencyGet()`

```php
accountingCurrencyGet($limit, $index, $parent): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingCurrency
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingCurrenciesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$parent = 56; // int | Allows to only receive the children of provided parent warehouse.

try {
    $result = $apiInstance->accountingCurrencyGet($limit, $index, $parent);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingCurrenciesApi->accountingCurrencyGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **parent** | **int**| Allows to only receive the children of provided parent warehouse. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingCurrency**](../Model/SimplifySoftPecuniariusDataNetAccountingCurrency.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingCurrencyIsonameDelete()`

```php
accountingCurrencyIsonameDelete($isoname)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingCurrenciesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$isoname = 'isoname_example'; // string | unavailable

try {
    $apiInstance->accountingCurrencyIsonameDelete($isoname);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingCurrenciesApi->accountingCurrencyIsonameDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isoname** | **string**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingCurrencyIsonamePut()`

```php
accountingCurrencyIsonamePut($isoname, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingCurrenciesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$isoname = 'isoname_example'; // string | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->accountingCurrencyIsonamePut($isoname, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingCurrenciesApi->accountingCurrencyIsonamePut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **isoname** | **string**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingCurrencyPost()`

```php
accountingCurrencyPost($simplify_soft_pecuniarius_data_net_accounting_currency): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingCurrency
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingCurrenciesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_accounting_currency = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingCurrency(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingCurrency

try {
    $result = $apiInstance->accountingCurrencyPost($simplify_soft_pecuniarius_data_net_accounting_currency);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingCurrenciesApi->accountingCurrencyPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_accounting_currency** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingCurrency**](../Model/SimplifySoftPecuniariusDataNetAccountingCurrency.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingCurrency**](../Model/SimplifySoftPecuniariusDataNetAccountingCurrency.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
