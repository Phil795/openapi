# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**authorityDataGet()**](SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi.md#authorityDataGet) | **GET** /authority/data | Endpoint to poll all available authorities.
[**authorityDataIdDelete()**](SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi.md#authorityDataIdDelete) | **DELETE** /authority/data/{id} | Provides the functionality to delete a single authority.
[**authorityDataIdGet()**](SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi.md#authorityDataIdGet) | **GET** /authority/data/{id} | Receive a single authority.
[**authorityDataIdLayoutGet()**](SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi.md#authorityDataIdLayoutGet) | **GET** /authority/data/{id}/layout | Allows to receive all available layouts for a given authority.
[**authorityDataIdLayoutLayoutidDelete()**](SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi.md#authorityDataIdLayoutLayoutidDelete) | **DELETE** /authority/data/{id}/layout/{layoutid} | Allows to add a layout to a given authority.
[**authorityDataIdLayoutLayoutidPut()**](SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi.md#authorityDataIdLayoutLayoutidPut) | **PUT** /authority/data/{id}/layout/{layoutid} | Allows to update an existing layout.
[**authorityDataIdLayoutPost()**](SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi.md#authorityDataIdLayoutPost) | **POST** /authority/data/{id}/layout | Allows to add a layout to a given authority.
[**authorityDataIdPut()**](SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi.md#authorityDataIdPut) | **PUT** /authority/data/{id} | Authority Update Endpoint.
[**authorityDataPost()**](SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi.md#authorityDataPost) | **POST** /authority/data | Allows to create new authorities.


## `authorityDataGet()`

```php
authorityDataGet($limit, $index, $users, $groups, $title): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityAuthorityBase
```

Endpoint to poll all available authorities.

Endpoint to poll all available authorities.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$users = true; // bool | Wether to get users. Either this or groups has to be true.
$groups = true; // bool | Wether to get groups. Either this or users has to be true.
$title = 'title_example'; // string | The title to search. Supports % wildcard.

try {
    $result = $apiInstance->authorityDataGet($limit, $index, $users, $groups, $title);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi->authorityDataGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **users** | **bool**| Wether to get users. Either this or groups has to be true. | [optional] [default to true]
 **groups** | **bool**| Wether to get groups. Either this or users has to be true. | [optional] [default to true]
 **title** | **string**| The title to search. Supports % wildcard. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityAuthorityBase**](../Model/SimplifySoftPecuniariusDataNetAuthorityAuthorityBase.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `authorityDataIdDelete()`

```php
authorityDataIdDelete($id)
```

Provides the functionality to delete a single authority.

Provides the functionality to delete a single authority.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->authorityDataIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi->authorityDataIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `authorityDataIdGet()`

```php
authorityDataIdGet($id)
```

Receive a single authority.

Receive a single authority.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->authorityDataIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi->authorityDataIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `authorityDataIdLayoutGet()`

```php
authorityDataIdLayoutGet($id, $recursive): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityLayout
```

Allows to receive all available layouts for a given authority.

Allows to receive all available layouts for a given authority.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$recursive = true; // bool | If true, will receive all available layouts, including those inherited by parents.

try {
    $result = $apiInstance->authorityDataIdLayoutGet($id, $recursive);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi->authorityDataIdLayoutGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **recursive** | **bool**| If true, will receive all available layouts, including those inherited by parents. | [optional] [default to true]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityLayout**](../Model/SimplifySoftPecuniariusDataNetAuthorityLayout.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `authorityDataIdLayoutLayoutidDelete()`

```php
authorityDataIdLayoutLayoutidDelete($id, $layoutid)
```

Allows to add a layout to a given authority.

Allows to add a layout to a given authority.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$layoutid = 56; // int | unavailable

try {
    $apiInstance->authorityDataIdLayoutLayoutidDelete($id, $layoutid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi->authorityDataIdLayoutLayoutidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **layoutid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `authorityDataIdLayoutLayoutidPut()`

```php
authorityDataIdLayoutLayoutidPut($id, $layoutid, $unknown_base_type)
```

Allows to update an existing layout.

Allows to update an existing layout.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$layoutid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->authorityDataIdLayoutLayoutidPut($id, $layoutid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi->authorityDataIdLayoutLayoutidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **layoutid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `authorityDataIdLayoutPost()`

```php
authorityDataIdLayoutPost($id, $unknown_base_type)
```

Allows to add a layout to a given authority.

Allows to add a layout to a given authority.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->authorityDataIdLayoutPost($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi->authorityDataIdLayoutPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `authorityDataIdPut()`

```php
authorityDataIdPut($id, $unknown_base_type)
```

Authority Update Endpoint.

Authority Update Endpoint.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->authorityDataIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi->authorityDataIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `authorityDataPost()`

```php
authorityDataPost($simplify_soft_pecuniarius_data_net_authority_authority_base): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityAuthorityBase
```

Allows to create new authorities.

Allows to create new authorities.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_authority_authority_base = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityAuthorityBase(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityAuthorityBase

try {
    $result = $apiInstance->authorityDataPost($simplify_soft_pecuniarius_data_net_authority_authority_base);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AuthoritiesAuthoritiesApi->authorityDataPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_authority_authority_base** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityAuthorityBase**](../Model/SimplifySoftPecuniariusDataNetAuthorityAuthorityBase.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityAuthorityBase**](../Model/SimplifySoftPecuniariusDataNetAuthorityAuthorityBase.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
