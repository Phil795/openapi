# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**contactdataKindContactGet()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi.md#contactdataKindContactGet) | **GET** /contactdata/kind/contact | Returns all available ContactKinds.
[**contactdataKindContactIdDelete()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi.md#contactdataKindContactIdDelete) | **DELETE** /contactdata/kind/contact/{id} | Deletes a single ContactKind.
[**contactdataKindContactIdGet()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi.md#contactdataKindContactIdGet) | **GET** /contactdata/kind/contact/{id} | Returns a single ContactKind.
[**contactdataKindContactIdPut()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi.md#contactdataKindContactIdPut) | **PUT** /contactdata/kind/contact/{id} | Endpoint to update a single ContactKind.
[**contactdataKindContactIdStateEntryidLinkPost()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi.md#contactdataKindContactIdStateEntryidLinkPost) | **POST** /contactdata/kind/contact/{id}/state/{entryid}/link | Allows to link a ContactKind to an entry-interaction.
[**contactdataKindContactIdStateEntryidUnlinkPost()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi.md#contactdataKindContactIdStateEntryidUnlinkPost) | **POST** /contactdata/kind/contact/{id}/state/{entryid}/unlink | Allows to unlink a ContactKind from an entry-interaction.
[**contactdataKindContactIdStateGet()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi.md#contactdataKindContactIdStateGet) | **GET** /contactdata/kind/contact/{id}/state | Returns all linked entry-interactions from a single ContactKind.
[**contactdataKindContactPost()**](SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi.md#contactdataKindContactPost) | **POST** /contactdata/kind/contact | Allows to create new ContactKinds.


## `contactdataKindContactGet()`

```php
contactdataKindContactGet(): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContactKind
```

Returns all available ContactKinds.

Returns all available ContactKinds.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->contactdataKindContactGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi->contactdataKindContactGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContactKind**](../Model/SimplifySoftPecuniariusDataNetContactDataContactKind.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataKindContactIdDelete()`

```php
contactdataKindContactIdDelete($id)
```

Deletes a single ContactKind.

Deletes a single ContactKind.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->contactdataKindContactIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi->contactdataKindContactIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataKindContactIdGet()`

```php
contactdataKindContactIdGet($id)
```

Returns a single ContactKind.

Returns a single ContactKind.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->contactdataKindContactIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi->contactdataKindContactIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataKindContactIdPut()`

```php
contactdataKindContactIdPut($id, $unknown_base_type)
```

Endpoint to update a single ContactKind.

Endpoint to update a single ContactKind.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->contactdataKindContactIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi->contactdataKindContactIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataKindContactIdStateEntryidLinkPost()`

```php
contactdataKindContactIdStateEntryidLinkPost($id, $entryid)
```

Allows to link a ContactKind to an entry-interaction.

Allows to link a ContactKind to an entry-interaction.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$entryid = 56; // int | unavailable

try {
    $apiInstance->contactdataKindContactIdStateEntryidLinkPost($id, $entryid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi->contactdataKindContactIdStateEntryidLinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **entryid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataKindContactIdStateEntryidUnlinkPost()`

```php
contactdataKindContactIdStateEntryidUnlinkPost($id, $entryid)
```

Allows to unlink a ContactKind from an entry-interaction.

Allows to unlink a ContactKind from an entry-interaction.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$entryid = 56; // int | unavailable

try {
    $apiInstance->contactdataKindContactIdStateEntryidUnlinkPost($id, $entryid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi->contactdataKindContactIdStateEntryidUnlinkPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **entryid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataKindContactIdStateGet()`

```php
contactdataKindContactIdStateGet($id): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineInteractionsEnterInteraction
```

Returns all linked entry-interactions from a single ContactKind.

Returns all linked entry-interactions from a single ContactKind.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $result = $apiInstance->contactdataKindContactIdStateGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi->contactdataKindContactIdStateGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineInteractionsEnterInteraction**](../Model/SimplifySoftPecuniariusDataNetStateMachineInteractionsEnterInteraction.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `contactdataKindContactPost()`

```php
contactdataKindContactPost($simplify_soft_pecuniarius_data_net_contact_data_contact_kind): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContactKind
```

Allows to create new ContactKinds.

Allows to create new ContactKinds.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_contact_data_contact_kind = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContactKind(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContactKind

try {
    $result = $apiInstance->contactdataKindContactPost($simplify_soft_pecuniarius_data_net_contact_data_contact_kind);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ContactDataContactKindsApi->contactdataKindContactPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_contact_data_contact_kind** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContactKind**](../Model/SimplifySoftPecuniariusDataNetContactDataContactKind.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContactKind**](../Model/SimplifySoftPecuniariusDataNetContactDataContactKind.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
