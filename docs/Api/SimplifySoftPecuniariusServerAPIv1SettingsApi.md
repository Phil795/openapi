# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1SettingsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**settingsGet()**](SimplifySoftPecuniariusServerAPIv1SettingsApi.md#settingsGet) | **GET** /settings | unavailable
[**settingsKeyGet()**](SimplifySoftPecuniariusServerAPIv1SettingsApi.md#settingsKeyGet) | **GET** /settings/{key} | unavailable
[**settingsPatch()**](SimplifySoftPecuniariusServerAPIv1SettingsApi.md#settingsPatch) | **PATCH** /settings | unavailable


## `settingsGet()`

```php
settingsGet(): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetSettingsEntry
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1SettingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->settingsGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1SettingsApi->settingsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetSettingsEntry**](../Model/SimplifySoftPecuniariusDataNetSettingsEntry.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `settingsKeyGet()`

```php
settingsKeyGet($key): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetSettingsEntry
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1SettingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$key = 'key_example'; // string | unavailable

try {
    $result = $apiInstance->settingsKeyGet($key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1SettingsApi->settingsKeyGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **string**| unavailable |

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetSettingsEntry**](../Model/SimplifySoftPecuniariusDataNetSettingsEntry.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `settingsPatch()`

```php
settingsPatch($simplify_soft_pecuniarius_data_net_settings_entry)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1SettingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_settings_entry = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetSettingsEntry(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetSettingsEntry

try {
    $apiInstance->settingsPatch($simplify_soft_pecuniarius_data_net_settings_entry);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1SettingsApi->settingsPatch: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_settings_entry** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetSettingsEntry**](../Model/SimplifySoftPecuniariusDataNetSettingsEntry.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
