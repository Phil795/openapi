# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**receiptDataOptionDefinitionAllCountGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi.md#receiptDataOptionDefinitionAllCountGet) | **GET** /receipt-data/option-definition/all/count | unavailable
[**receiptDataOptionDefinitionAllGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi.md#receiptDataOptionDefinitionAllGet) | **GET** /receipt-data/option-definition/all | unavailable
[**receiptDataOptionDefinitionOptdefidDelete()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi.md#receiptDataOptionDefinitionOptdefidDelete) | **DELETE** /receipt-data/option-definition/{optdefid} | unavailable
[**receiptDataOptionDefinitionOptdefidGet()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi.md#receiptDataOptionDefinitionOptdefidGet) | **GET** /receipt-data/option-definition/{optdefid} | unavailable
[**receiptDataOptionDefinitionOptdefidPut()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi.md#receiptDataOptionDefinitionOptdefidPut) | **PUT** /receipt-data/option-definition/{optdefid} | unavailable
[**receiptDataOptionDefinitionPost()**](SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi.md#receiptDataOptionDefinitionPost) | **POST** /receipt-data/option-definition | unavailable


## `receiptDataOptionDefinitionAllCountGet()`

```php
receiptDataOptionDefinitionAllCountGet($index, $limit, $option_definition_id, $chain_using_or)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.
$option_definition_id = array(56); // int[] | The option definitions to receive. If empty, all cases will be received.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $apiInstance->receiptDataOptionDefinitionAllCountGet($index, $limit, $option_definition_id, $chain_using_or);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi->receiptDataOptionDefinitionAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **option_definition_id** | [**int[]**](../Model/int.md)| The option definitions to receive. If empty, all cases will be received. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataOptionDefinitionAllGet()`

```php
receiptDataOptionDefinitionAllGet($index, $limit, $option_definition_id, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataOptionDefinition
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.
$option_definition_id = array(56); // int[] | The option definitions to receive. If empty, all cases will be received.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->receiptDataOptionDefinitionAllGet($index, $limit, $option_definition_id, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi->receiptDataOptionDefinitionAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **option_definition_id** | [**int[]**](../Model/int.md)| The option definitions to receive. If empty, all cases will be received. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataOptionDefinition**](../Model/SimplifySoftPecuniariusDataNetReceiptDataOptionDefinition.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataOptionDefinitionOptdefidDelete()`

```php
receiptDataOptionDefinitionOptdefidDelete($optdefid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$optdefid = 56; // int | unavailable

try {
    $apiInstance->receiptDataOptionDefinitionOptdefidDelete($optdefid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi->receiptDataOptionDefinitionOptdefidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optdefid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataOptionDefinitionOptdefidGet()`

```php
receiptDataOptionDefinitionOptdefidGet($optdefid, $include_taxdefinition, $include_taxdefinition_tax, $no_include)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$optdefid = 56; // int | unavailable
$include_taxdefinition = true; // bool | Sets wether returning the OptionDefinition.TaxDefinition is desired.
$include_taxdefinition_tax = true; // bool | Sets wether returning the OptionDefinition.TaxDefinition.Tax is desired.
$no_include = false; // bool | Sets that no inclusion of anything is wanted.

try {
    $apiInstance->receiptDataOptionDefinitionOptdefidGet($optdefid, $include_taxdefinition, $include_taxdefinition_tax, $no_include);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi->receiptDataOptionDefinitionOptdefidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optdefid** | **int**| unavailable |
 **include_taxdefinition** | **bool**| Sets wether returning the OptionDefinition.TaxDefinition is desired. | [optional] [default to true]
 **include_taxdefinition_tax** | **bool**| Sets wether returning the OptionDefinition.TaxDefinition.Tax is desired. | [optional] [default to true]
 **no_include** | **bool**| Sets that no inclusion of anything is wanted. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataOptionDefinitionOptdefidPut()`

```php
receiptDataOptionDefinitionOptdefidPut($optdefid, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$optdefid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->receiptDataOptionDefinitionOptdefidPut($optdefid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi->receiptDataOptionDefinitionOptdefidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optdefid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `receiptDataOptionDefinitionPost()`

```php
receiptDataOptionDefinitionPost($simplify_soft_pecuniarius_data_net_receipt_data_option_definition): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataOptionDefinition
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_receipt_data_option_definition = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataOptionDefinition(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataOptionDefinition

try {
    $result = $apiInstance->receiptDataOptionDefinitionPost($simplify_soft_pecuniarius_data_net_receipt_data_option_definition);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ReceiptDataOptionDefinitionsApi->receiptDataOptionDefinitionPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_receipt_data_option_definition** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataOptionDefinition**](../Model/SimplifySoftPecuniariusDataNetReceiptDataOptionDefinition.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataOptionDefinition**](../Model/SimplifySoftPecuniariusDataNetReceiptDataOptionDefinition.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
