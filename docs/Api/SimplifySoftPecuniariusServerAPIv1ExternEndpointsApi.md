# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1ExternEndpointsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**externEndpointsGet()**](SimplifySoftPecuniariusServerAPIv1ExternEndpointsApi.md#externEndpointsGet) | **GET** /extern/endpoints | Receives a list of all available extern/authoritys.
[**externEndpointsIdPropertiesKeyDelete()**](SimplifySoftPecuniariusServerAPIv1ExternEndpointsApi.md#externEndpointsIdPropertiesKeyDelete) | **DELETE** /extern/endpoints/{id}/properties/{key} | Allows to remove a property on a given endpoint. &#x60;id&#x60; is expected to be the id of an ExternAuthority.
[**externEndpointsIdPropertiesKeyPost()**](SimplifySoftPecuniariusServerAPIv1ExternEndpointsApi.md#externEndpointsIdPropertiesKeyPost) | **POST** /extern/endpoints/{id}/properties/{key} | Allows to create a property on a given endpoint. &#x60;id&#x60; is expected to be the id of an ExternAuthority.


## `externEndpointsGet()`

```php
externEndpointsGet(): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternEndpoint
```

Receives a list of all available extern/authoritys.

Receives a list of all available extern/authoritys.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternEndpointsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->externEndpointsGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternEndpointsApi->externEndpointsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternEndpoint**](../Model/SimplifySoftPecuniariusDataNetExternEndpoint.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externEndpointsIdPropertiesKeyDelete()`

```php
externEndpointsIdPropertiesKeyDelete($id, $key, $target)
```

Allows to remove a property on a given endpoint. `id` is expected to be the id of an ExternAuthority.

Allows to remove a property on a given endpoint. `id` is expected to be the id of an ExternAuthority.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternEndpointsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$key = 'key_example'; // string | unavailable
$target = 'target_example'; // string | The target of this operation. Can be: contact, item

try {
    $apiInstance->externEndpointsIdPropertiesKeyDelete($id, $key, $target);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternEndpointsApi->externEndpointsIdPropertiesKeyDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **key** | **string**| unavailable |
 **target** | **string**| The target of this operation. Can be: contact, item | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externEndpointsIdPropertiesKeyPost()`

```php
externEndpointsIdPropertiesKeyPost($id, $key, $target, $simplify_soft_pecuniarius_data_net_custom_property): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty
```

Allows to create a property on a given endpoint. `id` is expected to be the id of an ExternAuthority.

Allows to create a property on a given endpoint. `id` is expected to be the id of an ExternAuthority.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternEndpointsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$key = 'key_example'; // string | unavailable
$target = 'target_example'; // string | The target of this operation. Can be: contact, item
$simplify_soft_pecuniarius_data_net_custom_property = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty

try {
    $result = $apiInstance->externEndpointsIdPropertiesKeyPost($id, $key, $target, $simplify_soft_pecuniarius_data_net_custom_property);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternEndpointsApi->externEndpointsIdPropertiesKeyPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **key** | **string**| unavailable |
 **target** | **string**| The target of this operation. Can be: contact, item | [optional]
 **simplify_soft_pecuniarius_data_net_custom_property** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty**](../Model/SimplifySoftPecuniariusDataNetCustomProperty.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty**](../Model/SimplifySoftPecuniariusDataNetCustomProperty.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
