# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickBoxesApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**warehousedataPickboxesGet()**](SimplifySoftPecuniariusServerAPIv1InventoryPickBoxesApi.md#warehousedataPickboxesGet) | **GET** /warehousedata/pickboxes | unavailable
[**warehousedataPickboxesIdDelete()**](SimplifySoftPecuniariusServerAPIv1InventoryPickBoxesApi.md#warehousedataPickboxesIdDelete) | **DELETE** /warehousedata/pickboxes/{id} | unavailable
[**warehousedataPickboxesIdPut()**](SimplifySoftPecuniariusServerAPIv1InventoryPickBoxesApi.md#warehousedataPickboxesIdPut) | **PUT** /warehousedata/pickboxes/{id} | unavailable
[**warehousedataPickboxesPost()**](SimplifySoftPecuniariusServerAPIv1InventoryPickBoxesApi.md#warehousedataPickboxesPost) | **POST** /warehousedata/pickboxes | unavailable


## `warehousedataPickboxesGet()`

```php
warehousedataPickboxesGet($limit, $index, $groups, $pickbox_id): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickBox
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickBoxesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 1000; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$groups = array(56); // int[] | unavailable
$pickbox_id = array(56); // int[] | PickBox.Id's to receive

try {
    $result = $apiInstance->warehousedataPickboxesGet($limit, $index, $groups, $pickbox_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickBoxesApi->warehousedataPickboxesGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 1000]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **groups** | [**int[]**](../Model/int.md)| unavailable | [optional]
 **pickbox_id** | [**int[]**](../Model/int.md)| PickBox.Id&#39;s to receive | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickBox**](../Model/SimplifySoftPecuniariusDataNetInventoryPickBox.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `warehousedataPickboxesIdDelete()`

```php
warehousedataPickboxesIdDelete($id)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickBoxesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->warehousedataPickboxesIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickBoxesApi->warehousedataPickboxesIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `warehousedataPickboxesIdPut()`

```php
warehousedataPickboxesIdPut($id, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickBoxesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->warehousedataPickboxesIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickBoxesApi->warehousedataPickboxesIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `warehousedataPickboxesPost()`

```php
warehousedataPickboxesPost($simplify_soft_pecuniarius_data_net_inventory_pick_box): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickBox
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickBoxesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_inventory_pick_box = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickBox(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickBox

try {
    $result = $apiInstance->warehousedataPickboxesPost($simplify_soft_pecuniarius_data_net_inventory_pick_box);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickBoxesApi->warehousedataPickboxesPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_inventory_pick_box** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickBox**](../Model/SimplifySoftPecuniariusDataNetInventoryPickBox.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickBox**](../Model/SimplifySoftPecuniariusDataNetInventoryPickBox.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
