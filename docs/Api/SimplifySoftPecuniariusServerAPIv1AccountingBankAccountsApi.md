# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1AccountingBankAccountsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountingBankGet()**](SimplifySoftPecuniariusServerAPIv1AccountingBankAccountsApi.md#accountingBankGet) | **GET** /accounting/bank | unavailable
[**accountingBankIdDelete()**](SimplifySoftPecuniariusServerAPIv1AccountingBankAccountsApi.md#accountingBankIdDelete) | **DELETE** /accounting/bank/{id} | unavailable
[**accountingBankIdGet()**](SimplifySoftPecuniariusServerAPIv1AccountingBankAccountsApi.md#accountingBankIdGet) | **GET** /accounting/bank/{id} | unavailable
[**accountingBankIdPut()**](SimplifySoftPecuniariusServerAPIv1AccountingBankAccountsApi.md#accountingBankIdPut) | **PUT** /accounting/bank/{id} | unavailable
[**accountingBankPost()**](SimplifySoftPecuniariusServerAPIv1AccountingBankAccountsApi.md#accountingBankPost) | **POST** /accounting/bank | unavailable


## `accountingBankGet()`

```php
accountingBankGet($limit, $index, $title_exact, $title_starts_with, $title_like, $id): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingBankAccount
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingBankAccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$title_exact = array('title_exact_example'); // string[] | Allows looking up BankAccount's using their Title. Uses Exact match.
$title_starts_with = array('title_starts_with_example'); // string[] | Allows looking up BankAccount's using their Title. Uses Starts-With match. Case Insensitive.
$title_like = 'title_like_example'; // string | Allows looking up BankAccount's using their Title. Uses DBFunction.Like match. Case Insensitive.
$id = array(56); // int[] | Uses ID's to query the BankAccount's.

try {
    $result = $apiInstance->accountingBankGet($limit, $index, $title_exact, $title_starts_with, $title_like, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingBankAccountsApi->accountingBankGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **title_exact** | [**string[]**](../Model/string.md)| Allows looking up BankAccount&#39;s using their Title. Uses Exact match. | [optional]
 **title_starts_with** | [**string[]**](../Model/string.md)| Allows looking up BankAccount&#39;s using their Title. Uses Starts-With match. Case Insensitive. | [optional]
 **title_like** | **string**| Allows looking up BankAccount&#39;s using their Title. Uses DBFunction.Like match. Case Insensitive. | [optional]
 **id** | [**int[]**](../Model/int.md)| Uses ID&#39;s to query the BankAccount&#39;s. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingBankAccount**](../Model/SimplifySoftPecuniariusDataNetAccountingBankAccount.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingBankIdDelete()`

```php
accountingBankIdDelete($id)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingBankAccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->accountingBankIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingBankAccountsApi->accountingBankIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingBankIdGet()`

```php
accountingBankIdGet($id)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingBankAccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->accountingBankIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingBankAccountsApi->accountingBankIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingBankIdPut()`

```php
accountingBankIdPut($id, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingBankAccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->accountingBankIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingBankAccountsApi->accountingBankIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingBankPost()`

```php
accountingBankPost($simplify_soft_pecuniarius_data_net_accounting_bank_account): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingBankAccount
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingBankAccountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_accounting_bank_account = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingBankAccount(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingBankAccount

try {
    $result = $apiInstance->accountingBankPost($simplify_soft_pecuniarius_data_net_accounting_bank_account);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingBankAccountsApi->accountingBankPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_accounting_bank_account** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingBankAccount**](../Model/SimplifySoftPecuniariusDataNetAccountingBankAccount.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingBankAccount**](../Model/SimplifySoftPecuniariusDataNetAccountingBankAccount.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
