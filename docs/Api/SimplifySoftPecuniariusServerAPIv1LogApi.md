# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1LogApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**logCountGet()**](SimplifySoftPecuniariusServerAPIv1LogApi.md#logCountGet) | **GET** /log/count | Allows to poll the log.
[**logGet()**](SimplifySoftPecuniariusServerAPIv1LogApi.md#logGet) | **GET** /log | Allows to poll the log.


## `logCountGet()`

```php
logCountGet($index, $limit, $executee, $attributedefinition, $attribute, $accountingaccount, $accountingitem, $bankaccount, $tax, $taxaccount, $taxdefinition, $transactionroot, $transactionevent, $transactionparty, $paymentdiscount, $paymentmethod, $paymentoption, $paymentprovidercontext, $authoritybase, $unit, $itemframe, $itemlink, $itemroot, $category, $pricegroup, $unitgroup, $unittier, $contact, $address, $addresskind, $contactpossibility, $contactkind, $externauthority, $endpoint, $file, $printdocument, $printcondition, $shipment, $deliverygoal, $invoice, $optiondefinition, $case, $caseoption, $receipt, $receiptoption, $position, $positionoption, $warehouse, $storage, $pickbox, $storagelocation, $deliveryoption, $deliveryprovidercontext, $picklist, $picklistentry, $tag, $taggroup, $customproperty, $currency, $price, $privilege)
```

Allows to poll the log.

Allows to poll the log.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1LogApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 5; // int | The amount of records to take.
$executee = array(56); // int[] | Limits the search to provided executee.
$attributedefinition = array(56); // int[] | Limits the search to provided attributedefinition.
$attribute = array(56); // int[] | Limits the search to provided attribute.
$accountingaccount = array(56); // int[] | Limits the search to provided accountingaccount.
$accountingitem = array(56); // int[] | Limits the search to provided accountingitem.
$bankaccount = array(56); // int[] | Limits the search to provided bankaccount.
$tax = array(56); // int[] | Limits the search to provided tax.
$taxaccount = array(56); // int[] | Limits the search to provided taxaccount.
$taxdefinition = array(56); // int[] | Limits the search to provided taxdefinition.
$transactionroot = array(56); // int[] | Limits the search to provided transactionroot.
$transactionevent = array(56); // int[] | Limits the search to provided transactionevent.
$transactionparty = array(56); // int[] | Limits the search to provided transactionparty.
$paymentdiscount = array(56); // int[] | Limits the search to provided paymentdiscount.
$paymentmethod = array(56); // int[] | Limits the search to provided paymentmethod.
$paymentoption = array(56); // int[] | Limits the search to provided paymentoption.
$paymentprovidercontext = array(56); // int[] | Limits the search to provided paymentprovidercontext.
$authoritybase = array(56); // int[] | Limits the search to provided authoritybase.
$unit = array(56); // int[] | Limits the search to provided unit.
$itemframe = array(56); // int[] | Limits the search to provided itemframe.
$itemlink = array(56); // int[] | Limits the search to provided itemlink.
$itemroot = array(56); // int[] | Limits the search to provided itemroot.
$category = array(56); // int[] | Limits the search to provided category.
$pricegroup = array(56); // int[] | Limits the search to provided pricegroup.
$unitgroup = array(56); // int[] | Limits the search to provided unitgroup.
$unittier = array(56); // int[] | Limits the search to provided unittier.
$contact = array(56); // int[] | Limits the search to provided contact.
$address = array(56); // int[] | Limits the search to provided address.
$addresskind = array(56); // int[] | Limits the search to provided addresskind.
$contactpossibility = array(56); // int[] | Limits the search to provided contactpossibility.
$contactkind = array(56); // int[] | Limits the search to provided contactkind.
$externauthority = array(56); // int[] | Limits the search to provided externauthority.
$endpoint = array(56); // int[] | Limits the search to provided endpoint.
$file = array(56); // int[] | Limits the search to provided file.
$printdocument = array(56); // int[] | Limits the search to provided printdocument.
$printcondition = array(56); // int[] | Limits the search to provided printcondition.
$shipment = array(56); // int[] | Limits the search to provided shipment.
$deliverygoal = array(56); // int[] | Limits the search to provided deliverygoal.
$invoice = array(56); // int[] | Limits the search to provided invoice.
$optiondefinition = array(56); // int[] | Limits the search to provided optiondefinition.
$case = array(56); // int[] | Limits the search to provided case.
$caseoption = array(56); // int[] | Limits the search to provided caseoption.
$receipt = array(56); // int[] | Limits the search to provided receipt.
$receiptoption = array(56); // int[] | Limits the search to provided receiptoption.
$position = array(56); // int[] | Limits the search to provided position.
$positionoption = array(56); // int[] | Limits the search to provided positionoption.
$warehouse = array(56); // int[] | Limits the search to provided warehouse.
$storage = array(56); // int[] | Limits the search to provided storage.
$pickbox = array(56); // int[] | Limits the search to provided pickbox.
$storagelocation = array(56); // int[] | Limits the search to provided storagelocation.
$deliveryoption = array(56); // int[] | Limits the search to provided deliveryoption.
$deliveryprovidercontext = array(56); // int[] | Limits the search to provided deliveryprovidercontext.
$picklist = array(56); // int[] | Limits the search to provided picklist.
$picklistentry = array(56); // int[] | Limits the search to provided picklistentry.
$tag = array(56); // int[] | Limits the search to provided tag.
$taggroup = array(56); // int[] | Limits the search to provided taggroup.
$customproperty = array(56); // int[] | Limits the search to provided customproperty.
$currency = array('currency_example'); // string[] | Limits the search to provided currency.
$price = array(56); // int[] | Limits the search to provided price.
$privilege = array('privilege_example'); // string[] | Limits the search to provided privilege.

try {
    $apiInstance->logCountGet($index, $limit, $executee, $attributedefinition, $attribute, $accountingaccount, $accountingitem, $bankaccount, $tax, $taxaccount, $taxdefinition, $transactionroot, $transactionevent, $transactionparty, $paymentdiscount, $paymentmethod, $paymentoption, $paymentprovidercontext, $authoritybase, $unit, $itemframe, $itemlink, $itemroot, $category, $pricegroup, $unitgroup, $unittier, $contact, $address, $addresskind, $contactpossibility, $contactkind, $externauthority, $endpoint, $file, $printdocument, $printcondition, $shipment, $deliverygoal, $invoice, $optiondefinition, $case, $caseoption, $receipt, $receiptoption, $position, $positionoption, $warehouse, $storage, $pickbox, $storagelocation, $deliveryoption, $deliveryprovidercontext, $picklist, $picklistentry, $tag, $taggroup, $customproperty, $currency, $price, $privilege);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1LogApi->logCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 5]
 **executee** | [**int[]**](../Model/int.md)| Limits the search to provided executee. | [optional]
 **attributedefinition** | [**int[]**](../Model/int.md)| Limits the search to provided attributedefinition. | [optional]
 **attribute** | [**int[]**](../Model/int.md)| Limits the search to provided attribute. | [optional]
 **accountingaccount** | [**int[]**](../Model/int.md)| Limits the search to provided accountingaccount. | [optional]
 **accountingitem** | [**int[]**](../Model/int.md)| Limits the search to provided accountingitem. | [optional]
 **bankaccount** | [**int[]**](../Model/int.md)| Limits the search to provided bankaccount. | [optional]
 **tax** | [**int[]**](../Model/int.md)| Limits the search to provided tax. | [optional]
 **taxaccount** | [**int[]**](../Model/int.md)| Limits the search to provided taxaccount. | [optional]
 **taxdefinition** | [**int[]**](../Model/int.md)| Limits the search to provided taxdefinition. | [optional]
 **transactionroot** | [**int[]**](../Model/int.md)| Limits the search to provided transactionroot. | [optional]
 **transactionevent** | [**int[]**](../Model/int.md)| Limits the search to provided transactionevent. | [optional]
 **transactionparty** | [**int[]**](../Model/int.md)| Limits the search to provided transactionparty. | [optional]
 **paymentdiscount** | [**int[]**](../Model/int.md)| Limits the search to provided paymentdiscount. | [optional]
 **paymentmethod** | [**int[]**](../Model/int.md)| Limits the search to provided paymentmethod. | [optional]
 **paymentoption** | [**int[]**](../Model/int.md)| Limits the search to provided paymentoption. | [optional]
 **paymentprovidercontext** | [**int[]**](../Model/int.md)| Limits the search to provided paymentprovidercontext. | [optional]
 **authoritybase** | [**int[]**](../Model/int.md)| Limits the search to provided authoritybase. | [optional]
 **unit** | [**int[]**](../Model/int.md)| Limits the search to provided unit. | [optional]
 **itemframe** | [**int[]**](../Model/int.md)| Limits the search to provided itemframe. | [optional]
 **itemlink** | [**int[]**](../Model/int.md)| Limits the search to provided itemlink. | [optional]
 **itemroot** | [**int[]**](../Model/int.md)| Limits the search to provided itemroot. | [optional]
 **category** | [**int[]**](../Model/int.md)| Limits the search to provided category. | [optional]
 **pricegroup** | [**int[]**](../Model/int.md)| Limits the search to provided pricegroup. | [optional]
 **unitgroup** | [**int[]**](../Model/int.md)| Limits the search to provided unitgroup. | [optional]
 **unittier** | [**int[]**](../Model/int.md)| Limits the search to provided unittier. | [optional]
 **contact** | [**int[]**](../Model/int.md)| Limits the search to provided contact. | [optional]
 **address** | [**int[]**](../Model/int.md)| Limits the search to provided address. | [optional]
 **addresskind** | [**int[]**](../Model/int.md)| Limits the search to provided addresskind. | [optional]
 **contactpossibility** | [**int[]**](../Model/int.md)| Limits the search to provided contactpossibility. | [optional]
 **contactkind** | [**int[]**](../Model/int.md)| Limits the search to provided contactkind. | [optional]
 **externauthority** | [**int[]**](../Model/int.md)| Limits the search to provided externauthority. | [optional]
 **endpoint** | [**int[]**](../Model/int.md)| Limits the search to provided endpoint. | [optional]
 **file** | [**int[]**](../Model/int.md)| Limits the search to provided file. | [optional]
 **printdocument** | [**int[]**](../Model/int.md)| Limits the search to provided printdocument. | [optional]
 **printcondition** | [**int[]**](../Model/int.md)| Limits the search to provided printcondition. | [optional]
 **shipment** | [**int[]**](../Model/int.md)| Limits the search to provided shipment. | [optional]
 **deliverygoal** | [**int[]**](../Model/int.md)| Limits the search to provided deliverygoal. | [optional]
 **invoice** | [**int[]**](../Model/int.md)| Limits the search to provided invoice. | [optional]
 **optiondefinition** | [**int[]**](../Model/int.md)| Limits the search to provided optiondefinition. | [optional]
 **case** | [**int[]**](../Model/int.md)| Limits the search to provided case. | [optional]
 **caseoption** | [**int[]**](../Model/int.md)| Limits the search to provided caseoption. | [optional]
 **receipt** | [**int[]**](../Model/int.md)| Limits the search to provided receipt. | [optional]
 **receiptoption** | [**int[]**](../Model/int.md)| Limits the search to provided receiptoption. | [optional]
 **position** | [**int[]**](../Model/int.md)| Limits the search to provided position. | [optional]
 **positionoption** | [**int[]**](../Model/int.md)| Limits the search to provided positionoption. | [optional]
 **warehouse** | [**int[]**](../Model/int.md)| Limits the search to provided warehouse. | [optional]
 **storage** | [**int[]**](../Model/int.md)| Limits the search to provided storage. | [optional]
 **pickbox** | [**int[]**](../Model/int.md)| Limits the search to provided pickbox. | [optional]
 **storagelocation** | [**int[]**](../Model/int.md)| Limits the search to provided storagelocation. | [optional]
 **deliveryoption** | [**int[]**](../Model/int.md)| Limits the search to provided deliveryoption. | [optional]
 **deliveryprovidercontext** | [**int[]**](../Model/int.md)| Limits the search to provided deliveryprovidercontext. | [optional]
 **picklist** | [**int[]**](../Model/int.md)| Limits the search to provided picklist. | [optional]
 **picklistentry** | [**int[]**](../Model/int.md)| Limits the search to provided picklistentry. | [optional]
 **tag** | [**int[]**](../Model/int.md)| Limits the search to provided tag. | [optional]
 **taggroup** | [**int[]**](../Model/int.md)| Limits the search to provided taggroup. | [optional]
 **customproperty** | [**int[]**](../Model/int.md)| Limits the search to provided customproperty. | [optional]
 **currency** | [**string[]**](../Model/string.md)| Limits the search to provided currency. | [optional]
 **price** | [**int[]**](../Model/int.md)| Limits the search to provided price. | [optional]
 **privilege** | [**string[]**](../Model/string.md)| Limits the search to provided privilege. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `logGet()`

```php
logGet($index, $limit, $executee, $attributedefinition, $attribute, $accountingaccount, $accountingitem, $bankaccount, $tax, $taxaccount, $taxdefinition, $transactionroot, $transactionevent, $transactionparty, $paymentdiscount, $paymentmethod, $paymentoption, $paymentprovidercontext, $authoritybase, $unit, $itemframe, $itemlink, $itemroot, $category, $pricegroup, $unitgroup, $unittier, $contact, $address, $addresskind, $contactpossibility, $contactkind, $externauthority, $endpoint, $file, $printdocument, $printcondition, $shipment, $deliverygoal, $invoice, $optiondefinition, $case, $caseoption, $receipt, $receiptoption, $position, $positionoption, $warehouse, $storage, $pickbox, $storagelocation, $deliveryoption, $deliveryprovidercontext, $picklist, $picklistentry, $tag, $taggroup, $customproperty, $currency, $price, $privilege): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetChangeLog
```

Allows to poll the log.

Allows to poll the log.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1LogApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 5; // int | The amount of records to take.
$executee = array(56); // int[] | Limits the search to provided executee.
$attributedefinition = array(56); // int[] | Limits the search to provided attributedefinition.
$attribute = array(56); // int[] | Limits the search to provided attribute.
$accountingaccount = array(56); // int[] | Limits the search to provided accountingaccount.
$accountingitem = array(56); // int[] | Limits the search to provided accountingitem.
$bankaccount = array(56); // int[] | Limits the search to provided bankaccount.
$tax = array(56); // int[] | Limits the search to provided tax.
$taxaccount = array(56); // int[] | Limits the search to provided taxaccount.
$taxdefinition = array(56); // int[] | Limits the search to provided taxdefinition.
$transactionroot = array(56); // int[] | Limits the search to provided transactionroot.
$transactionevent = array(56); // int[] | Limits the search to provided transactionevent.
$transactionparty = array(56); // int[] | Limits the search to provided transactionparty.
$paymentdiscount = array(56); // int[] | Limits the search to provided paymentdiscount.
$paymentmethod = array(56); // int[] | Limits the search to provided paymentmethod.
$paymentoption = array(56); // int[] | Limits the search to provided paymentoption.
$paymentprovidercontext = array(56); // int[] | Limits the search to provided paymentprovidercontext.
$authoritybase = array(56); // int[] | Limits the search to provided authoritybase.
$unit = array(56); // int[] | Limits the search to provided unit.
$itemframe = array(56); // int[] | Limits the search to provided itemframe.
$itemlink = array(56); // int[] | Limits the search to provided itemlink.
$itemroot = array(56); // int[] | Limits the search to provided itemroot.
$category = array(56); // int[] | Limits the search to provided category.
$pricegroup = array(56); // int[] | Limits the search to provided pricegroup.
$unitgroup = array(56); // int[] | Limits the search to provided unitgroup.
$unittier = array(56); // int[] | Limits the search to provided unittier.
$contact = array(56); // int[] | Limits the search to provided contact.
$address = array(56); // int[] | Limits the search to provided address.
$addresskind = array(56); // int[] | Limits the search to provided addresskind.
$contactpossibility = array(56); // int[] | Limits the search to provided contactpossibility.
$contactkind = array(56); // int[] | Limits the search to provided contactkind.
$externauthority = array(56); // int[] | Limits the search to provided externauthority.
$endpoint = array(56); // int[] | Limits the search to provided endpoint.
$file = array(56); // int[] | Limits the search to provided file.
$printdocument = array(56); // int[] | Limits the search to provided printdocument.
$printcondition = array(56); // int[] | Limits the search to provided printcondition.
$shipment = array(56); // int[] | Limits the search to provided shipment.
$deliverygoal = array(56); // int[] | Limits the search to provided deliverygoal.
$invoice = array(56); // int[] | Limits the search to provided invoice.
$optiondefinition = array(56); // int[] | Limits the search to provided optiondefinition.
$case = array(56); // int[] | Limits the search to provided case.
$caseoption = array(56); // int[] | Limits the search to provided caseoption.
$receipt = array(56); // int[] | Limits the search to provided receipt.
$receiptoption = array(56); // int[] | Limits the search to provided receiptoption.
$position = array(56); // int[] | Limits the search to provided position.
$positionoption = array(56); // int[] | Limits the search to provided positionoption.
$warehouse = array(56); // int[] | Limits the search to provided warehouse.
$storage = array(56); // int[] | Limits the search to provided storage.
$pickbox = array(56); // int[] | Limits the search to provided pickbox.
$storagelocation = array(56); // int[] | Limits the search to provided storagelocation.
$deliveryoption = array(56); // int[] | Limits the search to provided deliveryoption.
$deliveryprovidercontext = array(56); // int[] | Limits the search to provided deliveryprovidercontext.
$picklist = array(56); // int[] | Limits the search to provided picklist.
$picklistentry = array(56); // int[] | Limits the search to provided picklistentry.
$tag = array(56); // int[] | Limits the search to provided tag.
$taggroup = array(56); // int[] | Limits the search to provided taggroup.
$customproperty = array(56); // int[] | Limits the search to provided customproperty.
$currency = array('currency_example'); // string[] | Limits the search to provided currency.
$price = array(56); // int[] | Limits the search to provided price.
$privilege = array('privilege_example'); // string[] | Limits the search to provided privilege.

try {
    $result = $apiInstance->logGet($index, $limit, $executee, $attributedefinition, $attribute, $accountingaccount, $accountingitem, $bankaccount, $tax, $taxaccount, $taxdefinition, $transactionroot, $transactionevent, $transactionparty, $paymentdiscount, $paymentmethod, $paymentoption, $paymentprovidercontext, $authoritybase, $unit, $itemframe, $itemlink, $itemroot, $category, $pricegroup, $unitgroup, $unittier, $contact, $address, $addresskind, $contactpossibility, $contactkind, $externauthority, $endpoint, $file, $printdocument, $printcondition, $shipment, $deliverygoal, $invoice, $optiondefinition, $case, $caseoption, $receipt, $receiptoption, $position, $positionoption, $warehouse, $storage, $pickbox, $storagelocation, $deliveryoption, $deliveryprovidercontext, $picklist, $picklistentry, $tag, $taggroup, $customproperty, $currency, $price, $privilege);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1LogApi->logGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 5]
 **executee** | [**int[]**](../Model/int.md)| Limits the search to provided executee. | [optional]
 **attributedefinition** | [**int[]**](../Model/int.md)| Limits the search to provided attributedefinition. | [optional]
 **attribute** | [**int[]**](../Model/int.md)| Limits the search to provided attribute. | [optional]
 **accountingaccount** | [**int[]**](../Model/int.md)| Limits the search to provided accountingaccount. | [optional]
 **accountingitem** | [**int[]**](../Model/int.md)| Limits the search to provided accountingitem. | [optional]
 **bankaccount** | [**int[]**](../Model/int.md)| Limits the search to provided bankaccount. | [optional]
 **tax** | [**int[]**](../Model/int.md)| Limits the search to provided tax. | [optional]
 **taxaccount** | [**int[]**](../Model/int.md)| Limits the search to provided taxaccount. | [optional]
 **taxdefinition** | [**int[]**](../Model/int.md)| Limits the search to provided taxdefinition. | [optional]
 **transactionroot** | [**int[]**](../Model/int.md)| Limits the search to provided transactionroot. | [optional]
 **transactionevent** | [**int[]**](../Model/int.md)| Limits the search to provided transactionevent. | [optional]
 **transactionparty** | [**int[]**](../Model/int.md)| Limits the search to provided transactionparty. | [optional]
 **paymentdiscount** | [**int[]**](../Model/int.md)| Limits the search to provided paymentdiscount. | [optional]
 **paymentmethod** | [**int[]**](../Model/int.md)| Limits the search to provided paymentmethod. | [optional]
 **paymentoption** | [**int[]**](../Model/int.md)| Limits the search to provided paymentoption. | [optional]
 **paymentprovidercontext** | [**int[]**](../Model/int.md)| Limits the search to provided paymentprovidercontext. | [optional]
 **authoritybase** | [**int[]**](../Model/int.md)| Limits the search to provided authoritybase. | [optional]
 **unit** | [**int[]**](../Model/int.md)| Limits the search to provided unit. | [optional]
 **itemframe** | [**int[]**](../Model/int.md)| Limits the search to provided itemframe. | [optional]
 **itemlink** | [**int[]**](../Model/int.md)| Limits the search to provided itemlink. | [optional]
 **itemroot** | [**int[]**](../Model/int.md)| Limits the search to provided itemroot. | [optional]
 **category** | [**int[]**](../Model/int.md)| Limits the search to provided category. | [optional]
 **pricegroup** | [**int[]**](../Model/int.md)| Limits the search to provided pricegroup. | [optional]
 **unitgroup** | [**int[]**](../Model/int.md)| Limits the search to provided unitgroup. | [optional]
 **unittier** | [**int[]**](../Model/int.md)| Limits the search to provided unittier. | [optional]
 **contact** | [**int[]**](../Model/int.md)| Limits the search to provided contact. | [optional]
 **address** | [**int[]**](../Model/int.md)| Limits the search to provided address. | [optional]
 **addresskind** | [**int[]**](../Model/int.md)| Limits the search to provided addresskind. | [optional]
 **contactpossibility** | [**int[]**](../Model/int.md)| Limits the search to provided contactpossibility. | [optional]
 **contactkind** | [**int[]**](../Model/int.md)| Limits the search to provided contactkind. | [optional]
 **externauthority** | [**int[]**](../Model/int.md)| Limits the search to provided externauthority. | [optional]
 **endpoint** | [**int[]**](../Model/int.md)| Limits the search to provided endpoint. | [optional]
 **file** | [**int[]**](../Model/int.md)| Limits the search to provided file. | [optional]
 **printdocument** | [**int[]**](../Model/int.md)| Limits the search to provided printdocument. | [optional]
 **printcondition** | [**int[]**](../Model/int.md)| Limits the search to provided printcondition. | [optional]
 **shipment** | [**int[]**](../Model/int.md)| Limits the search to provided shipment. | [optional]
 **deliverygoal** | [**int[]**](../Model/int.md)| Limits the search to provided deliverygoal. | [optional]
 **invoice** | [**int[]**](../Model/int.md)| Limits the search to provided invoice. | [optional]
 **optiondefinition** | [**int[]**](../Model/int.md)| Limits the search to provided optiondefinition. | [optional]
 **case** | [**int[]**](../Model/int.md)| Limits the search to provided case. | [optional]
 **caseoption** | [**int[]**](../Model/int.md)| Limits the search to provided caseoption. | [optional]
 **receipt** | [**int[]**](../Model/int.md)| Limits the search to provided receipt. | [optional]
 **receiptoption** | [**int[]**](../Model/int.md)| Limits the search to provided receiptoption. | [optional]
 **position** | [**int[]**](../Model/int.md)| Limits the search to provided position. | [optional]
 **positionoption** | [**int[]**](../Model/int.md)| Limits the search to provided positionoption. | [optional]
 **warehouse** | [**int[]**](../Model/int.md)| Limits the search to provided warehouse. | [optional]
 **storage** | [**int[]**](../Model/int.md)| Limits the search to provided storage. | [optional]
 **pickbox** | [**int[]**](../Model/int.md)| Limits the search to provided pickbox. | [optional]
 **storagelocation** | [**int[]**](../Model/int.md)| Limits the search to provided storagelocation. | [optional]
 **deliveryoption** | [**int[]**](../Model/int.md)| Limits the search to provided deliveryoption. | [optional]
 **deliveryprovidercontext** | [**int[]**](../Model/int.md)| Limits the search to provided deliveryprovidercontext. | [optional]
 **picklist** | [**int[]**](../Model/int.md)| Limits the search to provided picklist. | [optional]
 **picklistentry** | [**int[]**](../Model/int.md)| Limits the search to provided picklistentry. | [optional]
 **tag** | [**int[]**](../Model/int.md)| Limits the search to provided tag. | [optional]
 **taggroup** | [**int[]**](../Model/int.md)| Limits the search to provided taggroup. | [optional]
 **customproperty** | [**int[]**](../Model/int.md)| Limits the search to provided customproperty. | [optional]
 **currency** | [**string[]**](../Model/string.md)| Limits the search to provided currency. | [optional]
 **price** | [**int[]**](../Model/int.md)| Limits the search to provided price. | [optional]
 **privilege** | [**string[]**](../Model/string.md)| Limits the search to provided privilege. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetChangeLog**](../Model/SimplifySoftPecuniariusDataNetChangeLog.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
