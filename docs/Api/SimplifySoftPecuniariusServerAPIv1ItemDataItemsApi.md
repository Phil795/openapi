# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**itemRootAllCountGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootAllCountGet) | **GET** /item/root/all/count | Receives a list of ItemRoot&#39;s according to provided filtering provided via GET parameters.
[**itemRootAllFrameAllCountGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootAllFrameAllCountGet) | **GET** /item/root/all/frame/all/count | Receives a list of ItemRoot&#39;s according to provided filtering provided via GET parameters.
[**itemRootAllFrameAllGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootAllFrameAllGet) | **GET** /item/root/all/frame/all | Receives a list of ItemRoot&#39;s according to provided filtering provided via GET parameters.
[**itemRootAllFrameAllIdsGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootAllFrameAllIdsGet) | **GET** /item/root/all/frame/all/ids | Receives a list of ItemRoot&#39;s according to provided filtering provided via GET parameters.
[**itemRootAllGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootAllGet) | **GET** /item/root/all | Receives a list of ItemRoot&#39;s according to provided filtering provided via GET parameters.
[**itemRootAllIdsGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootAllIdsGet) | **GET** /item/root/all/ids | Receives a list of ItemRoot&#39;s according to provided filtering provided via GET parameters.
[**itemRootAllSearchGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootAllSearchGet) | **GET** /item/root/all/search | Receives a list of ItemRoot&#39;s according to provided filtering provided via GET parameters.
[**itemRootIdentCategoryAllGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentCategoryAllGet) | **GET** /item/root/{ident}/category/all | Receives the categories this item is located in via its ID or SKU.
[**itemRootIdentCategoryCatidDelete()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentCategoryCatidDelete) | **DELETE** /item/root/{ident}/category/{catid} | Allows to remove a category relation from an item via its ID or SKU.
[**itemRootIdentCategoryCatidPost()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentCategoryCatidPost) | **POST** /item/root/{ident}/category/{catid} | Allows to add a category relation onto an item via its ID or SKU.
[**itemRootIdentDelete()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentDelete) | **DELETE** /item/root/{ident} | Provides the ability to delete (mark as IsSoftDeleted) a given item via its ID or SKU.
[**itemRootIdentFileAllGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentFileAllGet) | **GET** /item/root/{ident}/file/all | Receives the files this item is linked with via its ID or SKU.
[**itemRootIdentFileFileidDefaultPost()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentFileFileidDefaultPost) | **POST** /item/root/{ident}/file/{fileid}/default | Allows to set the default file relation of an item via its ID or SKU.
[**itemRootIdentFileFileidDelete()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentFileFileidDelete) | **DELETE** /item/root/{ident}/file/{fileid} | Allows to remove a file relation from an item via its ID or SKU.
[**itemRootIdentFileFileidPost()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentFileFileidPost) | **POST** /item/root/{ident}/file/{fileid} | Allows to add a file relation onto an item via its ID or SKU.
[**itemRootIdentFrameFrameidGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentFrameFrameidGet) | **GET** /item/root/{ident}/frame/{frameid} | Allows to receive a given frame by its id.
[**itemRootIdentFramePut()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentFramePut) | **PUT** /item/root/{ident}/frame | Provides the ability to update a given items frame via its ID or SKU.
[**itemRootIdentGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentGet) | **GET** /item/root/{ident} | Receives a single item via its ID or SKU.
[**itemRootIdentLinkedAllGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentLinkedAllGet) | **GET** /item/root/{ident}/linked/all | Receives the list of linked items related to another item. Both can be identified via either their corresponding ID or SKU.
[**itemRootIdentLinkedOtherDelete()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentLinkedOtherDelete) | **DELETE** /item/root/{ident}/linked/{other} | Allows to remove an item relation from provided item. Both can be identified via either their corresponding ID or SKU.
[**itemRootIdentLinkedOtherPost()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentLinkedOtherPost) | **POST** /item/root/{ident}/linked/{other} | Allows to add an item relation onto provided item. Both can be identified via either their corresponding ID or SKU.
[**itemRootIdentLinkedOtherPut()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentLinkedOtherPut) | **PUT** /item/root/{ident}/linked/{other} | Allows to update an item relation onto provided item. Both can be identified via either their corresponding ID or SKU.
[**itemRootIdentPut()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentPut) | **PUT** /item/root/{ident} | Provides the ability to update a given item via its ID or SKU.
[**itemRootIdentTagAllGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentTagAllGet) | **GET** /item/root/{ident}/tag/all | Receives the tags this item is linked with via its ID or SKU.
[**itemRootIdentTagTagidDelete()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentTagTagidDelete) | **DELETE** /item/root/{ident}/tag/{tagid} | Allows to remove a tag relation from an item via its ID or SKU.
[**itemRootIdentTagTagidPost()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootIdentTagTagidPost) | **POST** /item/root/{ident}/tag/{tagid} | Allows to add a tag relation onto an item via its ID or SKU.
[**itemRootPost()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemRootPost) | **POST** /item/root | Provides the ability to create a new item.
[**itemdataRootSkuNextGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemdataRootSkuNextGet) | **GET** /itemdata/root/{sku}/next | Receives the id, that follows logically after this one (determined via SKU).
[**itemdataRootSkuPreviousGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi.md#itemdataRootSkuPreviousGet) | **GET** /itemdata/root/{sku}/previous | Receives the id, that came logically beofre this one (determined via SKU).


## `itemRootAllCountGet()`

```php
itemRootAllCountGet($limit, $index, $include_deleted, $psql)
```

Receives a list of ItemRoot's according to provided filtering provided via GET parameters.

Receives a list of ItemRoot's according to provided filtering provided via GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$include_deleted = false; // bool | Allows to include soft-deleted items.
$psql = 'psql_example'; // string | The PSQL query.

try {
    $apiInstance->itemRootAllCountGet($limit, $index, $include_deleted, $psql);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **include_deleted** | **bool**| Allows to include soft-deleted items. | [optional] [default to false]
 **psql** | **string**| The PSQL query. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootAllFrameAllCountGet()`

```php
itemRootAllFrameAllCountGet($limit, $index, $frame_id, $root_id, $gtin_e, $gtin_sw, $gtin_l, $sku_e, $sku_sw, $sku_l, $title_e, $title_sw, $title_l, $item_type, $include_parent, $include_parent_tax_def, $chain_using_or, $force_id)
```

Receives a list of ItemRoot's according to provided filtering provided via GET parameters.

Receives a list of ItemRoot's according to provided filtering provided via GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$frame_id = array(56); // int[] | Allows to limit the range of items to the provided ItemFrames. Note that this will still always yield a ItemRoot with the LATEST frame revision, not the one the ItemRoot was found with.
$root_id = array(56); // int[] | Allows to limit the range of items to the provided ItemRoots.
$gtin_e = array('gtin_e_example'); // string[] | Checks the GlobalTradeItemNumber field on Item using exact comparison. If sku-e is provided too, gtin-e & sku-e will be linked via OR, not AND.
$gtin_sw = array('gtin_sw_example'); // string[] | Checks the GlobalTradeItemNumber field on Item using StartsWith. If sku-sw is provided too, gtin-sw & sku-sw will be linked via OR, not AND.
$gtin_l = 'gtin_l_example'; // string | Checks the GlobalTradeItemNumber field on Item using the DB-Function LIKE. If sku-l is provided too, gtin-l & sku-l will be linked via OR, not AND.
$sku_e = array('sku_e_example'); // string[] | Checks the StockKeepingUnit field on Item using exact comparison. If gtin-e is provided too, gtin-e & gtin-e will be linked via OR, not AND.
$sku_sw = array('sku_sw_example'); // string[] | Checks the StockKeepingUnit field on Item using StartsWith. If gtin-sw is provided too, gtin-sw & sku-sw will be linked via OR, not AND.
$sku_l = 'sku_l_example'; // string | Checks the StockKeepingUnit field on Item using the DB-Function LIKE. If gtin-l is provided too, gtin-l & sku-l will be linked via OR, not AND.
$title_e = array('title_e_example'); // string[] | Checks the Name field on Item using the DB-Function LIKE.
$title_sw = array('title_sw_example'); // string[] | Checks the Name field on Item using the DB-Function LIKE.
$title_l = 'title_l_example'; // string | Checks the Name field on Item using the DB-Function LIKE.
$item_type = array('item_type_example'); // string[] | Applies a filter, matching all ItemFrame instances where the parents ItemRoot.ItemType is in the provided EItemType collection. Always applied with AND, regardless of chain-using-or.
$include_parent = false; // bool | Includes ItemFrame.Parent in the result.
$include_parent_tax_def = false; // bool | Includes ItemFrame.Parent.TaxDefinition
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.
$force_id = false; // bool | Enforces AND linking, regardless of chain-using-or, for '*-id' parameters.

try {
    $apiInstance->itemRootAllFrameAllCountGet($limit, $index, $frame_id, $root_id, $gtin_e, $gtin_sw, $gtin_l, $sku_e, $sku_sw, $sku_l, $title_e, $title_sw, $title_l, $item_type, $include_parent, $include_parent_tax_def, $chain_using_or, $force_id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootAllFrameAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **frame_id** | [**int[]**](../Model/int.md)| Allows to limit the range of items to the provided ItemFrames. Note that this will still always yield a ItemRoot with the LATEST frame revision, not the one the ItemRoot was found with. | [optional]
 **root_id** | [**int[]**](../Model/int.md)| Allows to limit the range of items to the provided ItemRoots. | [optional]
 **gtin_e** | [**string[]**](../Model/string.md)| Checks the GlobalTradeItemNumber field on Item using exact comparison. If sku-e is provided too, gtin-e &amp; sku-e will be linked via OR, not AND. | [optional]
 **gtin_sw** | [**string[]**](../Model/string.md)| Checks the GlobalTradeItemNumber field on Item using StartsWith. If sku-sw is provided too, gtin-sw &amp; sku-sw will be linked via OR, not AND. | [optional]
 **gtin_l** | **string**| Checks the GlobalTradeItemNumber field on Item using the DB-Function LIKE. If sku-l is provided too, gtin-l &amp; sku-l will be linked via OR, not AND. | [optional]
 **sku_e** | [**string[]**](../Model/string.md)| Checks the StockKeepingUnit field on Item using exact comparison. If gtin-e is provided too, gtin-e &amp; gtin-e will be linked via OR, not AND. | [optional]
 **sku_sw** | [**string[]**](../Model/string.md)| Checks the StockKeepingUnit field on Item using StartsWith. If gtin-sw is provided too, gtin-sw &amp; sku-sw will be linked via OR, not AND. | [optional]
 **sku_l** | **string**| Checks the StockKeepingUnit field on Item using the DB-Function LIKE. If gtin-l is provided too, gtin-l &amp; sku-l will be linked via OR, not AND. | [optional]
 **title_e** | [**string[]**](../Model/string.md)| Checks the Name field on Item using the DB-Function LIKE. | [optional]
 **title_sw** | [**string[]**](../Model/string.md)| Checks the Name field on Item using the DB-Function LIKE. | [optional]
 **title_l** | **string**| Checks the Name field on Item using the DB-Function LIKE. | [optional]
 **item_type** | [**string[]**](../Model/string.md)| Applies a filter, matching all ItemFrame instances where the parents ItemRoot.ItemType is in the provided EItemType collection. Always applied with AND, regardless of chain-using-or. | [optional]
 **include_parent** | **bool**| Includes ItemFrame.Parent in the result. | [optional] [default to false]
 **include_parent_tax_def** | **bool**| Includes ItemFrame.Parent.TaxDefinition | [optional] [default to false]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]
 **force_id** | **bool**| Enforces AND linking, regardless of chain-using-or, for &#39;*-id&#39; parameters. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootAllFrameAllGet()`

```php
itemRootAllFrameAllGet($limit, $index, $frame_id, $root_id, $gtin_e, $gtin_sw, $gtin_l, $sku_e, $sku_sw, $sku_l, $title_e, $title_sw, $title_l, $item_type, $include_parent, $include_parent_tax_def, $chain_using_or, $force_id): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemFrame
```

Receives a list of ItemRoot's according to provided filtering provided via GET parameters.

Receives a list of ItemRoot's according to provided filtering provided via GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$frame_id = array(56); // int[] | Allows to limit the range of items to the provided ItemFrames. Note that this will still always yield a ItemRoot with the LATEST frame revision, not the one the ItemRoot was found with.
$root_id = array(56); // int[] | Allows to limit the range of items to the provided ItemRoots.
$gtin_e = array('gtin_e_example'); // string[] | Checks the GlobalTradeItemNumber field on Item using exact comparison. If sku-e is provided too, gtin-e & sku-e will be linked via OR, not AND.
$gtin_sw = array('gtin_sw_example'); // string[] | Checks the GlobalTradeItemNumber field on Item using StartsWith. If sku-sw is provided too, gtin-sw & sku-sw will be linked via OR, not AND.
$gtin_l = 'gtin_l_example'; // string | Checks the GlobalTradeItemNumber field on Item using the DB-Function LIKE. If sku-l is provided too, gtin-l & sku-l will be linked via OR, not AND.
$sku_e = array('sku_e_example'); // string[] | Checks the StockKeepingUnit field on Item using exact comparison. If gtin-e is provided too, gtin-e & gtin-e will be linked via OR, not AND.
$sku_sw = array('sku_sw_example'); // string[] | Checks the StockKeepingUnit field on Item using StartsWith. If gtin-sw is provided too, gtin-sw & sku-sw will be linked via OR, not AND.
$sku_l = 'sku_l_example'; // string | Checks the StockKeepingUnit field on Item using the DB-Function LIKE. If gtin-l is provided too, gtin-l & sku-l will be linked via OR, not AND.
$title_e = array('title_e_example'); // string[] | Checks the Name field on Item using the DB-Function LIKE.
$title_sw = array('title_sw_example'); // string[] | Checks the Name field on Item using the DB-Function LIKE.
$title_l = 'title_l_example'; // string | Checks the Name field on Item using the DB-Function LIKE.
$item_type = array('item_type_example'); // string[] | Applies a filter, matching all ItemFrame instances where the parents ItemRoot.ItemType is in the provided EItemType collection. Always applied with AND, regardless of chain-using-or.
$include_parent = false; // bool | Includes ItemFrame.Parent in the result.
$include_parent_tax_def = false; // bool | Includes ItemFrame.Parent.TaxDefinition
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.
$force_id = false; // bool | Enforces AND linking, regardless of chain-using-or, for '*-id' parameters.

try {
    $result = $apiInstance->itemRootAllFrameAllGet($limit, $index, $frame_id, $root_id, $gtin_e, $gtin_sw, $gtin_l, $sku_e, $sku_sw, $sku_l, $title_e, $title_sw, $title_l, $item_type, $include_parent, $include_parent_tax_def, $chain_using_or, $force_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootAllFrameAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **frame_id** | [**int[]**](../Model/int.md)| Allows to limit the range of items to the provided ItemFrames. Note that this will still always yield a ItemRoot with the LATEST frame revision, not the one the ItemRoot was found with. | [optional]
 **root_id** | [**int[]**](../Model/int.md)| Allows to limit the range of items to the provided ItemRoots. | [optional]
 **gtin_e** | [**string[]**](../Model/string.md)| Checks the GlobalTradeItemNumber field on Item using exact comparison. If sku-e is provided too, gtin-e &amp; sku-e will be linked via OR, not AND. | [optional]
 **gtin_sw** | [**string[]**](../Model/string.md)| Checks the GlobalTradeItemNumber field on Item using StartsWith. If sku-sw is provided too, gtin-sw &amp; sku-sw will be linked via OR, not AND. | [optional]
 **gtin_l** | **string**| Checks the GlobalTradeItemNumber field on Item using the DB-Function LIKE. If sku-l is provided too, gtin-l &amp; sku-l will be linked via OR, not AND. | [optional]
 **sku_e** | [**string[]**](../Model/string.md)| Checks the StockKeepingUnit field on Item using exact comparison. If gtin-e is provided too, gtin-e &amp; gtin-e will be linked via OR, not AND. | [optional]
 **sku_sw** | [**string[]**](../Model/string.md)| Checks the StockKeepingUnit field on Item using StartsWith. If gtin-sw is provided too, gtin-sw &amp; sku-sw will be linked via OR, not AND. | [optional]
 **sku_l** | **string**| Checks the StockKeepingUnit field on Item using the DB-Function LIKE. If gtin-l is provided too, gtin-l &amp; sku-l will be linked via OR, not AND. | [optional]
 **title_e** | [**string[]**](../Model/string.md)| Checks the Name field on Item using the DB-Function LIKE. | [optional]
 **title_sw** | [**string[]**](../Model/string.md)| Checks the Name field on Item using the DB-Function LIKE. | [optional]
 **title_l** | **string**| Checks the Name field on Item using the DB-Function LIKE. | [optional]
 **item_type** | [**string[]**](../Model/string.md)| Applies a filter, matching all ItemFrame instances where the parents ItemRoot.ItemType is in the provided EItemType collection. Always applied with AND, regardless of chain-using-or. | [optional]
 **include_parent** | **bool**| Includes ItemFrame.Parent in the result. | [optional] [default to false]
 **include_parent_tax_def** | **bool**| Includes ItemFrame.Parent.TaxDefinition | [optional] [default to false]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]
 **force_id** | **bool**| Enforces AND linking, regardless of chain-using-or, for &#39;*-id&#39; parameters. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemFrame**](../Model/SimplifySoftPecuniariusDataNetItemDataItemFrame.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootAllFrameAllIdsGet()`

```php
itemRootAllFrameAllIdsGet($limit, $index, $frame_id, $root_id, $gtin_e, $gtin_sw, $gtin_l, $sku_e, $sku_sw, $sku_l, $title_e, $title_sw, $title_l, $item_type, $include_parent, $include_parent_tax_def, $chain_using_or, $force_id): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32
```

Receives a list of ItemRoot's according to provided filtering provided via GET parameters.

Receives a list of ItemRoot's according to provided filtering provided via GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$frame_id = array(56); // int[] | Allows to limit the range of items to the provided ItemFrames. Note that this will still always yield a ItemRoot with the LATEST frame revision, not the one the ItemRoot was found with.
$root_id = array(56); // int[] | Allows to limit the range of items to the provided ItemRoots.
$gtin_e = array('gtin_e_example'); // string[] | Checks the GlobalTradeItemNumber field on Item using exact comparison. If sku-e is provided too, gtin-e & sku-e will be linked via OR, not AND.
$gtin_sw = array('gtin_sw_example'); // string[] | Checks the GlobalTradeItemNumber field on Item using StartsWith. If sku-sw is provided too, gtin-sw & sku-sw will be linked via OR, not AND.
$gtin_l = 'gtin_l_example'; // string | Checks the GlobalTradeItemNumber field on Item using the DB-Function LIKE. If sku-l is provided too, gtin-l & sku-l will be linked via OR, not AND.
$sku_e = array('sku_e_example'); // string[] | Checks the StockKeepingUnit field on Item using exact comparison. If gtin-e is provided too, gtin-e & gtin-e will be linked via OR, not AND.
$sku_sw = array('sku_sw_example'); // string[] | Checks the StockKeepingUnit field on Item using StartsWith. If gtin-sw is provided too, gtin-sw & sku-sw will be linked via OR, not AND.
$sku_l = 'sku_l_example'; // string | Checks the StockKeepingUnit field on Item using the DB-Function LIKE. If gtin-l is provided too, gtin-l & sku-l will be linked via OR, not AND.
$title_e = array('title_e_example'); // string[] | Checks the Name field on Item using the DB-Function LIKE.
$title_sw = array('title_sw_example'); // string[] | Checks the Name field on Item using the DB-Function LIKE.
$title_l = 'title_l_example'; // string | Checks the Name field on Item using the DB-Function LIKE.
$item_type = array('item_type_example'); // string[] | Applies a filter, matching all ItemFrame instances where the parents ItemRoot.ItemType is in the provided EItemType collection. Always applied with AND, regardless of chain-using-or.
$include_parent = false; // bool | Includes ItemFrame.Parent in the result.
$include_parent_tax_def = false; // bool | Includes ItemFrame.Parent.TaxDefinition
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.
$force_id = false; // bool | Enforces AND linking, regardless of chain-using-or, for '*-id' parameters.

try {
    $result = $apiInstance->itemRootAllFrameAllIdsGet($limit, $index, $frame_id, $root_id, $gtin_e, $gtin_sw, $gtin_l, $sku_e, $sku_sw, $sku_l, $title_e, $title_sw, $title_l, $item_type, $include_parent, $include_parent_tax_def, $chain_using_or, $force_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootAllFrameAllIdsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **frame_id** | [**int[]**](../Model/int.md)| Allows to limit the range of items to the provided ItemFrames. Note that this will still always yield a ItemRoot with the LATEST frame revision, not the one the ItemRoot was found with. | [optional]
 **root_id** | [**int[]**](../Model/int.md)| Allows to limit the range of items to the provided ItemRoots. | [optional]
 **gtin_e** | [**string[]**](../Model/string.md)| Checks the GlobalTradeItemNumber field on Item using exact comparison. If sku-e is provided too, gtin-e &amp; sku-e will be linked via OR, not AND. | [optional]
 **gtin_sw** | [**string[]**](../Model/string.md)| Checks the GlobalTradeItemNumber field on Item using StartsWith. If sku-sw is provided too, gtin-sw &amp; sku-sw will be linked via OR, not AND. | [optional]
 **gtin_l** | **string**| Checks the GlobalTradeItemNumber field on Item using the DB-Function LIKE. If sku-l is provided too, gtin-l &amp; sku-l will be linked via OR, not AND. | [optional]
 **sku_e** | [**string[]**](../Model/string.md)| Checks the StockKeepingUnit field on Item using exact comparison. If gtin-e is provided too, gtin-e &amp; gtin-e will be linked via OR, not AND. | [optional]
 **sku_sw** | [**string[]**](../Model/string.md)| Checks the StockKeepingUnit field on Item using StartsWith. If gtin-sw is provided too, gtin-sw &amp; sku-sw will be linked via OR, not AND. | [optional]
 **sku_l** | **string**| Checks the StockKeepingUnit field on Item using the DB-Function LIKE. If gtin-l is provided too, gtin-l &amp; sku-l will be linked via OR, not AND. | [optional]
 **title_e** | [**string[]**](../Model/string.md)| Checks the Name field on Item using the DB-Function LIKE. | [optional]
 **title_sw** | [**string[]**](../Model/string.md)| Checks the Name field on Item using the DB-Function LIKE. | [optional]
 **title_l** | **string**| Checks the Name field on Item using the DB-Function LIKE. | [optional]
 **item_type** | [**string[]**](../Model/string.md)| Applies a filter, matching all ItemFrame instances where the parents ItemRoot.ItemType is in the provided EItemType collection. Always applied with AND, regardless of chain-using-or. | [optional]
 **include_parent** | **bool**| Includes ItemFrame.Parent in the result. | [optional] [default to false]
 **include_parent_tax_def** | **bool**| Includes ItemFrame.Parent.TaxDefinition | [optional] [default to false]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]
 **force_id** | **bool**| Enforces AND linking, regardless of chain-using-or, for &#39;*-id&#39; parameters. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32**](../Model/SimplifySoftPecuniariusDataNetPrimitivesNetInt32.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootAllGet()`

```php
itemRootAllGet($limit, $index, $include_frame, $include_stocks, $include_tags, $include_unit, $include_deleted, $psql): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemRoot
```

Receives a list of ItemRoot's according to provided filtering provided via GET parameters.

Receives a list of ItemRoot's according to provided filtering provided via GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$include_frame = true; // bool | Allows to enforce inclusion of frame.
$include_stocks = false; // bool | Allows to enforce inclusion of stocks.
$include_tags = false; // bool | Allows to enforce inclusion of tags.
$include_unit = false; // bool | Allows to enforce inclusion of units.
$include_deleted = false; // bool | Allows to include soft-deleted items.
$psql = 'psql_example'; // string | The PSQL query.

try {
    $result = $apiInstance->itemRootAllGet($limit, $index, $include_frame, $include_stocks, $include_tags, $include_unit, $include_deleted, $psql);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **include_frame** | **bool**| Allows to enforce inclusion of frame. | [optional] [default to true]
 **include_stocks** | **bool**| Allows to enforce inclusion of stocks. | [optional] [default to false]
 **include_tags** | **bool**| Allows to enforce inclusion of tags. | [optional] [default to false]
 **include_unit** | **bool**| Allows to enforce inclusion of units. | [optional] [default to false]
 **include_deleted** | **bool**| Allows to include soft-deleted items. | [optional] [default to false]
 **psql** | **string**| The PSQL query. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemRoot**](../Model/SimplifySoftPecuniariusDataNetItemDataItemRoot.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootAllIdsGet()`

```php
itemRootAllIdsGet($limit, $index, $include_stocks, $include_tags, $include_unit, $include_deleted, $psql): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32
```

Receives a list of ItemRoot's according to provided filtering provided via GET parameters.

Receives a list of ItemRoot's according to provided filtering provided via GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$include_stocks = false; // bool | Allows to enforce inclusion of stocks.
$include_tags = false; // bool | Allows to enforce inclusion of tags.
$include_unit = false; // bool | Allows to enforce inclusion of units.
$include_deleted = false; // bool | Allows to include soft-deleted items.
$psql = 'psql_example'; // string | The PSQL query.

try {
    $result = $apiInstance->itemRootAllIdsGet($limit, $index, $include_stocks, $include_tags, $include_unit, $include_deleted, $psql);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootAllIdsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **include_stocks** | **bool**| Allows to enforce inclusion of stocks. | [optional] [default to false]
 **include_tags** | **bool**| Allows to enforce inclusion of tags. | [optional] [default to false]
 **include_unit** | **bool**| Allows to enforce inclusion of units. | [optional] [default to false]
 **include_deleted** | **bool**| Allows to include soft-deleted items. | [optional] [default to false]
 **psql** | **string**| The PSQL query. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32**](../Model/SimplifySoftPecuniariusDataNetPrimitivesNetInt32.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootAllSearchGet()`

```php
itemRootAllSearchGet($limit, $index, $include_deleted, $psql): object
```

Receives a list of ItemRoot's according to provided filtering provided via GET parameters.

Receives a list of ItemRoot's according to provided filtering provided via GET parameters.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$include_deleted = false; // bool | Allows to include soft-deleted items.
$psql = 'psql_example'; // string | The PSQL query.

try {
    $result = $apiInstance->itemRootAllSearchGet($limit, $index, $include_deleted, $psql);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootAllSearchGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **include_deleted** | **bool**| Allows to include soft-deleted items. | [optional] [default to false]
 **psql** | **string**| The PSQL query. | [optional]

### Return type

**object**

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentCategoryAllGet()`

```php
itemRootIdentCategoryAllGet($ident, $forcesku): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataCategory
```

Receives the categories this item is located in via its ID or SKU.

Receives the categories this item is located in via its ID or SKU.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$forcesku = True; // bool | Allows to force to use SKU. Required if the SKU is all numeric.

try {
    $result = $apiInstance->itemRootIdentCategoryAllGet($ident, $forcesku);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentCategoryAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU. Required if the SKU is all numeric. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataCategory**](../Model/SimplifySoftPecuniariusDataNetItemDataCategory.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentCategoryCatidDelete()`

```php
itemRootIdentCategoryCatidDelete($ident, $catid, $forcesku)
```

Allows to remove a category relation from an item via its ID or SKU.

Allows to remove a category relation from an item via its ID or SKU.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$catid = 56; // int | unavailable
$forcesku = True; // bool | Allows to force to use SKU. Required if the SKU is all numeric.

try {
    $apiInstance->itemRootIdentCategoryCatidDelete($ident, $catid, $forcesku);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentCategoryCatidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **catid** | **int**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU. Required if the SKU is all numeric. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentCategoryCatidPost()`

```php
itemRootIdentCategoryCatidPost($ident, $catid, $forcesku)
```

Allows to add a category relation onto an item via its ID or SKU.

Allows to add a category relation onto an item via its ID or SKU.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$catid = 56; // int | unavailable
$forcesku = True; // bool | Allows to force to use SKU. Required if the SKU is all numeric.

try {
    $apiInstance->itemRootIdentCategoryCatidPost($ident, $catid, $forcesku);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentCategoryCatidPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **catid** | **int**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU. Required if the SKU is all numeric. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentDelete()`

```php
itemRootIdentDelete($ident, $forcesku)
```

Provides the ability to delete (mark as IsSoftDeleted) a given item via its ID or SKU.

Provides the ability to delete (mark as IsSoftDeleted) a given item via its ID or SKU.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$forcesku = True; // bool | Allows to force to use SKU. Required if the SKU is all numeric.

try {
    $apiInstance->itemRootIdentDelete($ident, $forcesku);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU. Required if the SKU is all numeric. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentFileAllGet()`

```php
itemRootIdentFileAllGet($ident, $limit, $index, $forcesku, $getdata, $getpreview): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetFile
```

Receives the files this item is linked with via its ID or SKU.

Receives the files this item is linked with via its ID or SKU.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$forcesku = True; // bool | Allows to force to use SKU. Required if the SKU is all numeric.
$getdata = false; // bool | If false, the DataBase64 property will be cleared before transmission.
$getpreview = true; // bool | If false, the PreviewBase64 property will be cleared before transmission.

try {
    $result = $apiInstance->itemRootIdentFileAllGet($ident, $limit, $index, $forcesku, $getdata, $getpreview);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentFileAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **forcesku** | **bool**| Allows to force to use SKU. Required if the SKU is all numeric. | [optional]
 **getdata** | **bool**| If false, the DataBase64 property will be cleared before transmission. | [optional] [default to false]
 **getpreview** | **bool**| If false, the PreviewBase64 property will be cleared before transmission. | [optional] [default to true]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetFile**](../Model/SimplifySoftPecuniariusDataNetFile.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentFileFileidDefaultPost()`

```php
itemRootIdentFileFileidDefaultPost($ident, $fileid, $forcesku)
```

Allows to set the default file relation of an item via its ID or SKU.

Allows to set the default file relation of an item via its ID or SKU.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$fileid = 56; // int | unavailable
$forcesku = True; // bool | Allows to force to use SKU. Required if the SKU is all numeric.

try {
    $apiInstance->itemRootIdentFileFileidDefaultPost($ident, $fileid, $forcesku);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentFileFileidDefaultPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **fileid** | **int**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU. Required if the SKU is all numeric. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentFileFileidDelete()`

```php
itemRootIdentFileFileidDelete($ident, $fileid, $forcesku)
```

Allows to remove a file relation from an item via its ID or SKU.

Allows to remove a file relation from an item via its ID or SKU.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$fileid = 56; // int | unavailable
$forcesku = True; // bool | Allows to force to use SKU. Required if the SKU is all numeric.

try {
    $apiInstance->itemRootIdentFileFileidDelete($ident, $fileid, $forcesku);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentFileFileidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **fileid** | **int**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU. Required if the SKU is all numeric. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentFileFileidPost()`

```php
itemRootIdentFileFileidPost($ident, $fileid, $forcesku)
```

Allows to add a file relation onto an item via its ID or SKU.

Allows to add a file relation onto an item via its ID or SKU.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$fileid = 56; // int | unavailable
$forcesku = True; // bool | Allows to force to use SKU. Required if the SKU is all numeric.

try {
    $apiInstance->itemRootIdentFileFileidPost($ident, $fileid, $forcesku);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentFileFileidPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **fileid** | **int**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU. Required if the SKU is all numeric. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentFrameFrameidGet()`

```php
itemRootIdentFrameFrameidGet($ident, $frameid, $forcesku)
```

Allows to receive a given frame by its id.

Allows to receive a given frame by its id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$frameid = 56; // int | unavailable
$forcesku = True; // bool | Allows to force to use SKU. Required if the SKU is all numeric.

try {
    $apiInstance->itemRootIdentFrameFrameidGet($ident, $frameid, $forcesku);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentFrameFrameidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **frameid** | **int**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU. Required if the SKU is all numeric. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentFramePut()`

```php
itemRootIdentFramePut($ident, $forcesku, $unknown_base_type)
```

Provides the ability to update a given items frame via its ID or SKU.

Provides the ability to update a given items frame via its ID or SKU.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$forcesku = True; // bool | Allows to force to use SKU. Required if the SKU is all numeric.
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->itemRootIdentFramePut($ident, $forcesku, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentFramePut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU. Required if the SKU is all numeric. | [optional]
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentGet()`

```php
itemRootIdentGet($ident, $forcesku, $include)
```

Receives a single item via its ID or SKU.

Receives a single item via its ID or SKU. Will also contain Tags, UnitType and Locations.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$forcesku = True; // bool | Allows to force to use SKU. Required if the SKU is all numeric.
$include = array('include_example'); // string[] | Allows to change what should be included on the request.

try {
    $apiInstance->itemRootIdentGet($ident, $forcesku, $include);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU. Required if the SKU is all numeric. | [optional]
 **include** | [**string[]**](../Model/string.md)| Allows to change what should be included on the request. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentLinkedAllGet()`

```php
itemRootIdentLinkedAllGet($ident, $forcesku): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemLink
```

Receives the list of linked items related to another item. Both can be identified via either their corresponding ID or SKU.

Receives the list of linked items related to another item. Both can be identified via either their corresponding ID or SKU.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$forcesku = True; // bool | Allows to force to use SKU for the primary item. Required if the SKU is all numeric.

try {
    $result = $apiInstance->itemRootIdentLinkedAllGet($ident, $forcesku);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentLinkedAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU for the primary item. Required if the SKU is all numeric. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemLink**](../Model/SimplifySoftPecuniariusDataNetItemDataItemLink.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentLinkedOtherDelete()`

```php
itemRootIdentLinkedOtherDelete($ident, $other, $forcesku, $forcesku_link)
```

Allows to remove an item relation from provided item. Both can be identified via either their corresponding ID or SKU.

Allows to remove an item relation from provided item. Both can be identified via either their corresponding ID or SKU.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$other = 'other_example'; // string | unavailable
$forcesku = True; // bool | Allows to force to use SKU for the primary item. Required if the SKU is all numeric.
$forcesku_link = True; // bool | Allows to force to use SKU for the secondary item. Required if the SKU is all numeric.

try {
    $apiInstance->itemRootIdentLinkedOtherDelete($ident, $other, $forcesku, $forcesku_link);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentLinkedOtherDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **other** | **string**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU for the primary item. Required if the SKU is all numeric. | [optional]
 **forcesku_link** | **bool**| Allows to force to use SKU for the secondary item. Required if the SKU is all numeric. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentLinkedOtherPost()`

```php
itemRootIdentLinkedOtherPost($ident, $other, $forcesku, $forcesku_link, $unknown_base_type)
```

Allows to add an item relation onto provided item. Both can be identified via either their corresponding ID or SKU.

Allows to add an item relation onto provided item. Both can be identified via either their corresponding ID or SKU.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$other = 'other_example'; // string | unavailable
$forcesku = True; // bool | Allows to force to use SKU for the primary item. Required if the SKU is all numeric.
$forcesku_link = True; // bool | Allows to force to use SKU for the secondary item. Required if the SKU is all numeric.
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->itemRootIdentLinkedOtherPost($ident, $other, $forcesku, $forcesku_link, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentLinkedOtherPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **other** | **string**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU for the primary item. Required if the SKU is all numeric. | [optional]
 **forcesku_link** | **bool**| Allows to force to use SKU for the secondary item. Required if the SKU is all numeric. | [optional]
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentLinkedOtherPut()`

```php
itemRootIdentLinkedOtherPut($ident, $other, $forcesku, $forcesku_link, $unknown_base_type)
```

Allows to update an item relation onto provided item. Both can be identified via either their corresponding ID or SKU.

Allows to update an item relation onto provided item. Both can be identified via either their corresponding ID or SKU.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$other = 'other_example'; // string | unavailable
$forcesku = True; // bool | Allows to force to use SKU for the primary item. Required if the SKU is all numeric.
$forcesku_link = True; // bool | Allows to force to use SKU for the secondary item. Required if the SKU is all numeric.
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->itemRootIdentLinkedOtherPut($ident, $other, $forcesku, $forcesku_link, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentLinkedOtherPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **other** | **string**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU for the primary item. Required if the SKU is all numeric. | [optional]
 **forcesku_link** | **bool**| Allows to force to use SKU for the secondary item. Required if the SKU is all numeric. | [optional]
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentPut()`

```php
itemRootIdentPut($ident, $forcesku, $unknown_base_type)
```

Provides the ability to update a given item via its ID or SKU.

Provides the ability to update a given item via its ID or SKU.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$forcesku = True; // bool | Allows to force to use SKU. Required if the SKU is all numeric.
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->itemRootIdentPut($ident, $forcesku, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU. Required if the SKU is all numeric. | [optional]
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentTagAllGet()`

```php
itemRootIdentTagAllGet($ident, $forcesku): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTag
```

Receives the tags this item is linked with via its ID or SKU.

Receives the tags this item is linked with via its ID or SKU.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$forcesku = True; // bool | Allows to force to use SKU. Required if the SKU is all numeric.

try {
    $result = $apiInstance->itemRootIdentTagAllGet($ident, $forcesku);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentTagAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU. Required if the SKU is all numeric. | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTag**](../Model/SimplifySoftPecuniariusDataNetTag.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentTagTagidDelete()`

```php
itemRootIdentTagTagidDelete($ident, $tagid, $forcesku)
```

Allows to remove a tag relation from an item via its ID or SKU.

Allows to remove a tag relation from an item via its ID or SKU.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$tagid = 56; // int | unavailable
$forcesku = True; // bool | Allows to force to use SKU. Required if the SKU is all numeric.

try {
    $apiInstance->itemRootIdentTagTagidDelete($ident, $tagid, $forcesku);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentTagTagidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **tagid** | **int**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU. Required if the SKU is all numeric. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootIdentTagTagidPost()`

```php
itemRootIdentTagTagidPost($ident, $tagid, $forcesku)
```

Allows to add a tag relation onto an item via its ID or SKU.

Allows to add a tag relation onto an item via its ID or SKU.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$ident = 'ident_example'; // string | unavailable
$tagid = 56; // int | unavailable
$forcesku = True; // bool | Allows to force to use SKU. Required if the SKU is all numeric.

try {
    $apiInstance->itemRootIdentTagTagidPost($ident, $tagid, $forcesku);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootIdentTagTagidPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ident** | **string**| unavailable |
 **tagid** | **int**| unavailable |
 **forcesku** | **bool**| Allows to force to use SKU. Required if the SKU is all numeric. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemRootPost()`

```php
itemRootPost($gensku, $simplify_soft_pecuniarius_data_net_item_data_item_root): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemRoot
```

Provides the ability to create a new item.

Provides the ability to create a new item.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$gensku = true; // bool | Allows to change wether or not SKUs should be generated by the system.
$simplify_soft_pecuniarius_data_net_item_data_item_root = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemRoot(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemRoot

try {
    $result = $apiInstance->itemRootPost($gensku, $simplify_soft_pecuniarius_data_net_item_data_item_root);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemRootPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gensku** | **bool**| Allows to change wether or not SKUs should be generated by the system. | [optional] [default to true]
 **simplify_soft_pecuniarius_data_net_item_data_item_root** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemRoot**](../Model/SimplifySoftPecuniariusDataNetItemDataItemRoot.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemRoot**](../Model/SimplifySoftPecuniariusDataNetItemDataItemRoot.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataRootSkuNextGet()`

```php
itemdataRootSkuNextGet($sku)
```

Receives the id, that follows logically after this one (determined via SKU).

Receives the id, that follows logically after this one (determined via SKU).

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sku = 'sku_example'; // string | unavailable

try {
    $apiInstance->itemdataRootSkuNextGet($sku);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemdataRootSkuNextGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sku** | **string**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataRootSkuPreviousGet()`

```php
itemdataRootSkuPreviousGet($sku)
```

Receives the id, that came logically beofre this one (determined via SKU).

Receives the id, that came logically beofre this one (determined via SKU).

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sku = 'sku_example'; // string | unavailable

try {
    $apiInstance->itemdataRootSkuPreviousGet($sku);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataItemsApi->itemdataRootSkuPreviousGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sku** | **string**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
