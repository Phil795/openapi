# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**externLinksCategoryGet()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksCategoryGet) | **GET** /extern/links/category | Receives a list of available extern links.
[**externLinksContactGet()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksContactGet) | **GET** /extern/links/contact | Receives a list of available extern links.
[**externLinksDeliverymethodGet()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksDeliverymethodGet) | **GET** /extern/links/deliverymethod | Receives a list of available extern links.
[**externLinksExternauthorityCategoryCategoryDelete()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityCategoryCategoryDelete) | **DELETE** /extern/links/{externauthority}/category/{category} | Removes a link between the provided extern authority and the item root.
[**externLinksExternauthorityCategoryCategoryPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityCategoryCategoryPost) | **POST** /extern/links/{externauthority}/category/{category} | Creates a link between the provided extern authority and the item root.
[**externLinksExternauthorityContactContactDelete()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityContactContactDelete) | **DELETE** /extern/links/{externauthority}/contact/{contact} | Removes a link between the provided extern authority and the item root.
[**externLinksExternauthorityContactContactPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityContactContactPost) | **POST** /extern/links/{externauthority}/contact/{contact} | Creates a link between the provided extern authority and the item root.
[**externLinksExternauthorityDeliverymethodDeliverymethodDelete()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityDeliverymethodDeliverymethodDelete) | **DELETE** /extern/links/{externauthority}/deliverymethod/{deliverymethod} | Removes a link between the provided extern authority and the item root.
[**externLinksExternauthorityDeliverymethodDeliverymethodPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityDeliverymethodDeliverymethodPost) | **POST** /extern/links/{externauthority}/deliverymethod/{deliverymethod} | Creates a link between the provided extern authority and the item root.
[**externLinksExternauthorityFileFileDelete()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityFileFileDelete) | **DELETE** /extern/links/{externauthority}/file/{file} | Removes a link between the provided extern authority and the item root.
[**externLinksExternauthorityFileFilePost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityFileFilePost) | **POST** /extern/links/{externauthority}/file/{file} | Creates a link between the provided extern authority and the item root.
[**externLinksExternauthorityItemrootItemrootDelete()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityItemrootItemrootDelete) | **DELETE** /extern/links/{externauthority}/itemroot/{itemroot} | Removes a link between the provided extern authority and the item root.
[**externLinksExternauthorityItemrootItemrootPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityItemrootItemrootPost) | **POST** /extern/links/{externauthority}/itemroot/{itemroot} | Creates a link between the provided extern authority and the item root.
[**externLinksExternauthorityPaymentoptionPaymentoptionDelete()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityPaymentoptionPaymentoptionDelete) | **DELETE** /extern/links/{externauthority}/paymentoption/{paymentoption} | Removes a link between the provided extern authority and the item root.
[**externLinksExternauthorityPaymentoptionPaymentoptionPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityPaymentoptionPaymentoptionPost) | **POST** /extern/links/{externauthority}/paymentoption/{paymentoption} | Creates a link between the provided extern authority and the item root.
[**externLinksExternauthorityPricegroupPricegroupDelete()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityPricegroupPricegroupDelete) | **DELETE** /extern/links/{externauthority}/pricegroup/{pricegroup} | Removes a link between the provided extern authority and the item root.
[**externLinksExternauthorityPricegroupPricegroupPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityPricegroupPricegroupPost) | **POST** /extern/links/{externauthority}/pricegroup/{pricegroup} | Creates a link between the provided extern authority and the item root.
[**externLinksExternauthorityReceiptReceiptDelete()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityReceiptReceiptDelete) | **DELETE** /extern/links/{externauthority}/receipt/{receipt} | Removes a link between the provided extern authority and the item root.
[**externLinksExternauthorityReceiptReceiptPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityReceiptReceiptPost) | **POST** /extern/links/{externauthority}/receipt/{receipt} | Creates a link between the provided extern authority and the item root.
[**externLinksExternauthorityTagTagDelete()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityTagTagDelete) | **DELETE** /extern/links/{externauthority}/tag/{tag} | Removes a link between the provided extern authority and the item root.
[**externLinksExternauthorityTagTagPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityTagTagPost) | **POST** /extern/links/{externauthority}/tag/{tag} | Creates a link between the provided extern authority and the item root.
[**externLinksExternauthorityTaggroupTaggroupDelete()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityTaggroupTaggroupDelete) | **DELETE** /extern/links/{externauthority}/taggroup/{taggroup} | Removes a link between the provided extern authority and the item root.
[**externLinksExternauthorityTaggroupTaggroupPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksExternauthorityTaggroupTaggroupPost) | **POST** /extern/links/{externauthority}/taggroup/{taggroup} | Creates a link between the provided extern authority and the item root.
[**externLinksFileGet()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksFileGet) | **GET** /extern/links/file | Receives a list of available extern links.
[**externLinksIdCategoryPropertyKeyPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksIdCategoryPropertyKeyPost) | **POST** /extern/links/{id}/category/property/{key} | unavailable
[**externLinksIdContactPropertyKeyPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksIdContactPropertyKeyPost) | **POST** /extern/links/{id}/contact/property/{key} | unavailable
[**externLinksIdDeliverymethodPropertyKeyPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksIdDeliverymethodPropertyKeyPost) | **POST** /extern/links/{id}/deliverymethod/property/{key} | unavailable
[**externLinksIdFilePropertyKeyPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksIdFilePropertyKeyPost) | **POST** /extern/links/{id}/file/property/{key} | unavailable
[**externLinksIdItemrootPropertyKeyPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksIdItemrootPropertyKeyPost) | **POST** /extern/links/{id}/itemroot/property/{key} | unavailable
[**externLinksIdPathPropertyKeyPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksIdPathPropertyKeyPost) | **POST** /extern/links/{id}/{path}/property/{key} | Diagnostics Only. Not intended for actual use (will always return &#39;403 Forbidden&#39;).
[**externLinksIdPaymentoptionPropertyKeyPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksIdPaymentoptionPropertyKeyPost) | **POST** /extern/links/{id}/paymentoption/property/{key} | unavailable
[**externLinksIdPricegroupPropertyKeyPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksIdPricegroupPropertyKeyPost) | **POST** /extern/links/{id}/pricegroup/property/{key} | unavailable
[**externLinksIdReceiptPropertyKeyPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksIdReceiptPropertyKeyPost) | **POST** /extern/links/{id}/receipt/property/{key} | unavailable
[**externLinksIdTagPropertyKeyPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksIdTagPropertyKeyPost) | **POST** /extern/links/{id}/tag/property/{key} | unavailable
[**externLinksIdTaggroupPropertyKeyPost()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksIdTaggroupPropertyKeyPost) | **POST** /extern/links/{id}/taggroup/property/{key} | unavailable
[**externLinksItemrootGet()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksItemrootGet) | **GET** /extern/links/itemroot | Receives a list of available extern links.
[**externLinksPaymentoptionGet()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksPaymentoptionGet) | **GET** /extern/links/paymentoption | Receives a list of available extern links.
[**externLinksPricegroupGet()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksPricegroupGet) | **GET** /extern/links/pricegroup | Receives a list of available extern links.
[**externLinksReceiptGet()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksReceiptGet) | **GET** /extern/links/receipt | Receives a list of available extern links.
[**externLinksTagGet()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksTagGet) | **GET** /extern/links/tag | Receives a list of available extern links.
[**externLinksTaggroupGet()**](SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi.md#externLinksTaggroupGet) | **GET** /extern/links/taggroup | Receives a list of available extern links.


## `externLinksCategoryGet()`

```php
externLinksCategoryGet($limit, $index, $extauth, $category, $includeinactive): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternCategoryLink
```

Receives a list of available extern links.

Receives a list of available extern links.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$extauth = array(56); // int[] | Receives only the links for the provided extern authorities.
$category = array(56); // int[] | Receives only the links for the provided items.
$includeinactive = false; // bool | Specifies wether inactive links should be included too.

try {
    $result = $apiInstance->externLinksCategoryGet($limit, $index, $extauth, $category, $includeinactive);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksCategoryGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **extauth** | [**int[]**](../Model/int.md)| Receives only the links for the provided extern authorities. | [optional]
 **category** | [**int[]**](../Model/int.md)| Receives only the links for the provided items. | [optional]
 **includeinactive** | **bool**| Specifies wether inactive links should be included too. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternCategoryLink**](../Model/SimplifySoftPecuniariusDataNetExternExternCategoryLink.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksContactGet()`

```php
externLinksContactGet($limit, $index, $extauth, $contact, $includeinactive): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternContactLink
```

Receives a list of available extern links.

Receives a list of available extern links.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$extauth = array(56); // int[] | Receives only the links for the provided extern authorities.
$contact = array(56); // int[] | Receives only the links for the provided items.
$includeinactive = false; // bool | Specifies wether inactive links should be included too.

try {
    $result = $apiInstance->externLinksContactGet($limit, $index, $extauth, $contact, $includeinactive);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksContactGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **extauth** | [**int[]**](../Model/int.md)| Receives only the links for the provided extern authorities. | [optional]
 **contact** | [**int[]**](../Model/int.md)| Receives only the links for the provided items. | [optional]
 **includeinactive** | **bool**| Specifies wether inactive links should be included too. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternContactLink**](../Model/SimplifySoftPecuniariusDataNetExternExternContactLink.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksDeliverymethodGet()`

```php
externLinksDeliverymethodGet($limit, $index, $extauth, $deliverymethod, $includeinactive): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternDeliveryOptionLink
```

Receives a list of available extern links.

Receives a list of available extern links.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$extauth = array(56); // int[] | Receives only the links for the provided extern authorities.
$deliverymethod = array(56); // int[] | Receives only the links for the provided items.
$includeinactive = false; // bool | Specifies wether inactive links should be included too.

try {
    $result = $apiInstance->externLinksDeliverymethodGet($limit, $index, $extauth, $deliverymethod, $includeinactive);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksDeliverymethodGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **extauth** | [**int[]**](../Model/int.md)| Receives only the links for the provided extern authorities. | [optional]
 **deliverymethod** | [**int[]**](../Model/int.md)| Receives only the links for the provided items. | [optional]
 **includeinactive** | **bool**| Specifies wether inactive links should be included too. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternDeliveryOptionLink**](../Model/SimplifySoftPecuniariusDataNetExternExternDeliveryOptionLink.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityCategoryCategoryDelete()`

```php
externLinksExternauthorityCategoryCategoryDelete($externauthority, $category)
```

Removes a link between the provided extern authority and the item root.

Removes a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$category = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityCategoryCategoryDelete($externauthority, $category);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityCategoryCategoryDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **category** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityCategoryCategoryPost()`

```php
externLinksExternauthorityCategoryCategoryPost($externauthority, $category)
```

Creates a link between the provided extern authority and the item root.

Creates a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$category = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityCategoryCategoryPost($externauthority, $category);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityCategoryCategoryPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **category** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityContactContactDelete()`

```php
externLinksExternauthorityContactContactDelete($externauthority, $contact)
```

Removes a link between the provided extern authority and the item root.

Removes a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$contact = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityContactContactDelete($externauthority, $contact);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityContactContactDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **contact** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityContactContactPost()`

```php
externLinksExternauthorityContactContactPost($externauthority, $contact)
```

Creates a link between the provided extern authority and the item root.

Creates a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$contact = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityContactContactPost($externauthority, $contact);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityContactContactPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **contact** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityDeliverymethodDeliverymethodDelete()`

```php
externLinksExternauthorityDeliverymethodDeliverymethodDelete($externauthority, $deliverymethod)
```

Removes a link between the provided extern authority and the item root.

Removes a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$deliverymethod = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityDeliverymethodDeliverymethodDelete($externauthority, $deliverymethod);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityDeliverymethodDeliverymethodDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **deliverymethod** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityDeliverymethodDeliverymethodPost()`

```php
externLinksExternauthorityDeliverymethodDeliverymethodPost($externauthority, $deliverymethod)
```

Creates a link between the provided extern authority and the item root.

Creates a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$deliverymethod = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityDeliverymethodDeliverymethodPost($externauthority, $deliverymethod);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityDeliverymethodDeliverymethodPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **deliverymethod** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityFileFileDelete()`

```php
externLinksExternauthorityFileFileDelete($externauthority, $file)
```

Removes a link between the provided extern authority and the item root.

Removes a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$file = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityFileFileDelete($externauthority, $file);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityFileFileDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **file** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityFileFilePost()`

```php
externLinksExternauthorityFileFilePost($externauthority, $file)
```

Creates a link between the provided extern authority and the item root.

Creates a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$file = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityFileFilePost($externauthority, $file);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityFileFilePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **file** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityItemrootItemrootDelete()`

```php
externLinksExternauthorityItemrootItemrootDelete($externauthority, $itemroot)
```

Removes a link between the provided extern authority and the item root.

Removes a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$itemroot = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityItemrootItemrootDelete($externauthority, $itemroot);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityItemrootItemrootDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **itemroot** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityItemrootItemrootPost()`

```php
externLinksExternauthorityItemrootItemrootPost($externauthority, $itemroot)
```

Creates a link between the provided extern authority and the item root.

Creates a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$itemroot = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityItemrootItemrootPost($externauthority, $itemroot);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityItemrootItemrootPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **itemroot** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityPaymentoptionPaymentoptionDelete()`

```php
externLinksExternauthorityPaymentoptionPaymentoptionDelete($externauthority, $paymentoption)
```

Removes a link between the provided extern authority and the item root.

Removes a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$paymentoption = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityPaymentoptionPaymentoptionDelete($externauthority, $paymentoption);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityPaymentoptionPaymentoptionDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **paymentoption** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityPaymentoptionPaymentoptionPost()`

```php
externLinksExternauthorityPaymentoptionPaymentoptionPost($externauthority, $paymentoption)
```

Creates a link between the provided extern authority and the item root.

Creates a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$paymentoption = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityPaymentoptionPaymentoptionPost($externauthority, $paymentoption);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityPaymentoptionPaymentoptionPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **paymentoption** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityPricegroupPricegroupDelete()`

```php
externLinksExternauthorityPricegroupPricegroupDelete($externauthority, $pricegroup)
```

Removes a link between the provided extern authority and the item root.

Removes a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$pricegroup = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityPricegroupPricegroupDelete($externauthority, $pricegroup);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityPricegroupPricegroupDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **pricegroup** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityPricegroupPricegroupPost()`

```php
externLinksExternauthorityPricegroupPricegroupPost($externauthority, $pricegroup)
```

Creates a link between the provided extern authority and the item root.

Creates a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$pricegroup = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityPricegroupPricegroupPost($externauthority, $pricegroup);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityPricegroupPricegroupPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **pricegroup** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityReceiptReceiptDelete()`

```php
externLinksExternauthorityReceiptReceiptDelete($externauthority, $receipt)
```

Removes a link between the provided extern authority and the item root.

Removes a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$receipt = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityReceiptReceiptDelete($externauthority, $receipt);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityReceiptReceiptDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **receipt** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityReceiptReceiptPost()`

```php
externLinksExternauthorityReceiptReceiptPost($externauthority, $receipt)
```

Creates a link between the provided extern authority and the item root.

Creates a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$receipt = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityReceiptReceiptPost($externauthority, $receipt);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityReceiptReceiptPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **receipt** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityTagTagDelete()`

```php
externLinksExternauthorityTagTagDelete($externauthority, $tag)
```

Removes a link between the provided extern authority and the item root.

Removes a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$tag = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityTagTagDelete($externauthority, $tag);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityTagTagDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **tag** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityTagTagPost()`

```php
externLinksExternauthorityTagTagPost($externauthority, $tag)
```

Creates a link between the provided extern authority and the item root.

Creates a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$tag = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityTagTagPost($externauthority, $tag);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityTagTagPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **tag** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityTaggroupTaggroupDelete()`

```php
externLinksExternauthorityTaggroupTaggroupDelete($externauthority, $taggroup)
```

Removes a link between the provided extern authority and the item root.

Removes a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$taggroup = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityTaggroupTaggroupDelete($externauthority, $taggroup);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityTaggroupTaggroupDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **taggroup** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksExternauthorityTaggroupTaggroupPost()`

```php
externLinksExternauthorityTaggroupTaggroupPost($externauthority, $taggroup)
```

Creates a link between the provided extern authority and the item root.

Creates a link between the provided extern authority and the item root.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$externauthority = 56; // int | unavailable
$taggroup = 56; // int | unavailable

try {
    $apiInstance->externLinksExternauthorityTaggroupTaggroupPost($externauthority, $taggroup);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksExternauthorityTaggroupTaggroupPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **externauthority** | **int**| unavailable |
 **taggroup** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksFileGet()`

```php
externLinksFileGet($limit, $index, $extauth, $file, $includeinactive): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternFileLink
```

Receives a list of available extern links.

Receives a list of available extern links.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$extauth = array(56); // int[] | Receives only the links for the provided extern authorities.
$file = array(56); // int[] | Receives only the links for the provided items.
$includeinactive = false; // bool | Specifies wether inactive links should be included too.

try {
    $result = $apiInstance->externLinksFileGet($limit, $index, $extauth, $file, $includeinactive);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksFileGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **extauth** | [**int[]**](../Model/int.md)| Receives only the links for the provided extern authorities. | [optional]
 **file** | [**int[]**](../Model/int.md)| Receives only the links for the provided items. | [optional]
 **includeinactive** | **bool**| Specifies wether inactive links should be included too. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternFileLink**](../Model/SimplifySoftPecuniariusDataNetExternExternFileLink.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksIdCategoryPropertyKeyPost()`

```php
externLinksIdCategoryPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$key = 'key_example'; // string | unavailable
$string = 'string_example'; // string | The string value to set.
$int32 = 56; // int | The int value to set.
$int64 = 56; // int | The long value to set.
$double = 3.4; // double | The double value to set.
$boolean = True; // bool | The boolean value to set.

try {
    $apiInstance->externLinksIdCategoryPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksIdCategoryPropertyKeyPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **key** | **string**| unavailable |
 **string** | **string**| The string value to set. | [optional]
 **int32** | **int**| The int value to set. | [optional]
 **int64** | **int**| The long value to set. | [optional]
 **double** | **double**| The double value to set. | [optional]
 **boolean** | **bool**| The boolean value to set. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksIdContactPropertyKeyPost()`

```php
externLinksIdContactPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$key = 'key_example'; // string | unavailable
$string = 'string_example'; // string | The string value to set.
$int32 = 56; // int | The int value to set.
$int64 = 56; // int | The long value to set.
$double = 3.4; // double | The double value to set.
$boolean = True; // bool | The boolean value to set.

try {
    $apiInstance->externLinksIdContactPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksIdContactPropertyKeyPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **key** | **string**| unavailable |
 **string** | **string**| The string value to set. | [optional]
 **int32** | **int**| The int value to set. | [optional]
 **int64** | **int**| The long value to set. | [optional]
 **double** | **double**| The double value to set. | [optional]
 **boolean** | **bool**| The boolean value to set. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksIdDeliverymethodPropertyKeyPost()`

```php
externLinksIdDeliverymethodPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$key = 'key_example'; // string | unavailable
$string = 'string_example'; // string | The string value to set.
$int32 = 56; // int | The int value to set.
$int64 = 56; // int | The long value to set.
$double = 3.4; // double | The double value to set.
$boolean = True; // bool | The boolean value to set.

try {
    $apiInstance->externLinksIdDeliverymethodPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksIdDeliverymethodPropertyKeyPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **key** | **string**| unavailable |
 **string** | **string**| The string value to set. | [optional]
 **int32** | **int**| The int value to set. | [optional]
 **int64** | **int**| The long value to set. | [optional]
 **double** | **double**| The double value to set. | [optional]
 **boolean** | **bool**| The boolean value to set. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksIdFilePropertyKeyPost()`

```php
externLinksIdFilePropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$key = 'key_example'; // string | unavailable
$string = 'string_example'; // string | The string value to set.
$int32 = 56; // int | The int value to set.
$int64 = 56; // int | The long value to set.
$double = 3.4; // double | The double value to set.
$boolean = True; // bool | The boolean value to set.

try {
    $apiInstance->externLinksIdFilePropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksIdFilePropertyKeyPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **key** | **string**| unavailable |
 **string** | **string**| The string value to set. | [optional]
 **int32** | **int**| The int value to set. | [optional]
 **int64** | **int**| The long value to set. | [optional]
 **double** | **double**| The double value to set. | [optional]
 **boolean** | **bool**| The boolean value to set. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksIdItemrootPropertyKeyPost()`

```php
externLinksIdItemrootPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$key = 'key_example'; // string | unavailable
$string = 'string_example'; // string | The string value to set.
$int32 = 56; // int | The int value to set.
$int64 = 56; // int | The long value to set.
$double = 3.4; // double | The double value to set.
$boolean = True; // bool | The boolean value to set.

try {
    $apiInstance->externLinksIdItemrootPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksIdItemrootPropertyKeyPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **key** | **string**| unavailable |
 **string** | **string**| The string value to set. | [optional]
 **int32** | **int**| The int value to set. | [optional]
 **int64** | **int**| The long value to set. | [optional]
 **double** | **double**| The double value to set. | [optional]
 **boolean** | **bool**| The boolean value to set. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksIdPathPropertyKeyPost()`

```php
externLinksIdPathPropertyKeyPost($id, $path, $key, $string, $int32, $int64, $double, $boolean)
```

Diagnostics Only. Not intended for actual use (will always return '403 Forbidden').

Diagnostics Only. Not intended for actual use (will always return '403 Forbidden').

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$path = 'path_example'; // string | unavailable
$key = 'key_example'; // string | unavailable
$string = 'string_example'; // string | The string value to set.
$int32 = 56; // int | The int value to set.
$int64 = 56; // int | The long value to set.
$double = 3.4; // double | The double value to set.
$boolean = True; // bool | The boolean value to set.

try {
    $apiInstance->externLinksIdPathPropertyKeyPost($id, $path, $key, $string, $int32, $int64, $double, $boolean);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksIdPathPropertyKeyPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **path** | **string**| unavailable |
 **key** | **string**| unavailable |
 **string** | **string**| The string value to set. | [optional]
 **int32** | **int**| The int value to set. | [optional]
 **int64** | **int**| The long value to set. | [optional]
 **double** | **double**| The double value to set. | [optional]
 **boolean** | **bool**| The boolean value to set. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksIdPaymentoptionPropertyKeyPost()`

```php
externLinksIdPaymentoptionPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$key = 'key_example'; // string | unavailable
$string = 'string_example'; // string | The string value to set.
$int32 = 56; // int | The int value to set.
$int64 = 56; // int | The long value to set.
$double = 3.4; // double | The double value to set.
$boolean = True; // bool | The boolean value to set.

try {
    $apiInstance->externLinksIdPaymentoptionPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksIdPaymentoptionPropertyKeyPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **key** | **string**| unavailable |
 **string** | **string**| The string value to set. | [optional]
 **int32** | **int**| The int value to set. | [optional]
 **int64** | **int**| The long value to set. | [optional]
 **double** | **double**| The double value to set. | [optional]
 **boolean** | **bool**| The boolean value to set. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksIdPricegroupPropertyKeyPost()`

```php
externLinksIdPricegroupPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$key = 'key_example'; // string | unavailable
$string = 'string_example'; // string | The string value to set.
$int32 = 56; // int | The int value to set.
$int64 = 56; // int | The long value to set.
$double = 3.4; // double | The double value to set.
$boolean = True; // bool | The boolean value to set.

try {
    $apiInstance->externLinksIdPricegroupPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksIdPricegroupPropertyKeyPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **key** | **string**| unavailable |
 **string** | **string**| The string value to set. | [optional]
 **int32** | **int**| The int value to set. | [optional]
 **int64** | **int**| The long value to set. | [optional]
 **double** | **double**| The double value to set. | [optional]
 **boolean** | **bool**| The boolean value to set. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksIdReceiptPropertyKeyPost()`

```php
externLinksIdReceiptPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$key = 'key_example'; // string | unavailable
$string = 'string_example'; // string | The string value to set.
$int32 = 56; // int | The int value to set.
$int64 = 56; // int | The long value to set.
$double = 3.4; // double | The double value to set.
$boolean = True; // bool | The boolean value to set.

try {
    $apiInstance->externLinksIdReceiptPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksIdReceiptPropertyKeyPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **key** | **string**| unavailable |
 **string** | **string**| The string value to set. | [optional]
 **int32** | **int**| The int value to set. | [optional]
 **int64** | **int**| The long value to set. | [optional]
 **double** | **double**| The double value to set. | [optional]
 **boolean** | **bool**| The boolean value to set. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksIdTagPropertyKeyPost()`

```php
externLinksIdTagPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$key = 'key_example'; // string | unavailable
$string = 'string_example'; // string | The string value to set.
$int32 = 56; // int | The int value to set.
$int64 = 56; // int | The long value to set.
$double = 3.4; // double | The double value to set.
$boolean = True; // bool | The boolean value to set.

try {
    $apiInstance->externLinksIdTagPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksIdTagPropertyKeyPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **key** | **string**| unavailable |
 **string** | **string**| The string value to set. | [optional]
 **int32** | **int**| The int value to set. | [optional]
 **int64** | **int**| The long value to set. | [optional]
 **double** | **double**| The double value to set. | [optional]
 **boolean** | **bool**| The boolean value to set. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksIdTaggroupPropertyKeyPost()`

```php
externLinksIdTaggroupPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$key = 'key_example'; // string | unavailable
$string = 'string_example'; // string | The string value to set.
$int32 = 56; // int | The int value to set.
$int64 = 56; // int | The long value to set.
$double = 3.4; // double | The double value to set.
$boolean = True; // bool | The boolean value to set.

try {
    $apiInstance->externLinksIdTaggroupPropertyKeyPost($id, $key, $string, $int32, $int64, $double, $boolean);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksIdTaggroupPropertyKeyPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **key** | **string**| unavailable |
 **string** | **string**| The string value to set. | [optional]
 **int32** | **int**| The int value to set. | [optional]
 **int64** | **int**| The long value to set. | [optional]
 **double** | **double**| The double value to set. | [optional]
 **boolean** | **bool**| The boolean value to set. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksItemrootGet()`

```php
externLinksItemrootGet($limit, $index, $extauth, $itemroot, $includeinactive): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternItemLink
```

Receives a list of available extern links.

Receives a list of available extern links.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$extauth = array(56); // int[] | Receives only the links for the provided extern authorities.
$itemroot = array(56); // int[] | Receives only the links for the provided items.
$includeinactive = false; // bool | Specifies wether inactive links should be included too.

try {
    $result = $apiInstance->externLinksItemrootGet($limit, $index, $extauth, $itemroot, $includeinactive);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksItemrootGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **extauth** | [**int[]**](../Model/int.md)| Receives only the links for the provided extern authorities. | [optional]
 **itemroot** | [**int[]**](../Model/int.md)| Receives only the links for the provided items. | [optional]
 **includeinactive** | **bool**| Specifies wether inactive links should be included too. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternItemLink**](../Model/SimplifySoftPecuniariusDataNetExternExternItemLink.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksPaymentoptionGet()`

```php
externLinksPaymentoptionGet($limit, $index, $extauth, $paymentoption, $includeinactive): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternPaymentOptionLink
```

Receives a list of available extern links.

Receives a list of available extern links.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$extauth = array(56); // int[] | Receives only the links for the provided extern authorities.
$paymentoption = array(56); // int[] | Receives only the links for the provided items.
$includeinactive = false; // bool | Specifies wether inactive links should be included too.

try {
    $result = $apiInstance->externLinksPaymentoptionGet($limit, $index, $extauth, $paymentoption, $includeinactive);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksPaymentoptionGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **extauth** | [**int[]**](../Model/int.md)| Receives only the links for the provided extern authorities. | [optional]
 **paymentoption** | [**int[]**](../Model/int.md)| Receives only the links for the provided items. | [optional]
 **includeinactive** | **bool**| Specifies wether inactive links should be included too. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternPaymentOptionLink**](../Model/SimplifySoftPecuniariusDataNetExternExternPaymentOptionLink.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksPricegroupGet()`

```php
externLinksPricegroupGet($limit, $index, $extauth, $pricegroup, $includeinactive): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternPriceGroupLink
```

Receives a list of available extern links.

Receives a list of available extern links.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$extauth = array(56); // int[] | Receives only the links for the provided extern authorities.
$pricegroup = array(56); // int[] | Receives only the links for the provided items.
$includeinactive = false; // bool | Specifies wether inactive links should be included too.

try {
    $result = $apiInstance->externLinksPricegroupGet($limit, $index, $extauth, $pricegroup, $includeinactive);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksPricegroupGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **extauth** | [**int[]**](../Model/int.md)| Receives only the links for the provided extern authorities. | [optional]
 **pricegroup** | [**int[]**](../Model/int.md)| Receives only the links for the provided items. | [optional]
 **includeinactive** | **bool**| Specifies wether inactive links should be included too. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternPriceGroupLink**](../Model/SimplifySoftPecuniariusDataNetExternExternPriceGroupLink.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksReceiptGet()`

```php
externLinksReceiptGet($limit, $index, $extauth, $receipt, $includeinactive): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternReceiptLink
```

Receives a list of available extern links.

Receives a list of available extern links.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$extauth = array(56); // int[] | Receives only the links for the provided extern authorities.
$receipt = array(56); // int[] | Receives only the links for the provided items.
$includeinactive = false; // bool | Specifies wether inactive links should be included too.

try {
    $result = $apiInstance->externLinksReceiptGet($limit, $index, $extauth, $receipt, $includeinactive);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksReceiptGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **extauth** | [**int[]**](../Model/int.md)| Receives only the links for the provided extern authorities. | [optional]
 **receipt** | [**int[]**](../Model/int.md)| Receives only the links for the provided items. | [optional]
 **includeinactive** | **bool**| Specifies wether inactive links should be included too. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternReceiptLink**](../Model/SimplifySoftPecuniariusDataNetExternExternReceiptLink.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksTagGet()`

```php
externLinksTagGet($limit, $index, $extauth, $tag, $includeinactive): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternTagLink
```

Receives a list of available extern links.

Receives a list of available extern links.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$extauth = array(56); // int[] | Receives only the links for the provided extern authorities.
$tag = array(56); // int[] | Receives only the links for the provided items.
$includeinactive = false; // bool | Specifies wether inactive links should be included too.

try {
    $result = $apiInstance->externLinksTagGet($limit, $index, $extauth, $tag, $includeinactive);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksTagGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **extauth** | [**int[]**](../Model/int.md)| Receives only the links for the provided extern authorities. | [optional]
 **tag** | [**int[]**](../Model/int.md)| Receives only the links for the provided items. | [optional]
 **includeinactive** | **bool**| Specifies wether inactive links should be included too. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternTagLink**](../Model/SimplifySoftPecuniariusDataNetExternExternTagLink.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `externLinksTaggroupGet()`

```php
externLinksTaggroupGet($limit, $index, $extauth, $taggroup, $includeinactive): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternTagGroupLink
```

Receives a list of available extern links.

Receives a list of available extern links.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.
$extauth = array(56); // int[] | Receives only the links for the provided extern authorities.
$taggroup = array(56); // int[] | Receives only the links for the provided items.
$includeinactive = false; // bool | Specifies wether inactive links should be included too.

try {
    $result = $apiInstance->externLinksTaggroupGet($limit, $index, $extauth, $taggroup, $includeinactive);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ExternExternLinksApi->externLinksTaggroupGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **extauth** | [**int[]**](../Model/int.md)| Receives only the links for the provided extern authorities. | [optional]
 **taggroup** | [**int[]**](../Model/int.md)| Receives only the links for the provided items. | [optional]
 **includeinactive** | **bool**| Specifies wether inactive links should be included too. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternTagGroupLink**](../Model/SimplifySoftPecuniariusDataNetExternExternTagGroupLink.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
