# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**inventoryPicklistAllCountGet()**](SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi.md#inventoryPicklistAllCountGet) | **GET** /inventory/picklist/all/count | Provides a way to query the count of all PickList&#39;s available.
[**inventoryPicklistAllEntryAllCountGet()**](SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi.md#inventoryPicklistAllEntryAllCountGet) | **GET** /inventory/picklist/all/entry/all/count | Provides a way to query the count of all PickListEntry&#39;s available.
[**inventoryPicklistAllEntryAllGet()**](SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi.md#inventoryPicklistAllEntryAllGet) | **GET** /inventory/picklist/all/entry/all | Provides a way to query all PickListEntry&#39;s available.
[**inventoryPicklistAllGet()**](SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi.md#inventoryPicklistAllGet) | **GET** /inventory/picklist/all | Provides a way to query all PickList&#39;s available.
[**inventoryPicklistPicklistidDelete()**](SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi.md#inventoryPicklistPicklistidDelete) | **DELETE** /inventory/picklist/{picklistid} | unavailable
[**inventoryPicklistPicklistidEntryAllGet()**](SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi.md#inventoryPicklistPicklistidEntryAllGet) | **GET** /inventory/picklist/{picklistid}/entry/all | Can be used to poll the child entries of a given PickList. Empty result will be returned if no PickList.Id &#x3D;&#x3D; pickilistid was fo
[**inventoryPicklistPicklistidEntryEntryidDelete()**](SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi.md#inventoryPicklistPicklistidEntryEntryidDelete) | **DELETE** /inventory/picklist/{picklistid}/entry/{entryid} | unavailable
[**inventoryPicklistPicklistidEntryEntryidGet()**](SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi.md#inventoryPicklistPicklistidEntryEntryidGet) | **GET** /inventory/picklist/{picklistid}/entry/{entryid} | Allows receiving a single PickListEntry according to the entryid. NetException will be returned if no PickListEntry with PickLis
[**inventoryPicklistPicklistidEntryEntryidPut()**](SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi.md#inventoryPicklistPicklistidEntryEntryidPut) | **PUT** /inventory/picklist/{picklistid}/entry/{entryid} | unavailable
[**inventoryPicklistPicklistidEntryIdsGet()**](SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi.md#inventoryPicklistPicklistidEntryIdsGet) | **GET** /inventory/picklist/{picklistid}/entry/ids | Can be used to poll the IDs of the childs entries of a given PickList. NetException will be returned if no PickList.Id &#x3D;&#x3D; pickil
[**inventoryPicklistPicklistidEntryPost()**](SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi.md#inventoryPicklistPicklistidEntryPost) | **POST** /inventory/picklist/{picklistid}/entry | unavailable
[**inventoryPicklistPicklistidGet()**](SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi.md#inventoryPicklistPicklistidGet) | **GET** /inventory/picklist/{picklistid} | Allows receiving a single PickList according to the picklistid. NetException will be returned if no PickList.Id &#x3D;&#x3D; pickilistid w
[**inventoryPicklistPicklistidPut()**](SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi.md#inventoryPicklistPicklistidPut) | **PUT** /inventory/picklist/{picklistid} | unavailable
[**inventoryPicklistPicklistidReceiptReceiptidAppendGoalidPost()**](SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi.md#inventoryPicklistPicklistidReceiptReceiptidAppendGoalidPost) | **POST** /inventory/picklist/{picklistid}/receipt/{receiptid}/append/{goalid} | unavailable
[**inventoryPicklistPost()**](SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi.md#inventoryPicklistPost) | **POST** /inventory/picklist | unavailable


## `inventoryPicklistAllCountGet()`

```php
inventoryPicklistAllCountGet($index, $limit, $picklist_id, $picklist_entry_id, $chain_using_or)
```

Provides a way to query the count of all PickList's available.

Provides a way to query the count of all PickList's available.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.
$picklist_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$picklist_entry_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $apiInstance->inventoryPicklistAllCountGet($index, $limit, $picklist_id, $picklist_entry_id, $chain_using_or);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi->inventoryPicklistAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **picklist_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **picklist_entry_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `inventoryPicklistAllEntryAllCountGet()`

```php
inventoryPicklistAllEntryAllCountGet($index, $limit, $case_id, $receipt_id, $position_id, $picklist_id, $picklist_entry_id, $chain_using_or)
```

Provides a way to query the count of all PickListEntry's available.

Provides a way to query the count of all PickListEntry's available.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.
$case_id = array(56); // int[] | Queries only those, where any picklist entry has a connection to the provided case ids.
$receipt_id = array(56); // int[] | Queries only those, where any picklist entry has a connection to the provided receipt ids.
$position_id = array(56); // int[] | Queries only those, where any picklist entry has a connection to the provided receipt entry ids.
$picklist_id = array(56); // int[] | Queries only those, where any picklist entry has a connection to the provided picklist ids.
$picklist_entry_id = array(56); // int[] | Limits the query to the provided picklist entry ids.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $apiInstance->inventoryPicklistAllEntryAllCountGet($index, $limit, $case_id, $receipt_id, $position_id, $picklist_id, $picklist_entry_id, $chain_using_or);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi->inventoryPicklistAllEntryAllCountGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **case_id** | [**int[]**](../Model/int.md)| Queries only those, where any picklist entry has a connection to the provided case ids. | [optional]
 **receipt_id** | [**int[]**](../Model/int.md)| Queries only those, where any picklist entry has a connection to the provided receipt ids. | [optional]
 **position_id** | [**int[]**](../Model/int.md)| Queries only those, where any picklist entry has a connection to the provided receipt entry ids. | [optional]
 **picklist_id** | [**int[]**](../Model/int.md)| Queries only those, where any picklist entry has a connection to the provided picklist ids. | [optional]
 **picklist_entry_id** | [**int[]**](../Model/int.md)| Limits the query to the provided picklist entry ids. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `inventoryPicklistAllEntryAllGet()`

```php
inventoryPicklistAllEntryAllGet($index, $limit, $case_id, $receipt_id, $position_id, $picklist_id, $picklist_entry_id, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickListEntry
```

Provides a way to query all PickListEntry's available.

Provides a way to query all PickListEntry's available.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.
$case_id = array(56); // int[] | Queries only those, where any picklist entry has a connection to the provided case ids.
$receipt_id = array(56); // int[] | Queries only those, where any picklist entry has a connection to the provided receipt ids.
$position_id = array(56); // int[] | Queries only those, where any picklist entry has a connection to the provided receipt entry ids.
$picklist_id = array(56); // int[] | Queries only those, where any picklist entry has a connection to the provided picklist ids.
$picklist_entry_id = array(56); // int[] | Limits the query to the provided picklist entry ids.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->inventoryPicklistAllEntryAllGet($index, $limit, $case_id, $receipt_id, $position_id, $picklist_id, $picklist_entry_id, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi->inventoryPicklistAllEntryAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **case_id** | [**int[]**](../Model/int.md)| Queries only those, where any picklist entry has a connection to the provided case ids. | [optional]
 **receipt_id** | [**int[]**](../Model/int.md)| Queries only those, where any picklist entry has a connection to the provided receipt ids. | [optional]
 **position_id** | [**int[]**](../Model/int.md)| Queries only those, where any picklist entry has a connection to the provided receipt entry ids. | [optional]
 **picklist_id** | [**int[]**](../Model/int.md)| Queries only those, where any picklist entry has a connection to the provided picklist ids. | [optional]
 **picklist_entry_id** | [**int[]**](../Model/int.md)| Limits the query to the provided picklist entry ids. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickListEntry**](../Model/SimplifySoftPecuniariusDataNetInventoryPickListEntry.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `inventoryPicklistAllGet()`

```php
inventoryPicklistAllGet($index, $limit, $picklist_id, $picklist_entry_id, $chain_using_or): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickList
```

Provides a way to query all PickList's available.

Provides a way to query all PickList's available.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index = 0; // int | The amount of records to skip.
$limit = 100; // int | The amount of records to take.
$picklist_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$picklist_entry_id = array(56); // int[] | The cases to receive. If empty, all cases will be received.
$chain_using_or = false; // bool | Sets wether chaining parameters should be done using AND or OR.

try {
    $result = $apiInstance->inventoryPicklistAllGet($index, $limit, $picklist_id, $picklist_entry_id, $chain_using_or);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi->inventoryPicklistAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **picklist_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **picklist_entry_id** | [**int[]**](../Model/int.md)| The cases to receive. If empty, all cases will be received. | [optional]
 **chain_using_or** | **bool**| Sets wether chaining parameters should be done using AND or OR. | [optional] [default to false]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickList**](../Model/SimplifySoftPecuniariusDataNetInventoryPickList.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `inventoryPicklistPicklistidDelete()`

```php
inventoryPicklistPicklistidDelete($picklistid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$picklistid = 56; // int | unavailable

try {
    $apiInstance->inventoryPicklistPicklistidDelete($picklistid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi->inventoryPicklistPicklistidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **picklistid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `inventoryPicklistPicklistidEntryAllGet()`

```php
inventoryPicklistPicklistidEntryAllGet($picklistid): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickListEntry
```

Can be used to poll the child entries of a given PickList. Empty result will be returned if no PickList.Id == pickilistid was fo

Can be used to poll the child entries of a given PickList. Empty result will be returned if no PickList.Id == pickilistid was found.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$picklistid = 56; // int | unavailable

try {
    $result = $apiInstance->inventoryPicklistPicklistidEntryAllGet($picklistid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi->inventoryPicklistPicklistidEntryAllGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **picklistid** | **int**| unavailable |

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickListEntry**](../Model/SimplifySoftPecuniariusDataNetInventoryPickListEntry.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `inventoryPicklistPicklistidEntryEntryidDelete()`

```php
inventoryPicklistPicklistidEntryEntryidDelete($picklistid, $entryid)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$picklistid = 56; // int | unavailable
$entryid = 56; // int | unavailable

try {
    $apiInstance->inventoryPicklistPicklistidEntryEntryidDelete($picklistid, $entryid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi->inventoryPicklistPicklistidEntryEntryidDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **picklistid** | **int**| unavailable |
 **entryid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `inventoryPicklistPicklistidEntryEntryidGet()`

```php
inventoryPicklistPicklistidEntryEntryidGet($picklistid, $entryid)
```

Allows receiving a single PickListEntry according to the entryid. NetException will be returned if no PickListEntry with PickLis

Allows receiving a single PickListEntry according to the entryid. NetException will be returned if no PickListEntry with PickListEntry.Id == entryid or PickListEntry.ParentFK == picklistid was found. Using 0 for picklistid allows to receive a PickListEntry without knowing the correct picklistid.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$picklistid = 56; // int | unavailable
$entryid = 56; // int | unavailable

try {
    $apiInstance->inventoryPicklistPicklistidEntryEntryidGet($picklistid, $entryid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi->inventoryPicklistPicklistidEntryEntryidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **picklistid** | **int**| unavailable |
 **entryid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `inventoryPicklistPicklistidEntryEntryidPut()`

```php
inventoryPicklistPicklistidEntryEntryidPut($picklistid, $entryid, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$picklistid = 56; // int | unavailable
$entryid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->inventoryPicklistPicklistidEntryEntryidPut($picklistid, $entryid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi->inventoryPicklistPicklistidEntryEntryidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **picklistid** | **int**| unavailable |
 **entryid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `inventoryPicklistPicklistidEntryIdsGet()`

```php
inventoryPicklistPicklistidEntryIdsGet($picklistid): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32
```

Can be used to poll the IDs of the childs entries of a given PickList. NetException will be returned if no PickList.Id == pickil

Can be used to poll the IDs of the childs entries of a given PickList. NetException will be returned if no PickList.Id == pickilistid was found.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$picklistid = 56; // int | unavailable

try {
    $result = $apiInstance->inventoryPicklistPicklistidEntryIdsGet($picklistid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi->inventoryPicklistPicklistidEntryIdsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **picklistid** | **int**| unavailable |

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrimitivesNetInt32**](../Model/SimplifySoftPecuniariusDataNetPrimitivesNetInt32.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `inventoryPicklistPicklistidEntryPost()`

```php
inventoryPicklistPicklistidEntryPost($picklistid, $simplify_soft_pecuniarius_data_net_inventory_pick_list_entry): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickListEntry
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$picklistid = 56; // int | unavailable
$simplify_soft_pecuniarius_data_net_inventory_pick_list_entry = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickListEntry(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickListEntry

try {
    $result = $apiInstance->inventoryPicklistPicklistidEntryPost($picklistid, $simplify_soft_pecuniarius_data_net_inventory_pick_list_entry);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi->inventoryPicklistPicklistidEntryPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **picklistid** | **int**| unavailable |
 **simplify_soft_pecuniarius_data_net_inventory_pick_list_entry** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickListEntry**](../Model/SimplifySoftPecuniariusDataNetInventoryPickListEntry.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickListEntry**](../Model/SimplifySoftPecuniariusDataNetInventoryPickListEntry.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `inventoryPicklistPicklistidGet()`

```php
inventoryPicklistPicklistidGet($picklistid)
```

Allows receiving a single PickList according to the picklistid. NetException will be returned if no PickList.Id == pickilistid w

Allows receiving a single PickList according to the picklistid. NetException will be returned if no PickList.Id == pickilistid was found.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$picklistid = 56; // int | unavailable

try {
    $apiInstance->inventoryPicklistPicklistidGet($picklistid);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi->inventoryPicklistPicklistidGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **picklistid** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `inventoryPicklistPicklistidPut()`

```php
inventoryPicklistPicklistidPut($picklistid, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$picklistid = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->inventoryPicklistPicklistidPut($picklistid, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi->inventoryPicklistPicklistidPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **picklistid** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `inventoryPicklistPicklistidReceiptReceiptidAppendGoalidPost()`

```php
inventoryPicklistPicklistidReceiptReceiptidAppendGoalidPost($picklistid, $receiptid, $goalid, $pickbox_group, $storage_id, $allow_partial)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$picklistid = 56; // int | unavailable
$receiptid = 56; // int | unavailable
$goalid = 56; // int | unavailable
$pickbox_group = 56; // int | unavailable
$storage_id = array(56); // int[] | unavailable
$allow_partial = True; // bool | unavailable

try {
    $apiInstance->inventoryPicklistPicklistidReceiptReceiptidAppendGoalidPost($picklistid, $receiptid, $goalid, $pickbox_group, $storage_id, $allow_partial);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi->inventoryPicklistPicklistidReceiptReceiptidAppendGoalidPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **picklistid** | **int**| unavailable |
 **receiptid** | **int**| unavailable |
 **goalid** | **int**| unavailable |
 **pickbox_group** | **int**| unavailable | [optional]
 **storage_id** | [**int[]**](../Model/int.md)| unavailable | [optional]
 **allow_partial** | **bool**| unavailable | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `inventoryPicklistPost()`

```php
inventoryPicklistPost($simplify_soft_pecuniarius_data_net_inventory_pick_list): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickList
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_inventory_pick_list = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickList(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickList

try {
    $result = $apiInstance->inventoryPicklistPost($simplify_soft_pecuniarius_data_net_inventory_pick_list);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1InventoryPickListsApi->inventoryPicklistPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_inventory_pick_list** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickList**](../Model/SimplifySoftPecuniariusDataNetInventoryPickList.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickList**](../Model/SimplifySoftPecuniariusDataNetInventoryPickList.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
