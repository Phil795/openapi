# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1ItemDataPriceGroupsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**itemdataPricegroupsGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataPriceGroupsApi.md#itemdataPricegroupsGet) | **GET** /itemdata/pricegroups | Returns the list of available price groups.
[**itemdataPricegroupsIdDelete()**](SimplifySoftPecuniariusServerAPIv1ItemDataPriceGroupsApi.md#itemdataPricegroupsIdDelete) | **DELETE** /itemdata/pricegroups/{id} | Allows to delete a single existing price group.
[**itemdataPricegroupsIdGet()**](SimplifySoftPecuniariusServerAPIv1ItemDataPriceGroupsApi.md#itemdataPricegroupsIdGet) | **GET** /itemdata/pricegroups/{id} | Returns a single price groups.
[**itemdataPricegroupsIdPut()**](SimplifySoftPecuniariusServerAPIv1ItemDataPriceGroupsApi.md#itemdataPricegroupsIdPut) | **PUT** /itemdata/pricegroups/{id} | Interface for updating existing price groups.
[**itemdataPricegroupsPost()**](SimplifySoftPecuniariusServerAPIv1ItemDataPriceGroupsApi.md#itemdataPricegroupsPost) | **POST** /itemdata/pricegroups | Provides an interface to create a new price group.


## `itemdataPricegroupsGet()`

```php
itemdataPricegroupsGet($includeprivate, $includeprices, $title, $search, $id, $contact, $owner, $item, $limit, $index): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPriceGroup
```

Returns the list of available price groups.

Returns the list of available price groups.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataPriceGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$includeprivate = false; // bool | Wether or not to include the private Contact-PriceGroups.
$includeprices = false; // bool | Wether or not to include the prices.
$title = array('title_example'); // string[] | Filters for provided title. Check is done by looking up wether the start matches the provided titles.
$search = 'search_example'; // string | Applies tolower on `PriceGroup.Title` and incomming search. Afterwards, a check is performed via LIKE. If includeprivate is passed to, lookup will be extended to include `Contact.Nickname`, `Contact.Identifier`, `Address.NameA`, `Address.NameB` and `Address.NameC`.
$id = array(56); // int[] | PriceGroups IDs to receive.
$contact = array(56); // int[] | Contact IDs to receive. Inclusive Or with owner.
$owner = 56; // int | Contact ID of the PriceGroup Owner. Implicitly sets includeprivate to true. Inclusive Or with contact.
$item = array(56); // int[] | Item IDs to receive.
$limit = 100; // int | The amount of records to take.
$index = 100; // int | unavailable

try {
    $result = $apiInstance->itemdataPricegroupsGet($includeprivate, $includeprices, $title, $search, $id, $contact, $owner, $item, $limit, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataPriceGroupsApi->itemdataPricegroupsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **includeprivate** | **bool**| Wether or not to include the private Contact-PriceGroups. | [optional] [default to false]
 **includeprices** | **bool**| Wether or not to include the prices. | [optional] [default to false]
 **title** | [**string[]**](../Model/string.md)| Filters for provided title. Check is done by looking up wether the start matches the provided titles. | [optional]
 **search** | **string**| Applies tolower on &#x60;PriceGroup.Title&#x60; and incomming search. Afterwards, a check is performed via LIKE. If includeprivate is passed to, lookup will be extended to include &#x60;Contact.Nickname&#x60;, &#x60;Contact.Identifier&#x60;, &#x60;Address.NameA&#x60;, &#x60;Address.NameB&#x60; and &#x60;Address.NameC&#x60;. | [optional]
 **id** | [**int[]**](../Model/int.md)| PriceGroups IDs to receive. | [optional]
 **contact** | [**int[]**](../Model/int.md)| Contact IDs to receive. Inclusive Or with owner. | [optional]
 **owner** | **int**| Contact ID of the PriceGroup Owner. Implicitly sets includeprivate to true. Inclusive Or with contact. | [optional]
 **item** | [**int[]**](../Model/int.md)| Item IDs to receive. | [optional]
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| unavailable | [optional] [default to 100]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPriceGroup**](../Model/SimplifySoftPecuniariusDataNetItemDataPriceGroup.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataPricegroupsIdDelete()`

```php
itemdataPricegroupsIdDelete($id)
```

Allows to delete a single existing price group.

Allows to delete a single existing price group.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataPriceGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->itemdataPricegroupsIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataPriceGroupsApi->itemdataPricegroupsIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataPricegroupsIdGet()`

```php
itemdataPricegroupsIdGet($id)
```

Returns a single price groups.

Returns a single price groups.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataPriceGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->itemdataPricegroupsIdGet($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataPriceGroupsApi->itemdataPricegroupsIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataPricegroupsIdPut()`

```php
itemdataPricegroupsIdPut($id, $unknown_base_type)
```

Interface for updating existing price groups.

Interface for updating existing price groups.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataPriceGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->itemdataPricegroupsIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataPriceGroupsApi->itemdataPricegroupsIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `itemdataPricegroupsPost()`

```php
itemdataPricegroupsPost($simplify_soft_pecuniarius_data_net_item_data_price_group): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPriceGroup
```

Provides an interface to create a new price group.

Provides an interface to create a new price group.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1ItemDataPriceGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_item_data_price_group = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPriceGroup(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPriceGroup

try {
    $result = $apiInstance->itemdataPricegroupsPost($simplify_soft_pecuniarius_data_net_item_data_price_group);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1ItemDataPriceGroupsApi->itemdataPricegroupsPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_item_data_price_group** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPriceGroup**](../Model/SimplifySoftPecuniariusDataNetItemDataPriceGroup.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPriceGroup**](../Model/SimplifySoftPecuniariusDataNetItemDataPriceGroup.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
