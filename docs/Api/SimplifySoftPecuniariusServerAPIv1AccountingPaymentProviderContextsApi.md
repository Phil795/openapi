# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**accountingPaymentproviderEndpointsGet()**](SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi.md#accountingPaymentproviderEndpointsGet) | **GET** /accounting/paymentprovider/endpoints | unavailable
[**accountingPaymentproviderPaymentMethodIdGet()**](SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi.md#accountingPaymentproviderPaymentMethodIdGet) | **GET** /accounting/paymentprovider/{paymentMethodId} | unavailable
[**accountingPaymentproviderPaymentMethodIdIdDelete()**](SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi.md#accountingPaymentproviderPaymentMethodIdIdDelete) | **DELETE** /accounting/paymentprovider/{paymentMethodId}/{id} | unavailable
[**accountingPaymentproviderPaymentMethodIdIdPropertyPost()**](SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi.md#accountingPaymentproviderPaymentMethodIdIdPropertyPost) | **POST** /accounting/paymentprovider/{paymentMethodId}/{id}/{property} | unavailable
[**accountingPaymentproviderPaymentMethodIdIdPut()**](SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi.md#accountingPaymentproviderPaymentMethodIdIdPut) | **PUT** /accounting/paymentprovider/{paymentMethodId}/{id} | unavailable
[**accountingPaymentproviderPaymentMethodIdPost()**](SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi.md#accountingPaymentproviderPaymentMethodIdPost) | **POST** /accounting/paymentprovider/{paymentMethodId} | unavailable


## `accountingPaymentproviderEndpointsGet()`

```php
accountingPaymentproviderEndpointsGet(): object
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->accountingPaymentproviderEndpointsGet();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi->accountingPaymentproviderEndpointsGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingPaymentproviderPaymentMethodIdGet()`

```php
accountingPaymentproviderPaymentMethodIdGet($payment_method_id, $limit, $index): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingPaymentProviderContext
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$payment_method_id = 56; // int | unavailable
$limit = 100; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.

try {
    $result = $apiInstance->accountingPaymentproviderPaymentMethodIdGet($payment_method_id, $limit, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi->accountingPaymentproviderPaymentMethodIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment_method_id** | **int**| unavailable |
 **limit** | **int**| The amount of records to take. | [optional] [default to 100]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingPaymentProviderContext**](../Model/SimplifySoftPecuniariusDataNetAccountingPaymentProviderContext.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingPaymentproviderPaymentMethodIdIdDelete()`

```php
accountingPaymentproviderPaymentMethodIdIdDelete($payment_method_id, $id)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$payment_method_id = 56; // int | unavailable
$id = 56; // int | unavailable

try {
    $apiInstance->accountingPaymentproviderPaymentMethodIdIdDelete($payment_method_id, $id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi->accountingPaymentproviderPaymentMethodIdIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment_method_id** | **int**| unavailable |
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingPaymentproviderPaymentMethodIdIdPropertyPost()`

```php
accountingPaymentproviderPaymentMethodIdIdPropertyPost($payment_method_id, $id, $property, $string, $int32, $int64, $double, $boolean)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$payment_method_id = 56; // int | unavailable
$id = 56; // int | unavailable
$property = 'property_example'; // string | unavailable
$string = 'string_example'; // string | The string value to set.
$int32 = 56; // int | The int value to set.
$int64 = 56; // int | The long value to set.
$double = 3.4; // double | The double value to set.
$boolean = True; // bool | The boolean value to set.

try {
    $apiInstance->accountingPaymentproviderPaymentMethodIdIdPropertyPost($payment_method_id, $id, $property, $string, $int32, $int64, $double, $boolean);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi->accountingPaymentproviderPaymentMethodIdIdPropertyPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment_method_id** | **int**| unavailable |
 **id** | **int**| unavailable |
 **property** | **string**| unavailable |
 **string** | **string**| The string value to set. | [optional]
 **int32** | **int**| The int value to set. | [optional]
 **int64** | **int**| The long value to set. | [optional]
 **double** | **double**| The double value to set. | [optional]
 **boolean** | **bool**| The boolean value to set. | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingPaymentproviderPaymentMethodIdIdPut()`

```php
accountingPaymentproviderPaymentMethodIdIdPut($payment_method_id, $id, $unknown_base_type)
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$payment_method_id = 56; // int | unavailable
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->accountingPaymentproviderPaymentMethodIdIdPut($payment_method_id, $id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi->accountingPaymentproviderPaymentMethodIdIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment_method_id** | **int**| unavailable |
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `accountingPaymentproviderPaymentMethodIdPost()`

```php
accountingPaymentproviderPaymentMethodIdPost($payment_method_id, $simplify_soft_pecuniarius_data_net_accounting_payment_provider_context): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingPaymentProviderContext
```

unavailable

unavailable

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$payment_method_id = 56; // int | unavailable
$simplify_soft_pecuniarius_data_net_accounting_payment_provider_context = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingPaymentProviderContext(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingPaymentProviderContext

try {
    $result = $apiInstance->accountingPaymentproviderPaymentMethodIdPost($payment_method_id, $simplify_soft_pecuniarius_data_net_accounting_payment_provider_context);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1AccountingPaymentProviderContextsApi->accountingPaymentproviderPaymentMethodIdPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **payment_method_id** | **int**| unavailable |
 **simplify_soft_pecuniarius_data_net_accounting_payment_provider_context** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingPaymentProviderContext**](../Model/SimplifySoftPecuniariusDataNetAccountingPaymentProviderContext.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingPaymentProviderContext**](../Model/SimplifySoftPecuniariusDataNetAccountingPaymentProviderContext.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
