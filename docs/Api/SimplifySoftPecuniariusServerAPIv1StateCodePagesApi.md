# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusServerAPIv1StateCodePagesApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**stateCodePageGet()**](SimplifySoftPecuniariusServerAPIv1StateCodePagesApi.md#stateCodePageGet) | **GET** /state/code-page | Receives a list of all available code-pages.
[**stateCodePageIdDelete()**](SimplifySoftPecuniariusServerAPIv1StateCodePagesApi.md#stateCodePageIdDelete) | **DELETE** /state/code-page/{id} | Deletes a code-page.
[**stateCodePageIdGet()**](SimplifySoftPecuniariusServerAPIv1StateCodePagesApi.md#stateCodePageIdGet) | **GET** /state/code-page/{id} | Receives a single code-page.
[**stateCodePageIdPut()**](SimplifySoftPecuniariusServerAPIv1StateCodePagesApi.md#stateCodePageIdPut) | **PUT** /state/code-page/{id} | Updates a code-page.
[**stateCodePagePost()**](SimplifySoftPecuniariusServerAPIv1StateCodePagesApi.md#stateCodePagePost) | **POST** /state/code-page | Creates one or more new code-pages.


## `stateCodePageGet()`

```php
stateCodePageGet($limit, $index): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineCodePage
```

Receives a list of all available code-pages.

Receives a list of all available code-pages.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateCodePagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$limit = 10; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.

try {
    $result = $apiInstance->stateCodePageGet($limit, $index);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateCodePagesApi->stateCodePageGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineCodePage**](../Model/SimplifySoftPecuniariusDataNetStateMachineCodePage.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateCodePageIdDelete()`

```php
stateCodePageIdDelete($id)
```

Deletes a code-page.

Deletes a code-page.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateCodePagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable

try {
    $apiInstance->stateCodePageIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateCodePagesApi->stateCodePageIdDelete: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateCodePageIdGet()`

```php
stateCodePageIdGet($id, $limit, $index)
```

Receives a single code-page.

Receives a single code-page.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateCodePagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$limit = 10; // int | The amount of records to take.
$index = 0; // int | The amount of records to skip.

try {
    $apiInstance->stateCodePageIdGet($id, $limit, $index);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateCodePagesApi->stateCodePageIdGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **limit** | **int**| The amount of records to take. | [optional] [default to 10]
 **index** | **int**| The amount of records to skip. | [optional] [default to 0]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateCodePageIdPut()`

```php
stateCodePageIdPut($id, $unknown_base_type)
```

Updates a code-page.

Updates a code-page.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateCodePagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | unavailable
$unknown_base_type = new \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE(); // \SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE

try {
    $apiInstance->stateCodePageIdPut($id, $unknown_base_type);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateCodePagesApi->stateCodePageIdPut: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| unavailable |
 **unknown_base_type** | [**\SimplifySoft\Pecuniarius\Api\Model\UNKNOWN_BASE_TYPE**](../Model/UNKNOWN_BASE_TYPE.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `stateCodePagePost()`

```php
stateCodePagePost($simplify_soft_pecuniarius_data_net_state_machine_code_page): \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineCodePage
```

Creates one or more new code-pages.

Creates one or more new code-pages.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusServerAPIv1StateCodePagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$simplify_soft_pecuniarius_data_net_state_machine_code_page = new \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineCodePage(); // \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineCodePage

try {
    $result = $apiInstance->stateCodePagePost($simplify_soft_pecuniarius_data_net_state_machine_code_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusServerAPIv1StateCodePagesApi->stateCodePagePost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simplify_soft_pecuniarius_data_net_state_machine_code_page** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineCodePage**](../Model/SimplifySoftPecuniariusDataNetStateMachineCodePage.md)|  | [optional]

### Return type

[**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineCodePage**](../Model/SimplifySoftPecuniariusDataNetStateMachineCodePage.md)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
