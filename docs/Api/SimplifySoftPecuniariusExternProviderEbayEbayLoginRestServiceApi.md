# SimplifySoft\Pecuniarius\Api\SimplifySoftPecuniariusExternProviderEbayEbayLoginRestServiceApi

All URIs are relative to http://192.168.1.105:8080.

Method | HTTP request | Description
------------- | ------------- | -------------
[**pluginEbayOauthInitializeGet()**](SimplifySoftPecuniariusExternProviderEbayEbayLoginRestServiceApi.md#pluginEbayOauthInitializeGet) | **GET** /plugin/ebay/oauth/initialize | Returns a GUID that can be used for confirming a given Ebay-OAuth-Token.


## `pluginEbayOauthInitializeGet()`

```php
pluginEbayOauthInitializeGet()
```

Returns a GUID that can be used for confirming a given Ebay-OAuth-Token.

Returns a GUID that can be used for confirming a given Ebay-OAuth-Token. At any point in time, only one OAuth request shall be active.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basic_auth
$config = SimplifySoft\Pecuniarius\Api\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new SimplifySoft\Pecuniarius\Api\Api\SimplifySoftPecuniariusExternProviderEbayEbayLoginRestServiceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $apiInstance->pluginEbayOauthInitializeGet();
} catch (Exception $e) {
    echo 'Exception when calling SimplifySoftPecuniariusExternProviderEbayEbayLoginRestServiceApi->pluginEbayOauthInitializeGet: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[basic_auth](../../README.md#basic_auth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
