# # SimplifySoftPecuniariusDataNetTagGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**max_selected_tags** | **int** |  | [optional]
**parent** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTagGroup**](SimplifySoftPecuniariusDataNetTagGroup.md) |  | [optional]
**parent_fk** | **int[]** |  | [optional]
**extern_links** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternTagGroupLink[]**](SimplifySoftPecuniariusDataNetExternExternTagGroupLink.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
