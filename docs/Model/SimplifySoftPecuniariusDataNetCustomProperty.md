# # SimplifySoftPecuniariusDataNetCustomProperty

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**group** | **string** |  | [optional]
**title** | **string** |  | [optional]
**description** | **string** |  | [optional]
**key** | **string** |  | [optional]
**value_type** | **string** |  | [optional]
**is_mandatory** | **bool** |  | [optional]
**is_read_only** | **bool** |  | [optional]
**entry_values** | **string[]** |  | [optional]
**value_string** | **string** |  | [optional]
**value_int32** | **int[]** |  | [optional]
**value_int64** | **int[]** |  | [optional]
**value_double** | **double[]** |  | [optional]
**value_boolean** | **bool[]** |  | [optional]
**value_date_time_serialization** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
