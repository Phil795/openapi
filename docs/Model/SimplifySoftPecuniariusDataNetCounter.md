# # SimplifySoftPecuniariusDataNetCounter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **string** |  | [optional]
**index** | **int** |  | [optional]
**format** | **string** |  | [optional]
**formatted** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
