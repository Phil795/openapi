# # SimplifySoftPecuniariusDataNetItemDataPrice

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**from_quantity** | **int** |  | [optional]
**auto_update_on_item_price_change** | **string** |  | [optional]
**expected_surcharge** | **float** |  | [optional]
**price_per_unit** | **float** |  | [optional]
**price_per_unit_kind** | **string** |  | [optional]
**parent_item_root** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemRoot**](SimplifySoftPecuniariusDataNetItemDataItemRoot.md) |  | [optional]
**parent_item_root_fk** | **int** |  | [optional]
**parent** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPriceGroup**](SimplifySoftPecuniariusDataNetItemDataPriceGroup.md) |  | [optional]
**parent_fk** | **int[]** |  | [optional]
**valid_from** | **string[]** |  | [optional]
**valid_to** | **string[]** |  | [optional]
**buy_stock_keeping_unit** | **string** |  | [optional]
**buy_delivery_time** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
