# # SimplifySoftPecuniariusDataNetLicenseResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_users** | **int** |  | [optional]
**delay_ms** | **int** |  | [optional]
**single_request_handling_only** | **bool** |  | [optional]
**success** | **bool** |  | [optional]
**license_key** | **string** |  | [optional]
**error** | **string** |  | [optional]
**usage** | **string[]** |  | [optional]
**current_price** | **float** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
