# # SimplifySoftPecuniariusDataNetAccountingTaxDefinition

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**is_default** | **bool** |  | [optional]
**enabled** | **bool** |  | [optional]
**tax** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTax**](SimplifySoftPecuniariusDataNetAccountingTax.md) |  | [optional]
**tax_fk** | **int[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
