# # SimplifySoftPecuniariusDataNetAuthorityPrivilegeDefinition

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **string** |  | [optional]
**name** | **string** |  | [optional]
**name_resolved** | **string** |  | [optional]
**description** | **string** |  | [optional]
**description_resolved** | **string** |  | [optional]
**disallowed** | **bool** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
