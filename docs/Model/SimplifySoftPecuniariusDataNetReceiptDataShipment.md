# # SimplifySoftPecuniariusDataNetReceiptDataShipment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**value** | **string** |  | [optional]
**is_arrived** | **bool** |  | [optional]
**is_delivered** | **bool** |  | [optional]
**time_stamp_created** | **string** |  | [optional]
**time_stamp_last_changed** | **string** |  | [optional]
**note** | **string** |  | [optional]
**tracking_url** | **string** |  | [optional]
**price** | **float** |  | [optional]
**weight_in_kilos** | **float** |  | [optional]
**price_kind** | **string** |  | [optional]
**parent_fk** | **int** |  | [optional]
**delivery_option** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryDeliveryOption**](SimplifySoftPecuniariusDataNetInventoryDeliveryOption.md) |  | [optional]
**delivery_option_fk** | **int[]** |  | [optional]
**receipt_fk** | **int[]** |  | [optional]
**tax** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTax**](SimplifySoftPecuniariusDataNetAccountingTax.md) |  | [optional]
**tax_fk** | **int[]** |  | [optional]
**entries** | **object[]** |  | [optional]
**files** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetFile[]**](SimplifySoftPecuniariusDataNetFile.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
