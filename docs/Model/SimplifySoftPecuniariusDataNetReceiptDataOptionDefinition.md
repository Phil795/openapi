# # SimplifySoftPecuniariusDataNetReceiptDataOptionDefinition

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**is_default** | **bool** |  | [optional]
**value_kind** | **string** |  | [optional]
**option_kind** | **string** |  | [optional]
**tax_definition** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTaxDefinition**](SimplifySoftPecuniariusDataNetAccountingTaxDefinition.md) |  | [optional]
**tax_definition_fk** | **int** |  | [optional]
**tax_account** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTaxAccount**](SimplifySoftPecuniariusDataNetAccountingTaxAccount.md) |  | [optional]
**tax_account_fk** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
