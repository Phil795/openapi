# # SimplifySoftPecuniariusDataNetSQLRowSet

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rows** | **object[]** |  | [optional]
**has_more** | **bool** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
