# # SimplifySoftPecuniariusDataNetReceiptDataPosition

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**amount** | **float** |  | [optional]
**price** | **float** |  | [optional]
**price_kind** | **string** |  | [optional]
**note** | **string** |  | [optional]
**extern_reference** | **string** |  | [optional]
**extern_item_reference** | **string** |  | [optional]
**estimated_date** | **string[]** |  | [optional]
**time_stamp_created** | **string** |  | [optional]
**time_stamp_last_updated** | **string** |  | [optional]
**serialnumber** | **string** |  | [optional]
**batchnumber** | **string** |  | [optional]
**best_before_date** | **string[]** |  | [optional]
**is_editible** | **bool** |  | [optional]
**tax** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTax**](SimplifySoftPecuniariusDataNetAccountingTax.md) |  | [optional]
**tax_fk** | **int[]** |  | [optional]
**item_frame** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemFrame**](SimplifySoftPecuniariusDataNetItemDataItemFrame.md) |  | [optional]
**item_frame_fk** | **int[]** |  | [optional]
**part_of** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPosition**](SimplifySoftPecuniariusDataNetReceiptDataPosition.md) |  | [optional]
**part_of_fk** | **int[]** |  | [optional]
**invoice** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataInvoice**](SimplifySoftPecuniariusDataNetReceiptDataInvoice.md) |  | [optional]
**invoice_fk** | **int[]** |  | [optional]
**parent** | **object** |  | [optional]
**parent_fk** | **int** |  | [optional]
**pick_list_entries** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickListEntry[]**](SimplifySoftPecuniariusDataNetInventoryPickListEntry.md) |  | [optional]
**options** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPositionOption[]**](SimplifySoftPecuniariusDataNetReceiptDataPositionOption.md) |  | [optional]
**delivery_goals** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal[]**](SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal.md) |  | [optional]
**receipts** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt[]**](SimplifySoftPecuniariusDataNetReceiptDataReceipt.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
