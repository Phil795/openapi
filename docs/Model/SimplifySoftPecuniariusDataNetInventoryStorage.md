# # SimplifySoftPecuniariusDataNetInventoryStorage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**warehouse** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryWarehouse**](SimplifySoftPecuniariusDataNetInventoryWarehouse.md) |  | [optional]
**warehouse_fk** | **int[]** |  | [optional]
**locations** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorageLocation[]**](SimplifySoftPecuniariusDataNetInventoryStorageLocation.md) |  | [optional]
**is_shipping** | **bool** |  | [optional]
**auto_assign_empty** | **bool** |  | [optional]
**keep_location** | **bool** |  | [optional]
**is_used_in_available** | **bool** |  | [optional]
**is_default_for_new_stock** | **bool** |  | [optional]
**width** | **float** |  | [optional]
**height** | **float** |  | [optional]
**depth** | **float** |  | [optional]
**position_x** | **float** |  | [optional]
**position_y** | **float** |  | [optional]
**position_z** | **float** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
