# # SimplifySoftPecuniariusDataNetAccountingPaymentMethod

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**thumb_16_base64** | **string** |  | [optional]
**status** | **string** |  | [optional]
**repeat_time_span** | **string** |  | [optional]
**last_executed_time_stamp** | **string** |  | [optional]
**bank_account_fk** | **int[]** |  | [optional]
**payment_provider_contexts** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingPaymentProviderContext[]**](SimplifySoftPecuniariusDataNetAccountingPaymentProviderContext.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
