# # SimplifySoftPecuniariusDataNetExternEndpoint

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**identifier** | **string** |  | [optional]
**is_library** | **bool** |  | [optional]
**authors** | **string[]** |  | [optional]
**provider_properties** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty[]**](SimplifySoftPecuniariusDataNetCustomProperty.md) |  | [optional]
**category_properties** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty[]**](SimplifySoftPecuniariusDataNetCustomProperty.md) |  | [optional]
**contact_properties** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty[]**](SimplifySoftPecuniariusDataNetCustomProperty.md) |  | [optional]
**delivery_option_properties** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty[]**](SimplifySoftPecuniariusDataNetCustomProperty.md) |  | [optional]
**file_properties** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty[]**](SimplifySoftPecuniariusDataNetCustomProperty.md) |  | [optional]
**item_properties** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty[]**](SimplifySoftPecuniariusDataNetCustomProperty.md) |  | [optional]
**payment_option_properties** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty[]**](SimplifySoftPecuniariusDataNetCustomProperty.md) |  | [optional]
**price_group_properties** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty[]**](SimplifySoftPecuniariusDataNetCustomProperty.md) |  | [optional]
**receipt_properties** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty[]**](SimplifySoftPecuniariusDataNetCustomProperty.md) |  | [optional]
**tag_properties** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty[]**](SimplifySoftPecuniariusDataNetCustomProperty.md) |  | [optional]
**tag_group_properties** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty[]**](SimplifySoftPecuniariusDataNetCustomProperty.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
