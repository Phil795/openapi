# # SimplifySoftPecuniariusDataNetReceiptDataPackagesTransitionPath

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**interaction_fk** | **int** |  | [optional]
**title** | **string** |  | [optional]
**description** | **string** |  | [optional]
**is_user_interaction** | **bool** |  | [optional]
**server_interaction_kind** | **string[]** |  | [optional]
**is_available** | **bool** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
