# # SimplifySoftPecuniariusDataNetStateMachineStateMachine

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**time_stamp_created** | **string** |  | [optional]
**guid** | [**\SimplifySoft\Pecuniarius\Api\Model\\DateTime[]**](\DateTime.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
