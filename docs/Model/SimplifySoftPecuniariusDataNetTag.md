# # SimplifySoftPecuniariusDataNetTag

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**parent** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTagGroup**](SimplifySoftPecuniariusDataNetTagGroup.md) |  | [optional]
**parent_fk** | **int[]** |  | [optional]
**red** | **string** |  | [optional]
**green** | **string** |  | [optional]
**blue** | **string** |  | [optional]
**extern_links** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternTagLink[]**](SimplifySoftPecuniariusDataNetExternExternTagLink.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
