# # SimplifySoftPecuniariusDataNetInventoryPackagesCombinedStock

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**available** | **float** |  | [optional]
**reserved** | **float** |  | [optional]
**producing** | **float** |  | [optional]
**picking** | **float** |  | [optional]
**ordered** | **float** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
