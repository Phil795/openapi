# # SimplifySoftPecuniariusDataNetItemDataUnit

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**abbreviation** | **string** |  | [optional]
**calculation_base** | **double** |  | [optional]
**parent_fk** | **int[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
