# # SimplifySoftPecuniariusDataNetItemDataPriceGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**is_default** | **bool** |  | [optional]
**owner_fk** | **int[]** |  | [optional]
**extern_links** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternPriceGroupLink[]**](SimplifySoftPecuniariusDataNetExternExternPriceGroupLink.md) |  | [optional]
**prices** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPrice[]**](SimplifySoftPecuniariusDataNetItemDataPrice.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
