# # SimplifySoftPecuniariusDataNetAccountingAccountingItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payer_fk** | **int[]** |  | [optional]
**id** | **int** |  | [optional]
**identifier** | **string** |  | [optional]
**time_stamp_created** | **string** |  | [optional]
**time_stamp_last_changed** | **string** |  | [optional]
**time_stamp_expected** | **string** |  | [optional]
**promised_value** | **float** |  | [optional]
**promised_currency** | **string** |  | [optional]
**remaining_value** | **float** |  | [optional]
**remaining_currency** | **string** |  | [optional]
**events** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTransactionEvent[]**](SimplifySoftPecuniariusDataNetAccountingTransactionEvent.md) |  | [optional]
**receipts** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt[]**](SimplifySoftPecuniariusDataNetReceiptDataReceipt.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
