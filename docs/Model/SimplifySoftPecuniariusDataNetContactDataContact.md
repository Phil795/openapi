# # SimplifySoftPecuniariusDataNetContactDataContact

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**end_of_life** | **bool** |  | [optional]
**restricted** | **bool** |  | [optional]
**exclude_tax** | **bool** |  | [optional]
**delivery_lock** | **bool** |  | [optional]
**payment_reminder** | **bool** |  | [optional]
**tax_id** | **string** |  | [optional]
**vat_id** | **string** |  | [optional]
**registry_court** | **string** |  | [optional]
**registry_number** | **string** |  | [optional]
**note** | **string** |  | [optional]
**date_of_birth** | **string** |  | [optional]
**nickname** | **string** |  | [optional]
**identifier** | **string** |  | [optional]
**creditor** | **string** |  | [optional]
**debtor** | **string** |  | [optional]
**credit_limit** | **float** |  | [optional]
**created** | **string** |  | [optional]
**last_updated** | **string** |  | [optional]
**price_group_fk** | **int** |  | [optional]
**kind_fk** | **int[]** |  | [optional]
**created_by_fk** | **int[]** |  | [optional]
**last_updated_by_fk** | **int[]** |  | [optional]
**default_delivery_option_fk** | **int[]** |  | [optional]
**default_payment_option_fk** | **int[]** |  | [optional]
**addresses** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddress[]**](SimplifySoftPecuniariusDataNetContactDataAddress.md) |  | [optional]
**contact_possibilities** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContactPossibility[]**](SimplifySoftPecuniariusDataNetContactDataContactPossibility.md) |  | [optional]
**tags** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTag[]**](SimplifySoftPecuniariusDataNetTag.md) |  | [optional]
**extern_links** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternContactLink[]**](SimplifySoftPecuniariusDataNetExternExternContactLink.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
