# # SimplifySoftPecuniariusDataNetItemDataItemRoot

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**external_stock_keeping_unit** | **string** |  | [optional]
**price_per_unit_buy** | **float** |  | [optional]
**price_per_unit_buy_kind** | **string** |  | [optional]
**price_unit** | **float[]** |  | [optional]
**price_per_unit_sell** | **float** |  | [optional]
**price_per_unit_sell_kind** | **string** |  | [optional]
**expected_surcharge** | **float** |  | [optional]
**time_stamp_created** | **string** |  | [optional]
**time_stamp_last_changed** | **string** |  | [optional]
**minimum_margin** | **float** |  | [optional]
**default_buy_amount** | **float** |  | [optional]
**default_sell_amount** | **float** |  | [optional]
**end_of_life** | **bool** |  | [optional]
**available** | **bool** |  | [optional]
**enabled** | **bool** |  | [optional]
**discountable** | **bool** |  | [optional]
**item_type** | **string** |  | [optional]
**is_soft_deleted** | **bool** |  | [optional]
**tax_account** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTaxAccount**](SimplifySoftPecuniariusDataNetAccountingTaxAccount.md) |  | [optional]
**tax_account_fk** | **int** |  | [optional]
**tax_definition** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTaxDefinition**](SimplifySoftPecuniariusDataNetAccountingTaxDefinition.md) |  | [optional]
**tax_definition_fk** | **int** |  | [optional]
**frame** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemFrame**](SimplifySoftPecuniariusDataNetItemDataItemFrame.md) |  | [optional]
**frame_fk** | **int[]** |  | [optional]
**generic_files** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetFile[]**](SimplifySoftPecuniariusDataNetFile.md) |  | [optional]
**prices** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataPrice[]**](SimplifySoftPecuniariusDataNetItemDataPrice.md) |  | [optional]
**tags** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetTag[]**](SimplifySoftPecuniariusDataNetTag.md) |  | [optional]
**categories** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataCategory[]**](SimplifySoftPecuniariusDataNetItemDataCategory.md) |  | [optional]
**extern_links** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternItemLink[]**](SimplifySoftPecuniariusDataNetExternExternItemLink.md) |  | [optional]
**stocks** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStock[]**](SimplifySoftPecuniariusDataNetInventoryStock.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
