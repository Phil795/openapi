# # SimplifySoftPecuniariusDataNetReceiptDataInvoice

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**time_stamp_created** | **string** |  | [optional]
**time_stamp_targeted** | **string** |  | [optional]
**address** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddress**](SimplifySoftPecuniariusDataNetContactDataAddress.md) |  | [optional]
**address_fk** | **int[]** |  | [optional]
**receipts** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt[]**](SimplifySoftPecuniariusDataNetReceiptDataReceipt.md) |  | [optional]
**positions** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPosition[]**](SimplifySoftPecuniariusDataNetReceiptDataPosition.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
