# # SimplifySoftPecuniariusDataNetContactDataContactPossibility

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**value** | **string** |  | [optional]
**person** | **string** |  | [optional]
**kind** | **string** |  | [optional]
**parent_fk** | **int[]** |  | [optional]
**note** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
