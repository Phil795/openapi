# # SimplifySoftPecuniariusDataNetAccountingTransactionEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**time_stamp** | **string** |  | [optional]
**extern_event_reference** | **string** |  | [optional]
**event** | **string** |  | [optional]
**extern_event_code** | **string** |  | [optional]
**event_reason** | **string** |  | [optional]
**transaction_value** | **float** |  | [optional]
**transaction_currency** | **string** |  | [optional]
**fee_value** | **float** |  | [optional]
**fee_currency** | **string** |  | [optional]
**purpose** | **string** |  | [optional]
**note** | **string** |  | [optional]
**parent_fk** | **int[]** |  | [optional]
**target_accounting_item_fk** | **int[]** |  | [optional]
**source_payment_method_fk** | **int[]** |  | [optional]
**source_payment_provider_context_fk** | **int[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
