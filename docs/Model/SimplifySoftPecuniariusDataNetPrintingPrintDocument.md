# # SimplifySoftPecuniariusDataNetPrintingPrintDocument

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**xml** | **string** |  | [optional]
**default_print** | **bool** |  | [optional]
**skip_printer_selection** | **bool** |  | [optional]
**print_document_type** | **string** |  | [optional]
**conditions** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetPrintingPrintCondition[]**](SimplifySoftPecuniariusDataNetPrintingPrintCondition.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
