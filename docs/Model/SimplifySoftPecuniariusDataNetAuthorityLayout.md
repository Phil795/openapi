# # SimplifySoftPecuniariusDataNetAuthorityLayout

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**main_window_layout_base64** | **string** |  | [optional]
**main_window_config_base64** | **string** |  | [optional]
**is_read_only** | **bool** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
