# # SimplifySoftPecuniariusDataNetAccountingPaymentDiscount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**parent_fk** | **int[]** |  | [optional]
**value** | **float** |  | [optional]
**goal_in_days** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
