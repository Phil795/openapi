# # SimplifySoftPecuniariusDataNetStateMachineStateNode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**description** | **string** |  | [optional]
**state_kind** | **string** |  | [optional]
**time_string** | **string** |  | [optional]
**hours** | **int** |  | [optional]
**hours_data_field** | **string** |  | [optional]
**minutes** | **int** |  | [optional]
**minutes_data_field** | **string** |  | [optional]
**max_repeats** | **int** |  | [optional]
**guid** | [**\DateTime**](\DateTime.md) |  | [optional]
**auto_recheck** | **bool** |  | [optional]
**localizations** | **object[]** |  | [optional]
**parent_fk** | **int[]** |  | [optional]
**group_name** | **string** |  | [optional]
**group_kind** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
