# # SimplifySoftPecuniariusDataNetFileInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional]
**mime_type** | **string** |  | [optional]
**size** | **int** |  | [optional]
**id** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
