# # SimplifySoftPecuniariusDataNetContactDataAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**salutation** | **string** |  | [optional]
**name_a** | **string** |  | [optional]
**name_b** | **string** |  | [optional]
**name_c** | **string** |  | [optional]
**street** | **string** |  | [optional]
**street_number** | **string** |  | [optional]
**town** | **string** |  | [optional]
**postal_code** | **string** |  | [optional]
**addition** | **string** |  | [optional]
**country** | **string** |  | [optional]
**is_final** | **bool** |  | [optional]
**is_default_delivery** | **bool** |  | [optional]
**is_default_billing** | **bool** |  | [optional]
**is_valid** | **bool** |  | [optional]
**time_stamp_validated** | **string** |  | [optional]
**kind** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddressKind**](SimplifySoftPecuniariusDataNetContactDataAddressKind.md) |  | [optional]
**kind_fk** | **int[]** |  | [optional]
**parent_fk** | **int[]** |  | [optional]
**default_storage** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorage**](SimplifySoftPecuniariusDataNetInventoryStorage.md) |  | [optional]
**default_storage_fk** | **int[]** |  | [optional]
**validated_by** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityUser**](SimplifySoftPecuniariusDataNetAuthorityUser.md) |  | [optional]
**validated_by_fk** | **int[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
