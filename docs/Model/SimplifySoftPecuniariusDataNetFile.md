# # SimplifySoftPecuniariusDataNetFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**mime_type** | **string** |  | [optional]
**data_base64** | **string** |  | [optional]
**preview_base64** | **string** |  | [optional]
**title** | **string** |  | [optional]
**extension** | **string** |  | [optional]
**size** | **int** |  | [optional]
**extern_links** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternFileLink[]**](SimplifySoftPecuniariusDataNetExternExternFileLink.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
