# # SimplifySoftPecuniariusDataNetAccountingTaxAccount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**is_default** | **bool** |  | [optional]
**prefix_buy_account** | **string** |  | [optional]
**prefix_sell_account** | **string** |  | [optional]
**prefix_differential_taxation_account** | **string** |  | [optional]
**domestic_buy_account** | **string** |  | [optional]
**domestic_sell_account** | **string** |  | [optional]
**domestic_differential_taxation_account** | **string** |  | [optional]
**foreign_buy_account** | **string** |  | [optional]
**foreign_sell_account** | **string** |  | [optional]
**foreign_differential_taxation_account** | **string** |  | [optional]
**foreign_eu_buy_account** | **string** |  | [optional]
**foreign_eu_sell_account** | **string** |  | [optional]
**foreign_eu_differential_taxation_account** | **string** |  | [optional]
**foreign_eu_tax_free_buy_account** | **string** |  | [optional]
**foreign_eu_tax_free_sell_account** | **string** |  | [optional]
**foreign_eu_tax_free_differential_taxation_account** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
