# # SimplifySoftPecuniariusDataNetAccountingTax

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**rate** | **float** |  | [optional]
**end_of_life** | **bool** |  | [optional]
**parent_fk** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
