# # SimplifySoftPecuniariusDataNetItemDataCategory

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**parent_fk** | **int[]** |  | [optional]
**extern_links** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternCategoryLink[]**](SimplifySoftPecuniariusDataNetExternExternCategoryLink.md) |  | [optional]
**item_roots** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemRoot[]**](SimplifySoftPecuniariusDataNetItemDataItemRoot.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
