# # SimplifySoftPecuniariusDataNetStateMachineInteractionsInteraction

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**description** | **string** |  | [optional]
**previous_node** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateNode**](SimplifySoftPecuniariusDataNetStateMachineStateNode.md) |  | [optional]
**previous_node_fk** | **int[]** |  | [optional]
**next_node** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateNode**](SimplifySoftPecuniariusDataNetStateMachineStateNode.md) |  | [optional]
**next_node_fk** | **int[]** |  | [optional]
**parent_fk** | **int[]** |  | [optional]
**localizations** | **object[]** |  | [optional]
**interaction_type** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
