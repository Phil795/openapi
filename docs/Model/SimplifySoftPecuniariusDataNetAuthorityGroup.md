# # SimplifySoftPecuniariusDataNetAuthorityGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**children** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityAuthorityBase[]**](SimplifySoftPecuniariusDataNetAuthorityAuthorityBase.md) |  | [optional]
**authority_type** | **string** |  | [optional]
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**parent_fk** | **int[]** |  | [optional]
**privileges** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityPrivilege[]**](SimplifySoftPecuniariusDataNetAuthorityPrivilege.md) |  | [optional]
**ui_layouts** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityLayout[]**](SimplifySoftPecuniariusDataNetAuthorityLayout.md) |  | [optional]
**log** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetChangeLog[]**](SimplifySoftPecuniariusDataNetChangeLog.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
