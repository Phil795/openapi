# # SimplifySoftPecuniariusDataNetInventoryPickListEntry

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**position_fk** | **int[]** |  | [optional]
**parent_fk** | **int** |  | [optional]
**amount** | **float** |  | [optional]
**amount_processed** | **float** |  | [optional]
**item_root_fk** | **int[]** |  | [optional]
**warehouse** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryWarehouse**](SimplifySoftPecuniariusDataNetInventoryWarehouse.md) |  | [optional]
**warehouse_fk** | **int[]** |  | [optional]
**storage** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorage**](SimplifySoftPecuniariusDataNetInventoryStorage.md) |  | [optional]
**storage_fk** | **int[]** |  | [optional]
**storage_location** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorageLocation**](SimplifySoftPecuniariusDataNetInventoryStorageLocation.md) |  | [optional]
**storage_location_fk** | **int[]** |  | [optional]
**stock** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStock**](SimplifySoftPecuniariusDataNetInventoryStock.md) |  | [optional]
**stock_fk** | **int[]** |  | [optional]
**pick_box** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickBox**](SimplifySoftPecuniariusDataNetInventoryPickBox.md) |  | [optional]
**pick_box_fk** | **int[]** |  | [optional]
**note** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
