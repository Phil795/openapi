# # SimplifySoftPecuniariusDataNetItemDataUnitTier

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**from_value** | **double** |  | [optional]
**to_value** | **double** |  | [optional]
**target_value** | **double** |  | [optional]
**parent_fk** | **int[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
