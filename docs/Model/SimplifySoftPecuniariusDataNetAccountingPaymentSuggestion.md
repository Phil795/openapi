# # SimplifySoftPecuniariusDataNetAccountingPaymentSuggestion

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_root_id** | **int** |  | [optional]
**accounting_item** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingAccountingItem**](SimplifySoftPecuniariusDataNetAccountingAccountingItem.md) |  | [optional]
**receipt** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt**](SimplifySoftPecuniariusDataNetReceiptDataReceipt.md) |  | [optional]
**reason** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
