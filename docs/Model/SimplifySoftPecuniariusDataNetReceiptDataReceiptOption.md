# # SimplifySoftPecuniariusDataNetReceiptDataReceiptOption

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**price** | **float** |  | [optional]
**price_kind** | **string** |  | [optional]
**note** | **string** |  | [optional]
**parent** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt**](SimplifySoftPecuniariusDataNetReceiptDataReceipt.md) |  | [optional]
**parent_fk** | **int** |  | [optional]
**definition** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataOptionDefinition**](SimplifySoftPecuniariusDataNetReceiptDataOptionDefinition.md) |  | [optional]
**definition_fk** | **int** |  | [optional]
**tax** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTax**](SimplifySoftPecuniariusDataNetAccountingTax.md) |  | [optional]
**tax_fk** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
