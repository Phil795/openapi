# # SimplifySoftPecuniariusDataNetInventoryPickBox

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**group_number** | **int** |  | [optional]
**title** | **string** |  | [optional]
**width** | **float** |  | [optional]
**height** | **float** |  | [optional]
**depth** | **float** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
