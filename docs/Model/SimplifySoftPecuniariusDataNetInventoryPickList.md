# # SimplifySoftPecuniariusDataNetInventoryPickList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**time_stamp_created** | **string** |  | [optional]
**identifier** | **string** |  | [optional]
**state** | **string** |  | [optional]
**note** | **string** |  | [optional]
**entries** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickListEntry[]**](SimplifySoftPecuniariusDataNetInventoryPickListEntry.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
