# # SimplifySoftPecuniariusDataNetItemDataItemLink

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**parent** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemFrame**](SimplifySoftPecuniariusDataNetItemDataItemFrame.md) |  | [optional]
**parent_fk** | **int[]** |  | [optional]
**secondary** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemRoot**](SimplifySoftPecuniariusDataNetItemDataItemRoot.md) |  | [optional]
**secondary_fk** | **int[]** |  | [optional]
**amount** | **float** |  | [optional]
**surcharge** | **float** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
