# # SimplifySoftPecuniariusDataNetInventoryWarehouse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**is_virtual** | **bool** |  | [optional]
**is_default_for_new_stock** | **bool** |  | [optional]
**owner_fk** | **int[]** |  | [optional]
**address_fk** | **int[]** |  | [optional]
**contact_possibility_fk** | **int[]** |  | [optional]
**storages** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorage[]**](SimplifySoftPecuniariusDataNetInventoryStorage.md) |  | [optional]
**width** | **float** |  | [optional]
**height** | **float** |  | [optional]
**depth** | **float** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
