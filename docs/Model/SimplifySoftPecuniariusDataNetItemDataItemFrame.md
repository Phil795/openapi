# # SimplifySoftPecuniariusDataNetItemDataItemFrame

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**stock_keeping_unit** | **string** |  | [optional]
**title** | **string** |  | [optional]
**description** | **string** |  | [optional]
**global_trade_item_number** | **string** |  | [optional]
**weight_net** | **float** |  | [optional]
**weight_gross** | **float** |  | [optional]
**unit_value** | **double** |  | [optional]
**width** | **float** |  | [optional]
**height** | **float** |  | [optional]
**depth** | **float** |  | [optional]
**thumb_16_base64** | **string** |  | [optional]
**is_final** | **bool** |  | [optional]
**unit** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnit**](SimplifySoftPecuniariusDataNetItemDataUnit.md) |  | [optional]
**unit_fk** | **int[]** |  | [optional]
**parent** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemRoot**](SimplifySoftPecuniariusDataNetItemDataItemRoot.md) |  | [optional]
**parent_fk** | **int** |  | [optional]
**default_file_fk** | **int[]** |  | [optional]
**linked_items** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemLink[]**](SimplifySoftPecuniariusDataNetItemDataItemLink.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
