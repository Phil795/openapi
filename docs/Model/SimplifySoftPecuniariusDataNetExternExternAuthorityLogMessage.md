# # SimplifySoftPecuniariusDataNetExternExternAuthorityLogMessage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**message** | **string** |  | [optional]
**exception_data** | **string** |  | [optional]
**time_stamp** | **string** |  | [optional]
**severity** | **string** |  | [optional]
**parent_fk** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
