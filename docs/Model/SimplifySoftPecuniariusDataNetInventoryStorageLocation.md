# # SimplifySoftPecuniariusDataNetInventoryStorageLocation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**note** | **string** |  | [optional]
**parent_fk** | **int[]** |  | [optional]
**storage** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorage**](SimplifySoftPecuniariusDataNetInventoryStorage.md) |  | [optional]
**storage_fk** | **int[]** |  | [optional]
**children** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorageLocation[]**](SimplifySoftPecuniariusDataNetInventoryStorageLocation.md) |  | [optional]
**stocks** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStock[]**](SimplifySoftPecuniariusDataNetInventoryStock.md) |  | [optional]
**entries** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickListEntry[]**](SimplifySoftPecuniariusDataNetInventoryPickListEntry.md) |  | [optional]
**item_root** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemRoot**](SimplifySoftPecuniariusDataNetItemDataItemRoot.md) |  | [optional]
**item_root_fk** | **int[]** |  | [optional]
**position_x** | **float** |  | [optional]
**position_y** | **float** |  | [optional]
**position_z** | **float** |  | [optional]
**width** | **float** |  | [optional]
**height** | **float** |  | [optional]
**depth** | **float** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
