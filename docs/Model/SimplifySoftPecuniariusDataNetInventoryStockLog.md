# # SimplifySoftPecuniariusDataNetInventoryStockLog

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**item_root_fk** | **int** |  | [optional]
**note** | **string** |  | [optional]
**timestamp** | **string** |  | [optional]
**amount** | **float** |  | [optional]
**storage_location_fk** | **int[]** |  | [optional]
**receipt_entry_fk** | **int[]** |  | [optional]
**issued_by** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityUser**](SimplifySoftPecuniariusDataNetAuthorityUser.md) |  | [optional]
**issued_by_fk** | **int[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
