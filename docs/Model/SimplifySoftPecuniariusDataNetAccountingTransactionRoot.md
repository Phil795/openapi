# # SimplifySoftPecuniariusDataNetAccountingTransactionRoot

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**status** | **string** |  | [optional]
**time_stamp_initialized** | **string** |  | [optional]
**extern_reference** | **string** |  | [optional]
**initial_value** | **float** |  | [optional]
**initial_currency** | **string** |  | [optional]
**remaining_value** | **float** |  | [optional]
**remaining_currency** | **string** |  | [optional]
**payer** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTransactionParty**](SimplifySoftPecuniariusDataNetAccountingTransactionParty.md) |  | [optional]
**payer_fk** | **int[]** |  | [optional]
**receiver** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTransactionParty**](SimplifySoftPecuniariusDataNetAccountingTransactionParty.md) |  | [optional]
**receiver_fk** | **int[]** |  | [optional]
**source_payment_method_fk** | **int[]** |  | [optional]
**source_payment_provider_context_fk** | **int[]** |  | [optional]
**events** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTransactionEvent[]**](SimplifySoftPecuniariusDataNetAccountingTransactionEvent.md) |  | [optional]
**is_hidden** | **bool** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
