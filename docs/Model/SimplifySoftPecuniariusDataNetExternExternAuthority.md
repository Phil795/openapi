# # SimplifySoftPecuniariusDataNetExternExternAuthority

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**repeat_time_span** | **string** |  | [optional]
**last_executed_time_stamp** | **string** |  | [optional]
**advisory_lock_number** | **int** |  | [optional]
**endpoint_identifier** | **string** |  | [optional]
**status** | **string** |  | [optional]
**properties** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty[]**](SimplifySoftPecuniariusDataNetCustomProperty.md) |  | [optional]
**log_messages** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternAuthorityLogMessage[]**](SimplifySoftPecuniariusDataNetExternExternAuthorityLogMessage.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
