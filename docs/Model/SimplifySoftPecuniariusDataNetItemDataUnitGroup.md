# # SimplifySoftPecuniariusDataNetItemDataUnitGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**units** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnit[]**](SimplifySoftPecuniariusDataNetItemDataUnit.md) |  | [optional]
**unit_tiers** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataUnitTier[]**](SimplifySoftPecuniariusDataNetItemDataUnitTier.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
