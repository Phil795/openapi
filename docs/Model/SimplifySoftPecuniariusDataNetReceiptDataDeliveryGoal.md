# # SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**time_stamp_created** | **string** |  | [optional]
**time_stamp_targeted** | **string** |  | [optional]
**price** | **float** |  | [optional]
**price_kind** | **string** |  | [optional]
**freeze_price** | **bool** |  | [optional]
**storage** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorage**](SimplifySoftPecuniariusDataNetInventoryStorage.md) |  | [optional]
**storage_fk** | **int[]** |  | [optional]
**address** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataAddress**](SimplifySoftPecuniariusDataNetContactDataAddress.md) |  | [optional]
**address_fk** | **int[]** |  | [optional]
**method** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryDeliveryOption**](SimplifySoftPecuniariusDataNetInventoryDeliveryOption.md) |  | [optional]
**method_fk** | **int[]** |  | [optional]
**parent** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt**](SimplifySoftPecuniariusDataNetReceiptDataReceipt.md) |  | [optional]
**parent_fk** | **int** |  | [optional]
**tax** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTax**](SimplifySoftPecuniariusDataNetAccountingTax.md) |  | [optional]
**tax_fk** | **int[]** |  | [optional]
**positions** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPosition[]**](SimplifySoftPecuniariusDataNetReceiptDataPosition.md) |  | [optional]
**shipments** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataShipment[]**](SimplifySoftPecuniariusDataNetReceiptDataShipment.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
