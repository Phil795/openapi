# # SimplifySoftPecuniariusDataNetAccountingTransactionParty

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**account** | **string** |  | [optional]
**reference** | **string** |  | [optional]
**full_name** | **string** |  | [optional]
**country_code** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
