# # SimplifySoftPecuniariusDataNetPrintingPrintCondition

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**condition_type** | **string** |  | [optional]
**print** | **bool** |  | [optional]
**parent_fk** | **int[]** |  | [optional]
**order** | **int** |  | [optional]
**extern_authority_fk** | **int[]** |  | [optional]
**contact_kind_fk** | **int[]** |  | [optional]
**price_group_fk** | **int[]** |  | [optional]
**tag_fk** | **int[]** |  | [optional]
**contact_fk** | **int[]** |  | [optional]
**item_root_fk** | **int[]** |  | [optional]
**trigger** | **string** |  | [optional]
**trigger_data** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
