# # SimplifySoftPecuniariusDataNetAccountingPaymentOption

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**percentage_required_in_advance** | **float** |  | [optional]
**extern_links** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternPaymentOptionLink[]**](SimplifySoftPecuniariusDataNetExternExternPaymentOptionLink.md) |  | [optional]
**payment_discounts** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingPaymentDiscount[]**](SimplifySoftPecuniariusDataNetAccountingPaymentDiscount.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
