# # SimplifySoftPecuniariusDataNetAuthorityUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**change_password_on_login** | **bool** |  | [optional]
**enabled** | **bool** |  | [optional]
**password_hash** | **string** |  | [optional]
**salt** | **string** |  | [optional]
**avatar_fk** | **int[]** |  | [optional]
**active_layout** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityLayout**](SimplifySoftPecuniariusDataNetAuthorityLayout.md) |  | [optional]
**local_warehouse_fk** | **int[]** |  | [optional]
**authority_type** | **string** |  | [optional]
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**parent_fk** | **int[]** |  | [optional]
**privileges** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityPrivilege[]**](SimplifySoftPecuniariusDataNetAuthorityPrivilege.md) |  | [optional]
**ui_layouts** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAuthorityLayout[]**](SimplifySoftPecuniariusDataNetAuthorityLayout.md) |  | [optional]
**log** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetChangeLog[]**](SimplifySoftPecuniariusDataNetChangeLog.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
