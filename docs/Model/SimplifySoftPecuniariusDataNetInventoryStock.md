# # SimplifySoftPecuniariusDataNetInventoryStock

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**storage_location** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryStorageLocation**](SimplifySoftPecuniariusDataNetInventoryStorageLocation.md) |  | [optional]
**storage_location_fk** | **int** |  | [optional]
**item_root_fk** | **int[]** |  | [optional]
**amount_minimum** | **float** |  | [optional]
**amount_maximum** | **float** |  | [optional]
**note** | **string** |  | [optional]
**amount_available** | **float** |  | [optional]
**amount_picking** | **float** |  | [optional]
**amount_reserved** | **float** |  | [optional]
**amount_producing** | **float** |  | [optional]
**amount_ordered** | **float** |  | [optional]
**serialnumber** | **string** |  | [optional]
**batchnumber** | **string** |  | [optional]
**best_before_date** | **string[]** |  | [optional]
**is_private_purchased** | **bool** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
