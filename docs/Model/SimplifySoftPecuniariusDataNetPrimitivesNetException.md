# # SimplifySoftPecuniariusDataNetPrimitivesNetException

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **string** |  | [optional]
**stack_trace** | **string** |  | [optional]
**original_type_name** | **string** |  | [optional]
**error_code** | **int** |  | [optional]
**data** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
