# # SimplifySoftPecuniariusDataNetStateMachineInteractionsEnterInteraction

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**interaction_type** | **string** |  | [optional]
**user_visible** | **bool** |  | [optional]
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**description** | **string** |  | [optional]
**previous_node** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateNode**](SimplifySoftPecuniariusDataNetStateMachineStateNode.md) |  | [optional]
**previous_node_fk** | **int[]** |  | [optional]
**next_node** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetStateMachineStateNode**](SimplifySoftPecuniariusDataNetStateMachineStateNode.md) |  | [optional]
**next_node_fk** | **int[]** |  | [optional]
**parent_fk** | **int[]** |  | [optional]
**localizations** | **object[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
