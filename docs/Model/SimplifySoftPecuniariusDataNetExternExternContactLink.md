# # SimplifySoftPecuniariusDataNetExternExternContactLink

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**is_active** | **bool** |  | [optional]
**linked_fk** | **int[]** |  | [optional]
**parent_fk** | **int[]** |  | [optional]
**properties** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty[]**](SimplifySoftPecuniariusDataNetCustomProperty.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
