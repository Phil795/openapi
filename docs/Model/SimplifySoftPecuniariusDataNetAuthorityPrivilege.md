# # SimplifySoftPecuniariusDataNetAuthorityPrivilege

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parent_fk** | **int** |  | [optional]
**key** | **string** |  | [optional]
**flags** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
