# # SimplifySoftPecuniariusDataNetItemDataPriceLog

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**item_root** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemRoot**](SimplifySoftPecuniariusDataNetItemDataItemRoot.md) |  | [optional]
**item_root_fk** | **int** |  | [optional]
**contact** | [**\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetContactDataContact**](SimplifySoftPecuniariusDataNetContactDataContact.md) |  | [optional]
**contact_fk** | **int** |  | [optional]
**time_stamp** | **string** |  | [optional]
**price_gross** | **float** |  | [optional]
**price_net** | **float** |  | [optional]
**is_buy** | **bool** |  | [optional]
**amount** | **float[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
