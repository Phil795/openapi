# # SimplifySoftPecuniariusDataNetStateMachineCodePage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**title** | **string** |  | [optional]
**description** | **string** |  | [optional]
**page_type** | **string** |  | [optional]
**code** | **string** |  | [optional]
**guid** | [**\SimplifySoft\Pecuniarius\Api\Model\\DateTime[]**](\DateTime.md) |  | [optional]
**parent_fk** | **int[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
