# # SimplifySoftPecuniariusDataNetAccountingCurrency

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iso_name** | **string** |  | [optional]
**country** | **string** |  | [optional]
**minor_unit** | **string** |  | [optional]
**title** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
