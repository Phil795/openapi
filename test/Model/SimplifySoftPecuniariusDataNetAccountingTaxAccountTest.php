<?php
/**
 * SimplifySoftPecuniariusDataNetAccountingTaxAccountTest
 *
 * PHP version 7.2
 *
 * @category Class
 * @package  SimplifySoft\Pecuniarius\Api
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Pecuniarius API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace SimplifySoft\Pecuniarius\Api\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * SimplifySoftPecuniariusDataNetAccountingTaxAccountTest Class Doc Comment
 *
 * @category    Class
 * @description SimplifySoftPecuniariusDataNetAccountingTaxAccount
 * @package     SimplifySoft\Pecuniarius\Api
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class SimplifySoftPecuniariusDataNetAccountingTaxAccountTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "SimplifySoftPecuniariusDataNetAccountingTaxAccount"
     */
    public function testSimplifySoftPecuniariusDataNetAccountingTaxAccount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "title"
     */
    public function testPropertyTitle()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "is_default"
     */
    public function testPropertyIsDefault()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "prefix_buy_account"
     */
    public function testPropertyPrefixBuyAccount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "prefix_sell_account"
     */
    public function testPropertyPrefixSellAccount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "prefix_differential_taxation_account"
     */
    public function testPropertyPrefixDifferentialTaxationAccount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "domestic_buy_account"
     */
    public function testPropertyDomesticBuyAccount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "domestic_sell_account"
     */
    public function testPropertyDomesticSellAccount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "domestic_differential_taxation_account"
     */
    public function testPropertyDomesticDifferentialTaxationAccount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "foreign_buy_account"
     */
    public function testPropertyForeignBuyAccount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "foreign_sell_account"
     */
    public function testPropertyForeignSellAccount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "foreign_differential_taxation_account"
     */
    public function testPropertyForeignDifferentialTaxationAccount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "foreign_eu_buy_account"
     */
    public function testPropertyForeignEuBuyAccount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "foreign_eu_sell_account"
     */
    public function testPropertyForeignEuSellAccount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "foreign_eu_differential_taxation_account"
     */
    public function testPropertyForeignEuDifferentialTaxationAccount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "foreign_eu_tax_free_buy_account"
     */
    public function testPropertyForeignEuTaxFreeBuyAccount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "foreign_eu_tax_free_sell_account"
     */
    public function testPropertyForeignEuTaxFreeSellAccount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "foreign_eu_tax_free_differential_taxation_account"
     */
    public function testPropertyForeignEuTaxFreeDifferentialTaxationAccount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
