<?php
/**
 * SimplifySoftPecuniariusDataNetReceiptDataPositionTest
 *
 * PHP version 7.2
 *
 * @category Class
 * @package  SimplifySoft\Pecuniarius\Api
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Pecuniarius API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace SimplifySoft\Pecuniarius\Api\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * SimplifySoftPecuniariusDataNetReceiptDataPositionTest Class Doc Comment
 *
 * @category    Class
 * @description SimplifySoftPecuniariusDataNetReceiptDataPosition
 * @package     SimplifySoft\Pecuniarius\Api
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class SimplifySoftPecuniariusDataNetReceiptDataPositionTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "SimplifySoftPecuniariusDataNetReceiptDataPosition"
     */
    public function testSimplifySoftPecuniariusDataNetReceiptDataPosition()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "amount"
     */
    public function testPropertyAmount()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "price"
     */
    public function testPropertyPrice()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "price_kind"
     */
    public function testPropertyPriceKind()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "note"
     */
    public function testPropertyNote()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "extern_reference"
     */
    public function testPropertyExternReference()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "extern_item_reference"
     */
    public function testPropertyExternItemReference()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "estimated_date"
     */
    public function testPropertyEstimatedDate()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "time_stamp_created"
     */
    public function testPropertyTimeStampCreated()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "time_stamp_last_updated"
     */
    public function testPropertyTimeStampLastUpdated()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "serialnumber"
     */
    public function testPropertySerialnumber()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "batchnumber"
     */
    public function testPropertyBatchnumber()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "best_before_date"
     */
    public function testPropertyBestBeforeDate()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "is_editible"
     */
    public function testPropertyIsEditible()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "tax"
     */
    public function testPropertyTax()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "tax_fk"
     */
    public function testPropertyTaxFk()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "item_frame"
     */
    public function testPropertyItemFrame()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "item_frame_fk"
     */
    public function testPropertyItemFrameFk()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "part_of"
     */
    public function testPropertyPartOf()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "part_of_fk"
     */
    public function testPropertyPartOfFk()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "invoice"
     */
    public function testPropertyInvoice()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "invoice_fk"
     */
    public function testPropertyInvoiceFk()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "parent"
     */
    public function testPropertyParent()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "parent_fk"
     */
    public function testPropertyParentFk()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "pick_list_entries"
     */
    public function testPropertyPickListEntries()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "options"
     */
    public function testPropertyOptions()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "delivery_goals"
     */
    public function testPropertyDeliveryGoals()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "receipts"
     */
    public function testPropertyReceipts()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
