<?php
/**
 * SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApiTest
 * PHP version 7.2
 *
 * @category Class
 * @package  SimplifySoft\Pecuniarius\Api
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Pecuniarius API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace SimplifySoft\Pecuniarius\Api\Test\Api;

use \SimplifySoft\Pecuniarius\Api\Configuration;
use \SimplifySoft\Pecuniarius\Api\ApiException;
use \SimplifySoft\Pecuniarius\Api\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApiTest Class Doc Comment
 *
 * @category Class
 * @package  SimplifySoft\Pecuniarius\Api
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class SimplifySoftPecuniariusServerAPIv1PrintingPrintDocumentsApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for printDocumentsDidConditionsCidDelete
     *
     * unavailable.
     *
     */
    public function testPrintDocumentsDidConditionsCidDelete()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for printDocumentsDidConditionsCidPut
     *
     * unavailable.
     *
     */
    public function testPrintDocumentsDidConditionsCidPut()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for printDocumentsDidConditionsGet
     *
     * unavailable.
     *
     */
    public function testPrintDocumentsDidConditionsGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for printDocumentsDidConditionsPost
     *
     * unavailable.
     *
     */
    public function testPrintDocumentsDidConditionsPost()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for printDocumentsGet
     *
     * unavailable.
     *
     */
    public function testPrintDocumentsGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for printDocumentsIdDelete
     *
     * unavailable.
     *
     */
    public function testPrintDocumentsIdDelete()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for printDocumentsIdGet
     *
     * unavailable.
     *
     */
    public function testPrintDocumentsIdGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for printDocumentsIdPut
     *
     * unavailable.
     *
     */
    public function testPrintDocumentsIdPut()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for printDocumentsPost
     *
     * unavailable.
     *
     */
    public function testPrintDocumentsPost()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
