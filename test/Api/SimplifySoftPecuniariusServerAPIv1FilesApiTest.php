<?php
/**
 * SimplifySoftPecuniariusServerAPIv1FilesApiTest
 * PHP version 7.2
 *
 * @category Class
 * @package  SimplifySoft\Pecuniarius\Api
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Pecuniarius API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace SimplifySoft\Pecuniarius\Api\Test\Api;

use \SimplifySoft\Pecuniarius\Api\Configuration;
use \SimplifySoft\Pecuniarius\Api\ApiException;
use \SimplifySoft\Pecuniarius\Api\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * SimplifySoftPecuniariusServerAPIv1FilesApiTest Class Doc Comment
 *
 * @category Class
 * @package  SimplifySoft\Pecuniarius\Api
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class SimplifySoftPecuniariusServerAPIv1FilesApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for filesGet
     *
     * Returns a list of files (no content)..
     *
     */
    public function testFilesGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for filesIdClonePost
     *
     * Clones provided file id. Only the new ID will be returned..
     *
     */
    public function testFilesIdClonePost()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for filesIdDelete
     *
     * Allows to remove a file entirely..
     *
     */
    public function testFilesIdDelete()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for filesIdGet
     *
     * Returns a single, full file..
     *
     */
    public function testFilesIdGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for filesIdPut
     *
     * Provides an interface to update a file..
     *
     */
    public function testFilesIdPut()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for filesPost
     *
     * Allows to create new files..
     *
     */
    public function testFilesPost()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
