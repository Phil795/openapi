<?php
/**
 * SimplifySoftPecuniariusServerAPIv1InventoryPickListsApiTest
 * PHP version 7.2
 *
 * @category Class
 * @package  SimplifySoft\Pecuniarius\Api
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Pecuniarius API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace SimplifySoft\Pecuniarius\Api\Test\Api;

use \SimplifySoft\Pecuniarius\Api\Configuration;
use \SimplifySoft\Pecuniarius\Api\ApiException;
use \SimplifySoft\Pecuniarius\Api\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * SimplifySoftPecuniariusServerAPIv1InventoryPickListsApiTest Class Doc Comment
 *
 * @category Class
 * @package  SimplifySoft\Pecuniarius\Api
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class SimplifySoftPecuniariusServerAPIv1InventoryPickListsApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for inventoryPicklistAllCountGet
     *
     * Provides a way to query the count of all PickList's available..
     *
     */
    public function testInventoryPicklistAllCountGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for inventoryPicklistAllEntryAllCountGet
     *
     * Provides a way to query the count of all PickListEntry's available..
     *
     */
    public function testInventoryPicklistAllEntryAllCountGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for inventoryPicklistAllEntryAllGet
     *
     * Provides a way to query all PickListEntry's available..
     *
     */
    public function testInventoryPicklistAllEntryAllGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for inventoryPicklistAllGet
     *
     * Provides a way to query all PickList's available..
     *
     */
    public function testInventoryPicklistAllGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for inventoryPicklistPicklistidDelete
     *
     * unavailable.
     *
     */
    public function testInventoryPicklistPicklistidDelete()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for inventoryPicklistPicklistidEntryAllGet
     *
     * Can be used to poll the child entries of a given PickList. Empty result will be returned if no PickList.Id == pickilistid was fo.
     *
     */
    public function testInventoryPicklistPicklistidEntryAllGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for inventoryPicklistPicklistidEntryEntryidDelete
     *
     * unavailable.
     *
     */
    public function testInventoryPicklistPicklistidEntryEntryidDelete()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for inventoryPicklistPicklistidEntryEntryidGet
     *
     * Allows receiving a single PickListEntry according to the entryid. NetException will be returned if no PickListEntry with PickLis.
     *
     */
    public function testInventoryPicklistPicklistidEntryEntryidGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for inventoryPicklistPicklistidEntryEntryidPut
     *
     * unavailable.
     *
     */
    public function testInventoryPicklistPicklistidEntryEntryidPut()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for inventoryPicklistPicklistidEntryIdsGet
     *
     * Can be used to poll the IDs of the childs entries of a given PickList. NetException will be returned if no PickList.Id == pickil.
     *
     */
    public function testInventoryPicklistPicklistidEntryIdsGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for inventoryPicklistPicklistidEntryPost
     *
     * unavailable.
     *
     */
    public function testInventoryPicklistPicklistidEntryPost()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for inventoryPicklistPicklistidGet
     *
     * Allows receiving a single PickList according to the picklistid. NetException will be returned if no PickList.Id == pickilistid w.
     *
     */
    public function testInventoryPicklistPicklistidGet()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for inventoryPicklistPicklistidPut
     *
     * unavailable.
     *
     */
    public function testInventoryPicklistPicklistidPut()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for inventoryPicklistPicklistidReceiptReceiptidAppendGoalidPost
     *
     * unavailable.
     *
     */
    public function testInventoryPicklistPicklistidReceiptReceiptidAppendGoalidPost()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for inventoryPicklistPost
     *
     * unavailable.
     *
     */
    public function testInventoryPicklistPost()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
