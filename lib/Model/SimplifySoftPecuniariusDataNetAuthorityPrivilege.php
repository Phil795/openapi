<?php
/**
 * SimplifySoftPecuniariusDataNetAuthorityPrivilege
 *
 * PHP version 7.2
 *
 * @category Class
 * @package  SimplifySoft\Pecuniarius\Api
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Pecuniarius API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace SimplifySoft\Pecuniarius\Api\Model;

use \ArrayAccess;
use \SimplifySoft\Pecuniarius\Api\ObjectSerializer;

/**
 * SimplifySoftPecuniariusDataNetAuthorityPrivilege Class Doc Comment
 *
 * @category Class
 * @package  SimplifySoft\Pecuniarius\Api
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<TKey, TValue>
 * @template TKey int|null
 * @template TValue mixed|null
 */
class SimplifySoftPecuniariusDataNetAuthorityPrivilege implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'SimplifySoft.Pecuniarius.Data.Net.Authority.Privilege';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'parent_fk' => 'int',
        'key' => 'string',
        'flags' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'parent_fk' => 'int32',
        'key' => null,
        'flags' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'parent_fk' => 'ParentFK',
        'key' => 'Key',
        'flags' => 'Flags'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'parent_fk' => 'setParentFk',
        'key' => 'setKey',
        'flags' => 'setFlags'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'parent_fk' => 'getParentFk',
        'key' => 'getKey',
        'flags' => 'getFlags'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }

    const FLAGS_NONE = 'None';
    const FLAGS_READ = 'Read';
    const FLAGS_WRITE = 'Write';
    const FLAGS_READ_WRITE = 'ReadWrite';
    const FLAGS_DELETE = 'Delete';
    const FLAGS_EXECUTE = 'Execute';
    const FLAGS_EXECUTE_READ = 'ExecuteRead';
    const FLAGS_POLL = 'Poll';
    const FLAGS_INSERT = 'Insert';
    const FLAGS_UPDATE = 'Update';
    const FLAGS_DROP = 'Drop';
    const FLAGS_FULL = 'Full';
    const FLAGS_ANY = 'Any';
    const FLAGS_ALL = 'All';
    const FLAGS_NO_READ = 'NoRead';
    const FLAGS_NO_WRITE = 'NoWrite';
    const FLAGS_NO_READ_WRITE = 'NoReadWrite';
    const FLAGS_NO_DELETE = 'NoDelete';
    const FLAGS_NO_EXECUTE = 'NoExecute';
    const FLAGS_NO_POLL = 'NoPoll';
    const FLAGS_NO_EXECUTE_READ = 'NoExecuteRead';
    const FLAGS_NO_INSERT = 'NoInsert';
    const FLAGS_NO_UPDATE = 'NoUpdate';
    const FLAGS_NO_DROP = 'NoDrop';
    const FLAGS_NO_ANY = 'NoAny';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getFlagsAllowableValues()
    {
        return [
            self::FLAGS_NONE,
            self::FLAGS_READ,
            self::FLAGS_WRITE,
            self::FLAGS_READ_WRITE,
            self::FLAGS_DELETE,
            self::FLAGS_EXECUTE,
            self::FLAGS_EXECUTE_READ,
            self::FLAGS_POLL,
            self::FLAGS_INSERT,
            self::FLAGS_UPDATE,
            self::FLAGS_DROP,
            self::FLAGS_FULL,
            self::FLAGS_ANY,
            self::FLAGS_ALL,
            self::FLAGS_NO_READ,
            self::FLAGS_NO_WRITE,
            self::FLAGS_NO_READ_WRITE,
            self::FLAGS_NO_DELETE,
            self::FLAGS_NO_EXECUTE,
            self::FLAGS_NO_POLL,
            self::FLAGS_NO_EXECUTE_READ,
            self::FLAGS_NO_INSERT,
            self::FLAGS_NO_UPDATE,
            self::FLAGS_NO_DROP,
            self::FLAGS_NO_ANY,
        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['parent_fk'] = $data['parent_fk'] ?? null;
        $this->container['key'] = $data['key'] ?? null;
        $this->container['flags'] = $data['flags'] ?? null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        $allowedValues = $this->getFlagsAllowableValues();
        if (!is_null($this->container['flags']) && !in_array($this->container['flags'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value '%s' for 'flags', must be one of '%s'",
                $this->container['flags'],
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets parent_fk
     *
     * @return int|null
     */
    public function getParentFk()
    {
        return $this->container['parent_fk'];
    }

    /**
     * Sets parent_fk
     *
     * @param int|null $parent_fk parent_fk
     *
     * @return self
     */
    public function setParentFk($parent_fk)
    {
        $this->container['parent_fk'] = $parent_fk;

        return $this;
    }

    /**
     * Gets key
     *
     * @return string|null
     */
    public function getKey()
    {
        return $this->container['key'];
    }

    /**
     * Sets key
     *
     * @param string|null $key key
     *
     * @return self
     */
    public function setKey($key)
    {
        $this->container['key'] = $key;

        return $this;
    }

    /**
     * Gets flags
     *
     * @return string|null
     */
    public function getFlags()
    {
        return $this->container['flags'];
    }

    /**
     * Sets flags
     *
     * @param string|null $flags flags
     *
     * @return self
     */
    public function setFlags($flags)
    {
        $allowedValues = $this->getFlagsAllowableValues();
        if (!is_null($flags) && !in_array($flags, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value '%s' for 'flags', must be one of '%s'",
                    $flags,
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['flags'] = $flags;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


