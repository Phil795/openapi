<?php
/**
 * SimplifySoftPecuniariusDataNetReceiptDataPosition
 *
 * PHP version 7.2
 *
 * @category Class
 * @package  SimplifySoft\Pecuniarius\Api
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Pecuniarius API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace SimplifySoft\Pecuniarius\Api\Model;

use \ArrayAccess;
use \SimplifySoft\Pecuniarius\Api\ObjectSerializer;

/**
 * SimplifySoftPecuniariusDataNetReceiptDataPosition Class Doc Comment
 *
 * @category Class
 * @package  SimplifySoft\Pecuniarius\Api
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<TKey, TValue>
 * @template TKey int|null
 * @template TValue mixed|null
 */
class SimplifySoftPecuniariusDataNetReceiptDataPosition implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'SimplifySoft.Pecuniarius.Data.Net.ReceiptData.Position';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'id' => 'int',
        'amount' => 'float',
        'price' => 'float',
        'price_kind' => 'string',
        'note' => 'string',
        'extern_reference' => 'string',
        'extern_item_reference' => 'string',
        'estimated_date' => 'string[]',
        'time_stamp_created' => 'string',
        'time_stamp_last_updated' => 'string',
        'serialnumber' => 'string',
        'batchnumber' => 'string',
        'best_before_date' => 'string[]',
        'is_editible' => 'bool',
        'tax' => '\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTax',
        'tax_fk' => 'int[]',
        'item_frame' => '\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemFrame',
        'item_frame_fk' => 'int[]',
        'part_of' => '\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPosition',
        'part_of_fk' => 'int[]',
        'invoice' => '\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataInvoice',
        'invoice_fk' => 'int[]',
        'parent' => 'object',
        'parent_fk' => 'int',
        'pick_list_entries' => '\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickListEntry[]',
        'options' => '\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPositionOption[]',
        'delivery_goals' => '\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal[]',
        'receipts' => '\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt[]'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'id' => 'int32',
        'amount' => null,
        'price' => null,
        'price_kind' => null,
        'note' => null,
        'extern_reference' => null,
        'extern_item_reference' => null,
        'estimated_date' => null,
        'time_stamp_created' => null,
        'time_stamp_last_updated' => null,
        'serialnumber' => null,
        'batchnumber' => null,
        'best_before_date' => null,
        'is_editible' => null,
        'tax' => null,
        'tax_fk' => 'int32',
        'item_frame' => null,
        'item_frame_fk' => 'int32',
        'part_of' => null,
        'part_of_fk' => 'int32',
        'invoice' => null,
        'invoice_fk' => 'int32',
        'parent' => null,
        'parent_fk' => 'int32',
        'pick_list_entries' => null,
        'options' => null,
        'delivery_goals' => null,
        'receipts' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'Id',
        'amount' => 'Amount',
        'price' => 'Price',
        'price_kind' => 'PriceKind',
        'note' => 'Note',
        'extern_reference' => 'ExternReference',
        'extern_item_reference' => 'ExternItemReference',
        'estimated_date' => 'EstimatedDate',
        'time_stamp_created' => 'TimeStampCreated',
        'time_stamp_last_updated' => 'TimeStampLastUpdated',
        'serialnumber' => 'Serialnumber',
        'batchnumber' => 'Batchnumber',
        'best_before_date' => 'BestBeforeDate',
        'is_editible' => 'IsEditible',
        'tax' => 'Tax',
        'tax_fk' => 'TaxFK',
        'item_frame' => 'ItemFrame',
        'item_frame_fk' => 'ItemFrameFK',
        'part_of' => 'PartOf',
        'part_of_fk' => 'PartOfFK',
        'invoice' => 'Invoice',
        'invoice_fk' => 'InvoiceFK',
        'parent' => 'Parent',
        'parent_fk' => 'ParentFK',
        'pick_list_entries' => 'PickListEntries',
        'options' => 'Options',
        'delivery_goals' => 'DeliveryGoals',
        'receipts' => 'Receipts'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'amount' => 'setAmount',
        'price' => 'setPrice',
        'price_kind' => 'setPriceKind',
        'note' => 'setNote',
        'extern_reference' => 'setExternReference',
        'extern_item_reference' => 'setExternItemReference',
        'estimated_date' => 'setEstimatedDate',
        'time_stamp_created' => 'setTimeStampCreated',
        'time_stamp_last_updated' => 'setTimeStampLastUpdated',
        'serialnumber' => 'setSerialnumber',
        'batchnumber' => 'setBatchnumber',
        'best_before_date' => 'setBestBeforeDate',
        'is_editible' => 'setIsEditible',
        'tax' => 'setTax',
        'tax_fk' => 'setTaxFk',
        'item_frame' => 'setItemFrame',
        'item_frame_fk' => 'setItemFrameFk',
        'part_of' => 'setPartOf',
        'part_of_fk' => 'setPartOfFk',
        'invoice' => 'setInvoice',
        'invoice_fk' => 'setInvoiceFk',
        'parent' => 'setParent',
        'parent_fk' => 'setParentFk',
        'pick_list_entries' => 'setPickListEntries',
        'options' => 'setOptions',
        'delivery_goals' => 'setDeliveryGoals',
        'receipts' => 'setReceipts'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'amount' => 'getAmount',
        'price' => 'getPrice',
        'price_kind' => 'getPriceKind',
        'note' => 'getNote',
        'extern_reference' => 'getExternReference',
        'extern_item_reference' => 'getExternItemReference',
        'estimated_date' => 'getEstimatedDate',
        'time_stamp_created' => 'getTimeStampCreated',
        'time_stamp_last_updated' => 'getTimeStampLastUpdated',
        'serialnumber' => 'getSerialnumber',
        'batchnumber' => 'getBatchnumber',
        'best_before_date' => 'getBestBeforeDate',
        'is_editible' => 'getIsEditible',
        'tax' => 'getTax',
        'tax_fk' => 'getTaxFk',
        'item_frame' => 'getItemFrame',
        'item_frame_fk' => 'getItemFrameFk',
        'part_of' => 'getPartOf',
        'part_of_fk' => 'getPartOfFk',
        'invoice' => 'getInvoice',
        'invoice_fk' => 'getInvoiceFk',
        'parent' => 'getParent',
        'parent_fk' => 'getParentFk',
        'pick_list_entries' => 'getPickListEntries',
        'options' => 'getOptions',
        'delivery_goals' => 'getDeliveryGoals',
        'receipts' => 'getReceipts'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }

    const PRICE_KIND_NET = 'Net';
    const PRICE_KIND_GROSS = 'Gross';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getPriceKindAllowableValues()
    {
        return [
            self::PRICE_KIND_NET,
            self::PRICE_KIND_GROSS,
        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = $data['id'] ?? null;
        $this->container['amount'] = $data['amount'] ?? null;
        $this->container['price'] = $data['price'] ?? null;
        $this->container['price_kind'] = $data['price_kind'] ?? null;
        $this->container['note'] = $data['note'] ?? null;
        $this->container['extern_reference'] = $data['extern_reference'] ?? null;
        $this->container['extern_item_reference'] = $data['extern_item_reference'] ?? null;
        $this->container['estimated_date'] = $data['estimated_date'] ?? null;
        $this->container['time_stamp_created'] = $data['time_stamp_created'] ?? null;
        $this->container['time_stamp_last_updated'] = $data['time_stamp_last_updated'] ?? null;
        $this->container['serialnumber'] = $data['serialnumber'] ?? null;
        $this->container['batchnumber'] = $data['batchnumber'] ?? null;
        $this->container['best_before_date'] = $data['best_before_date'] ?? null;
        $this->container['is_editible'] = $data['is_editible'] ?? null;
        $this->container['tax'] = $data['tax'] ?? null;
        $this->container['tax_fk'] = $data['tax_fk'] ?? null;
        $this->container['item_frame'] = $data['item_frame'] ?? null;
        $this->container['item_frame_fk'] = $data['item_frame_fk'] ?? null;
        $this->container['part_of'] = $data['part_of'] ?? null;
        $this->container['part_of_fk'] = $data['part_of_fk'] ?? null;
        $this->container['invoice'] = $data['invoice'] ?? null;
        $this->container['invoice_fk'] = $data['invoice_fk'] ?? null;
        $this->container['parent'] = $data['parent'] ?? null;
        $this->container['parent_fk'] = $data['parent_fk'] ?? null;
        $this->container['pick_list_entries'] = $data['pick_list_entries'] ?? null;
        $this->container['options'] = $data['options'] ?? null;
        $this->container['delivery_goals'] = $data['delivery_goals'] ?? null;
        $this->container['receipts'] = $data['receipts'] ?? null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        $allowedValues = $this->getPriceKindAllowableValues();
        if (!is_null($this->container['price_kind']) && !in_array($this->container['price_kind'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value '%s' for 'price_kind', must be one of '%s'",
                $this->container['price_kind'],
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int|null $id id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets amount
     *
     * @return float|null
     */
    public function getAmount()
    {
        return $this->container['amount'];
    }

    /**
     * Sets amount
     *
     * @param float|null $amount amount
     *
     * @return self
     */
    public function setAmount($amount)
    {
        $this->container['amount'] = $amount;

        return $this;
    }

    /**
     * Gets price
     *
     * @return float|null
     */
    public function getPrice()
    {
        return $this->container['price'];
    }

    /**
     * Sets price
     *
     * @param float|null $price price
     *
     * @return self
     */
    public function setPrice($price)
    {
        $this->container['price'] = $price;

        return $this;
    }

    /**
     * Gets price_kind
     *
     * @return string|null
     */
    public function getPriceKind()
    {
        return $this->container['price_kind'];
    }

    /**
     * Sets price_kind
     *
     * @param string|null $price_kind price_kind
     *
     * @return self
     */
    public function setPriceKind($price_kind)
    {
        $allowedValues = $this->getPriceKindAllowableValues();
        if (!is_null($price_kind) && !in_array($price_kind, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value '%s' for 'price_kind', must be one of '%s'",
                    $price_kind,
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['price_kind'] = $price_kind;

        return $this;
    }

    /**
     * Gets note
     *
     * @return string|null
     */
    public function getNote()
    {
        return $this->container['note'];
    }

    /**
     * Sets note
     *
     * @param string|null $note note
     *
     * @return self
     */
    public function setNote($note)
    {
        $this->container['note'] = $note;

        return $this;
    }

    /**
     * Gets extern_reference
     *
     * @return string|null
     */
    public function getExternReference()
    {
        return $this->container['extern_reference'];
    }

    /**
     * Sets extern_reference
     *
     * @param string|null $extern_reference extern_reference
     *
     * @return self
     */
    public function setExternReference($extern_reference)
    {
        $this->container['extern_reference'] = $extern_reference;

        return $this;
    }

    /**
     * Gets extern_item_reference
     *
     * @return string|null
     */
    public function getExternItemReference()
    {
        return $this->container['extern_item_reference'];
    }

    /**
     * Sets extern_item_reference
     *
     * @param string|null $extern_item_reference extern_item_reference
     *
     * @return self
     */
    public function setExternItemReference($extern_item_reference)
    {
        $this->container['extern_item_reference'] = $extern_item_reference;

        return $this;
    }

    /**
     * Gets estimated_date
     *
     * @return string[]|null
     */
    public function getEstimatedDate()
    {
        return $this->container['estimated_date'];
    }

    /**
     * Sets estimated_date
     *
     * @param string[]|null $estimated_date estimated_date
     *
     * @return self
     */
    public function setEstimatedDate($estimated_date)
    {
        $this->container['estimated_date'] = $estimated_date;

        return $this;
    }

    /**
     * Gets time_stamp_created
     *
     * @return string|null
     */
    public function getTimeStampCreated()
    {
        return $this->container['time_stamp_created'];
    }

    /**
     * Sets time_stamp_created
     *
     * @param string|null $time_stamp_created time_stamp_created
     *
     * @return self
     */
    public function setTimeStampCreated($time_stamp_created)
    {
        $this->container['time_stamp_created'] = $time_stamp_created;

        return $this;
    }

    /**
     * Gets time_stamp_last_updated
     *
     * @return string|null
     */
    public function getTimeStampLastUpdated()
    {
        return $this->container['time_stamp_last_updated'];
    }

    /**
     * Sets time_stamp_last_updated
     *
     * @param string|null $time_stamp_last_updated time_stamp_last_updated
     *
     * @return self
     */
    public function setTimeStampLastUpdated($time_stamp_last_updated)
    {
        $this->container['time_stamp_last_updated'] = $time_stamp_last_updated;

        return $this;
    }

    /**
     * Gets serialnumber
     *
     * @return string|null
     */
    public function getSerialnumber()
    {
        return $this->container['serialnumber'];
    }

    /**
     * Sets serialnumber
     *
     * @param string|null $serialnumber serialnumber
     *
     * @return self
     */
    public function setSerialnumber($serialnumber)
    {
        $this->container['serialnumber'] = $serialnumber;

        return $this;
    }

    /**
     * Gets batchnumber
     *
     * @return string|null
     */
    public function getBatchnumber()
    {
        return $this->container['batchnumber'];
    }

    /**
     * Sets batchnumber
     *
     * @param string|null $batchnumber batchnumber
     *
     * @return self
     */
    public function setBatchnumber($batchnumber)
    {
        $this->container['batchnumber'] = $batchnumber;

        return $this;
    }

    /**
     * Gets best_before_date
     *
     * @return string[]|null
     */
    public function getBestBeforeDate()
    {
        return $this->container['best_before_date'];
    }

    /**
     * Sets best_before_date
     *
     * @param string[]|null $best_before_date best_before_date
     *
     * @return self
     */
    public function setBestBeforeDate($best_before_date)
    {
        $this->container['best_before_date'] = $best_before_date;

        return $this;
    }

    /**
     * Gets is_editible
     *
     * @return bool|null
     */
    public function getIsEditible()
    {
        return $this->container['is_editible'];
    }

    /**
     * Sets is_editible
     *
     * @param bool|null $is_editible is_editible
     *
     * @return self
     */
    public function setIsEditible($is_editible)
    {
        $this->container['is_editible'] = $is_editible;

        return $this;
    }

    /**
     * Gets tax
     *
     * @return \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTax|null
     */
    public function getTax()
    {
        return $this->container['tax'];
    }

    /**
     * Sets tax
     *
     * @param \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetAccountingTax|null $tax tax
     *
     * @return self
     */
    public function setTax($tax)
    {
        $this->container['tax'] = $tax;

        return $this;
    }

    /**
     * Gets tax_fk
     *
     * @return int[]|null
     */
    public function getTaxFk()
    {
        return $this->container['tax_fk'];
    }

    /**
     * Sets tax_fk
     *
     * @param int[]|null $tax_fk tax_fk
     *
     * @return self
     */
    public function setTaxFk($tax_fk)
    {
        $this->container['tax_fk'] = $tax_fk;

        return $this;
    }

    /**
     * Gets item_frame
     *
     * @return \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemFrame|null
     */
    public function getItemFrame()
    {
        return $this->container['item_frame'];
    }

    /**
     * Sets item_frame
     *
     * @param \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetItemDataItemFrame|null $item_frame item_frame
     *
     * @return self
     */
    public function setItemFrame($item_frame)
    {
        $this->container['item_frame'] = $item_frame;

        return $this;
    }

    /**
     * Gets item_frame_fk
     *
     * @return int[]|null
     */
    public function getItemFrameFk()
    {
        return $this->container['item_frame_fk'];
    }

    /**
     * Sets item_frame_fk
     *
     * @param int[]|null $item_frame_fk item_frame_fk
     *
     * @return self
     */
    public function setItemFrameFk($item_frame_fk)
    {
        $this->container['item_frame_fk'] = $item_frame_fk;

        return $this;
    }

    /**
     * Gets part_of
     *
     * @return \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPosition|null
     */
    public function getPartOf()
    {
        return $this->container['part_of'];
    }

    /**
     * Sets part_of
     *
     * @param \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPosition|null $part_of part_of
     *
     * @return self
     */
    public function setPartOf($part_of)
    {
        $this->container['part_of'] = $part_of;

        return $this;
    }

    /**
     * Gets part_of_fk
     *
     * @return int[]|null
     */
    public function getPartOfFk()
    {
        return $this->container['part_of_fk'];
    }

    /**
     * Sets part_of_fk
     *
     * @param int[]|null $part_of_fk part_of_fk
     *
     * @return self
     */
    public function setPartOfFk($part_of_fk)
    {
        $this->container['part_of_fk'] = $part_of_fk;

        return $this;
    }

    /**
     * Gets invoice
     *
     * @return \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataInvoice|null
     */
    public function getInvoice()
    {
        return $this->container['invoice'];
    }

    /**
     * Sets invoice
     *
     * @param \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataInvoice|null $invoice invoice
     *
     * @return self
     */
    public function setInvoice($invoice)
    {
        $this->container['invoice'] = $invoice;

        return $this;
    }

    /**
     * Gets invoice_fk
     *
     * @return int[]|null
     */
    public function getInvoiceFk()
    {
        return $this->container['invoice_fk'];
    }

    /**
     * Sets invoice_fk
     *
     * @param int[]|null $invoice_fk invoice_fk
     *
     * @return self
     */
    public function setInvoiceFk($invoice_fk)
    {
        $this->container['invoice_fk'] = $invoice_fk;

        return $this;
    }

    /**
     * Gets parent
     *
     * @return object|null
     */
    public function getParent()
    {
        return $this->container['parent'];
    }

    /**
     * Sets parent
     *
     * @param object|null $parent parent
     *
     * @return self
     */
    public function setParent($parent)
    {
        $this->container['parent'] = $parent;

        return $this;
    }

    /**
     * Gets parent_fk
     *
     * @return int|null
     */
    public function getParentFk()
    {
        return $this->container['parent_fk'];
    }

    /**
     * Sets parent_fk
     *
     * @param int|null $parent_fk parent_fk
     *
     * @return self
     */
    public function setParentFk($parent_fk)
    {
        $this->container['parent_fk'] = $parent_fk;

        return $this;
    }

    /**
     * Gets pick_list_entries
     *
     * @return \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickListEntry[]|null
     */
    public function getPickListEntries()
    {
        return $this->container['pick_list_entries'];
    }

    /**
     * Sets pick_list_entries
     *
     * @param \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetInventoryPickListEntry[]|null $pick_list_entries pick_list_entries
     *
     * @return self
     */
    public function setPickListEntries($pick_list_entries)
    {
        $this->container['pick_list_entries'] = $pick_list_entries;

        return $this;
    }

    /**
     * Gets options
     *
     * @return \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPositionOption[]|null
     */
    public function getOptions()
    {
        return $this->container['options'];
    }

    /**
     * Sets options
     *
     * @param \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataPositionOption[]|null $options options
     *
     * @return self
     */
    public function setOptions($options)
    {
        $this->container['options'] = $options;

        return $this;
    }

    /**
     * Gets delivery_goals
     *
     * @return \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal[]|null
     */
    public function getDeliveryGoals()
    {
        return $this->container['delivery_goals'];
    }

    /**
     * Sets delivery_goals
     *
     * @param \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataDeliveryGoal[]|null $delivery_goals delivery_goals
     *
     * @return self
     */
    public function setDeliveryGoals($delivery_goals)
    {
        $this->container['delivery_goals'] = $delivery_goals;

        return $this;
    }

    /**
     * Gets receipts
     *
     * @return \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt[]|null
     */
    public function getReceipts()
    {
        return $this->container['receipts'];
    }

    /**
     * Sets receipts
     *
     * @param \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetReceiptDataReceipt[]|null $receipts receipts
     *
     * @return self
     */
    public function setReceipts($receipts)
    {
        $this->container['receipts'] = $receipts;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


