<?php
/**
 * SimplifySoftPecuniariusDataNetExternExternAuthority
 *
 * PHP version 7.2
 *
 * @category Class
 * @package  SimplifySoft\Pecuniarius\Api
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Pecuniarius API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace SimplifySoft\Pecuniarius\Api\Model;

use \ArrayAccess;
use \SimplifySoft\Pecuniarius\Api\ObjectSerializer;

/**
 * SimplifySoftPecuniariusDataNetExternExternAuthority Class Doc Comment
 *
 * @category Class
 * @package  SimplifySoft\Pecuniarius\Api
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<TKey, TValue>
 * @template TKey int|null
 * @template TValue mixed|null
 */
class SimplifySoftPecuniariusDataNetExternExternAuthority implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'SimplifySoft.Pecuniarius.Data.Net.Extern.ExternAuthority';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'id' => 'int',
        'title' => 'string',
        'repeat_time_span' => 'string',
        'last_executed_time_stamp' => 'string',
        'advisory_lock_number' => 'int',
        'endpoint_identifier' => 'string',
        'status' => 'string',
        'properties' => '\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty[]',
        'log_messages' => '\SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternAuthorityLogMessage[]'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'id' => 'int32',
        'title' => null,
        'repeat_time_span' => null,
        'last_executed_time_stamp' => null,
        'advisory_lock_number' => 'int32',
        'endpoint_identifier' => null,
        'status' => null,
        'properties' => null,
        'log_messages' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'Id',
        'title' => 'Title',
        'repeat_time_span' => 'RepeatTimeSpan',
        'last_executed_time_stamp' => 'LastExecutedTimeStamp',
        'advisory_lock_number' => 'AdvisoryLockNumber',
        'endpoint_identifier' => 'EndpointIdentifier',
        'status' => 'Status',
        'properties' => 'Properties',
        'log_messages' => 'LogMessages'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'title' => 'setTitle',
        'repeat_time_span' => 'setRepeatTimeSpan',
        'last_executed_time_stamp' => 'setLastExecutedTimeStamp',
        'advisory_lock_number' => 'setAdvisoryLockNumber',
        'endpoint_identifier' => 'setEndpointIdentifier',
        'status' => 'setStatus',
        'properties' => 'setProperties',
        'log_messages' => 'setLogMessages'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'title' => 'getTitle',
        'repeat_time_span' => 'getRepeatTimeSpan',
        'last_executed_time_stamp' => 'getLastExecutedTimeStamp',
        'advisory_lock_number' => 'getAdvisoryLockNumber',
        'endpoint_identifier' => 'getEndpointIdentifier',
        'status' => 'getStatus',
        'properties' => 'getProperties',
        'log_messages' => 'getLogMessages'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }

    const STATUS_STOPPED = 'Stopped';
    const STATUS_PAUSED = 'Paused';
    const STATUS_RUNNING = 'Running';

    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getStatusAllowableValues()
    {
        return [
            self::STATUS_STOPPED,
            self::STATUS_PAUSED,
            self::STATUS_RUNNING,
        ];
    }

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = $data['id'] ?? null;
        $this->container['title'] = $data['title'] ?? null;
        $this->container['repeat_time_span'] = $data['repeat_time_span'] ?? null;
        $this->container['last_executed_time_stamp'] = $data['last_executed_time_stamp'] ?? null;
        $this->container['advisory_lock_number'] = $data['advisory_lock_number'] ?? null;
        $this->container['endpoint_identifier'] = $data['endpoint_identifier'] ?? null;
        $this->container['status'] = $data['status'] ?? null;
        $this->container['properties'] = $data['properties'] ?? null;
        $this->container['log_messages'] = $data['log_messages'] ?? null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        $allowedValues = $this->getStatusAllowableValues();
        if (!is_null($this->container['status']) && !in_array($this->container['status'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value '%s' for 'status', must be one of '%s'",
                $this->container['status'],
                implode("', '", $allowedValues)
            );
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int|null $id id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets title
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->container['title'];
    }

    /**
     * Sets title
     *
     * @param string|null $title title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->container['title'] = $title;

        return $this;
    }

    /**
     * Gets repeat_time_span
     *
     * @return string|null
     */
    public function getRepeatTimeSpan()
    {
        return $this->container['repeat_time_span'];
    }

    /**
     * Sets repeat_time_span
     *
     * @param string|null $repeat_time_span repeat_time_span
     *
     * @return self
     */
    public function setRepeatTimeSpan($repeat_time_span)
    {
        $this->container['repeat_time_span'] = $repeat_time_span;

        return $this;
    }

    /**
     * Gets last_executed_time_stamp
     *
     * @return string|null
     */
    public function getLastExecutedTimeStamp()
    {
        return $this->container['last_executed_time_stamp'];
    }

    /**
     * Sets last_executed_time_stamp
     *
     * @param string|null $last_executed_time_stamp last_executed_time_stamp
     *
     * @return self
     */
    public function setLastExecutedTimeStamp($last_executed_time_stamp)
    {
        $this->container['last_executed_time_stamp'] = $last_executed_time_stamp;

        return $this;
    }

    /**
     * Gets advisory_lock_number
     *
     * @return int|null
     */
    public function getAdvisoryLockNumber()
    {
        return $this->container['advisory_lock_number'];
    }

    /**
     * Sets advisory_lock_number
     *
     * @param int|null $advisory_lock_number advisory_lock_number
     *
     * @return self
     */
    public function setAdvisoryLockNumber($advisory_lock_number)
    {
        $this->container['advisory_lock_number'] = $advisory_lock_number;

        return $this;
    }

    /**
     * Gets endpoint_identifier
     *
     * @return string|null
     */
    public function getEndpointIdentifier()
    {
        return $this->container['endpoint_identifier'];
    }

    /**
     * Sets endpoint_identifier
     *
     * @param string|null $endpoint_identifier endpoint_identifier
     *
     * @return self
     */
    public function setEndpointIdentifier($endpoint_identifier)
    {
        $this->container['endpoint_identifier'] = $endpoint_identifier;

        return $this;
    }

    /**
     * Gets status
     *
     * @return string|null
     */
    public function getStatus()
    {
        return $this->container['status'];
    }

    /**
     * Sets status
     *
     * @param string|null $status status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $allowedValues = $this->getStatusAllowableValues();
        if (!is_null($status) && !in_array($status, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value '%s' for 'status', must be one of '%s'",
                    $status,
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['status'] = $status;

        return $this;
    }

    /**
     * Gets properties
     *
     * @return \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty[]|null
     */
    public function getProperties()
    {
        return $this->container['properties'];
    }

    /**
     * Sets properties
     *
     * @param \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetCustomProperty[]|null $properties properties
     *
     * @return self
     */
    public function setProperties($properties)
    {
        $this->container['properties'] = $properties;

        return $this;
    }

    /**
     * Gets log_messages
     *
     * @return \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternAuthorityLogMessage[]|null
     */
    public function getLogMessages()
    {
        return $this->container['log_messages'];
    }

    /**
     * Sets log_messages
     *
     * @param \SimplifySoft\Pecuniarius\Api\Model\SimplifySoftPecuniariusDataNetExternExternAuthorityLogMessage[]|null $log_messages log_messages
     *
     * @return self
     */
    public function setLogMessages($log_messages)
    {
        $this->container['log_messages'] = $log_messages;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


